package dmn.ucad.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A CaisseMois.
 */
@Entity
@Table(name = "caisse_mois")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class CaisseMois implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "date")
    private LocalDate date;

    @NotNull
    @Column(name = "montant", nullable = false)
    private Integer montant;

    @Column(name = "valide")
    private Boolean valide;

    @ManyToOne
    @JsonIgnoreProperties(value = { "categorie" }, allowSetters = true)
    private Mensualite mensualite;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(
        value = { "xamXams", "niveauXamXam", "profession", "categorie", "coran", "departement", "user" },
        allowSetters = true
    )
    private Membre membre;

    @ManyToOne
    private User user;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public CaisseMois id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return this.date;
    }

    public CaisseMois date(LocalDate date) {
        this.setDate(date);
        return this;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Integer getMontant() {
        return this.montant;
    }

    public CaisseMois montant(Integer montant) {
        this.setMontant(montant);
        return this;
    }

    public void setMontant(Integer montant) {
        this.montant = montant;
    }

    public Boolean getValide() {
        return this.valide;
    }

    public CaisseMois valide(Boolean valide) {
        this.setValide(valide);
        return this;
    }

    public void setValide(Boolean valide) {
        this.valide = valide;
    }

    public Mensualite getMensualite() {
        return this.mensualite;
    }

    public void setMensualite(Mensualite mensualite) {
        this.mensualite = mensualite;
    }

    public CaisseMois mensualite(Mensualite mensualite) {
        this.setMensualite(mensualite);
        return this;
    }

    public Membre getMembre() {
        return this.membre;
    }

    public void setMembre(Membre membre) {
        this.membre = membre;
    }

    public CaisseMois membre(Membre membre) {
        this.setMembre(membre);
        return this;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public CaisseMois user(User user) {
        this.setUser(user);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CaisseMois)) {
            return false;
        }
        return id != null && id.equals(((CaisseMois) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CaisseMois{" +
            "id=" + getId() +
            ", date='" + getDate() + "'" +
            ", montant=" + getMontant() +
            ", valide='" + getValide() + "'" +
            "}";
    }
}
