package dmn.ucad.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A MembreEntite.
 */
@Entity
@Table(name = "membre_entite")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class MembreEntite implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "actif")
    private Boolean actif;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(
        value = { "xamXams", "niveauXamXam", "profession", "categorie", "coran", "departement", "user" },
        allowSetters = true
    )
    private Membre membre;

    @ManyToOne(optional = false)
    @NotNull
    private Entite entite;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public MembreEntite id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getActif() {
        return this.actif;
    }

    public MembreEntite actif(Boolean actif) {
        this.setActif(actif);
        return this;
    }

    public void setActif(Boolean actif) {
        this.actif = actif;
    }

    public Membre getMembre() {
        return this.membre;
    }

    public void setMembre(Membre membre) {
        this.membre = membre;
    }

    public MembreEntite membre(Membre membre) {
        this.setMembre(membre);
        return this;
    }

    public Entite getEntite() {
        return this.entite;
    }

    public void setEntite(Entite entite) {
        this.entite = entite;
    }

    public MembreEntite entite(Entite entite) {
        this.setEntite(entite);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MembreEntite)) {
            return false;
        }
        return id != null && id.equals(((MembreEntite) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MembreEntite{" +
            "id=" + getId() +
            ", actif='" + getActif() + "'" +
            "}";
    }
}
