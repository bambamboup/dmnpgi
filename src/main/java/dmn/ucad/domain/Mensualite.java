package dmn.ucad.domain;

import dmn.ucad.domain.enumeration.EnumTypeMensualite;
import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Mensualite.
 */
@Entity
@Table(name = "mensualite")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Mensualite implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "date_debut")
    private LocalDate dateDebut;

    @Column(name = "date_fin")
    private LocalDate dateFin;

    @Column(name = "actif")
    private Boolean actif;

    @NotNull
    @Column(name = "montant", nullable = false)
    private Integer montant;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "type_mensualite", nullable = false)
    private EnumTypeMensualite typeMensualite;

    @ManyToOne(optional = false)
    @NotNull
    private Categorie categorie;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Mensualite id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDateDebut() {
        return this.dateDebut;
    }

    public Mensualite dateDebut(LocalDate dateDebut) {
        this.setDateDebut(dateDebut);
        return this;
    }

    public void setDateDebut(LocalDate dateDebut) {
        this.dateDebut = dateDebut;
    }

    public LocalDate getDateFin() {
        return this.dateFin;
    }

    public Mensualite dateFin(LocalDate dateFin) {
        this.setDateFin(dateFin);
        return this;
    }

    public void setDateFin(LocalDate dateFin) {
        this.dateFin = dateFin;
    }

    public Boolean getActif() {
        return this.actif;
    }

    public Mensualite actif(Boolean actif) {
        this.setActif(actif);
        return this;
    }

    public void setActif(Boolean actif) {
        this.actif = actif;
    }

    public Integer getMontant() {
        return this.montant;
    }

    public Mensualite montant(Integer montant) {
        this.setMontant(montant);
        return this;
    }

    public void setMontant(Integer montant) {
        this.montant = montant;
    }

    public EnumTypeMensualite getTypeMensualite() {
        return this.typeMensualite;
    }

    public Mensualite typeMensualite(EnumTypeMensualite typeMensualite) {
        this.setTypeMensualite(typeMensualite);
        return this;
    }

    public void setTypeMensualite(EnumTypeMensualite typeMensualite) {
        this.typeMensualite = typeMensualite;
    }

    public Categorie getCategorie() {
        return this.categorie;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    public Mensualite categorie(Categorie categorie) {
        this.setCategorie(categorie);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Mensualite)) {
            return false;
        }
        return id != null && id.equals(((Mensualite) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Mensualite{" +
            "id=" + getId() +
            ", dateDebut='" + getDateDebut() + "'" +
            ", dateFin='" + getDateFin() + "'" +
            ", actif='" + getActif() + "'" +
            ", montant=" + getMontant() +
            ", typeMensualite='" + getTypeMensualite() + "'" +
            "}";
    }
}
