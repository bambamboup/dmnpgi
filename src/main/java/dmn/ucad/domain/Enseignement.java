package dmn.ucad.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Enseignement.
 */
@Entity
@Table(name = "enseignement")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Enseignement implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "date_debut", nullable = false)
    private LocalDate dateDebut;

    @Column(name = "date_fin")
    private LocalDate dateFin;

    @Column(name = "encours")
    private Boolean encours;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties(
        value = { "xamXams", "niveauXamXam", "profession", "categorie", "coran", "departement", "user" },
        allowSetters = true
    )
    private Membre membre;

    @ManyToOne
    private Coran coran;

    @ManyToOne
    @JsonIgnoreProperties(value = { "niveauXamXam" }, allowSetters = true)
    private XamXam xamXam;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Enseignement id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDateDebut() {
        return this.dateDebut;
    }

    public Enseignement dateDebut(LocalDate dateDebut) {
        this.setDateDebut(dateDebut);
        return this;
    }

    public void setDateDebut(LocalDate dateDebut) {
        this.dateDebut = dateDebut;
    }

    public LocalDate getDateFin() {
        return this.dateFin;
    }

    public Enseignement dateFin(LocalDate dateFin) {
        this.setDateFin(dateFin);
        return this;
    }

    public void setDateFin(LocalDate dateFin) {
        this.dateFin = dateFin;
    }

    public Boolean getEncours() {
        return this.encours;
    }

    public Enseignement encours(Boolean encours) {
        this.setEncours(encours);
        return this;
    }

    public void setEncours(Boolean encours) {
        this.encours = encours;
    }

    public Membre getMembre() {
        return this.membre;
    }

    public void setMembre(Membre membre) {
        this.membre = membre;
    }

    public Enseignement membre(Membre membre) {
        this.setMembre(membre);
        return this;
    }

    public Coran getCoran() {
        return this.coran;
    }

    public void setCoran(Coran coran) {
        this.coran = coran;
    }

    public Enseignement coran(Coran coran) {
        this.setCoran(coran);
        return this;
    }

    public XamXam getXamXam() {
        return this.xamXam;
    }

    public void setXamXam(XamXam xamXam) {
        this.xamXam = xamXam;
    }

    public Enseignement xamXam(XamXam xamXam) {
        this.setXamXam(xamXam);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Enseignement)) {
            return false;
        }
        return id != null && id.equals(((Enseignement) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Enseignement{" +
            "id=" + getId() +
            ", dateDebut='" + getDateDebut() + "'" +
            ", dateFin='" + getDateFin() + "'" +
            ", encours='" + getEncours() + "'" +
            "}";
    }
}
