package dmn.ucad.domain;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A NiveauXamXam.
 */
@Entity
@Table(name = "niveau_xam_xam")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class NiveauXamXam implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "libelle", nullable = false, unique = true)
    private String libelle;

    @NotNull
    @Column(name = "poids", nullable = false)
    private Integer poids;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public NiveauXamXam id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return this.libelle;
    }

    public NiveauXamXam libelle(String libelle) {
        this.setLibelle(libelle);
        return this;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Integer getPoids() {
        return this.poids;
    }

    public NiveauXamXam poids(Integer poids) {
        this.setPoids(poids);
        return this;
    }

    public void setPoids(Integer poids) {
        this.poids = poids;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof NiveauXamXam)) {
            return false;
        }
        return id != null && id.equals(((NiveauXamXam) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "NiveauXamXam{" +
            "id=" + getId() +
            ", libelle='" + getLibelle() + "'" +
            ", poids=" + getPoids() +
            "}";
    }
}
