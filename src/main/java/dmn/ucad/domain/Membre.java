package dmn.ucad.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import dmn.ucad.domain.enumeration.EnumCivilite;
import dmn.ucad.domain.enumeration.EnumEtatSante;
import dmn.ucad.domain.enumeration.EnumTypeSexe;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Membre.
 */
@Entity
@Table(name = "membre")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Membre implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "nom", nullable = false)
    private String nom;

    @NotNull
    @Column(name = "prenom", nullable = false)
    private String prenom;

    @NotNull
    @Column(name = "telephone", nullable = false)
    private String telephone;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "sexe", nullable = false)
    private EnumTypeSexe sexe;

    @Column(name = "email")
    private String email;

    @Column(name = "cni")
    private String cni;

    @Column(name = "adress_dakar")
    private String adressDakar;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "situation_matrimoniale", nullable = false)
    private EnumCivilite situationMatrimoniale;

    @Column(name = "adresse")
    private String adresse;

    @Column(name = "adresse_vacance")
    private String adresseVacance;

    @Lob
    @Column(name = "profil")
    private byte[] profil;

    @Column(name = "profil_content_type")
    private String profilContentType;

    @Column(name = "date_naissance")
    private LocalDate dateNaissance;

    @Column(name = "boursier")
    private Boolean boursier;

    @Column(name = "daara_origine")
    private String daaraOrigine;

    @Enumerated(EnumType.STRING)
    @Column(name = "etat_sante")
    private EnumEtatSante etatSante;

    @Column(name = "titeur")
    private String titeur;

    @Column(name = "telephone_titeur")
    private String telephoneTiteur;

    @ManyToMany
    @NotNull
    @JoinTable(
        name = "rel_membre__xam_xam",
        joinColumns = @JoinColumn(name = "membre_id"),
        inverseJoinColumns = @JoinColumn(name = "xam_xam_id")
    )
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "niveauXamXam" }, allowSetters = true)
    private Set<XamXam> xamXams = new HashSet<>();

    @ManyToOne
    private NiveauXamXam niveauXamXam;

    @ManyToOne
    private Profession profession;

    @ManyToOne
    private Categorie categorie;

    @ManyToOne
    private Coran coran;

    @ManyToOne
    @JsonIgnoreProperties(value = { "faculte" }, allowSetters = true)
    private Departement departement;

    @ManyToOne
    private User user;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Membre id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return this.nom;
    }

    public Membre nom(String nom) {
        this.setNom(nom);
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return this.prenom;
    }

    public Membre prenom(String prenom) {
        this.setPrenom(prenom);
        return this;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getTelephone() {
        return this.telephone;
    }

    public Membre telephone(String telephone) {
        this.setTelephone(telephone);
        return this;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public EnumTypeSexe getSexe() {
        return this.sexe;
    }

    public Membre sexe(EnumTypeSexe sexe) {
        this.setSexe(sexe);
        return this;
    }

    public void setSexe(EnumTypeSexe sexe) {
        this.sexe = sexe;
    }

    public String getEmail() {
        return this.email;
    }

    public Membre email(String email) {
        this.setEmail(email);
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCni() {
        return this.cni;
    }

    public Membre cni(String cni) {
        this.setCni(cni);
        return this;
    }

    public void setCni(String cni) {
        this.cni = cni;
    }

    public String getAdressDakar() {
        return this.adressDakar;
    }

    public Membre adressDakar(String adressDakar) {
        this.setAdressDakar(adressDakar);
        return this;
    }

    public void setAdressDakar(String adressDakar) {
        this.adressDakar = adressDakar;
    }

    public EnumCivilite getSituationMatrimoniale() {
        return this.situationMatrimoniale;
    }

    public Membre situationMatrimoniale(EnumCivilite situationMatrimoniale) {
        this.setSituationMatrimoniale(situationMatrimoniale);
        return this;
    }

    public void setSituationMatrimoniale(EnumCivilite situationMatrimoniale) {
        this.situationMatrimoniale = situationMatrimoniale;
    }

    public String getAdresse() {
        return this.adresse;
    }

    public Membre adresse(String adresse) {
        this.setAdresse(adresse);
        return this;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getAdresseVacance() {
        return this.adresseVacance;
    }

    public Membre adresseVacance(String adresseVacance) {
        this.setAdresseVacance(adresseVacance);
        return this;
    }

    public void setAdresseVacance(String adresseVacance) {
        this.adresseVacance = adresseVacance;
    }

    public byte[] getProfil() {
        return this.profil;
    }

    public Membre profil(byte[] profil) {
        this.setProfil(profil);
        return this;
    }

    public void setProfil(byte[] profil) {
        this.profil = profil;
    }

    public String getProfilContentType() {
        return this.profilContentType;
    }

    public Membre profilContentType(String profilContentType) {
        this.profilContentType = profilContentType;
        return this;
    }

    public void setProfilContentType(String profilContentType) {
        this.profilContentType = profilContentType;
    }

    public LocalDate getDateNaissance() {
        return this.dateNaissance;
    }

    public Membre dateNaissance(LocalDate dateNaissance) {
        this.setDateNaissance(dateNaissance);
        return this;
    }

    public void setDateNaissance(LocalDate dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public Boolean getBoursier() {
        return this.boursier;
    }

    public Membre boursier(Boolean boursier) {
        this.setBoursier(boursier);
        return this;
    }

    public void setBoursier(Boolean boursier) {
        this.boursier = boursier;
    }

    public String getDaaraOrigine() {
        return this.daaraOrigine;
    }

    public Membre daaraOrigine(String daaraOrigine) {
        this.setDaaraOrigine(daaraOrigine);
        return this;
    }

    public void setDaaraOrigine(String daaraOrigine) {
        this.daaraOrigine = daaraOrigine;
    }

    public EnumEtatSante getEtatSante() {
        return this.etatSante;
    }

    public Membre etatSante(EnumEtatSante etatSante) {
        this.setEtatSante(etatSante);
        return this;
    }

    public void setEtatSante(EnumEtatSante etatSante) {
        this.etatSante = etatSante;
    }

    public String getTiteur() {
        return this.titeur;
    }

    public Membre titeur(String titeur) {
        this.setTiteur(titeur);
        return this;
    }

    public void setTiteur(String titeur) {
        this.titeur = titeur;
    }

    public String getTelephoneTiteur() {
        return this.telephoneTiteur;
    }

    public Membre telephoneTiteur(String telephoneTiteur) {
        this.setTelephoneTiteur(telephoneTiteur);
        return this;
    }

    public void setTelephoneTiteur(String telephoneTiteur) {
        this.telephoneTiteur = telephoneTiteur;
    }

    public Set<XamXam> getXamXams() {
        return this.xamXams;
    }

    public void setXamXams(Set<XamXam> xamXams) {
        this.xamXams = xamXams;
    }

    public Membre xamXams(Set<XamXam> xamXams) {
        this.setXamXams(xamXams);
        return this;
    }

    public Membre addXamXam(XamXam xamXam) {
        this.xamXams.add(xamXam);
        return this;
    }

    public Membre removeXamXam(XamXam xamXam) {
        this.xamXams.remove(xamXam);
        return this;
    }

    public NiveauXamXam getNiveauXamXam() {
        return this.niveauXamXam;
    }

    public void setNiveauXamXam(NiveauXamXam niveauXamXam) {
        this.niveauXamXam = niveauXamXam;
    }

    public Membre niveauXamXam(NiveauXamXam niveauXamXam) {
        this.setNiveauXamXam(niveauXamXam);
        return this;
    }

    public Profession getProfession() {
        return this.profession;
    }

    public void setProfession(Profession profession) {
        this.profession = profession;
    }

    public Membre profession(Profession profession) {
        this.setProfession(profession);
        return this;
    }

    public Categorie getCategorie() {
        return this.categorie;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    public Membre categorie(Categorie categorie) {
        this.setCategorie(categorie);
        return this;
    }

    public Coran getCoran() {
        return this.coran;
    }

    public void setCoran(Coran coran) {
        this.coran = coran;
    }

    public Membre coran(Coran coran) {
        this.setCoran(coran);
        return this;
    }

    public Departement getDepartement() {
        return this.departement;
    }

    public void setDepartement(Departement departement) {
        this.departement = departement;
    }

    public Membre departement(Departement departement) {
        this.setDepartement(departement);
        return this;
    }

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Membre user(User user) {
        this.setUser(user);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Membre)) {
            return false;
        }
        return id != null && id.equals(((Membre) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Membre{" +
            "id=" + getId() +
            ", nom='" + getNom() + "'" +
            ", prenom='" + getPrenom() + "'" +
            ", telephone='" + getTelephone() + "'" +
            ", sexe='" + getSexe() + "'" +
            ", email='" + getEmail() + "'" +
            ", cni='" + getCni() + "'" +
            ", adressDakar='" + getAdressDakar() + "'" +
            ", situationMatrimoniale='" + getSituationMatrimoniale() + "'" +
            ", adresse='" + getAdresse() + "'" +
            ", adresseVacance='" + getAdresseVacance() + "'" +
            ", profil='" + getProfil() + "'" +
            ", profilContentType='" + getProfilContentType() + "'" +
            ", dateNaissance='" + getDateNaissance() + "'" +
            ", boursier='" + getBoursier() + "'" +
            ", daaraOrigine='" + getDaaraOrigine() + "'" +
            ", etatSante='" + getEtatSante() + "'" +
            ", titeur='" + getTiteur() + "'" +
            ", telephoneTiteur='" + getTelephoneTiteur() + "'" +
            "}";
    }
}
