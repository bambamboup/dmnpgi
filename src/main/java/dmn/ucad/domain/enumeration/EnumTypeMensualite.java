package dmn.ucad.domain.enumeration;

/**
 * The EnumTypeMensualite enumeration.
 */
public enum EnumTypeMensualite {
    KHABANE,
    MENSUALITE,
}
