package dmn.ucad.domain.enumeration;

/**
 * The EnumCivilite enumeration.
 */
public enum EnumCivilite {
    CELLIBATAIRE,
    MARIE,
}
