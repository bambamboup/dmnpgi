package dmn.ucad.domain.enumeration;

/**
 * The EnumEtatSante enumeration.
 */
public enum EnumEtatSante {
    INSTABLE,
    STABLE,
}
