package dmn.ucad.domain.enumeration;

/**
 * The EnumTypeEntite enumeration.
 */
public enum EnumTypeEntite {
    CELLULE,
    COMMISSION,
}
