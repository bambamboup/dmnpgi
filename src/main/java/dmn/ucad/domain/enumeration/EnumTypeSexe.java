package dmn.ucad.domain.enumeration;

/**
 * The EnumTypeSexe enumeration.
 */
public enum EnumTypeSexe {
    MASCULIN,
    FEMININ,
}
