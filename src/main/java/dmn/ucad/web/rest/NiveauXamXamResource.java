package dmn.ucad.web.rest;

import dmn.ucad.domain.NiveauXamXam;
import dmn.ucad.repository.NiveauXamXamRepository;
import dmn.ucad.service.NiveauXamXamService;
import dmn.ucad.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link dmn.ucad.domain.NiveauXamXam}.
 */
@RestController
@RequestMapping("/api")
public class NiveauXamXamResource {

    private final Logger log = LoggerFactory.getLogger(NiveauXamXamResource.class);

    private static final String ENTITY_NAME = "niveauXamXam";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final NiveauXamXamService niveauXamXamService;

    private final NiveauXamXamRepository niveauXamXamRepository;

    public NiveauXamXamResource(NiveauXamXamService niveauXamXamService, NiveauXamXamRepository niveauXamXamRepository) {
        this.niveauXamXamService = niveauXamXamService;
        this.niveauXamXamRepository = niveauXamXamRepository;
    }

    /**
     * {@code POST  /niveau-xam-xams} : Create a new niveauXamXam.
     *
     * @param niveauXamXam the niveauXamXam to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new niveauXamXam, or with status {@code 400 (Bad Request)} if the niveauXamXam has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/niveau-xam-xams")
    public ResponseEntity<NiveauXamXam> createNiveauXamXam(@Valid @RequestBody NiveauXamXam niveauXamXam) throws URISyntaxException {
        log.debug("REST request to save NiveauXamXam : {}", niveauXamXam);
        if (niveauXamXam.getId() != null) {
            throw new BadRequestAlertException("A new niveauXamXam cannot already have an ID", ENTITY_NAME, "idexists");
        }
        NiveauXamXam result = niveauXamXamService.save(niveauXamXam);
        return ResponseEntity
            .created(new URI("/api/niveau-xam-xams/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /niveau-xam-xams/:id} : Updates an existing niveauXamXam.
     *
     * @param id the id of the niveauXamXam to save.
     * @param niveauXamXam the niveauXamXam to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated niveauXamXam,
     * or with status {@code 400 (Bad Request)} if the niveauXamXam is not valid,
     * or with status {@code 500 (Internal Server Error)} if the niveauXamXam couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/niveau-xam-xams/{id}")
    public ResponseEntity<NiveauXamXam> updateNiveauXamXam(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody NiveauXamXam niveauXamXam
    ) throws URISyntaxException {
        log.debug("REST request to update NiveauXamXam : {}, {}", id, niveauXamXam);
        if (niveauXamXam.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, niveauXamXam.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!niveauXamXamRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        NiveauXamXam result = niveauXamXamService.save(niveauXamXam);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, niveauXamXam.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /niveau-xam-xams/:id} : Partial updates given fields of an existing niveauXamXam, field will ignore if it is null
     *
     * @param id the id of the niveauXamXam to save.
     * @param niveauXamXam the niveauXamXam to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated niveauXamXam,
     * or with status {@code 400 (Bad Request)} if the niveauXamXam is not valid,
     * or with status {@code 404 (Not Found)} if the niveauXamXam is not found,
     * or with status {@code 500 (Internal Server Error)} if the niveauXamXam couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/niveau-xam-xams/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<NiveauXamXam> partialUpdateNiveauXamXam(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody NiveauXamXam niveauXamXam
    ) throws URISyntaxException {
        log.debug("REST request to partial update NiveauXamXam partially : {}, {}", id, niveauXamXam);
        if (niveauXamXam.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, niveauXamXam.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!niveauXamXamRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<NiveauXamXam> result = niveauXamXamService.partialUpdate(niveauXamXam);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, niveauXamXam.getId().toString())
        );
    }

    /**
     * {@code GET  /niveau-xam-xams} : get all the niveauXamXams.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of niveauXamXams in body.
     */
    @GetMapping("/niveau-xam-xams")
    public ResponseEntity<List<NiveauXamXam>> getAllNiveauXamXams(@org.springdoc.api.annotations.ParameterObject Pageable pageable) {
        log.debug("REST request to get a page of NiveauXamXams");
        Page<NiveauXamXam> page = niveauXamXamService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /niveau-xam-xams/:id} : get the "id" niveauXamXam.
     *
     * @param id the id of the niveauXamXam to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the niveauXamXam, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/niveau-xam-xams/{id}")
    public ResponseEntity<NiveauXamXam> getNiveauXamXam(@PathVariable Long id) {
        log.debug("REST request to get NiveauXamXam : {}", id);
        Optional<NiveauXamXam> niveauXamXam = niveauXamXamService.findOne(id);
        return ResponseUtil.wrapOrNotFound(niveauXamXam);
    }

    /**
     * {@code DELETE  /niveau-xam-xams/:id} : delete the "id" niveauXamXam.
     *
     * @param id the id of the niveauXamXam to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/niveau-xam-xams/{id}")
    public ResponseEntity<Void> deleteNiveauXamXam(@PathVariable Long id) {
        log.debug("REST request to delete NiveauXamXam : {}", id);
        niveauXamXamService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
