package dmn.ucad.web.rest;

import dmn.ucad.domain.Mensualite;
import dmn.ucad.repository.MensualiteRepository;
import dmn.ucad.service.MensualiteService;
import dmn.ucad.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link dmn.ucad.domain.Mensualite}.
 */
@RestController
@RequestMapping("/api")
public class MensualiteResource {

    private final Logger log = LoggerFactory.getLogger(MensualiteResource.class);

    private static final String ENTITY_NAME = "mensualite";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MensualiteService mensualiteService;

    private final MensualiteRepository mensualiteRepository;

    public MensualiteResource(MensualiteService mensualiteService, MensualiteRepository mensualiteRepository) {
        this.mensualiteService = mensualiteService;
        this.mensualiteRepository = mensualiteRepository;
    }

    /**
     * {@code POST  /mensualites} : Create a new mensualite.
     *
     * @param mensualite the mensualite to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new mensualite, or with status {@code 400 (Bad Request)} if the mensualite has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/mensualites")
    public ResponseEntity<Mensualite> createMensualite(@Valid @RequestBody Mensualite mensualite) throws URISyntaxException {
        log.debug("REST request to save Mensualite : {}", mensualite);
        if (mensualite.getId() != null) {
            throw new BadRequestAlertException("A new mensualite cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Mensualite result = mensualiteService.save(mensualite);
        return ResponseEntity
            .created(new URI("/api/mensualites/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /mensualites/:id} : Updates an existing mensualite.
     *
     * @param id the id of the mensualite to save.
     * @param mensualite the mensualite to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated mensualite,
     * or with status {@code 400 (Bad Request)} if the mensualite is not valid,
     * or with status {@code 500 (Internal Server Error)} if the mensualite couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/mensualites/{id}")
    public ResponseEntity<Mensualite> updateMensualite(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody Mensualite mensualite
    ) throws URISyntaxException {
        log.debug("REST request to update Mensualite : {}, {}", id, mensualite);
        if (mensualite.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, mensualite.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!mensualiteRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Mensualite result = mensualiteService.save(mensualite);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, mensualite.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /mensualites/:id} : Partial updates given fields of an existing mensualite, field will ignore if it is null
     *
     * @param id the id of the mensualite to save.
     * @param mensualite the mensualite to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated mensualite,
     * or with status {@code 400 (Bad Request)} if the mensualite is not valid,
     * or with status {@code 404 (Not Found)} if the mensualite is not found,
     * or with status {@code 500 (Internal Server Error)} if the mensualite couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/mensualites/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Mensualite> partialUpdateMensualite(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody Mensualite mensualite
    ) throws URISyntaxException {
        log.debug("REST request to partial update Mensualite partially : {}, {}", id, mensualite);
        if (mensualite.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, mensualite.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!mensualiteRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Mensualite> result = mensualiteService.partialUpdate(mensualite);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, mensualite.getId().toString())
        );
    }

    /**
     * {@code GET  /mensualites} : get all the mensualites.
     *
     * @param pageable the pagination information.
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of mensualites in body.
     */
    @GetMapping("/mensualites")
    public ResponseEntity<List<Mensualite>> getAllMensualites(
        @org.springdoc.api.annotations.ParameterObject Pageable pageable,
        @RequestParam(required = false, defaultValue = "true") boolean eagerload
    ) {
        log.debug("REST request to get a page of Mensualites");
        Page<Mensualite> page;
        if (eagerload) {
            page = mensualiteService.findAllWithEagerRelationships(pageable);
        } else {
            page = mensualiteService.findAll(pageable);
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /mensualites/:id} : get the "id" mensualite.
     *
     * @param id the id of the mensualite to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the mensualite, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/mensualites/{id}")
    public ResponseEntity<Mensualite> getMensualite(@PathVariable Long id) {
        log.debug("REST request to get Mensualite : {}", id);
        Optional<Mensualite> mensualite = mensualiteService.findOne(id);
        return ResponseUtil.wrapOrNotFound(mensualite);
    }

    /**
     * {@code DELETE  /mensualites/:id} : delete the "id" mensualite.
     *
     * @param id the id of the mensualite to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/mensualites/{id}")
    public ResponseEntity<Void> deleteMensualite(@PathVariable Long id) {
        log.debug("REST request to delete Mensualite : {}", id);
        mensualiteService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
