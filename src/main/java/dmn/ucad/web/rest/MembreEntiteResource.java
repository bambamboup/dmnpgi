package dmn.ucad.web.rest;

import dmn.ucad.domain.MembreEntite;
import dmn.ucad.repository.MembreEntiteRepository;
import dmn.ucad.service.MembreEntiteService;
import dmn.ucad.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link dmn.ucad.domain.MembreEntite}.
 */
@RestController
@RequestMapping("/api")
public class MembreEntiteResource {

    private final Logger log = LoggerFactory.getLogger(MembreEntiteResource.class);

    private static final String ENTITY_NAME = "membreEntite";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MembreEntiteService membreEntiteService;

    private final MembreEntiteRepository membreEntiteRepository;

    public MembreEntiteResource(MembreEntiteService membreEntiteService, MembreEntiteRepository membreEntiteRepository) {
        this.membreEntiteService = membreEntiteService;
        this.membreEntiteRepository = membreEntiteRepository;
    }

    /**
     * {@code POST  /membre-entites} : Create a new membreEntite.
     *
     * @param membreEntite the membreEntite to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new membreEntite, or with status {@code 400 (Bad Request)} if the membreEntite has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/membre-entites")
    public ResponseEntity<MembreEntite> createMembreEntite(@Valid @RequestBody MembreEntite membreEntite) throws URISyntaxException {
        log.debug("REST request to save MembreEntite : {}", membreEntite);
        if (membreEntite.getId() != null) {
            throw new BadRequestAlertException("A new membreEntite cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MembreEntite result = membreEntiteService.save(membreEntite);
        return ResponseEntity
            .created(new URI("/api/membre-entites/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @PostMapping("/membre-entites/all")
    public ResponseEntity<Integer> createMembreEntiteAll(@Valid @RequestBody List<MembreEntite> membreEntites) throws URISyntaxException {
        List<MembreEntite> result = membreEntiteRepository.saveAll(membreEntites);
        return ResponseEntity
            .created(new URI("/api/membre-entites/all" + result.size()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, Integer.toString(result.size())))
            .body(result.size());
    }

    /**
     * {@code PUT  /membre-entites/:id} : Updates an existing membreEntite.
     *
     * @param id the id of the membreEntite to save.
     * @param membreEntite the membreEntite to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated membreEntite,
     * or with status {@code 400 (Bad Request)} if the membreEntite is not valid,
     * or with status {@code 500 (Internal Server Error)} if the membreEntite couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/membre-entites/{id}")
    public ResponseEntity<MembreEntite> updateMembreEntite(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody MembreEntite membreEntite
    ) throws URISyntaxException {
        log.debug("REST request to update MembreEntite : {}, {}", id, membreEntite);
        if (membreEntite.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, membreEntite.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!membreEntiteRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        MembreEntite result = membreEntiteService.save(membreEntite);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, membreEntite.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /membre-entites/:id} : Partial updates given fields of an existing membreEntite, field will ignore if it is null
     *
     * @param id the id of the membreEntite to save.
     * @param membreEntite the membreEntite to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated membreEntite,
     * or with status {@code 400 (Bad Request)} if the membreEntite is not valid,
     * or with status {@code 404 (Not Found)} if the membreEntite is not found,
     * or with status {@code 500 (Internal Server Error)} if the membreEntite couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/membre-entites/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<MembreEntite> partialUpdateMembreEntite(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody MembreEntite membreEntite
    ) throws URISyntaxException {
        log.debug("REST request to partial update MembreEntite partially : {}, {}", id, membreEntite);
        if (membreEntite.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, membreEntite.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!membreEntiteRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<MembreEntite> result = membreEntiteService.partialUpdate(membreEntite);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, membreEntite.getId().toString())
        );
    }

    /**
     * {@code GET  /membre-entites} : get all the membreEntites.
     *
     * @param pageable the pagination information.
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of membreEntites in body.
     */
    @GetMapping("/membre-entites")
    public ResponseEntity<List<MembreEntite>> getAllMembreEntites(
        @org.springdoc.api.annotations.ParameterObject Pageable pageable,
        @RequestParam(required = false, defaultValue = "true") boolean eagerload
    ) {
        log.debug("REST request to get a page of MembreEntites");
        Page<MembreEntite> page;
        if (eagerload) {
            page = membreEntiteService.findAllWithEagerRelationships(pageable);
        } else {
            page = membreEntiteService.findAll(pageable);
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /membre-entites/:id} : get the "id" membreEntite.
     *
     * @param id the id of the membreEntite to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the membreEntite, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/membre-entites/{id}")
    public ResponseEntity<MembreEntite> getMembreEntite(@PathVariable Long id) {
        log.debug("REST request to get MembreEntite : {}", id);
        Optional<MembreEntite> membreEntite = membreEntiteService.findOne(id);
        return ResponseUtil.wrapOrNotFound(membreEntite);
    }

    /**
     * {@code DELETE  /membre-entites/:id} : delete the "id" membreEntite.
     *
     * @param id the id of the membreEntite to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/membre-entites/{id}")
    public ResponseEntity<Void> deleteMembreEntite(@PathVariable Long id) {
        log.debug("REST request to delete MembreEntite : {}", id);
        membreEntiteService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
