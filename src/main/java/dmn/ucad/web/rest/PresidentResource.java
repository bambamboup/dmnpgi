package dmn.ucad.web.rest;

import dmn.ucad.domain.President;
import dmn.ucad.repository.PresidentRepository;
import dmn.ucad.service.PresidentService;
import dmn.ucad.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link dmn.ucad.domain.President}.
 */
@RestController
@RequestMapping("/api")
public class PresidentResource {

    private final Logger log = LoggerFactory.getLogger(PresidentResource.class);

    private static final String ENTITY_NAME = "president";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PresidentService presidentService;

    private final PresidentRepository presidentRepository;

    public PresidentResource(PresidentService presidentService, PresidentRepository presidentRepository) {
        this.presidentService = presidentService;
        this.presidentRepository = presidentRepository;
    }

    /**
     * {@code POST  /presidents} : Create a new president.
     *
     * @param president the president to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new president, or with status {@code 400 (Bad Request)} if the president has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/presidents")
    public ResponseEntity<President> createPresident(@Valid @RequestBody President president) throws URISyntaxException {
        log.debug("REST request to save President : {}", president);
        if (president.getId() != null) {
            throw new BadRequestAlertException("A new president cannot already have an ID", ENTITY_NAME, "idexists");
        }
        President result = presidentService.save(president);
        return ResponseEntity
            .created(new URI("/api/presidents/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /presidents/:id} : Updates an existing president.
     *
     * @param id the id of the president to save.
     * @param president the president to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated president,
     * or with status {@code 400 (Bad Request)} if the president is not valid,
     * or with status {@code 500 (Internal Server Error)} if the president couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/presidents/{id}")
    public ResponseEntity<President> updatePresident(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody President president
    ) throws URISyntaxException {
        log.debug("REST request to update President : {}, {}", id, president);
        if (president.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, president.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!presidentRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        President result = presidentService.save(president);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, president.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /presidents/:id} : Partial updates given fields of an existing president, field will ignore if it is null
     *
     * @param id the id of the president to save.
     * @param president the president to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated president,
     * or with status {@code 400 (Bad Request)} if the president is not valid,
     * or with status {@code 404 (Not Found)} if the president is not found,
     * or with status {@code 500 (Internal Server Error)} if the president couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/presidents/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<President> partialUpdatePresident(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody President president
    ) throws URISyntaxException {
        log.debug("REST request to partial update President partially : {}, {}", id, president);
        if (president.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, president.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!presidentRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<President> result = presidentService.partialUpdate(president);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, president.getId().toString())
        );
    }

    /**
     * {@code GET  /presidents} : get all the presidents.
     *
     * @param pageable the pagination information.
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of presidents in body.
     */
    @GetMapping("/presidents")
    public ResponseEntity<List<President>> getAllPresidents(
        @org.springdoc.api.annotations.ParameterObject Pageable pageable,
        @RequestParam(required = false, defaultValue = "true") boolean eagerload
    ) {
        log.debug("REST request to get a page of Presidents");
        Page<President> page;
        if (eagerload) {
            page = presidentService.findAllWithEagerRelationships(pageable);
        } else {
            page = presidentService.findAll(pageable);
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /presidents/:id} : get the "id" president.
     *
     * @param id the id of the president to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the president, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/presidents/{id}")
    public ResponseEntity<President> getPresident(@PathVariable Long id) {
        log.debug("REST request to get President : {}", id);
        Optional<President> president = presidentService.findOne(id);
        return ResponseUtil.wrapOrNotFound(president);
    }

    /**
     * {@code DELETE  /presidents/:id} : delete the "id" president.
     *
     * @param id the id of the president to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/presidents/{id}")
    public ResponseEntity<Void> deletePresident(@PathVariable Long id) {
        log.debug("REST request to delete President : {}", id);
        presidentService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
