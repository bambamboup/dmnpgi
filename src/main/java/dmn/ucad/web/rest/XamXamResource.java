package dmn.ucad.web.rest;

import dmn.ucad.domain.XamXam;
import dmn.ucad.repository.XamXamRepository;
import dmn.ucad.service.XamXamService;
import dmn.ucad.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link dmn.ucad.domain.XamXam}.
 */
@RestController
@RequestMapping("/api")
public class XamXamResource {

    private final Logger log = LoggerFactory.getLogger(XamXamResource.class);

    private static final String ENTITY_NAME = "xamXam";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final XamXamService xamXamService;

    private final XamXamRepository xamXamRepository;

    public XamXamResource(XamXamService xamXamService, XamXamRepository xamXamRepository) {
        this.xamXamService = xamXamService;
        this.xamXamRepository = xamXamRepository;
    }

    /**
     * {@code POST  /xam-xams} : Create a new xamXam.
     *
     * @param xamXam the xamXam to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new xamXam, or with status {@code 400 (Bad Request)} if the xamXam has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/xam-xams")
    public ResponseEntity<XamXam> createXamXam(@Valid @RequestBody XamXam xamXam) throws URISyntaxException {
        log.debug("REST request to save XamXam : {}", xamXam);
        if (xamXam.getId() != null) {
            throw new BadRequestAlertException("A new xamXam cannot already have an ID", ENTITY_NAME, "idexists");
        }
        XamXam result = xamXamService.save(xamXam);
        return ResponseEntity
            .created(new URI("/api/xam-xams/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /xam-xams/:id} : Updates an existing xamXam.
     *
     * @param id the id of the xamXam to save.
     * @param xamXam the xamXam to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated xamXam,
     * or with status {@code 400 (Bad Request)} if the xamXam is not valid,
     * or with status {@code 500 (Internal Server Error)} if the xamXam couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/xam-xams/{id}")
    public ResponseEntity<XamXam> updateXamXam(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody XamXam xamXam
    ) throws URISyntaxException {
        log.debug("REST request to update XamXam : {}, {}", id, xamXam);
        if (xamXam.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, xamXam.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!xamXamRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        XamXam result = xamXamService.save(xamXam);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, xamXam.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /xam-xams/:id} : Partial updates given fields of an existing xamXam, field will ignore if it is null
     *
     * @param id the id of the xamXam to save.
     * @param xamXam the xamXam to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated xamXam,
     * or with status {@code 400 (Bad Request)} if the xamXam is not valid,
     * or with status {@code 404 (Not Found)} if the xamXam is not found,
     * or with status {@code 500 (Internal Server Error)} if the xamXam couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/xam-xams/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<XamXam> partialUpdateXamXam(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody XamXam xamXam
    ) throws URISyntaxException {
        log.debug("REST request to partial update XamXam partially : {}, {}", id, xamXam);
        if (xamXam.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, xamXam.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!xamXamRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<XamXam> result = xamXamService.partialUpdate(xamXam);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, xamXam.getId().toString())
        );
    }

    /**
     * {@code GET  /xam-xams} : get all the xamXams.
     *
     * @param pageable the pagination information.
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of xamXams in body.
     */
    @GetMapping("/xam-xams")
    public ResponseEntity<List<XamXam>> getAllXamXams(
        @org.springdoc.api.annotations.ParameterObject Pageable pageable,
        @RequestParam(required = false, defaultValue = "true") boolean eagerload
    ) {
        log.debug("REST request to get a page of XamXams");
        Page<XamXam> page;
        if (eagerload) {
            page = xamXamService.findAllWithEagerRelationships(pageable);
        } else {
            page = xamXamService.findAll(pageable);
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /xam-xams/:id} : get the "id" xamXam.
     *
     * @param id the id of the xamXam to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the xamXam, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/xam-xams/{id}")
    public ResponseEntity<XamXam> getXamXam(@PathVariable Long id) {
        log.debug("REST request to get XamXam : {}", id);
        Optional<XamXam> xamXam = xamXamService.findOne(id);
        return ResponseUtil.wrapOrNotFound(xamXam);
    }

    /**
     * {@code DELETE  /xam-xams/:id} : delete the "id" xamXam.
     *
     * @param id the id of the xamXam to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/xam-xams/{id}")
    public ResponseEntity<Void> deleteXamXam(@PathVariable Long id) {
        log.debug("REST request to delete XamXam : {}", id);
        xamXamService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
