package dmn.ucad.web.rest;

import dmn.ucad.domain.CaisseMois;
import dmn.ucad.repository.CaisseMoisRepository;
import dmn.ucad.service.CaisseMoisService;
import dmn.ucad.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link dmn.ucad.domain.CaisseMois}.
 */
@RestController
@RequestMapping("/api")
public class CaisseMoisResource {

    private final Logger log = LoggerFactory.getLogger(CaisseMoisResource.class);

    private static final String ENTITY_NAME = "caisseMois";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CaisseMoisService caisseMoisService;

    private final CaisseMoisRepository caisseMoisRepository;

    public CaisseMoisResource(CaisseMoisService caisseMoisService, CaisseMoisRepository caisseMoisRepository) {
        this.caisseMoisService = caisseMoisService;
        this.caisseMoisRepository = caisseMoisRepository;
    }

    /**
     * {@code POST  /caisse-mois} : Create a new caisseMois.
     *
     * @param caisseMois the caisseMois to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new caisseMois, or with status {@code 400 (Bad Request)} if the caisseMois has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/caisse-mois")
    public ResponseEntity<CaisseMois> createCaisseMois(@Valid @RequestBody CaisseMois caisseMois) throws URISyntaxException {
        log.debug("REST request to save CaisseMois : {}", caisseMois);
        if (caisseMois.getId() != null) {
            throw new BadRequestAlertException("A new caisseMois cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CaisseMois result = caisseMoisService.save(caisseMois);
        return ResponseEntity
            .created(new URI("/api/caisse-mois/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /caisse-mois/:id} : Updates an existing caisseMois.
     *
     * @param id the id of the caisseMois to save.
     * @param caisseMois the caisseMois to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated caisseMois,
     * or with status {@code 400 (Bad Request)} if the caisseMois is not valid,
     * or with status {@code 500 (Internal Server Error)} if the caisseMois couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/caisse-mois/{id}")
    public ResponseEntity<CaisseMois> updateCaisseMois(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody CaisseMois caisseMois
    ) throws URISyntaxException {
        log.debug("REST request to update CaisseMois : {}, {}", id, caisseMois);
        if (caisseMois.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, caisseMois.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!caisseMoisRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        CaisseMois result = caisseMoisService.save(caisseMois);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, caisseMois.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /caisse-mois/:id} : Partial updates given fields of an existing caisseMois, field will ignore if it is null
     *
     * @param id the id of the caisseMois to save.
     * @param caisseMois the caisseMois to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated caisseMois,
     * or with status {@code 400 (Bad Request)} if the caisseMois is not valid,
     * or with status {@code 404 (Not Found)} if the caisseMois is not found,
     * or with status {@code 500 (Internal Server Error)} if the caisseMois couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/caisse-mois/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<CaisseMois> partialUpdateCaisseMois(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody CaisseMois caisseMois
    ) throws URISyntaxException {
        log.debug("REST request to partial update CaisseMois partially : {}, {}", id, caisseMois);
        if (caisseMois.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, caisseMois.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!caisseMoisRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<CaisseMois> result = caisseMoisService.partialUpdate(caisseMois);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, caisseMois.getId().toString())
        );
    }

    /**
     * {@code GET  /caisse-mois} : get all the caisseMois.
     *
     * @param pageable the pagination information.
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of caisseMois in body.
     */
    @GetMapping("/caisse-mois")
    public ResponseEntity<List<CaisseMois>> getAllCaisseMois(
        @org.springdoc.api.annotations.ParameterObject Pageable pageable,
        @RequestParam(required = false, defaultValue = "true") boolean eagerload
    ) {
        log.debug("REST request to get a page of CaisseMois");
        Page<CaisseMois> page;
        if (eagerload) {
            page = caisseMoisService.findAllWithEagerRelationships(pageable);
        } else {
            page = caisseMoisService.findAll(pageable);
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /caisse-mois/:id} : get the "id" caisseMois.
     *
     * @param id the id of the caisseMois to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the caisseMois, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/caisse-mois/{id}")
    public ResponseEntity<CaisseMois> getCaisseMois(@PathVariable Long id) {
        log.debug("REST request to get CaisseMois : {}", id);
        Optional<CaisseMois> caisseMois = caisseMoisService.findOne(id);
        return ResponseUtil.wrapOrNotFound(caisseMois);
    }

    /**
     * {@code DELETE  /caisse-mois/:id} : delete the "id" caisseMois.
     *
     * @param id the id of the caisseMois to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/caisse-mois/{id}")
    public ResponseEntity<Void> deleteCaisseMois(@PathVariable Long id) {
        log.debug("REST request to delete CaisseMois : {}", id);
        caisseMoisService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
