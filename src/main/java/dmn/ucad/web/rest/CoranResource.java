package dmn.ucad.web.rest;

import dmn.ucad.domain.Coran;
import dmn.ucad.repository.CoranRepository;
import dmn.ucad.service.CoranService;
import dmn.ucad.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link dmn.ucad.domain.Coran}.
 */
@RestController
@RequestMapping("/api")
public class CoranResource {

    private final Logger log = LoggerFactory.getLogger(CoranResource.class);

    private static final String ENTITY_NAME = "coran";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CoranService coranService;

    private final CoranRepository coranRepository;

    public CoranResource(CoranService coranService, CoranRepository coranRepository) {
        this.coranService = coranService;
        this.coranRepository = coranRepository;
    }

    /**
     * {@code POST  /corans} : Create a new coran.
     *
     * @param coran the coran to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new coran, or with status {@code 400 (Bad Request)} if the coran has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/corans")
    public ResponseEntity<Coran> createCoran(@Valid @RequestBody Coran coran) throws URISyntaxException {
        log.debug("REST request to save Coran : {}", coran);
        if (coran.getId() != null) {
            throw new BadRequestAlertException("A new coran cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Coran result = coranService.save(coran);
        return ResponseEntity
            .created(new URI("/api/corans/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /corans/:id} : Updates an existing coran.
     *
     * @param id the id of the coran to save.
     * @param coran the coran to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated coran,
     * or with status {@code 400 (Bad Request)} if the coran is not valid,
     * or with status {@code 500 (Internal Server Error)} if the coran couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/corans/{id}")
    public ResponseEntity<Coran> updateCoran(@PathVariable(value = "id", required = false) final Long id, @Valid @RequestBody Coran coran)
        throws URISyntaxException {
        log.debug("REST request to update Coran : {}, {}", id, coran);
        if (coran.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, coran.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!coranRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Coran result = coranService.save(coran);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, coran.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /corans/:id} : Partial updates given fields of an existing coran, field will ignore if it is null
     *
     * @param id the id of the coran to save.
     * @param coran the coran to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated coran,
     * or with status {@code 400 (Bad Request)} if the coran is not valid,
     * or with status {@code 404 (Not Found)} if the coran is not found,
     * or with status {@code 500 (Internal Server Error)} if the coran couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/corans/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Coran> partialUpdateCoran(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody Coran coran
    ) throws URISyntaxException {
        log.debug("REST request to partial update Coran partially : {}, {}", id, coran);
        if (coran.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, coran.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!coranRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Coran> result = coranService.partialUpdate(coran);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, coran.getId().toString())
        );
    }

    /**
     * {@code GET  /corans} : get all the corans.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of corans in body.
     */
    @GetMapping("/corans")
    public ResponseEntity<List<Coran>> getAllCorans(@org.springdoc.api.annotations.ParameterObject Pageable pageable) {
        log.debug("REST request to get a page of Corans");
        Page<Coran> page = coranService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /corans/:id} : get the "id" coran.
     *
     * @param id the id of the coran to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the coran, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/corans/{id}")
    public ResponseEntity<Coran> getCoran(@PathVariable Long id) {
        log.debug("REST request to get Coran : {}", id);
        Optional<Coran> coran = coranService.findOne(id);
        return ResponseUtil.wrapOrNotFound(coran);
    }

    /**
     * {@code DELETE  /corans/:id} : delete the "id" coran.
     *
     * @param id the id of the coran to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/corans/{id}")
    public ResponseEntity<Void> deleteCoran(@PathVariable Long id) {
        log.debug("REST request to delete Coran : {}", id);
        coranService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
