package dmn.ucad.web.rest;

import dmn.ucad.domain.Enseignement;
import dmn.ucad.repository.EnseignementRepository;
import dmn.ucad.service.EnseignementService;
import dmn.ucad.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link dmn.ucad.domain.Enseignement}.
 */
@RestController
@RequestMapping("/api")
public class EnseignementResource {

    private final Logger log = LoggerFactory.getLogger(EnseignementResource.class);

    private static final String ENTITY_NAME = "enseignement";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EnseignementService enseignementService;

    private final EnseignementRepository enseignementRepository;

    public EnseignementResource(EnseignementService enseignementService, EnseignementRepository enseignementRepository) {
        this.enseignementService = enseignementService;
        this.enseignementRepository = enseignementRepository;
    }

    /**
     * {@code POST  /enseignements} : Create a new enseignement.
     *
     * @param enseignement the enseignement to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new enseignement, or with status {@code 400 (Bad Request)} if the enseignement has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/enseignements")
    public ResponseEntity<Enseignement> createEnseignement(@Valid @RequestBody Enseignement enseignement) throws URISyntaxException {
        log.debug("REST request to save Enseignement : {}", enseignement);
        if (enseignement.getId() != null) {
            throw new BadRequestAlertException("A new enseignement cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Enseignement result = enseignementService.save(enseignement);
        return ResponseEntity
            .created(new URI("/api/enseignements/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /enseignements/:id} : Updates an existing enseignement.
     *
     * @param id the id of the enseignement to save.
     * @param enseignement the enseignement to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated enseignement,
     * or with status {@code 400 (Bad Request)} if the enseignement is not valid,
     * or with status {@code 500 (Internal Server Error)} if the enseignement couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/enseignements/{id}")
    public ResponseEntity<Enseignement> updateEnseignement(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody Enseignement enseignement
    ) throws URISyntaxException {
        log.debug("REST request to update Enseignement : {}, {}", id, enseignement);
        if (enseignement.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, enseignement.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!enseignementRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Enseignement result = enseignementService.save(enseignement);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, enseignement.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /enseignements/:id} : Partial updates given fields of an existing enseignement, field will ignore if it is null
     *
     * @param id the id of the enseignement to save.
     * @param enseignement the enseignement to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated enseignement,
     * or with status {@code 400 (Bad Request)} if the enseignement is not valid,
     * or with status {@code 404 (Not Found)} if the enseignement is not found,
     * or with status {@code 500 (Internal Server Error)} if the enseignement couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/enseignements/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Enseignement> partialUpdateEnseignement(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody Enseignement enseignement
    ) throws URISyntaxException {
        log.debug("REST request to partial update Enseignement partially : {}, {}", id, enseignement);
        if (enseignement.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, enseignement.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!enseignementRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Enseignement> result = enseignementService.partialUpdate(enseignement);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, enseignement.getId().toString())
        );
    }

    /**
     * {@code GET  /enseignements} : get all the enseignements.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of enseignements in body.
     */
    @GetMapping("/enseignements")
    public ResponseEntity<List<Enseignement>> getAllEnseignements(@org.springdoc.api.annotations.ParameterObject Pageable pageable) {
        log.debug("REST request to get a page of Enseignements");
        Page<Enseignement> page = enseignementService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /enseignements/:id} : get the "id" enseignement.
     *
     * @param id the id of the enseignement to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the enseignement, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/enseignements/{id}")
    public ResponseEntity<Enseignement> getEnseignement(@PathVariable Long id) {
        log.debug("REST request to get Enseignement : {}", id);
        Optional<Enseignement> enseignement = enseignementService.findOne(id);
        return ResponseUtil.wrapOrNotFound(enseignement);
    }

    /**
     * {@code DELETE  /enseignements/:id} : delete the "id" enseignement.
     *
     * @param id the id of the enseignement to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/enseignements/{id}")
    public ResponseEntity<Void> deleteEnseignement(@PathVariable Long id) {
        log.debug("REST request to delete Enseignement : {}", id);
        enseignementService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
