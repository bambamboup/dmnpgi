package dmn.ucad.web.rest;

import dmn.ucad.domain.Entite;
import dmn.ucad.repository.EntiteRepository;
import dmn.ucad.service.EntiteService;
import dmn.ucad.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link dmn.ucad.domain.Entite}.
 */
@RestController
@RequestMapping("/api")
public class EntiteResource {

    private final Logger log = LoggerFactory.getLogger(EntiteResource.class);

    private static final String ENTITY_NAME = "entite";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EntiteService entiteService;

    private final EntiteRepository entiteRepository;

    public EntiteResource(EntiteService entiteService, EntiteRepository entiteRepository) {
        this.entiteService = entiteService;
        this.entiteRepository = entiteRepository;
    }

    /**
     * {@code POST  /entites} : Create a new entite.
     *
     * @param entite the entite to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new entite, or with status {@code 400 (Bad Request)} if the entite has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/entites")
    public ResponseEntity<Entite> createEntite(@Valid @RequestBody Entite entite) throws URISyntaxException {
        log.debug("REST request to save Entite : {}", entite);
        if (entite.getId() != null) {
            throw new BadRequestAlertException("A new entite cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Entite result = entiteService.save(entite);
        return ResponseEntity
            .created(new URI("/api/entites/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /entites/:id} : Updates an existing entite.
     *
     * @param id the id of the entite to save.
     * @param entite the entite to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated entite,
     * or with status {@code 400 (Bad Request)} if the entite is not valid,
     * or with status {@code 500 (Internal Server Error)} if the entite couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/entites/{id}")
    public ResponseEntity<Entite> updateEntite(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody Entite entite
    ) throws URISyntaxException {
        log.debug("REST request to update Entite : {}, {}", id, entite);
        if (entite.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, entite.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!entiteRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Entite result = entiteService.save(entite);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, entite.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /entites/:id} : Partial updates given fields of an existing entite, field will ignore if it is null
     *
     * @param id the id of the entite to save.
     * @param entite the entite to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated entite,
     * or with status {@code 400 (Bad Request)} if the entite is not valid,
     * or with status {@code 404 (Not Found)} if the entite is not found,
     * or with status {@code 500 (Internal Server Error)} if the entite couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/entites/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Entite> partialUpdateEntite(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody Entite entite
    ) throws URISyntaxException {
        log.debug("REST request to partial update Entite partially : {}, {}", id, entite);
        if (entite.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, entite.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!entiteRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Entite> result = entiteService.partialUpdate(entite);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, entite.getId().toString())
        );
    }

    /**
     * {@code GET  /entites} : get all the entites.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of entites in body.
     */
    @GetMapping("/entites")
    public ResponseEntity<List<Entite>> getAllEntites(@org.springdoc.api.annotations.ParameterObject Pageable pageable) {
        log.debug("REST request to get a page of Entites");
        Page<Entite> page = entiteService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /entites/:id} : get the "id" entite.
     *
     * @param id the id of the entite to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the entite, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/entites/{id}")
    public ResponseEntity<Entite> getEntite(@PathVariable Long id) {
        log.debug("REST request to get Entite : {}", id);
        Optional<Entite> entite = entiteService.findOne(id);
        return ResponseUtil.wrapOrNotFound(entite);
    }

    /**
     * {@code DELETE  /entites/:id} : delete the "id" entite.
     *
     * @param id the id of the entite to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/entites/{id}")
    public ResponseEntity<Void> deleteEntite(@PathVariable Long id) {
        log.debug("REST request to delete Entite : {}", id);
        entiteService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
