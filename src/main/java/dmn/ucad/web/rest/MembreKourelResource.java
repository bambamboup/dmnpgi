package dmn.ucad.web.rest;

import dmn.ucad.domain.MembreKourel;
import dmn.ucad.repository.MembreKourelRepository;
import dmn.ucad.service.MembreKourelService;
import dmn.ucad.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link dmn.ucad.domain.MembreKourel}.
 */
@RestController
@RequestMapping("/api")
public class MembreKourelResource {

    private final Logger log = LoggerFactory.getLogger(MembreKourelResource.class);

    private static final String ENTITY_NAME = "membreKourel";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MembreKourelService membreKourelService;

    private final MembreKourelRepository membreKourelRepository;

    public MembreKourelResource(MembreKourelService membreKourelService, MembreKourelRepository membreKourelRepository) {
        this.membreKourelService = membreKourelService;
        this.membreKourelRepository = membreKourelRepository;
    }

    /**
     * {@code POST  /membre-kourels} : Create a new membreKourel.
     *
     * @param membreKourel the membreKourel to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new membreKourel, or with status {@code 400 (Bad Request)} if the membreKourel has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/membre-kourels")
    public ResponseEntity<MembreKourel> createMembreKourel(@Valid @RequestBody MembreKourel membreKourel) throws URISyntaxException {
        log.debug("REST request to save MembreKourel : {}", membreKourel);
        if (membreKourel.getId() != null) {
            throw new BadRequestAlertException("A new membreKourel cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MembreKourel result = membreKourelService.save(membreKourel);
        return ResponseEntity
            .created(new URI("/api/membre-kourels/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    @PostMapping("/membre-kourels/all")
    public ResponseEntity<Integer> createMembreKourelAll(@Valid @RequestBody List<MembreKourel> membreKourels) throws URISyntaxException {
        List<MembreKourel> result = membreKourelRepository.saveAll(membreKourels);
        return ResponseEntity
            .created(new URI("/api/membre-kourels/all" + result.size()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, Integer.toString(result.size())))
            .body(result.size());
    }

    /**
     * {@code PUT  /membre-kourels/:id} : Updates an existing membreKourel.
     *
     * @param id the id of the membreKourel to save.
     * @param membreKourel the membreKourel to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated membreKourel,
     * or with status {@code 400 (Bad Request)} if the membreKourel is not valid,
     * or with status {@code 500 (Internal Server Error)} if the membreKourel couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/membre-kourels/{id}")
    public ResponseEntity<MembreKourel> updateMembreKourel(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody MembreKourel membreKourel
    ) throws URISyntaxException {
        log.debug("REST request to update MembreKourel : {}, {}", id, membreKourel);
        if (membreKourel.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, membreKourel.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!membreKourelRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        MembreKourel result = membreKourelService.save(membreKourel);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, membreKourel.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /membre-kourels/:id} : Partial updates given fields of an existing membreKourel, field will ignore if it is null
     *
     * @param id the id of the membreKourel to save.
     * @param membreKourel the membreKourel to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated membreKourel,
     * or with status {@code 400 (Bad Request)} if the membreKourel is not valid,
     * or with status {@code 404 (Not Found)} if the membreKourel is not found,
     * or with status {@code 500 (Internal Server Error)} if the membreKourel couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/membre-kourels/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<MembreKourel> partialUpdateMembreKourel(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody MembreKourel membreKourel
    ) throws URISyntaxException {
        log.debug("REST request to partial update MembreKourel partially : {}, {}", id, membreKourel);
        if (membreKourel.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, membreKourel.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!membreKourelRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<MembreKourel> result = membreKourelService.partialUpdate(membreKourel);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, membreKourel.getId().toString())
        );
    }

    /**
     * {@code GET  /membre-kourels} : get all the membreKourels.
     *
     * @param pageable the pagination information.
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of membreKourels in body.
     */
    @GetMapping("/membre-kourels")
    public ResponseEntity<List<MembreKourel>> getAllMembreKourels(
        @org.springdoc.api.annotations.ParameterObject Pageable pageable,
        @RequestParam(required = false, defaultValue = "true") boolean eagerload
    ) {
        log.debug("REST request to get a page of MembreKourels");
        Page<MembreKourel> page;
        if (eagerload) {
            page = membreKourelService.findAllWithEagerRelationships(pageable);
        } else {
            page = membreKourelService.findAll(pageable);
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /membre-kourels/:id} : get the "id" membreKourel.
     *
     * @param id the id of the membreKourel to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the membreKourel, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/membre-kourels/{id}")
    public ResponseEntity<MembreKourel> getMembreKourel(@PathVariable Long id) {
        log.debug("REST request to get MembreKourel : {}", id);
        Optional<MembreKourel> membreKourel = membreKourelService.findOne(id);
        return ResponseUtil.wrapOrNotFound(membreKourel);
    }

    /**
     * {@code DELETE  /membre-kourels/:id} : delete the "id" membreKourel.
     *
     * @param id the id of the membreKourel to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/membre-kourels/{id}")
    public ResponseEntity<Void> deleteMembreKourel(@PathVariable Long id) {
        log.debug("REST request to delete MembreKourel : {}", id);
        membreKourelService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
