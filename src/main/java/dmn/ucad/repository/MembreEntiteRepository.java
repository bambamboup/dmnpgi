package dmn.ucad.repository;

import dmn.ucad.domain.MembreEntite;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the MembreEntite entity.
 */
@Repository
public interface MembreEntiteRepository extends JpaRepository<MembreEntite, Long> {
    default Optional<MembreEntite> findOneWithEagerRelationships(Long id) {
        return this.findOneWithToOneRelationships(id);
    }

    default List<MembreEntite> findAllWithEagerRelationships() {
        return this.findAllWithToOneRelationships();
    }

    default Page<MembreEntite> findAllWithEagerRelationships(Pageable pageable) {
        return this.findAllWithToOneRelationships(pageable);
    }

    @Query(
        value = "select distinct membreEntite from MembreEntite membreEntite left join fetch membreEntite.entite",
        countQuery = "select count(distinct membreEntite) from MembreEntite membreEntite"
    )
    Page<MembreEntite> findAllWithToOneRelationships(Pageable pageable);

    @Query("select distinct membreEntite from MembreEntite membreEntite left join fetch membreEntite.entite")
    List<MembreEntite> findAllWithToOneRelationships();

    @Query("select membreEntite from MembreEntite membreEntite left join fetch membreEntite.entite where membreEntite.id =:id")
    Optional<MembreEntite> findOneWithToOneRelationships(@Param("id") Long id);
}
