package dmn.ucad.repository;

import dmn.ucad.domain.Enseignement;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Enseignement entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EnseignementRepository extends JpaRepository<Enseignement, Long> {}
