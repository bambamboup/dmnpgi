package dmn.ucad.repository;

import dmn.ucad.domain.Membre;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import org.hibernate.annotations.QueryHints;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

/**
 * Utility repository to load bag relationships based on https://vladmihalcea.com/hibernate-multiplebagfetchexception/
 */
public class MembreRepositoryWithBagRelationshipsImpl implements MembreRepositoryWithBagRelationships {

    @Autowired
    private EntityManager entityManager;

    @Override
    public Optional<Membre> fetchBagRelationships(Optional<Membre> membre) {
        return membre.map(this::fetchXamXams);
    }

    @Override
    public Page<Membre> fetchBagRelationships(Page<Membre> membres) {
        return new PageImpl<>(fetchBagRelationships(membres.getContent()), membres.getPageable(), membres.getTotalElements());
    }

    @Override
    public List<Membre> fetchBagRelationships(List<Membre> membres) {
        return Optional.of(membres).map(this::fetchXamXams).get();
    }

    Membre fetchXamXams(Membre result) {
        return entityManager
            .createQuery("select membre from Membre membre left join fetch membre.xamXams where membre is :membre", Membre.class)
            .setParameter("membre", result)
            .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
            .getSingleResult();
    }

    List<Membre> fetchXamXams(List<Membre> membres) {
        return entityManager
            .createQuery("select distinct membre from Membre membre left join fetch membre.xamXams where membre in :membres", Membre.class)
            .setParameter("membres", membres)
            .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
            .getResultList();
    }
}
