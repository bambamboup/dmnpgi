package dmn.ucad.repository;

import dmn.ucad.domain.Membre;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;

public interface MembreRepositoryWithBagRelationships {
    Optional<Membre> fetchBagRelationships(Optional<Membre> membre);

    List<Membre> fetchBagRelationships(List<Membre> membres);

    Page<Membre> fetchBagRelationships(Page<Membre> membres);
}
