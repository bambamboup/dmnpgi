package dmn.ucad.repository;

import dmn.ucad.domain.CaisseMois;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the CaisseMois entity.
 */
@Repository
public interface CaisseMoisRepository extends JpaRepository<CaisseMois, Long> {
    @Query("select caisseMois from CaisseMois caisseMois where caisseMois.user.login = ?#{principal.username}")
    List<CaisseMois> findByUserIsCurrentUser();

    default Optional<CaisseMois> findOneWithEagerRelationships(Long id) {
        return this.findOneWithToOneRelationships(id);
    }

    default List<CaisseMois> findAllWithEagerRelationships() {
        return this.findAllWithToOneRelationships();
    }

    default Page<CaisseMois> findAllWithEagerRelationships(Pageable pageable) {
        return this.findAllWithToOneRelationships(pageable);
    }

    @Query(
        value = "select distinct caisseMois from CaisseMois caisseMois left join fetch caisseMois.user",
        countQuery = "select count(distinct caisseMois) from CaisseMois caisseMois"
    )
    Page<CaisseMois> findAllWithToOneRelationships(Pageable pageable);

    @Query("select distinct caisseMois from CaisseMois caisseMois left join fetch caisseMois.user")
    List<CaisseMois> findAllWithToOneRelationships();

    @Query("select caisseMois from CaisseMois caisseMois left join fetch caisseMois.user where caisseMois.id =:id")
    Optional<CaisseMois> findOneWithToOneRelationships(@Param("id") Long id);
}
