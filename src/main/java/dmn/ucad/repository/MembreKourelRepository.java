package dmn.ucad.repository;

import dmn.ucad.domain.MembreKourel;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the MembreKourel entity.
 */
@Repository
public interface MembreKourelRepository extends JpaRepository<MembreKourel, Long> {
    default Optional<MembreKourel> findOneWithEagerRelationships(Long id) {
        return this.findOneWithToOneRelationships(id);
    }

    default List<MembreKourel> findAllWithEagerRelationships() {
        return this.findAllWithToOneRelationships();
    }

    default Page<MembreKourel> findAllWithEagerRelationships(Pageable pageable) {
        return this.findAllWithToOneRelationships(pageable);
    }

    @Query(
        value = "select distinct membreKourel from MembreKourel membreKourel left join fetch membreKourel.kourel",
        countQuery = "select count(distinct membreKourel) from MembreKourel membreKourel"
    )
    Page<MembreKourel> findAllWithToOneRelationships(Pageable pageable);

    @Query("select distinct membreKourel from MembreKourel membreKourel left join fetch membreKourel.kourel")
    List<MembreKourel> findAllWithToOneRelationships();

    @Query("select membreKourel from MembreKourel membreKourel left join fetch membreKourel.kourel where membreKourel.id =:id")
    Optional<MembreKourel> findOneWithToOneRelationships(@Param("id") Long id);
}
