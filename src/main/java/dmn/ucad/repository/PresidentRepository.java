package dmn.ucad.repository;

import dmn.ucad.domain.President;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the President entity.
 */
@Repository
public interface PresidentRepository extends JpaRepository<President, Long> {
    default Optional<President> findOneWithEagerRelationships(Long id) {
        return this.findOneWithToOneRelationships(id);
    }

    default List<President> findAllWithEagerRelationships() {
        return this.findAllWithToOneRelationships();
    }

    default Page<President> findAllWithEagerRelationships(Pageable pageable) {
        return this.findAllWithToOneRelationships(pageable);
    }

    @Query(
        value = "select distinct president from President president left join fetch president.entite",
        countQuery = "select count(distinct president) from President president"
    )
    Page<President> findAllWithToOneRelationships(Pageable pageable);

    @Query("select distinct president from President president left join fetch president.entite")
    List<President> findAllWithToOneRelationships();

    @Query("select president from President president left join fetch president.entite where president.id =:id")
    Optional<President> findOneWithToOneRelationships(@Param("id") Long id);
}
