package dmn.ucad.repository;

import dmn.ucad.domain.Membre;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Membre entity.
 */
@Repository
public interface MembreRepository extends MembreRepositoryWithBagRelationships, JpaRepository<Membre, Long> {
    @Query("select membre from Membre membre where membre.user.login = ?#{principal.username}")
    List<Membre> findByUserIsCurrentUser();

    default Optional<Membre> findOneWithEagerRelationships(Long id) {
        return this.fetchBagRelationships(this.findOneWithToOneRelationships(id));
    }

    default List<Membre> findAllWithEagerRelationships() {
        return this.fetchBagRelationships(this.findAllWithToOneRelationships());
    }

    default Page<Membre> findAllWithEagerRelationships(Pageable pageable) {
        return this.fetchBagRelationships(this.findAllWithToOneRelationships(pageable));
    }

    @Query(
        value = "select distinct membre from Membre membre left join fetch membre.niveauXamXam left join fetch membre.coran left join fetch membre.user",
        countQuery = "select count(distinct membre) from Membre membre"
    )
    Page<Membre> findAllWithToOneRelationships(Pageable pageable);

    @Query(
        "select distinct membre from Membre membre left join fetch membre.niveauXamXam left join fetch membre.coran left join fetch membre.user"
    )
    List<Membre> findAllWithToOneRelationships();

    @Query(
        "select membre from Membre membre left join fetch membre.niveauXamXam left join fetch membre.coran left join fetch membre.user where membre.id =:id"
    )
    Optional<Membre> findOneWithToOneRelationships(@Param("id") Long id);
}
