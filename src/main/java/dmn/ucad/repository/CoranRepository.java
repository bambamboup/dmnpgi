package dmn.ucad.repository;

import dmn.ucad.domain.Coran;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Coran entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CoranRepository extends JpaRepository<Coran, Long> {}
