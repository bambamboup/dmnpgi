package dmn.ucad.repository;

import dmn.ucad.domain.NiveauXamXam;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the NiveauXamXam entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NiveauXamXamRepository extends JpaRepository<NiveauXamXam, Long> {}
