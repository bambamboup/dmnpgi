package dmn.ucad.repository;

import dmn.ucad.domain.Mensualite;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Mensualite entity.
 */
@Repository
public interface MensualiteRepository extends JpaRepository<Mensualite, Long> {
    default Optional<Mensualite> findOneWithEagerRelationships(Long id) {
        return this.findOneWithToOneRelationships(id);
    }

    default List<Mensualite> findAllWithEagerRelationships() {
        return this.findAllWithToOneRelationships();
    }

    default Page<Mensualite> findAllWithEagerRelationships(Pageable pageable) {
        return this.findAllWithToOneRelationships(pageable);
    }

    @Query(
        value = "select distinct mensualite from Mensualite mensualite left join fetch mensualite.categorie",
        countQuery = "select count(distinct mensualite) from Mensualite mensualite"
    )
    Page<Mensualite> findAllWithToOneRelationships(Pageable pageable);

    @Query("select distinct mensualite from Mensualite mensualite left join fetch mensualite.categorie")
    List<Mensualite> findAllWithToOneRelationships();

    @Query("select mensualite from Mensualite mensualite left join fetch mensualite.categorie where mensualite.id =:id")
    Optional<Mensualite> findOneWithToOneRelationships(@Param("id") Long id);
}
