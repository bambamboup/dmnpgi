package dmn.ucad.repository;

import dmn.ucad.domain.XamXam;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the XamXam entity.
 */
@Repository
public interface XamXamRepository extends JpaRepository<XamXam, Long> {
    default Optional<XamXam> findOneWithEagerRelationships(Long id) {
        return this.findOneWithToOneRelationships(id);
    }

    default List<XamXam> findAllWithEagerRelationships() {
        return this.findAllWithToOneRelationships();
    }

    default Page<XamXam> findAllWithEagerRelationships(Pageable pageable) {
        return this.findAllWithToOneRelationships(pageable);
    }

    @Query(
        value = "select distinct xamXam from XamXam xamXam left join fetch xamXam.niveauXamXam",
        countQuery = "select count(distinct xamXam) from XamXam xamXam"
    )
    Page<XamXam> findAllWithToOneRelationships(Pageable pageable);

    @Query("select distinct xamXam from XamXam xamXam left join fetch xamXam.niveauXamXam")
    List<XamXam> findAllWithToOneRelationships();

    @Query("select xamXam from XamXam xamXam left join fetch xamXam.niveauXamXam where xamXam.id =:id")
    Optional<XamXam> findOneWithToOneRelationships(@Param("id") Long id);
}
