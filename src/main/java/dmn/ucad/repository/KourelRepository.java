package dmn.ucad.repository;

import dmn.ucad.domain.Kourel;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Kourel entity.
 */
@SuppressWarnings("unused")
@Repository
public interface KourelRepository extends JpaRepository<Kourel, Long> {}
