package dmn.ucad.service;

import dmn.ucad.domain.Enseignement;
import dmn.ucad.repository.EnseignementRepository;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Enseignement}.
 */
@Service
@Transactional
public class EnseignementService {

    private final Logger log = LoggerFactory.getLogger(EnseignementService.class);

    private final EnseignementRepository enseignementRepository;

    public EnseignementService(EnseignementRepository enseignementRepository) {
        this.enseignementRepository = enseignementRepository;
    }

    /**
     * Save a enseignement.
     *
     * @param enseignement the entity to save.
     * @return the persisted entity.
     */
    public Enseignement save(Enseignement enseignement) {
        log.debug("Request to save Enseignement : {}", enseignement);
        return enseignementRepository.save(enseignement);
    }

    /**
     * Partially update a enseignement.
     *
     * @param enseignement the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<Enseignement> partialUpdate(Enseignement enseignement) {
        log.debug("Request to partially update Enseignement : {}", enseignement);

        return enseignementRepository
            .findById(enseignement.getId())
            .map(existingEnseignement -> {
                if (enseignement.getDateDebut() != null) {
                    existingEnseignement.setDateDebut(enseignement.getDateDebut());
                }
                if (enseignement.getDateFin() != null) {
                    existingEnseignement.setDateFin(enseignement.getDateFin());
                }
                if (enseignement.getEncours() != null) {
                    existingEnseignement.setEncours(enseignement.getEncours());
                }

                return existingEnseignement;
            })
            .map(enseignementRepository::save);
    }

    /**
     * Get all the enseignements.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Enseignement> findAll(Pageable pageable) {
        log.debug("Request to get all Enseignements");
        return enseignementRepository.findAll(pageable);
    }

    /**
     * Get one enseignement by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Enseignement> findOne(Long id) {
        log.debug("Request to get Enseignement : {}", id);
        return enseignementRepository.findById(id);
    }

    /**
     * Delete the enseignement by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Enseignement : {}", id);
        enseignementRepository.deleteById(id);
    }
}
