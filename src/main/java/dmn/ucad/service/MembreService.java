package dmn.ucad.service;

import dmn.ucad.domain.Membre;
import dmn.ucad.domain.MembreEntite;
import dmn.ucad.repository.MembreRepository;
import dmn.ucad.security.SecurityUtils;
import dmn.ucad.service.dto.MembreDTO;

import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Membre}.
 */
@Service
@Transactional
public class MembreService {

    private final Logger log = LoggerFactory.getLogger(MembreService.class);

    private final MembreRepository membreRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private MembreEntiteService membreEntiteService;

    public MembreService(MembreRepository membreRepository) {
        this.membreRepository = membreRepository;
    }

    /**
     * Save a membre.
     *
     * @param membre the entity to save.
     * @return the persisted entity.
     */
    public Membre save(MembreDTO membre) {
        log.debug("Request to save Membre : {}", membre);
        membre.getMembre().setUser(userService.getUserWithAuthoritiesByLogin(SecurityUtils.getCurrentUserLogin().get()).get());
        Membre result = membreRepository.save(membre.getMembre());
        if (membre.getCellule() != null) {
            membreEntiteService.save(new MembreEntite().actif(true).membre(result).entite(membre.getCellule()));
        }
        if (membre.getCommission() != null) {
            membreEntiteService.save(new MembreEntite().actif(true).membre(result).entite(membre.getCommission()));
        }
       return result;
    }

    /**
     * Partially update a membre.
     *
     * @param membre the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<Membre> partialUpdate(Membre membre) {
        log.debug("Request to partially update Membre : {}", membre);

        return membreRepository
            .findById(membre.getId())
            .map(existingMembre -> {
                if (membre.getNom() != null) {
                    existingMembre.setNom(membre.getNom());
                }
                if (membre.getPrenom() != null) {
                    existingMembre.setPrenom(membre.getPrenom());
                }
                if (membre.getTelephone() != null) {
                    existingMembre.setTelephone(membre.getTelephone());
                }
                if (membre.getSexe() != null) {
                    existingMembre.setSexe(membre.getSexe());
                }
                if (membre.getEmail() != null) {
                    existingMembre.setEmail(membre.getEmail());
                }
                if (membre.getCni() != null) {
                    existingMembre.setCni(membre.getCni());
                }
                if (membre.getAdressDakar() != null) {
                    existingMembre.setAdressDakar(membre.getAdressDakar());
                }
                if (membre.getSituationMatrimoniale() != null) {
                    existingMembre.setSituationMatrimoniale(membre.getSituationMatrimoniale());
                }
                if (membre.getAdresse() != null) {
                    existingMembre.setAdresse(membre.getAdresse());
                }
                if (membre.getAdresseVacance() != null) {
                    existingMembre.setAdresseVacance(membre.getAdresseVacance());
                }
                if (membre.getProfil() != null) {
                    existingMembre.setProfil(membre.getProfil());
                }
                if (membre.getProfilContentType() != null) {
                    existingMembre.setProfilContentType(membre.getProfilContentType());
                }
                if (membre.getDateNaissance() != null) {
                    existingMembre.setDateNaissance(membre.getDateNaissance());
                }
                if (membre.getBoursier() != null) {
                    existingMembre.setBoursier(membre.getBoursier());
                }
                if (membre.getDaaraOrigine() != null) {
                    existingMembre.setDaaraOrigine(membre.getDaaraOrigine());
                }
                if (membre.getEtatSante() != null) {
                    existingMembre.setEtatSante(membre.getEtatSante());
                }
                if (membre.getTiteur() != null) {
                    existingMembre.setTiteur(membre.getTiteur());
                }
                if (membre.getTelephoneTiteur() != null) {
                    existingMembre.setTelephoneTiteur(membre.getTelephoneTiteur());
                }

                return existingMembre;
            })
            .map(membreRepository::save);
    }

    /**
     * Get all the membres.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Membre> findAll(Pageable pageable) {
        log.debug("Request to get all Membres");
        return membreRepository.findAll(pageable);
    }

    /**
     * Get all the membres with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<Membre> findAllWithEagerRelationships(Pageable pageable) {
        return membreRepository.findAllWithEagerRelationships(pageable);
    }

    /**
     * Get one membre by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Membre> findOne(Long id) {
        log.debug("Request to get Membre : {}", id);
        return membreRepository.findOneWithEagerRelationships(id);
    }

    /**
     * Delete the membre by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Membre : {}", id);
        membreRepository.deleteById(id);
    }
}
