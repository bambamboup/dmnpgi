package dmn.ucad.service;

import dmn.ucad.domain.Entite;
import dmn.ucad.repository.EntiteRepository;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Entite}.
 */
@Service
@Transactional
public class EntiteService {

    private final Logger log = LoggerFactory.getLogger(EntiteService.class);

    private final EntiteRepository entiteRepository;

    public EntiteService(EntiteRepository entiteRepository) {
        this.entiteRepository = entiteRepository;
    }

    /**
     * Save a entite.
     *
     * @param entite the entity to save.
     * @return the persisted entity.
     */
    public Entite save(Entite entite) {
        log.debug("Request to save Entite : {}", entite);
        return entiteRepository.save(entite);
    }

    /**
     * Partially update a entite.
     *
     * @param entite the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<Entite> partialUpdate(Entite entite) {
        log.debug("Request to partially update Entite : {}", entite);

        return entiteRepository
            .findById(entite.getId())
            .map(existingEntite -> {
                if (entite.getNom() != null) {
                    existingEntite.setNom(entite.getNom());
                }
                if (entite.getDescription() != null) {
                    existingEntite.setDescription(entite.getDescription());
                }
                if (entite.getTypeEntite() != null) {
                    existingEntite.setTypeEntite(entite.getTypeEntite());
                }

                return existingEntite;
            })
            .map(entiteRepository::save);
    }

    /**
     * Get all the entites.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Entite> findAll(Pageable pageable) {
        log.debug("Request to get all Entites");
        return entiteRepository.findAll(pageable);
    }

    /**
     * Get one entite by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Entite> findOne(Long id) {
        log.debug("Request to get Entite : {}", id);
        return entiteRepository.findById(id);
    }

    /**
     * Delete the entite by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Entite : {}", id);
        entiteRepository.deleteById(id);
    }
}
