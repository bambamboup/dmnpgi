package dmn.ucad.service;

import dmn.ucad.domain.Kourel;
import dmn.ucad.repository.KourelRepository;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Kourel}.
 */
@Service
@Transactional
public class KourelService {

    private final Logger log = LoggerFactory.getLogger(KourelService.class);

    private final KourelRepository kourelRepository;

    public KourelService(KourelRepository kourelRepository) {
        this.kourelRepository = kourelRepository;
    }

    /**
     * Save a kourel.
     *
     * @param kourel the entity to save.
     * @return the persisted entity.
     */
    public Kourel save(Kourel kourel) {
        log.debug("Request to save Kourel : {}", kourel);
        return kourelRepository.save(kourel);
    }

    /**
     * Partially update a kourel.
     *
     * @param kourel the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<Kourel> partialUpdate(Kourel kourel) {
        log.debug("Request to partially update Kourel : {}", kourel);

        return kourelRepository
            .findById(kourel.getId())
            .map(existingKourel -> {
                if (kourel.getNom() != null) {
                    existingKourel.setNom(kourel.getNom());
                }

                return existingKourel;
            })
            .map(kourelRepository::save);
    }

    /**
     * Get all the kourels.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Kourel> findAll(Pageable pageable) {
        log.debug("Request to get all Kourels");
        return kourelRepository.findAll(pageable);
    }

    /**
     * Get one kourel by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Kourel> findOne(Long id) {
        log.debug("Request to get Kourel : {}", id);
        return kourelRepository.findById(id);
    }

    /**
     * Delete the kourel by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Kourel : {}", id);
        kourelRepository.deleteById(id);
    }
}
