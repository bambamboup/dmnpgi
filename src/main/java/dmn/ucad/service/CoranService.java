package dmn.ucad.service;

import dmn.ucad.domain.Coran;
import dmn.ucad.repository.CoranRepository;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Coran}.
 */
@Service
@Transactional
public class CoranService {

    private final Logger log = LoggerFactory.getLogger(CoranService.class);

    private final CoranRepository coranRepository;

    public CoranService(CoranRepository coranRepository) {
        this.coranRepository = coranRepository;
    }

    /**
     * Save a coran.
     *
     * @param coran the entity to save.
     * @return the persisted entity.
     */
    public Coran save(Coran coran) {
        log.debug("Request to save Coran : {}", coran);
        return coranRepository.save(coran);
    }

    /**
     * Partially update a coran.
     *
     * @param coran the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<Coran> partialUpdate(Coran coran) {
        log.debug("Request to partially update Coran : {}", coran);

        return coranRepository
            .findById(coran.getId())
            .map(existingCoran -> {
                if (coran.getLibelle() != null) {
                    existingCoran.setLibelle(coran.getLibelle());
                }

                return existingCoran;
            })
            .map(coranRepository::save);
    }

    /**
     * Get all the corans.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Coran> findAll(Pageable pageable) {
        log.debug("Request to get all Corans");
        return coranRepository.findAll(pageable);
    }

    /**
     * Get one coran by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Coran> findOne(Long id) {
        log.debug("Request to get Coran : {}", id);
        return coranRepository.findById(id);
    }

    /**
     * Delete the coran by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Coran : {}", id);
        coranRepository.deleteById(id);
    }
}
