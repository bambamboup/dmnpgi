package dmn.ucad.service;

import dmn.ucad.domain.NiveauXamXam;
import dmn.ucad.repository.NiveauXamXamRepository;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link NiveauXamXam}.
 */
@Service
@Transactional
public class NiveauXamXamService {

    private final Logger log = LoggerFactory.getLogger(NiveauXamXamService.class);

    private final NiveauXamXamRepository niveauXamXamRepository;

    public NiveauXamXamService(NiveauXamXamRepository niveauXamXamRepository) {
        this.niveauXamXamRepository = niveauXamXamRepository;
    }

    /**
     * Save a niveauXamXam.
     *
     * @param niveauXamXam the entity to save.
     * @return the persisted entity.
     */
    public NiveauXamXam save(NiveauXamXam niveauXamXam) {
        log.debug("Request to save NiveauXamXam : {}", niveauXamXam);
        return niveauXamXamRepository.save(niveauXamXam);
    }

    /**
     * Partially update a niveauXamXam.
     *
     * @param niveauXamXam the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<NiveauXamXam> partialUpdate(NiveauXamXam niveauXamXam) {
        log.debug("Request to partially update NiveauXamXam : {}", niveauXamXam);

        return niveauXamXamRepository
            .findById(niveauXamXam.getId())
            .map(existingNiveauXamXam -> {
                if (niveauXamXam.getLibelle() != null) {
                    existingNiveauXamXam.setLibelle(niveauXamXam.getLibelle());
                }
                if (niveauXamXam.getPoids() != null) {
                    existingNiveauXamXam.setPoids(niveauXamXam.getPoids());
                }

                return existingNiveauXamXam;
            })
            .map(niveauXamXamRepository::save);
    }

    /**
     * Get all the niveauXamXams.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<NiveauXamXam> findAll(Pageable pageable) {
        log.debug("Request to get all NiveauXamXams");
        return niveauXamXamRepository.findAll(pageable);
    }

    /**
     * Get one niveauXamXam by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<NiveauXamXam> findOne(Long id) {
        log.debug("Request to get NiveauXamXam : {}", id);
        return niveauXamXamRepository.findById(id);
    }

    /**
     * Delete the niveauXamXam by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete NiveauXamXam : {}", id);
        niveauXamXamRepository.deleteById(id);
    }
}
