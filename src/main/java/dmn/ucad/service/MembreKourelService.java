package dmn.ucad.service;

import dmn.ucad.domain.MembreKourel;
import dmn.ucad.repository.MembreKourelRepository;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link MembreKourel}.
 */
@Service
@Transactional
public class MembreKourelService {

    private final Logger log = LoggerFactory.getLogger(MembreKourelService.class);

    private final MembreKourelRepository membreKourelRepository;

    public MembreKourelService(MembreKourelRepository membreKourelRepository) {
        this.membreKourelRepository = membreKourelRepository;
    }

    /**
     * Save a membreKourel.
     *
     * @param membreKourel the entity to save.
     * @return the persisted entity.
     */
    public MembreKourel save(MembreKourel membreKourel) {
        log.debug("Request to save MembreKourel : {}", membreKourel);
        return membreKourelRepository.save(membreKourel);
    }

    /**
     * Partially update a membreKourel.
     *
     * @param membreKourel the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<MembreKourel> partialUpdate(MembreKourel membreKourel) {
        log.debug("Request to partially update MembreKourel : {}", membreKourel);

        return membreKourelRepository
            .findById(membreKourel.getId())
            .map(existingMembreKourel -> {
                if (membreKourel.getDateDebut() != null) {
                    existingMembreKourel.setDateDebut(membreKourel.getDateDebut());
                }
                if (membreKourel.getDateFin() != null) {
                    existingMembreKourel.setDateFin(membreKourel.getDateFin());
                }
                if (membreKourel.getActif() != null) {
                    existingMembreKourel.setActif(membreKourel.getActif());
                }

                return existingMembreKourel;
            })
            .map(membreKourelRepository::save);
    }

    /**
     * Get all the membreKourels.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<MembreKourel> findAll(Pageable pageable) {
        log.debug("Request to get all MembreKourels");
        return membreKourelRepository.findAll(pageable);
    }

    /**
     * Get all the membreKourels with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<MembreKourel> findAllWithEagerRelationships(Pageable pageable) {
        return membreKourelRepository.findAllWithEagerRelationships(pageable);
    }

    /**
     * Get one membreKourel by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<MembreKourel> findOne(Long id) {
        log.debug("Request to get MembreKourel : {}", id);
        return membreKourelRepository.findOneWithEagerRelationships(id);
    }

    /**
     * Delete the membreKourel by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete MembreKourel : {}", id);
        membreKourelRepository.deleteById(id);
    }
}
