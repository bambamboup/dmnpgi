package dmn.ucad.service;

import dmn.ucad.domain.XamXam;
import dmn.ucad.repository.XamXamRepository;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link XamXam}.
 */
@Service
@Transactional
public class XamXamService {

    private final Logger log = LoggerFactory.getLogger(XamXamService.class);

    private final XamXamRepository xamXamRepository;

    public XamXamService(XamXamRepository xamXamRepository) {
        this.xamXamRepository = xamXamRepository;
    }

    /**
     * Save a xamXam.
     *
     * @param xamXam the entity to save.
     * @return the persisted entity.
     */
    public XamXam save(XamXam xamXam) {
        log.debug("Request to save XamXam : {}", xamXam);
        return xamXamRepository.save(xamXam);
    }

    /**
     * Partially update a xamXam.
     *
     * @param xamXam the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<XamXam> partialUpdate(XamXam xamXam) {
        log.debug("Request to partially update XamXam : {}", xamXam);

        return xamXamRepository
            .findById(xamXam.getId())
            .map(existingXamXam -> {
                if (xamXam.getLibelle() != null) {
                    existingXamXam.setLibelle(xamXam.getLibelle());
                }

                return existingXamXam;
            })
            .map(xamXamRepository::save);
    }

    /**
     * Get all the xamXams.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<XamXam> findAll(Pageable pageable) {
        log.debug("Request to get all XamXams");
        return xamXamRepository.findAll(pageable);
    }

    /**
     * Get all the xamXams with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<XamXam> findAllWithEagerRelationships(Pageable pageable) {
        return xamXamRepository.findAllWithEagerRelationships(pageable);
    }

    /**
     * Get one xamXam by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<XamXam> findOne(Long id) {
        log.debug("Request to get XamXam : {}", id);
        return xamXamRepository.findOneWithEagerRelationships(id);
    }

    /**
     * Delete the xamXam by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete XamXam : {}", id);
        xamXamRepository.deleteById(id);
    }
}
