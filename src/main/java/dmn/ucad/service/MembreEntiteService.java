package dmn.ucad.service;

import dmn.ucad.domain.MembreEntite;
import dmn.ucad.repository.MembreEntiteRepository;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link MembreEntite}.
 */
@Service
@Transactional
public class MembreEntiteService {

    private final Logger log = LoggerFactory.getLogger(MembreEntiteService.class);

    private final MembreEntiteRepository membreEntiteRepository;

    public MembreEntiteService(MembreEntiteRepository membreEntiteRepository) {
        this.membreEntiteRepository = membreEntiteRepository;
    }

    /**
     * Save a membreEntite.
     *
     * @param membreEntite the entity to save.
     * @return the persisted entity.
     */
    public MembreEntite save(MembreEntite membreEntite) {
        log.debug("Request to save MembreEntite : {}", membreEntite);
        return membreEntiteRepository.save(membreEntite);
    }

    /**
     * Partially update a membreEntite.
     *
     * @param membreEntite the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<MembreEntite> partialUpdate(MembreEntite membreEntite) {
        log.debug("Request to partially update MembreEntite : {}", membreEntite);

        return membreEntiteRepository
            .findById(membreEntite.getId())
            .map(existingMembreEntite -> {
                if (membreEntite.getActif() != null) {
                    existingMembreEntite.setActif(membreEntite.getActif());
                }

                return existingMembreEntite;
            })
            .map(membreEntiteRepository::save);
    }

    /**
     * Get all the membreEntites.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<MembreEntite> findAll(Pageable pageable) {
        log.debug("Request to get all MembreEntites");
        return membreEntiteRepository.findAll(pageable);
    }

    /**
     * Get all the membreEntites with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<MembreEntite> findAllWithEagerRelationships(Pageable pageable) {
        return membreEntiteRepository.findAllWithEagerRelationships(pageable);
    }

    /**
     * Get one membreEntite by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<MembreEntite> findOne(Long id) {
        log.debug("Request to get MembreEntite : {}", id);
        return membreEntiteRepository.findOneWithEagerRelationships(id);
    }

    /**
     * Delete the membreEntite by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete MembreEntite : {}", id);
        membreEntiteRepository.deleteById(id);
    }
}
