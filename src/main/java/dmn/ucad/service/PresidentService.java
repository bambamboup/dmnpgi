package dmn.ucad.service;

import dmn.ucad.domain.President;
import dmn.ucad.repository.PresidentRepository;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link President}.
 */
@Service
@Transactional
public class PresidentService {

    private final Logger log = LoggerFactory.getLogger(PresidentService.class);

    private final PresidentRepository presidentRepository;

    public PresidentService(PresidentRepository presidentRepository) {
        this.presidentRepository = presidentRepository;
    }

    /**
     * Save a president.
     *
     * @param president the entity to save.
     * @return the persisted entity.
     */
    public President save(President president) {
        log.debug("Request to save President : {}", president);
        return presidentRepository.save(president);
    }

    /**
     * Partially update a president.
     *
     * @param president the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<President> partialUpdate(President president) {
        log.debug("Request to partially update President : {}", president);

        return presidentRepository
            .findById(president.getId())
            .map(existingPresident -> {
                if (president.getDateDebut() != null) {
                    existingPresident.setDateDebut(president.getDateDebut());
                }
                if (president.getDateFin() != null) {
                    existingPresident.setDateFin(president.getDateFin());
                }
                if (president.getActif() != null) {
                    existingPresident.setActif(president.getActif());
                }

                return existingPresident;
            })
            .map(presidentRepository::save);
    }

    /**
     * Get all the presidents.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<President> findAll(Pageable pageable) {
        log.debug("Request to get all Presidents");
        return presidentRepository.findAll(pageable);
    }

    /**
     * Get all the presidents with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<President> findAllWithEagerRelationships(Pageable pageable) {
        return presidentRepository.findAllWithEagerRelationships(pageable);
    }

    /**
     * Get one president by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<President> findOne(Long id) {
        log.debug("Request to get President : {}", id);
        return presidentRepository.findOneWithEagerRelationships(id);
    }

    /**
     * Delete the president by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete President : {}", id);
        presidentRepository.deleteById(id);
    }
}
