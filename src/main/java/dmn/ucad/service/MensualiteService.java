package dmn.ucad.service;

import dmn.ucad.domain.Mensualite;
import dmn.ucad.repository.MensualiteRepository;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Mensualite}.
 */
@Service
@Transactional
public class MensualiteService {

    private final Logger log = LoggerFactory.getLogger(MensualiteService.class);

    private final MensualiteRepository mensualiteRepository;

    public MensualiteService(MensualiteRepository mensualiteRepository) {
        this.mensualiteRepository = mensualiteRepository;
    }

    /**
     * Save a mensualite.
     *
     * @param mensualite the entity to save.
     * @return the persisted entity.
     */
    public Mensualite save(Mensualite mensualite) {
        log.debug("Request to save Mensualite : {}", mensualite);
        return mensualiteRepository.save(mensualite);
    }

    /**
     * Partially update a mensualite.
     *
     * @param mensualite the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<Mensualite> partialUpdate(Mensualite mensualite) {
        log.debug("Request to partially update Mensualite : {}", mensualite);

        return mensualiteRepository
            .findById(mensualite.getId())
            .map(existingMensualite -> {
                if (mensualite.getDateDebut() != null) {
                    existingMensualite.setDateDebut(mensualite.getDateDebut());
                }
                if (mensualite.getDateFin() != null) {
                    existingMensualite.setDateFin(mensualite.getDateFin());
                }
                if (mensualite.getActif() != null) {
                    existingMensualite.setActif(mensualite.getActif());
                }
                if (mensualite.getMontant() != null) {
                    existingMensualite.setMontant(mensualite.getMontant());
                }
                if (mensualite.getTypeMensualite() != null) {
                    existingMensualite.setTypeMensualite(mensualite.getTypeMensualite());
                }

                return existingMensualite;
            })
            .map(mensualiteRepository::save);
    }

    /**
     * Get all the mensualites.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Mensualite> findAll(Pageable pageable) {
        log.debug("Request to get all Mensualites");
        return mensualiteRepository.findAll(pageable);
    }

    /**
     * Get all the mensualites with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<Mensualite> findAllWithEagerRelationships(Pageable pageable) {
        return mensualiteRepository.findAllWithEagerRelationships(pageable);
    }

    /**
     * Get one mensualite by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Mensualite> findOne(Long id) {
        log.debug("Request to get Mensualite : {}", id);
        return mensualiteRepository.findOneWithEagerRelationships(id);
    }

    /**
     * Delete the mensualite by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Mensualite : {}", id);
        mensualiteRepository.deleteById(id);
    }
}
