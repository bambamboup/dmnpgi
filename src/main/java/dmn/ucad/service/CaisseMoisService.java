package dmn.ucad.service;

import dmn.ucad.domain.CaisseMois;
import dmn.ucad.repository.CaisseMoisRepository;
import dmn.ucad.repository.UserRepository;
import dmn.ucad.security.SecurityUtils;

import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link CaisseMois}.
 */
@Service
@Transactional
public class CaisseMoisService {

    private final Logger log = LoggerFactory.getLogger(CaisseMoisService.class);

    private final CaisseMoisRepository caisseMoisRepository;

    @Autowired
    private UserRepository userRepository;

    public CaisseMoisService(CaisseMoisRepository caisseMoisRepository) {
        this.caisseMoisRepository = caisseMoisRepository;
    }

    /**
     * Save a caisseMois.
     *
     * @param caisseMois the entity to save.
     * @return the persisted entity.
     */
    public CaisseMois save(CaisseMois caisseMois) {
        log.debug("Request to save CaisseMois : {}", caisseMois);
        caisseMois.setUser(userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin().get()).get());
        return caisseMoisRepository.save(caisseMois);
    }

    /**
     * Partially update a caisseMois.
     *
     * @param caisseMois the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<CaisseMois> partialUpdate(CaisseMois caisseMois) {
        log.debug("Request to partially update CaisseMois : {}", caisseMois);

        return caisseMoisRepository
            .findById(caisseMois.getId())
            .map(existingCaisseMois -> {
                if (caisseMois.getDate() != null) {
                    existingCaisseMois.setDate(caisseMois.getDate());
                }
                if (caisseMois.getMontant() != null) {
                    existingCaisseMois.setMontant(caisseMois.getMontant());
                }
                if (caisseMois.getValide() != null) {
                    existingCaisseMois.setValide(caisseMois.getValide());
                }

                return existingCaisseMois;
            })
            .map(caisseMoisRepository::save);
    }

    /**
     * Get all the caisseMois.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<CaisseMois> findAll(Pageable pageable) {
        log.debug("Request to get all CaisseMois");
        return caisseMoisRepository.findAll(pageable);
    }

    /**
     * Get all the caisseMois with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<CaisseMois> findAllWithEagerRelationships(Pageable pageable) {
        return caisseMoisRepository.findAllWithEagerRelationships(pageable);
    }

    /**
     * Get one caisseMois by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<CaisseMois> findOne(Long id) {
        log.debug("Request to get CaisseMois : {}", id);
        return caisseMoisRepository.findOneWithEagerRelationships(id);
    }

    /**
     * Delete the caisseMois by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete CaisseMois : {}", id);
        caisseMoisRepository.deleteById(id);
    }
}
