package dmn.ucad.service.dto;

import javax.validation.constraints.NotNull;
import dmn.ucad.domain.Entite;
import dmn.ucad.domain.Membre;

public class MembreDTO {

    @NotNull
    private Membre membre;
    private Entite commission;
    private Entite cellule;
    public Membre getMembre() {
        return membre;
    }
    public void setMembre(Membre membre) {
        this.membre = membre;
    }
    public Entite getCommission() {
        return commission;
    }
    public void setCommission(Entite commission) {
        this.commission = commission;
    }
    public Entite getCellule() {
        return cellule;
    }
    public void setCellule(Entite cellule) {
        this.cellule = cellule;
    }
}
