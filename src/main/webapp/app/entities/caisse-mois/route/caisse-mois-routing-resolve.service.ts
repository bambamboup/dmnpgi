import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ICaisseMois, CaisseMois } from '../caisse-mois.model';
import { CaisseMoisService } from '../service/caisse-mois.service';

@Injectable({ providedIn: 'root' })
export class CaisseMoisRoutingResolveService implements Resolve<ICaisseMois> {
  constructor(protected service: CaisseMoisService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICaisseMois> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((caisseMois: HttpResponse<CaisseMois>) => {
          if (caisseMois.body) {
            return of(caisseMois.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new CaisseMois());
  }
}
