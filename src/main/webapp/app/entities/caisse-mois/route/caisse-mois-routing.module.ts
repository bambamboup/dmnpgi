import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { CaisseMoisComponent } from '../list/caisse-mois.component';
import { CaisseMoisDetailComponent } from '../detail/caisse-mois-detail.component';
import { CaisseMoisUpdateComponent } from '../update/caisse-mois-update.component';
import { CaisseMoisRoutingResolveService } from './caisse-mois-routing-resolve.service';

const caisseMoisRoute: Routes = [
  {
    path: '',
    component: CaisseMoisComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CaisseMoisDetailComponent,
    resolve: {
      caisseMois: CaisseMoisRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CaisseMoisUpdateComponent,
    resolve: {
      caisseMois: CaisseMoisRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CaisseMoisUpdateComponent,
    resolve: {
      caisseMois: CaisseMoisRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(caisseMoisRoute)],
  exports: [RouterModule],
})
export class CaisseMoisRoutingModule {}
