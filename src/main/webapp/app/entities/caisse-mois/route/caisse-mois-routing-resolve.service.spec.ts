import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, ActivatedRoute, Router, convertToParamMap } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { ICaisseMois, CaisseMois } from '../caisse-mois.model';
import { CaisseMoisService } from '../service/caisse-mois.service';

import { CaisseMoisRoutingResolveService } from './caisse-mois-routing-resolve.service';

describe('CaisseMois routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: CaisseMoisRoutingResolveService;
  let service: CaisseMoisService;
  let resultCaisseMois: ICaisseMois | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: convertToParamMap({}),
            },
          },
        },
      ],
    });
    mockRouter = TestBed.inject(Router);
    jest.spyOn(mockRouter, 'navigate').mockImplementation(() => Promise.resolve(true));
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRoute).snapshot;
    routingResolveService = TestBed.inject(CaisseMoisRoutingResolveService);
    service = TestBed.inject(CaisseMoisService);
    resultCaisseMois = undefined;
  });

  describe('resolve', () => {
    it('should return ICaisseMois returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultCaisseMois = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultCaisseMois).toEqual({ id: 123 });
    });

    it('should return new ICaisseMois if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultCaisseMois = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultCaisseMois).toEqual(new CaisseMois());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as CaisseMois })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultCaisseMois = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultCaisseMois).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
