import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { CaisseMoisService } from '../service/caisse-mois.service';
import { ICaisseMois, CaisseMois } from '../caisse-mois.model';
import { IMensualite } from 'app/entities/mensualite/mensualite.model';
import { MensualiteService } from 'app/entities/mensualite/service/mensualite.service';
import { IMembre } from 'app/entities/membre/membre.model';
import { MembreService } from 'app/entities/membre/service/membre.service';

import { IUser } from 'app/entities/user/user.model';
import { UserService } from 'app/entities/user/user.service';

import { CaisseMoisUpdateComponent } from './caisse-mois-update.component';

describe('CaisseMois Management Update Component', () => {
  let comp: CaisseMoisUpdateComponent;
  let fixture: ComponentFixture<CaisseMoisUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let caisseMoisService: CaisseMoisService;
  let mensualiteService: MensualiteService;
  let membreService: MembreService;
  let userService: UserService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [CaisseMoisUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(CaisseMoisUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(CaisseMoisUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    caisseMoisService = TestBed.inject(CaisseMoisService);
    mensualiteService = TestBed.inject(MensualiteService);
    membreService = TestBed.inject(MembreService);
    userService = TestBed.inject(UserService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Mensualite query and add missing value', () => {
      const caisseMois: ICaisseMois = { id: 456 };
      const mensualite: IMensualite = { id: 96842 };
      caisseMois.mensualite = mensualite;

      const mensualiteCollection: IMensualite[] = [{ id: 62919 }];
      jest.spyOn(mensualiteService, 'query').mockReturnValue(of(new HttpResponse({ body: mensualiteCollection })));
      const additionalMensualites = [mensualite];
      const expectedCollection: IMensualite[] = [...additionalMensualites, ...mensualiteCollection];
      jest.spyOn(mensualiteService, 'addMensualiteToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ caisseMois });
      comp.ngOnInit();

      expect(mensualiteService.query).toHaveBeenCalled();
      expect(mensualiteService.addMensualiteToCollectionIfMissing).toHaveBeenCalledWith(mensualiteCollection, ...additionalMensualites);
      expect(comp.mensualitesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Membre query and add missing value', () => {
      const caisseMois: ICaisseMois = { id: 456 };
      const membre: IMembre = { id: 37593 };
      caisseMois.membre = membre;

      const membreCollection: IMembre[] = [{ id: 47279 }];
      jest.spyOn(membreService, 'query').mockReturnValue(of(new HttpResponse({ body: membreCollection })));
      const additionalMembres = [membre];
      const expectedCollection: IMembre[] = [...additionalMembres, ...membreCollection];
      jest.spyOn(membreService, 'addMembreToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ caisseMois });
      comp.ngOnInit();

      expect(membreService.query).toHaveBeenCalled();
      expect(membreService.addMembreToCollectionIfMissing).toHaveBeenCalledWith(membreCollection, ...additionalMembres);
      expect(comp.membresSharedCollection).toEqual(expectedCollection);
    });

    it('Should call User query and add missing value', () => {
      const caisseMois: ICaisseMois = { id: 456 };
      const user: IUser = { id: 92773 };
      caisseMois.user = user;

      const userCollection: IUser[] = [{ id: 4984 }];
      jest.spyOn(userService, 'query').mockReturnValue(of(new HttpResponse({ body: userCollection })));
      const additionalUsers = [user];
      const expectedCollection: IUser[] = [...additionalUsers, ...userCollection];
      jest.spyOn(userService, 'addUserToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ caisseMois });
      comp.ngOnInit();

      expect(userService.query).toHaveBeenCalled();
      expect(userService.addUserToCollectionIfMissing).toHaveBeenCalledWith(userCollection, ...additionalUsers);
      expect(comp.usersSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const caisseMois: ICaisseMois = { id: 456 };
      const mensualite: IMensualite = { id: 45610 };
      caisseMois.mensualite = mensualite;
      const membre: IMembre = { id: 58979 };
      caisseMois.membre = membre;
      const user: IUser = { id: 48991 };
      caisseMois.user = user;

      activatedRoute.data = of({ caisseMois });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(caisseMois));
      expect(comp.mensualitesSharedCollection).toContain(mensualite);
      expect(comp.membresSharedCollection).toContain(membre);
      expect(comp.usersSharedCollection).toContain(user);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<CaisseMois>>();
      const caisseMois = { id: 123 };
      jest.spyOn(caisseMoisService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ caisseMois });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: caisseMois }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(caisseMoisService.update).toHaveBeenCalledWith(caisseMois);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<CaisseMois>>();
      const caisseMois = new CaisseMois();
      jest.spyOn(caisseMoisService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ caisseMois });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: caisseMois }));
      saveSubject.complete();

      // THEN
      expect(caisseMoisService.create).toHaveBeenCalledWith(caisseMois);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<CaisseMois>>();
      const caisseMois = { id: 123 };
      jest.spyOn(caisseMoisService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ caisseMois });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(caisseMoisService.update).toHaveBeenCalledWith(caisseMois);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackMensualiteById', () => {
      it('Should return tracked Mensualite primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackMensualiteById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackMembreById', () => {
      it('Should return tracked Membre primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackMembreById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackUserById', () => {
      it('Should return tracked User primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackUserById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
