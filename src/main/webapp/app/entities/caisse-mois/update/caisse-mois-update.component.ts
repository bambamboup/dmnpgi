import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { ICaisseMois, CaisseMois } from '../caisse-mois.model';
import { CaisseMoisService } from '../service/caisse-mois.service';
import { IMensualite } from 'app/entities/mensualite/mensualite.model';
import { MensualiteService } from 'app/entities/mensualite/service/mensualite.service';
import { IMembre } from 'app/entities/membre/membre.model';
import { MembreService } from 'app/entities/membre/service/membre.service';
import { IUser } from 'app/entities/user/user.model';
import { UserService } from 'app/entities/user/user.service';

@Component({
  selector: 'jhi-caisse-mois-update',
  templateUrl: './caisse-mois-update.component.html',
})
export class CaisseMoisUpdateComponent implements OnInit {
  isSaving = false;

  mensualitesSharedCollection: IMensualite[] = [];
  membresSharedCollection: IMembre[] = [];
  usersSharedCollection: IUser[] = [];

  editForm = this.fb.group({
    id: [],
    date: [],
    montant: [null, [Validators.required]],
    valide: [],
    mensualite: [],
    membre: [null, Validators.required],
    user: [],
  });

  constructor(
    protected caisseMoisService: CaisseMoisService,
    protected mensualiteService: MensualiteService,
    protected membreService: MembreService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ caisseMois }) => {
      this.updateForm(caisseMois);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const caisseMois = this.createFromForm();
    if (caisseMois.id !== undefined) {
      this.subscribeToSaveResponse(this.caisseMoisService.update(caisseMois));
    } else {
      this.subscribeToSaveResponse(this.caisseMoisService.create(caisseMois));
    }
  }

  trackMensualiteById(index: number, item: IMensualite): number {
    return item.id!;
  }

  trackMembreById(index: number, item: IMembre): number {
    return item.id!;
  }

  trackUserById(index: number, item: IUser): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICaisseMois>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(caisseMois: ICaisseMois): void {
    this.editForm.patchValue({
      id: caisseMois.id,
      date: caisseMois.date,
      montant: caisseMois.montant,
      valide: caisseMois.valide,
      mensualite: caisseMois.mensualite,
      membre: caisseMois.membre,
      user: caisseMois.user,
    });

    this.mensualitesSharedCollection = this.mensualiteService.addMensualiteToCollectionIfMissing(
      this.mensualitesSharedCollection,
      caisseMois.mensualite
    );
    this.membresSharedCollection = this.membreService.addMembreToCollectionIfMissing(this.membresSharedCollection, caisseMois.membre);
    this.usersSharedCollection = this.userService.addUserToCollectionIfMissing(this.usersSharedCollection, caisseMois.user);
  }

  protected loadRelationshipsOptions(): void {
    this.mensualiteService
      .query()
      .pipe(map((res: HttpResponse<IMensualite[]>) => res.body ?? []))
      .pipe(
        map((mensualites: IMensualite[]) =>
          this.mensualiteService.addMensualiteToCollectionIfMissing(mensualites, this.editForm.get('mensualite')!.value)
        )
      )
      .subscribe((mensualites: IMensualite[]) => (this.mensualitesSharedCollection = mensualites));

    this.membreService
      .query()
      .pipe(map((res: HttpResponse<IMembre[]>) => res.body ?? []))
      .pipe(map((membres: IMembre[]) => this.membreService.addMembreToCollectionIfMissing(membres, this.editForm.get('membre')!.value)))
      .subscribe((membres: IMembre[]) => (this.membresSharedCollection = membres));

    this.userService
      .query()
      .pipe(map((res: HttpResponse<IUser[]>) => res.body ?? []))
      .pipe(map((users: IUser[]) => this.userService.addUserToCollectionIfMissing(users, this.editForm.get('user')!.value)))
      .subscribe((users: IUser[]) => (this.usersSharedCollection = users));
  }

  protected createFromForm(): ICaisseMois {
    return {
      ...new CaisseMois(),
      id: this.editForm.get(['id'])!.value,
      date: this.editForm.get(['date'])!.value,
      montant: this.editForm.get(['montant'])!.value,
      valide: this.editForm.get(['valide'])!.value,
      mensualite: this.editForm.get(['mensualite'])!.value,
      membre: this.editForm.get(['membre'])!.value,
      user: this.editForm.get(['user'])!.value,
    };
  }
}
