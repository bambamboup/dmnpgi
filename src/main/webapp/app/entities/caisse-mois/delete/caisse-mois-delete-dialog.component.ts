import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ICaisseMois } from '../caisse-mois.model';
import { CaisseMoisService } from '../service/caisse-mois.service';

@Component({
  templateUrl: './caisse-mois-delete-dialog.component.html',
})
export class CaisseMoisDeleteDialogComponent {
  caisseMois?: ICaisseMois;

  constructor(protected caisseMoisService: CaisseMoisService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.caisseMoisService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
