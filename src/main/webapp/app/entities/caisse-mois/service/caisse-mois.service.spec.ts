import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import dayjs from 'dayjs/esm';

import { DATE_FORMAT } from 'app/config/input.constants';
import { ICaisseMois, CaisseMois } from '../caisse-mois.model';

import { CaisseMoisService } from './caisse-mois.service';

describe('CaisseMois Service', () => {
  let service: CaisseMoisService;
  let httpMock: HttpTestingController;
  let elemDefault: ICaisseMois;
  let expectedResult: ICaisseMois | ICaisseMois[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(CaisseMoisService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      date: currentDate,
      montant: 0,
      valide: false,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          date: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a CaisseMois', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          date: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          date: currentDate,
        },
        returnedFromService
      );

      service.create(new CaisseMois()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a CaisseMois', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          date: currentDate.format(DATE_FORMAT),
          montant: 1,
          valide: true,
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          date: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a CaisseMois', () => {
      const patchObject = Object.assign({}, new CaisseMois());

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          date: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of CaisseMois', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          date: currentDate.format(DATE_FORMAT),
          montant: 1,
          valide: true,
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          date: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a CaisseMois', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addCaisseMoisToCollectionIfMissing', () => {
      it('should add a CaisseMois to an empty array', () => {
        const caisseMois: ICaisseMois = { id: 123 };
        expectedResult = service.addCaisseMoisToCollectionIfMissing([], caisseMois);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(caisseMois);
      });

      it('should not add a CaisseMois to an array that contains it', () => {
        const caisseMois: ICaisseMois = { id: 123 };
        const caisseMoisCollection: ICaisseMois[] = [
          {
            ...caisseMois,
          },
          { id: 456 },
        ];
        expectedResult = service.addCaisseMoisToCollectionIfMissing(caisseMoisCollection, caisseMois);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a CaisseMois to an array that doesn't contain it", () => {
        const caisseMois: ICaisseMois = { id: 123 };
        const caisseMoisCollection: ICaisseMois[] = [{ id: 456 }];
        expectedResult = service.addCaisseMoisToCollectionIfMissing(caisseMoisCollection, caisseMois);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(caisseMois);
      });

      it('should add only unique CaisseMois to an array', () => {
        const caisseMoisArray: ICaisseMois[] = [{ id: 123 }, { id: 456 }, { id: 81448 }];
        const caisseMoisCollection: ICaisseMois[] = [{ id: 123 }];
        expectedResult = service.addCaisseMoisToCollectionIfMissing(caisseMoisCollection, ...caisseMoisArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const caisseMois: ICaisseMois = { id: 123 };
        const caisseMois2: ICaisseMois = { id: 456 };
        expectedResult = service.addCaisseMoisToCollectionIfMissing([], caisseMois, caisseMois2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(caisseMois);
        expect(expectedResult).toContain(caisseMois2);
      });

      it('should accept null and undefined values', () => {
        const caisseMois: ICaisseMois = { id: 123 };
        expectedResult = service.addCaisseMoisToCollectionIfMissing([], null, caisseMois, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(caisseMois);
      });

      it('should return initial array if no CaisseMois is added', () => {
        const caisseMoisCollection: ICaisseMois[] = [{ id: 123 }];
        expectedResult = service.addCaisseMoisToCollectionIfMissing(caisseMoisCollection, undefined, null);
        expect(expectedResult).toEqual(caisseMoisCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
