import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ICaisseMois, getCaisseMoisIdentifier } from '../caisse-mois.model';

export type EntityResponseType = HttpResponse<ICaisseMois>;
export type EntityArrayResponseType = HttpResponse<ICaisseMois[]>;

@Injectable({ providedIn: 'root' })
export class CaisseMoisService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/caisse-mois');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(caisseMois: ICaisseMois): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(caisseMois);
    return this.http
      .post<ICaisseMois>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(caisseMois: ICaisseMois): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(caisseMois);
    return this.http
      .put<ICaisseMois>(`${this.resourceUrl}/${getCaisseMoisIdentifier(caisseMois) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(caisseMois: ICaisseMois): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(caisseMois);
    return this.http
      .patch<ICaisseMois>(`${this.resourceUrl}/${getCaisseMoisIdentifier(caisseMois) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ICaisseMois>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ICaisseMois[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addCaisseMoisToCollectionIfMissing(
    caisseMoisCollection: ICaisseMois[],
    ...caisseMoisToCheck: (ICaisseMois | null | undefined)[]
  ): ICaisseMois[] {
    const caisseMois: ICaisseMois[] = caisseMoisToCheck.filter(isPresent);
    if (caisseMois.length > 0) {
      const caisseMoisCollectionIdentifiers = caisseMoisCollection.map(caisseMoisItem => getCaisseMoisIdentifier(caisseMoisItem)!);
      const caisseMoisToAdd = caisseMois.filter(caisseMoisItem => {
        const caisseMoisIdentifier = getCaisseMoisIdentifier(caisseMoisItem);
        if (caisseMoisIdentifier == null || caisseMoisCollectionIdentifiers.includes(caisseMoisIdentifier)) {
          return false;
        }
        caisseMoisCollectionIdentifiers.push(caisseMoisIdentifier);
        return true;
      });
      return [...caisseMoisToAdd, ...caisseMoisCollection];
    }
    return caisseMoisCollection;
  }

  protected convertDateFromClient(caisseMois: ICaisseMois): ICaisseMois {
    return Object.assign({}, caisseMois, {
      date: caisseMois.date?.isValid() ? caisseMois.date.format(DATE_FORMAT) : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.date = res.body.date ? dayjs(res.body.date) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((caisseMois: ICaisseMois) => {
        caisseMois.date = caisseMois.date ? dayjs(caisseMois.date) : undefined;
      });
    }
    return res;
  }
}
