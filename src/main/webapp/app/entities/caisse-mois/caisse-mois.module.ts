import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { CaisseMoisComponent } from './list/caisse-mois.component';
import { CaisseMoisDetailComponent } from './detail/caisse-mois-detail.component';
import { CaisseMoisUpdateComponent } from './update/caisse-mois-update.component';
import { CaisseMoisDeleteDialogComponent } from './delete/caisse-mois-delete-dialog.component';
import { CaisseMoisRoutingModule } from './route/caisse-mois-routing.module';

@NgModule({
  imports: [SharedModule, CaisseMoisRoutingModule],
  declarations: [CaisseMoisComponent, CaisseMoisDetailComponent, CaisseMoisUpdateComponent, CaisseMoisDeleteDialogComponent],
  entryComponents: [CaisseMoisDeleteDialogComponent],
})
export class CaisseMoisModule {}
