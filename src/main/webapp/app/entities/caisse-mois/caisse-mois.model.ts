import dayjs from 'dayjs/esm';
import { IMensualite } from 'app/entities/mensualite/mensualite.model';
import { IMembre } from 'app/entities/membre/membre.model';
import { IUser } from 'app/entities/user/user.model';

export interface ICaisseMois {
  id?: number;
  date?: dayjs.Dayjs | null;
  montant?: number;
  valide?: boolean | null;
  mensualite?: IMensualite | null;
  membre?: IMembre;
  user?: IUser | null;
}

export class CaisseMois implements ICaisseMois {
  constructor(
    public id?: number,
    public date?: dayjs.Dayjs | null,
    public montant?: number,
    public valide?: boolean | null,
    public mensualite?: IMensualite | null,
    public membre?: IMembre,
    public user?: IUser | null
  ) {
    this.valide = this.valide ?? false;
  }
}

export function getCaisseMoisIdentifier(caisseMois: ICaisseMois): number | undefined {
  return caisseMois.id;
}
