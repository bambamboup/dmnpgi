import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { CaisseMoisDetailComponent } from './caisse-mois-detail.component';

describe('CaisseMois Management Detail Component', () => {
  let comp: CaisseMoisDetailComponent;
  let fixture: ComponentFixture<CaisseMoisDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CaisseMoisDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ caisseMois: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(CaisseMoisDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(CaisseMoisDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load caisseMois on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.caisseMois).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
