import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICaisseMois } from '../caisse-mois.model';

@Component({
  selector: 'jhi-caisse-mois-detail',
  templateUrl: './caisse-mois-detail.component.html',
})
export class CaisseMoisDetailComponent implements OnInit {
  caisseMois: ICaisseMois | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ caisseMois }) => {
      this.caisseMois = caisseMois;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
