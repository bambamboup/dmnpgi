import { IFaculte } from 'app/entities/faculte/faculte.model';

export interface IDepartement {
  id?: number;
  nom?: string;
  faculte?: IFaculte;
}

export class Departement implements IDepartement {
  constructor(public id?: number, public nom?: string, public faculte?: IFaculte) {}
}

export function getDepartementIdentifier(departement: IDepartement): number | undefined {
  return departement.id;
}
