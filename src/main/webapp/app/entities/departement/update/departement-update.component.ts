import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IDepartement, Departement } from '../departement.model';
import { DepartementService } from '../service/departement.service';
import { IFaculte } from 'app/entities/faculte/faculte.model';
import { FaculteService } from 'app/entities/faculte/service/faculte.service';

@Component({
  selector: 'jhi-departement-update',
  templateUrl: './departement-update.component.html',
})
export class DepartementUpdateComponent implements OnInit {
  isSaving = false;

  facultesSharedCollection: IFaculte[] = [];

  editForm = this.fb.group({
    id: [],
    nom: [null, [Validators.required]],
    faculte: [null, Validators.required],
  });

  constructor(
    protected departementService: DepartementService,
    protected faculteService: FaculteService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ departement }) => {
      this.updateForm(departement);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const departement = this.createFromForm();
    if (departement.id !== undefined) {
      this.subscribeToSaveResponse(this.departementService.update(departement));
    } else {
      this.subscribeToSaveResponse(this.departementService.create(departement));
    }
  }

  trackFaculteById(index: number, item: IFaculte): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDepartement>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(departement: IDepartement): void {
    this.editForm.patchValue({
      id: departement.id,
      nom: departement.nom,
      faculte: departement.faculte,
    });

    this.facultesSharedCollection = this.faculteService.addFaculteToCollectionIfMissing(this.facultesSharedCollection, departement.faculte);
  }

  protected loadRelationshipsOptions(): void {
    this.faculteService
      .query()
      .pipe(map((res: HttpResponse<IFaculte[]>) => res.body ?? []))
      .pipe(
        map((facultes: IFaculte[]) => this.faculteService.addFaculteToCollectionIfMissing(facultes, this.editForm.get('faculte')!.value))
      )
      .subscribe((facultes: IFaculte[]) => (this.facultesSharedCollection = facultes));
  }

  protected createFromForm(): IDepartement {
    return {
      ...new Departement(),
      id: this.editForm.get(['id'])!.value,
      nom: this.editForm.get(['nom'])!.value,
      faculte: this.editForm.get(['faculte'])!.value,
    };
  }
}
