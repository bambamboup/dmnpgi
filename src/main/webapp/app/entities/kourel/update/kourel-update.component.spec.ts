import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { KourelService } from '../service/kourel.service';
import { IKourel, Kourel } from '../kourel.model';

import { KourelUpdateComponent } from './kourel-update.component';

describe('Kourel Management Update Component', () => {
  let comp: KourelUpdateComponent;
  let fixture: ComponentFixture<KourelUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let kourelService: KourelService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [KourelUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(KourelUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(KourelUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    kourelService = TestBed.inject(KourelService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const kourel: IKourel = { id: 456 };

      activatedRoute.data = of({ kourel });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(kourel));
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Kourel>>();
      const kourel = { id: 123 };
      jest.spyOn(kourelService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ kourel });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: kourel }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(kourelService.update).toHaveBeenCalledWith(kourel);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Kourel>>();
      const kourel = new Kourel();
      jest.spyOn(kourelService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ kourel });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: kourel }));
      saveSubject.complete();

      // THEN
      expect(kourelService.create).toHaveBeenCalledWith(kourel);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Kourel>>();
      const kourel = { id: 123 };
      jest.spyOn(kourelService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ kourel });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(kourelService.update).toHaveBeenCalledWith(kourel);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
