export interface IKourel {
  id?: number;
  nom?: string;
}

export class Kourel implements IKourel {
  constructor(public id?: number, public nom?: string) {}
}

export function getKourelIdentifier(kourel: IKourel): number | undefined {
  return kourel.id;
}
