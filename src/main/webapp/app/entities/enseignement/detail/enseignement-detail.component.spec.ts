import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { EnseignementDetailComponent } from './enseignement-detail.component';

describe('Enseignement Management Detail Component', () => {
  let comp: EnseignementDetailComponent;
  let fixture: ComponentFixture<EnseignementDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EnseignementDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ enseignement: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(EnseignementDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(EnseignementDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load enseignement on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.enseignement).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
