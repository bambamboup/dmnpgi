import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IEnseignement, Enseignement } from '../enseignement.model';
import { EnseignementService } from '../service/enseignement.service';
import { IMembre } from 'app/entities/membre/membre.model';
import { MembreService } from 'app/entities/membre/service/membre.service';
import { ICoran } from 'app/entities/coran/coran.model';
import { CoranService } from 'app/entities/coran/service/coran.service';
import { IXamXam } from 'app/entities/xam-xam/xam-xam.model';
import { XamXamService } from 'app/entities/xam-xam/service/xam-xam.service';

@Component({
  selector: 'jhi-enseignement-update',
  templateUrl: './enseignement-update.component.html',
})
export class EnseignementUpdateComponent implements OnInit {
  isSaving = false;

  membresSharedCollection: IMembre[] = [];
  coransSharedCollection: ICoran[] = [];
  xamXamsSharedCollection: IXamXam[] = [];

  editForm = this.fb.group({
    id: [],
    dateDebut: [null, [Validators.required]],
    dateFin: [],
    encours: [],
    membre: [null, Validators.required],
    coran: [],
    xamXam: [],
  });

  constructor(
    protected enseignementService: EnseignementService,
    protected membreService: MembreService,
    protected coranService: CoranService,
    protected xamXamService: XamXamService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ enseignement }) => {
      this.updateForm(enseignement);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const enseignement = this.createFromForm();
    if (enseignement.id !== undefined) {
      this.subscribeToSaveResponse(this.enseignementService.update(enseignement));
    } else {
      this.subscribeToSaveResponse(this.enseignementService.create(enseignement));
    }
  }

  trackMembreById(index: number, item: IMembre): number {
    return item.id!;
  }

  trackCoranById(index: number, item: ICoran): number {
    return item.id!;
  }

  trackXamXamById(index: number, item: IXamXam): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IEnseignement>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(enseignement: IEnseignement): void {
    this.editForm.patchValue({
      id: enseignement.id,
      dateDebut: enseignement.dateDebut,
      dateFin: enseignement.dateFin,
      encours: enseignement.encours,
      membre: enseignement.membre,
      coran: enseignement.coran,
      xamXam: enseignement.xamXam,
    });

    this.membresSharedCollection = this.membreService.addMembreToCollectionIfMissing(this.membresSharedCollection, enseignement.membre);
    this.coransSharedCollection = this.coranService.addCoranToCollectionIfMissing(this.coransSharedCollection, enseignement.coran);
    this.xamXamsSharedCollection = this.xamXamService.addXamXamToCollectionIfMissing(this.xamXamsSharedCollection, enseignement.xamXam);
  }

  protected loadRelationshipsOptions(): void {
    this.membreService
      .query()
      .pipe(map((res: HttpResponse<IMembre[]>) => res.body ?? []))
      .pipe(map((membres: IMembre[]) => this.membreService.addMembreToCollectionIfMissing(membres, this.editForm.get('membre')!.value)))
      .subscribe((membres: IMembre[]) => (this.membresSharedCollection = membres));

    this.coranService
      .query()
      .pipe(map((res: HttpResponse<ICoran[]>) => res.body ?? []))
      .pipe(map((corans: ICoran[]) => this.coranService.addCoranToCollectionIfMissing(corans, this.editForm.get('coran')!.value)))
      .subscribe((corans: ICoran[]) => (this.coransSharedCollection = corans));

    this.xamXamService
      .query()
      .pipe(map((res: HttpResponse<IXamXam[]>) => res.body ?? []))
      .pipe(map((xamXams: IXamXam[]) => this.xamXamService.addXamXamToCollectionIfMissing(xamXams, this.editForm.get('xamXam')!.value)))
      .subscribe((xamXams: IXamXam[]) => (this.xamXamsSharedCollection = xamXams));
  }

  protected createFromForm(): IEnseignement {
    return {
      ...new Enseignement(),
      id: this.editForm.get(['id'])!.value,
      dateDebut: this.editForm.get(['dateDebut'])!.value,
      dateFin: this.editForm.get(['dateFin'])!.value,
      encours: this.editForm.get(['encours'])!.value,
      membre: this.editForm.get(['membre'])!.value,
      coran: this.editForm.get(['coran'])!.value,
      xamXam: this.editForm.get(['xamXam'])!.value,
    };
  }
}
