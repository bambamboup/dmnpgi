import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { EnseignementService } from '../service/enseignement.service';
import { IEnseignement, Enseignement } from '../enseignement.model';
import { IMembre } from 'app/entities/membre/membre.model';
import { MembreService } from 'app/entities/membre/service/membre.service';
import { ICoran } from 'app/entities/coran/coran.model';
import { CoranService } from 'app/entities/coran/service/coran.service';
import { IXamXam } from 'app/entities/xam-xam/xam-xam.model';
import { XamXamService } from 'app/entities/xam-xam/service/xam-xam.service';

import { EnseignementUpdateComponent } from './enseignement-update.component';

describe('Enseignement Management Update Component', () => {
  let comp: EnseignementUpdateComponent;
  let fixture: ComponentFixture<EnseignementUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let enseignementService: EnseignementService;
  let membreService: MembreService;
  let coranService: CoranService;
  let xamXamService: XamXamService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [EnseignementUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(EnseignementUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(EnseignementUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    enseignementService = TestBed.inject(EnseignementService);
    membreService = TestBed.inject(MembreService);
    coranService = TestBed.inject(CoranService);
    xamXamService = TestBed.inject(XamXamService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Membre query and add missing value', () => {
      const enseignement: IEnseignement = { id: 456 };
      const membre: IMembre = { id: 6619 };
      enseignement.membre = membre;

      const membreCollection: IMembre[] = [{ id: 64905 }];
      jest.spyOn(membreService, 'query').mockReturnValue(of(new HttpResponse({ body: membreCollection })));
      const additionalMembres = [membre];
      const expectedCollection: IMembre[] = [...additionalMembres, ...membreCollection];
      jest.spyOn(membreService, 'addMembreToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ enseignement });
      comp.ngOnInit();

      expect(membreService.query).toHaveBeenCalled();
      expect(membreService.addMembreToCollectionIfMissing).toHaveBeenCalledWith(membreCollection, ...additionalMembres);
      expect(comp.membresSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Coran query and add missing value', () => {
      const enseignement: IEnseignement = { id: 456 };
      const coran: ICoran = { id: 15973 };
      enseignement.coran = coran;

      const coranCollection: ICoran[] = [{ id: 21406 }];
      jest.spyOn(coranService, 'query').mockReturnValue(of(new HttpResponse({ body: coranCollection })));
      const additionalCorans = [coran];
      const expectedCollection: ICoran[] = [...additionalCorans, ...coranCollection];
      jest.spyOn(coranService, 'addCoranToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ enseignement });
      comp.ngOnInit();

      expect(coranService.query).toHaveBeenCalled();
      expect(coranService.addCoranToCollectionIfMissing).toHaveBeenCalledWith(coranCollection, ...additionalCorans);
      expect(comp.coransSharedCollection).toEqual(expectedCollection);
    });

    it('Should call XamXam query and add missing value', () => {
      const enseignement: IEnseignement = { id: 456 };
      const xamXam: IXamXam = { id: 5811 };
      enseignement.xamXam = xamXam;

      const xamXamCollection: IXamXam[] = [{ id: 67604 }];
      jest.spyOn(xamXamService, 'query').mockReturnValue(of(new HttpResponse({ body: xamXamCollection })));
      const additionalXamXams = [xamXam];
      const expectedCollection: IXamXam[] = [...additionalXamXams, ...xamXamCollection];
      jest.spyOn(xamXamService, 'addXamXamToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ enseignement });
      comp.ngOnInit();

      expect(xamXamService.query).toHaveBeenCalled();
      expect(xamXamService.addXamXamToCollectionIfMissing).toHaveBeenCalledWith(xamXamCollection, ...additionalXamXams);
      expect(comp.xamXamsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const enseignement: IEnseignement = { id: 456 };
      const membre: IMembre = { id: 36878 };
      enseignement.membre = membre;
      const coran: ICoran = { id: 23533 };
      enseignement.coran = coran;
      const xamXam: IXamXam = { id: 3841 };
      enseignement.xamXam = xamXam;

      activatedRoute.data = of({ enseignement });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(enseignement));
      expect(comp.membresSharedCollection).toContain(membre);
      expect(comp.coransSharedCollection).toContain(coran);
      expect(comp.xamXamsSharedCollection).toContain(xamXam);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Enseignement>>();
      const enseignement = { id: 123 };
      jest.spyOn(enseignementService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ enseignement });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: enseignement }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(enseignementService.update).toHaveBeenCalledWith(enseignement);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Enseignement>>();
      const enseignement = new Enseignement();
      jest.spyOn(enseignementService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ enseignement });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: enseignement }));
      saveSubject.complete();

      // THEN
      expect(enseignementService.create).toHaveBeenCalledWith(enseignement);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Enseignement>>();
      const enseignement = { id: 123 };
      jest.spyOn(enseignementService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ enseignement });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(enseignementService.update).toHaveBeenCalledWith(enseignement);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackMembreById', () => {
      it('Should return tracked Membre primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackMembreById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackCoranById', () => {
      it('Should return tracked Coran primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackCoranById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackXamXamById', () => {
      it('Should return tracked XamXam primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackXamXamById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
