import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import dayjs from 'dayjs/esm';

import { DATE_FORMAT } from 'app/config/input.constants';
import { IEnseignement, Enseignement } from '../enseignement.model';

import { EnseignementService } from './enseignement.service';

describe('Enseignement Service', () => {
  let service: EnseignementService;
  let httpMock: HttpTestingController;
  let elemDefault: IEnseignement;
  let expectedResult: IEnseignement | IEnseignement[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(EnseignementService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      dateDebut: currentDate,
      dateFin: currentDate,
      encours: false,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          dateDebut: currentDate.format(DATE_FORMAT),
          dateFin: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a Enseignement', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          dateDebut: currentDate.format(DATE_FORMAT),
          dateFin: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dateDebut: currentDate,
          dateFin: currentDate,
        },
        returnedFromService
      );

      service.create(new Enseignement()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Enseignement', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          dateDebut: currentDate.format(DATE_FORMAT),
          dateFin: currentDate.format(DATE_FORMAT),
          encours: true,
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dateDebut: currentDate,
          dateFin: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Enseignement', () => {
      const patchObject = Object.assign(
        {
          dateDebut: currentDate.format(DATE_FORMAT),
          encours: true,
        },
        new Enseignement()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          dateDebut: currentDate,
          dateFin: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Enseignement', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          dateDebut: currentDate.format(DATE_FORMAT),
          dateFin: currentDate.format(DATE_FORMAT),
          encours: true,
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dateDebut: currentDate,
          dateFin: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a Enseignement', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addEnseignementToCollectionIfMissing', () => {
      it('should add a Enseignement to an empty array', () => {
        const enseignement: IEnseignement = { id: 123 };
        expectedResult = service.addEnseignementToCollectionIfMissing([], enseignement);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(enseignement);
      });

      it('should not add a Enseignement to an array that contains it', () => {
        const enseignement: IEnseignement = { id: 123 };
        const enseignementCollection: IEnseignement[] = [
          {
            ...enseignement,
          },
          { id: 456 },
        ];
        expectedResult = service.addEnseignementToCollectionIfMissing(enseignementCollection, enseignement);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Enseignement to an array that doesn't contain it", () => {
        const enseignement: IEnseignement = { id: 123 };
        const enseignementCollection: IEnseignement[] = [{ id: 456 }];
        expectedResult = service.addEnseignementToCollectionIfMissing(enseignementCollection, enseignement);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(enseignement);
      });

      it('should add only unique Enseignement to an array', () => {
        const enseignementArray: IEnseignement[] = [{ id: 123 }, { id: 456 }, { id: 74366 }];
        const enseignementCollection: IEnseignement[] = [{ id: 123 }];
        expectedResult = service.addEnseignementToCollectionIfMissing(enseignementCollection, ...enseignementArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const enseignement: IEnseignement = { id: 123 };
        const enseignement2: IEnseignement = { id: 456 };
        expectedResult = service.addEnseignementToCollectionIfMissing([], enseignement, enseignement2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(enseignement);
        expect(expectedResult).toContain(enseignement2);
      });

      it('should accept null and undefined values', () => {
        const enseignement: IEnseignement = { id: 123 };
        expectedResult = service.addEnseignementToCollectionIfMissing([], null, enseignement, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(enseignement);
      });

      it('should return initial array if no Enseignement is added', () => {
        const enseignementCollection: IEnseignement[] = [{ id: 123 }];
        expectedResult = service.addEnseignementToCollectionIfMissing(enseignementCollection, undefined, null);
        expect(expectedResult).toEqual(enseignementCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
