import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IEnseignement, getEnseignementIdentifier } from '../enseignement.model';

export type EntityResponseType = HttpResponse<IEnseignement>;
export type EntityArrayResponseType = HttpResponse<IEnseignement[]>;

@Injectable({ providedIn: 'root' })
export class EnseignementService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/enseignements');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(enseignement: IEnseignement): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(enseignement);
    return this.http
      .post<IEnseignement>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(enseignement: IEnseignement): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(enseignement);
    return this.http
      .put<IEnseignement>(`${this.resourceUrl}/${getEnseignementIdentifier(enseignement) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(enseignement: IEnseignement): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(enseignement);
    return this.http
      .patch<IEnseignement>(`${this.resourceUrl}/${getEnseignementIdentifier(enseignement) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IEnseignement>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IEnseignement[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addEnseignementToCollectionIfMissing(
    enseignementCollection: IEnseignement[],
    ...enseignementsToCheck: (IEnseignement | null | undefined)[]
  ): IEnseignement[] {
    const enseignements: IEnseignement[] = enseignementsToCheck.filter(isPresent);
    if (enseignements.length > 0) {
      const enseignementCollectionIdentifiers = enseignementCollection.map(
        enseignementItem => getEnseignementIdentifier(enseignementItem)!
      );
      const enseignementsToAdd = enseignements.filter(enseignementItem => {
        const enseignementIdentifier = getEnseignementIdentifier(enseignementItem);
        if (enseignementIdentifier == null || enseignementCollectionIdentifiers.includes(enseignementIdentifier)) {
          return false;
        }
        enseignementCollectionIdentifiers.push(enseignementIdentifier);
        return true;
      });
      return [...enseignementsToAdd, ...enseignementCollection];
    }
    return enseignementCollection;
  }

  protected convertDateFromClient(enseignement: IEnseignement): IEnseignement {
    return Object.assign({}, enseignement, {
      dateDebut: enseignement.dateDebut?.isValid() ? enseignement.dateDebut.format(DATE_FORMAT) : undefined,
      dateFin: enseignement.dateFin?.isValid() ? enseignement.dateFin.format(DATE_FORMAT) : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dateDebut = res.body.dateDebut ? dayjs(res.body.dateDebut) : undefined;
      res.body.dateFin = res.body.dateFin ? dayjs(res.body.dateFin) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((enseignement: IEnseignement) => {
        enseignement.dateDebut = enseignement.dateDebut ? dayjs(enseignement.dateDebut) : undefined;
        enseignement.dateFin = enseignement.dateFin ? dayjs(enseignement.dateFin) : undefined;
      });
    }
    return res;
  }
}
