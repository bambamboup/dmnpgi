import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { EnseignementComponent } from './list/enseignement.component';
import { EnseignementDetailComponent } from './detail/enseignement-detail.component';
import { EnseignementUpdateComponent } from './update/enseignement-update.component';
import { EnseignementDeleteDialogComponent } from './delete/enseignement-delete-dialog.component';
import { EnseignementRoutingModule } from './route/enseignement-routing.module';

@NgModule({
  imports: [SharedModule, EnseignementRoutingModule],
  declarations: [EnseignementComponent, EnseignementDetailComponent, EnseignementUpdateComponent, EnseignementDeleteDialogComponent],
  entryComponents: [EnseignementDeleteDialogComponent],
})
export class EnseignementModule {}
