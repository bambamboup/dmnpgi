import dayjs from 'dayjs/esm';
import { IMembre } from 'app/entities/membre/membre.model';
import { ICoran } from 'app/entities/coran/coran.model';
import { IXamXam } from 'app/entities/xam-xam/xam-xam.model';

export interface IEnseignement {
  id?: number;
  dateDebut?: dayjs.Dayjs;
  dateFin?: dayjs.Dayjs | null;
  encours?: boolean | null;
  membre?: IMembre;
  coran?: ICoran | null;
  xamXam?: IXamXam | null;
}

export class Enseignement implements IEnseignement {
  constructor(
    public id?: number,
    public dateDebut?: dayjs.Dayjs,
    public dateFin?: dayjs.Dayjs | null,
    public encours?: boolean | null,
    public membre?: IMembre,
    public coran?: ICoran | null,
    public xamXam?: IXamXam | null
  ) {
    this.encours = this.encours ?? false;
  }
}

export function getEnseignementIdentifier(enseignement: IEnseignement): number | undefined {
  return enseignement.id;
}
