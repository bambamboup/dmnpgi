import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IEnseignement } from '../enseignement.model';
import { EnseignementService } from '../service/enseignement.service';

@Component({
  templateUrl: './enseignement-delete-dialog.component.html',
})
export class EnseignementDeleteDialogComponent {
  enseignement?: IEnseignement;

  constructor(protected enseignementService: EnseignementService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.enseignementService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
