import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { EnseignementComponent } from '../list/enseignement.component';
import { EnseignementDetailComponent } from '../detail/enseignement-detail.component';
import { EnseignementUpdateComponent } from '../update/enseignement-update.component';
import { EnseignementRoutingResolveService } from './enseignement-routing-resolve.service';

const enseignementRoute: Routes = [
  {
    path: '',
    component: EnseignementComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: EnseignementDetailComponent,
    resolve: {
      enseignement: EnseignementRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: EnseignementUpdateComponent,
    resolve: {
      enseignement: EnseignementRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: EnseignementUpdateComponent,
    resolve: {
      enseignement: EnseignementRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(enseignementRoute)],
  exports: [RouterModule],
})
export class EnseignementRoutingModule {}
