import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IEnseignement, Enseignement } from '../enseignement.model';
import { EnseignementService } from '../service/enseignement.service';

@Injectable({ providedIn: 'root' })
export class EnseignementRoutingResolveService implements Resolve<IEnseignement> {
  constructor(protected service: EnseignementService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IEnseignement> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((enseignement: HttpResponse<Enseignement>) => {
          if (enseignement.body) {
            return of(enseignement.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Enseignement());
  }
}
