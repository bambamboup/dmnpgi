export interface IProfession {
  id?: number;
  libelle?: string;
}

export class Profession implements IProfession {
  constructor(public id?: number, public libelle?: string) {}
}

export function getProfessionIdentifier(profession: IProfession): number | undefined {
  return profession.id;
}
