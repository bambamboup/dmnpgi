import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IProfession, Profession } from '../profession.model';
import { ProfessionService } from '../service/profession.service';

@Component({
  selector: 'jhi-profession-update',
  templateUrl: './profession-update.component.html',
})
export class ProfessionUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    libelle: [null, [Validators.required]],
  });

  constructor(protected professionService: ProfessionService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ profession }) => {
      this.updateForm(profession);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const profession = this.createFromForm();
    if (profession.id !== undefined) {
      this.subscribeToSaveResponse(this.professionService.update(profession));
    } else {
      this.subscribeToSaveResponse(this.professionService.create(profession));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IProfession>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(profession: IProfession): void {
    this.editForm.patchValue({
      id: profession.id,
      libelle: profession.libelle,
    });
  }

  protected createFromForm(): IProfession {
    return {
      ...new Profession(),
      id: this.editForm.get(['id'])!.value,
      libelle: this.editForm.get(['libelle'])!.value,
    };
  }
}
