import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ProfessionComponent } from '../list/profession.component';
import { ProfessionDetailComponent } from '../detail/profession-detail.component';
import { ProfessionUpdateComponent } from '../update/profession-update.component';
import { ProfessionRoutingResolveService } from './profession-routing-resolve.service';

const professionRoute: Routes = [
  {
    path: '',
    component: ProfessionComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ProfessionDetailComponent,
    resolve: {
      profession: ProfessionRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ProfessionUpdateComponent,
    resolve: {
      profession: ProfessionRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ProfessionUpdateComponent,
    resolve: {
      profession: ProfessionRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(professionRoute)],
  exports: [RouterModule],
})
export class ProfessionRoutingModule {}
