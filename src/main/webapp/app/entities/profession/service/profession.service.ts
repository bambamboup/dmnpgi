import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IProfession, getProfessionIdentifier } from '../profession.model';

export type EntityResponseType = HttpResponse<IProfession>;
export type EntityArrayResponseType = HttpResponse<IProfession[]>;

@Injectable({ providedIn: 'root' })
export class ProfessionService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/professions');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(profession: IProfession): Observable<EntityResponseType> {
    return this.http.post<IProfession>(this.resourceUrl, profession, { observe: 'response' });
  }

  update(profession: IProfession): Observable<EntityResponseType> {
    return this.http.put<IProfession>(`${this.resourceUrl}/${getProfessionIdentifier(profession) as number}`, profession, {
      observe: 'response',
    });
  }

  partialUpdate(profession: IProfession): Observable<EntityResponseType> {
    return this.http.patch<IProfession>(`${this.resourceUrl}/${getProfessionIdentifier(profession) as number}`, profession, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IProfession>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IProfession[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addProfessionToCollectionIfMissing(
    professionCollection: IProfession[],
    ...professionsToCheck: (IProfession | null | undefined)[]
  ): IProfession[] {
    const professions: IProfession[] = professionsToCheck.filter(isPresent);
    if (professions.length > 0) {
      const professionCollectionIdentifiers = professionCollection.map(professionItem => getProfessionIdentifier(professionItem)!);
      const professionsToAdd = professions.filter(professionItem => {
        const professionIdentifier = getProfessionIdentifier(professionItem);
        if (professionIdentifier == null || professionCollectionIdentifiers.includes(professionIdentifier)) {
          return false;
        }
        professionCollectionIdentifiers.push(professionIdentifier);
        return true;
      });
      return [...professionsToAdd, ...professionCollection];
    }
    return professionCollection;
  }
}
