import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IMensualite } from '../mensualite.model';
import { MensualiteService } from '../service/mensualite.service';

@Component({
  templateUrl: './mensualite-delete-dialog.component.html',
})
export class MensualiteDeleteDialogComponent {
  mensualite?: IMensualite;

  constructor(protected mensualiteService: MensualiteService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.mensualiteService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
