import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import dayjs from 'dayjs/esm';

import { DATE_FORMAT } from 'app/config/input.constants';
import { EnumTypeMensualite } from 'app/entities/enumerations/enum-type-mensualite.model';
import { IMensualite, Mensualite } from '../mensualite.model';

import { MensualiteService } from './mensualite.service';

describe('Mensualite Service', () => {
  let service: MensualiteService;
  let httpMock: HttpTestingController;
  let elemDefault: IMensualite;
  let expectedResult: IMensualite | IMensualite[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(MensualiteService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      dateDebut: currentDate,
      dateFin: currentDate,
      actif: false,
      montant: 0,
      typeMensualite: EnumTypeMensualite.KHABANE,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          dateDebut: currentDate.format(DATE_FORMAT),
          dateFin: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a Mensualite', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          dateDebut: currentDate.format(DATE_FORMAT),
          dateFin: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dateDebut: currentDate,
          dateFin: currentDate,
        },
        returnedFromService
      );

      service.create(new Mensualite()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Mensualite', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          dateDebut: currentDate.format(DATE_FORMAT),
          dateFin: currentDate.format(DATE_FORMAT),
          actif: true,
          montant: 1,
          typeMensualite: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dateDebut: currentDate,
          dateFin: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Mensualite', () => {
      const patchObject = Object.assign(
        {
          dateDebut: currentDate.format(DATE_FORMAT),
          dateFin: currentDate.format(DATE_FORMAT),
          actif: true,
        },
        new Mensualite()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          dateDebut: currentDate,
          dateFin: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Mensualite', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          dateDebut: currentDate.format(DATE_FORMAT),
          dateFin: currentDate.format(DATE_FORMAT),
          actif: true,
          montant: 1,
          typeMensualite: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dateDebut: currentDate,
          dateFin: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a Mensualite', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addMensualiteToCollectionIfMissing', () => {
      it('should add a Mensualite to an empty array', () => {
        const mensualite: IMensualite = { id: 123 };
        expectedResult = service.addMensualiteToCollectionIfMissing([], mensualite);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(mensualite);
      });

      it('should not add a Mensualite to an array that contains it', () => {
        const mensualite: IMensualite = { id: 123 };
        const mensualiteCollection: IMensualite[] = [
          {
            ...mensualite,
          },
          { id: 456 },
        ];
        expectedResult = service.addMensualiteToCollectionIfMissing(mensualiteCollection, mensualite);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Mensualite to an array that doesn't contain it", () => {
        const mensualite: IMensualite = { id: 123 };
        const mensualiteCollection: IMensualite[] = [{ id: 456 }];
        expectedResult = service.addMensualiteToCollectionIfMissing(mensualiteCollection, mensualite);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(mensualite);
      });

      it('should add only unique Mensualite to an array', () => {
        const mensualiteArray: IMensualite[] = [{ id: 123 }, { id: 456 }, { id: 49814 }];
        const mensualiteCollection: IMensualite[] = [{ id: 123 }];
        expectedResult = service.addMensualiteToCollectionIfMissing(mensualiteCollection, ...mensualiteArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const mensualite: IMensualite = { id: 123 };
        const mensualite2: IMensualite = { id: 456 };
        expectedResult = service.addMensualiteToCollectionIfMissing([], mensualite, mensualite2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(mensualite);
        expect(expectedResult).toContain(mensualite2);
      });

      it('should accept null and undefined values', () => {
        const mensualite: IMensualite = { id: 123 };
        expectedResult = service.addMensualiteToCollectionIfMissing([], null, mensualite, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(mensualite);
      });

      it('should return initial array if no Mensualite is added', () => {
        const mensualiteCollection: IMensualite[] = [{ id: 123 }];
        expectedResult = service.addMensualiteToCollectionIfMissing(mensualiteCollection, undefined, null);
        expect(expectedResult).toEqual(mensualiteCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
