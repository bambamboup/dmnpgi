import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IMensualite, getMensualiteIdentifier } from '../mensualite.model';

export type EntityResponseType = HttpResponse<IMensualite>;
export type EntityArrayResponseType = HttpResponse<IMensualite[]>;

@Injectable({ providedIn: 'root' })
export class MensualiteService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/mensualites');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(mensualite: IMensualite): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(mensualite);
    return this.http
      .post<IMensualite>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(mensualite: IMensualite): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(mensualite);
    return this.http
      .put<IMensualite>(`${this.resourceUrl}/${getMensualiteIdentifier(mensualite) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(mensualite: IMensualite): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(mensualite);
    return this.http
      .patch<IMensualite>(`${this.resourceUrl}/${getMensualiteIdentifier(mensualite) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IMensualite>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IMensualite[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addMensualiteToCollectionIfMissing(
    mensualiteCollection: IMensualite[],
    ...mensualitesToCheck: (IMensualite | null | undefined)[]
  ): IMensualite[] {
    const mensualites: IMensualite[] = mensualitesToCheck.filter(isPresent);
    if (mensualites.length > 0) {
      const mensualiteCollectionIdentifiers = mensualiteCollection.map(mensualiteItem => getMensualiteIdentifier(mensualiteItem)!);
      const mensualitesToAdd = mensualites.filter(mensualiteItem => {
        const mensualiteIdentifier = getMensualiteIdentifier(mensualiteItem);
        if (mensualiteIdentifier == null || mensualiteCollectionIdentifiers.includes(mensualiteIdentifier)) {
          return false;
        }
        mensualiteCollectionIdentifiers.push(mensualiteIdentifier);
        return true;
      });
      return [...mensualitesToAdd, ...mensualiteCollection];
    }
    return mensualiteCollection;
  }

  protected convertDateFromClient(mensualite: IMensualite): IMensualite {
    return Object.assign({}, mensualite, {
      dateDebut: mensualite.dateDebut?.isValid() ? mensualite.dateDebut.format(DATE_FORMAT) : undefined,
      dateFin: mensualite.dateFin?.isValid() ? mensualite.dateFin.format(DATE_FORMAT) : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dateDebut = res.body.dateDebut ? dayjs(res.body.dateDebut) : undefined;
      res.body.dateFin = res.body.dateFin ? dayjs(res.body.dateFin) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((mensualite: IMensualite) => {
        mensualite.dateDebut = mensualite.dateDebut ? dayjs(mensualite.dateDebut) : undefined;
        mensualite.dateFin = mensualite.dateFin ? dayjs(mensualite.dateFin) : undefined;
      });
    }
    return res;
  }
}
