import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IMensualite, Mensualite } from '../mensualite.model';
import { MensualiteService } from '../service/mensualite.service';
import { ICategorie } from 'app/entities/categorie/categorie.model';
import { CategorieService } from 'app/entities/categorie/service/categorie.service';
import { EnumTypeMensualite } from 'app/entities/enumerations/enum-type-mensualite.model';

@Component({
  selector: 'jhi-mensualite-update',
  templateUrl: './mensualite-update.component.html',
})
export class MensualiteUpdateComponent implements OnInit {
  isSaving = false;
  enumTypeMensualiteValues = Object.keys(EnumTypeMensualite);

  categoriesSharedCollection: ICategorie[] = [];

  editForm = this.fb.group({
    id: [],
    dateDebut: [],
    dateFin: [],
    actif: [],
    montant: [null, [Validators.required]],
    typeMensualite: [null, [Validators.required]],
    categorie: [null, Validators.required],
  });

  constructor(
    protected mensualiteService: MensualiteService,
    protected categorieService: CategorieService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ mensualite }) => {
      this.updateForm(mensualite);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const mensualite = this.createFromForm();
    if (mensualite.id !== undefined) {
      this.subscribeToSaveResponse(this.mensualiteService.update(mensualite));
    } else {
      this.subscribeToSaveResponse(this.mensualiteService.create(mensualite));
    }
  }

  trackCategorieById(index: number, item: ICategorie): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMensualite>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(mensualite: IMensualite): void {
    this.editForm.patchValue({
      id: mensualite.id,
      dateDebut: mensualite.dateDebut,
      dateFin: mensualite.dateFin,
      actif: mensualite.actif,
      montant: mensualite.montant,
      typeMensualite: mensualite.typeMensualite,
      categorie: mensualite.categorie,
    });

    this.categoriesSharedCollection = this.categorieService.addCategorieToCollectionIfMissing(
      this.categoriesSharedCollection,
      mensualite.categorie
    );
  }

  protected loadRelationshipsOptions(): void {
    this.categorieService
      .query()
      .pipe(map((res: HttpResponse<ICategorie[]>) => res.body ?? []))
      .pipe(
        map((categories: ICategorie[]) =>
          this.categorieService.addCategorieToCollectionIfMissing(categories, this.editForm.get('categorie')!.value)
        )
      )
      .subscribe((categories: ICategorie[]) => (this.categoriesSharedCollection = categories));
  }

  protected createFromForm(): IMensualite {
    return {
      ...new Mensualite(),
      id: this.editForm.get(['id'])!.value,
      dateDebut: this.editForm.get(['dateDebut'])!.value,
      dateFin: this.editForm.get(['dateFin'])!.value,
      actif: this.editForm.get(['actif'])!.value,
      montant: this.editForm.get(['montant'])!.value,
      typeMensualite: this.editForm.get(['typeMensualite'])!.value,
      categorie: this.editForm.get(['categorie'])!.value,
    };
  }
}
