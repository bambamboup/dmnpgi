import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IMensualite, Mensualite } from '../mensualite.model';
import { MensualiteService } from '../service/mensualite.service';

@Injectable({ providedIn: 'root' })
export class MensualiteRoutingResolveService implements Resolve<IMensualite> {
  constructor(protected service: MensualiteService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IMensualite> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((mensualite: HttpResponse<Mensualite>) => {
          if (mensualite.body) {
            return of(mensualite.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Mensualite());
  }
}
