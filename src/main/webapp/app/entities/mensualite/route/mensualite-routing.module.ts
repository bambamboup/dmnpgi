import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { MensualiteComponent } from '../list/mensualite.component';
import { MensualiteDetailComponent } from '../detail/mensualite-detail.component';
import { MensualiteUpdateComponent } from '../update/mensualite-update.component';
import { MensualiteRoutingResolveService } from './mensualite-routing-resolve.service';

const mensualiteRoute: Routes = [
  {
    path: '',
    component: MensualiteComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: MensualiteDetailComponent,
    resolve: {
      mensualite: MensualiteRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: MensualiteUpdateComponent,
    resolve: {
      mensualite: MensualiteRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: MensualiteUpdateComponent,
    resolve: {
      mensualite: MensualiteRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(mensualiteRoute)],
  exports: [RouterModule],
})
export class MensualiteRoutingModule {}
