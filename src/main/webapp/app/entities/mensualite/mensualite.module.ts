import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { MensualiteComponent } from './list/mensualite.component';
import { MensualiteDetailComponent } from './detail/mensualite-detail.component';
import { MensualiteUpdateComponent } from './update/mensualite-update.component';
import { MensualiteDeleteDialogComponent } from './delete/mensualite-delete-dialog.component';
import { MensualiteRoutingModule } from './route/mensualite-routing.module';

@NgModule({
  imports: [SharedModule, MensualiteRoutingModule],
  declarations: [MensualiteComponent, MensualiteDetailComponent, MensualiteUpdateComponent, MensualiteDeleteDialogComponent],
  entryComponents: [MensualiteDeleteDialogComponent],
})
export class MensualiteModule {}
