import dayjs from 'dayjs/esm';
import { ICategorie } from 'app/entities/categorie/categorie.model';
import { EnumTypeMensualite } from 'app/entities/enumerations/enum-type-mensualite.model';

export interface IMensualite {
  id?: number;
  dateDebut?: dayjs.Dayjs | null;
  dateFin?: dayjs.Dayjs | null;
  actif?: boolean | null;
  montant?: number;
  typeMensualite?: EnumTypeMensualite;
  categorie?: ICategorie;
}

export class Mensualite implements IMensualite {
  constructor(
    public id?: number,
    public dateDebut?: dayjs.Dayjs | null,
    public dateFin?: dayjs.Dayjs | null,
    public actif?: boolean | null,
    public montant?: number,
    public typeMensualite?: EnumTypeMensualite,
    public categorie?: ICategorie
  ) {
    this.actif = this.actif ?? false;
  }
}

export function getMensualiteIdentifier(mensualite: IMensualite): number | undefined {
  return mensualite.id;
}
