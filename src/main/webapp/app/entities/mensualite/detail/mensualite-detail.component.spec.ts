import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MensualiteDetailComponent } from './mensualite-detail.component';

describe('Mensualite Management Detail Component', () => {
  let comp: MensualiteDetailComponent;
  let fixture: ComponentFixture<MensualiteDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MensualiteDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ mensualite: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(MensualiteDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(MensualiteDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load mensualite on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.mensualite).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
