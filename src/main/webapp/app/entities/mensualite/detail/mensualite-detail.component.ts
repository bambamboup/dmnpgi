import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMensualite } from '../mensualite.model';

@Component({
  selector: 'jhi-mensualite-detail',
  templateUrl: './mensualite-detail.component.html',
})
export class MensualiteDetailComponent implements OnInit {
  mensualite: IMensualite | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ mensualite }) => {
      this.mensualite = mensualite;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
