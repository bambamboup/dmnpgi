import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMembre } from '../membre.model';
import { DataUtils } from 'app/core/util/data-util.service';

@Component({
  selector: 'jhi-membre-detail',
  templateUrl: './membre-detail.component.html',
})
export class MembreDetailComponent implements OnInit {
  membre: IMembre | null = null;

  constructor(protected dataUtils: DataUtils, protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ membre }) => {
      this.membre = membre;
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(base64String: string, contentType: string | null | undefined): void {
    this.dataUtils.openFile(base64String, contentType);
  }

  previousState(): void {
    window.history.back();
  }
}
