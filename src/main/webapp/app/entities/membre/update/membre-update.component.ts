import { Component, OnInit, ElementRef } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IMembre, Membre } from '../membre.model';
import { MembreService } from '../service/membre.service';
import { AlertError } from 'app/shared/alert/alert-error.model';
import { EventManager, EventWithContent } from 'app/core/util/event-manager.service';
import { DataUtils, FileLoadError } from 'app/core/util/data-util.service';
import { IXamXam } from 'app/entities/xam-xam/xam-xam.model';
import { XamXamService } from 'app/entities/xam-xam/service/xam-xam.service';
import { INiveauXamXam } from 'app/entities/niveau-xam-xam/niveau-xam-xam.model';
import { NiveauXamXamService } from 'app/entities/niveau-xam-xam/service/niveau-xam-xam.service';
import { IProfession } from 'app/entities/profession/profession.model';
import { ProfessionService } from 'app/entities/profession/service/profession.service';
import { ICategorie } from 'app/entities/categorie/categorie.model';
import { CategorieService } from 'app/entities/categorie/service/categorie.service';
import { ICoran } from 'app/entities/coran/coran.model';
import { CoranService } from 'app/entities/coran/service/coran.service';
import { IDepartement } from 'app/entities/departement/departement.model';
import { DepartementService } from 'app/entities/departement/service/departement.service';
import { EnumTypeSexe } from 'app/entities/enumerations/enum-type-sexe.model';
import { EnumCivilite } from 'app/entities/enumerations/enum-civilite.model';
import { EnumEtatSante } from 'app/entities/enumerations/enum-etat-sante.model';

@Component({
  selector: 'jhi-membre-update',
  templateUrl: './membre-update.component.html',
})
export class MembreUpdateComponent implements OnInit {
  isSaving = false;
  enumTypeSexeValues = Object.keys(EnumTypeSexe);
  enumCiviliteValues = Object.keys(EnumCivilite);
  enumEtatSanteValues = Object.keys(EnumEtatSante);

  xamXamsSharedCollection: IXamXam[] = [];
  niveauXamXamsSharedCollection: INiveauXamXam[] = [];
  professionsSharedCollection: IProfession[] = [];
  categoriesSharedCollection: ICategorie[] = [];
  coransSharedCollection: ICoran[] = [];
  departementsSharedCollection: IDepartement[] = [];

  editForm = this.fb.group({
    id: [],
    nom: [null, [Validators.required]],
    prenom: [null, [Validators.required]],
    telephone: [null, [Validators.required]],
    sexe: [null, [Validators.required]],
    email: [],
    cni: [],
    adressDakar: [],
    situationMatrimoniale: [null, [Validators.required]],
    adresse: [],
    adresseVacance: [],
    profil: [],
    profilContentType: [],
    dateNaissance: [],
    boursier: [],
    daaraOrigine: [],
    etatSante: [],
    titeur: [],
    telephoneTiteur: [],
    xamXams: [null, Validators.required],
    niveauXamXam: [],
    profession: [],
    categorie: [],
    coran: [],
    departement: [],
    user: [],
  });

  constructor(
    protected dataUtils: DataUtils,
    protected eventManager: EventManager,
    protected membreService: MembreService,
    protected xamXamService: XamXamService,
    protected niveauXamXamService: NiveauXamXamService,
    protected professionService: ProfessionService,
    protected categorieService: CategorieService,
    protected coranService: CoranService,
    protected departementService: DepartementService,
    protected elementRef: ElementRef,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ membre }) => {
      this.updateForm(membre);

      this.loadRelationshipsOptions();
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(base64String: string, contentType: string | null | undefined): void {
    this.dataUtils.openFile(base64String, contentType);
  }

  setFileData(event: Event, field: string, isImage: boolean): void {
    this.dataUtils.loadFileToForm(event, this.editForm, field, isImage).subscribe({
      error: (err: FileLoadError) =>
        this.eventManager.broadcast(new EventWithContent<AlertError>('dmnApp.error', { ...err, key: 'error.file.' + err.key })),
    });
  }

  clearInputImage(field: string, fieldContentType: string, idInput: string): void {
    this.editForm.patchValue({
      [field]: null,
      [fieldContentType]: null,
    });
    if (idInput && this.elementRef.nativeElement.querySelector('#' + idInput)) {
      this.elementRef.nativeElement.querySelector('#' + idInput).value = null;
    }
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const membre = this.createFromForm();
    if (membre.id !== undefined) {
      this.subscribeToSaveResponse(this.membreService.update(membre));
    } else {
      this.subscribeToSaveResponse(this.membreService.create(membre));
    }
  }

  trackXamXamById(index: number, item: IXamXam): number {
    return item.id!;
  }

  trackNiveauXamXamById(index: number, item: INiveauXamXam): number {
    return item.id!;
  }

  trackProfessionById(index: number, item: IProfession): number {
    return item.id!;
  }

  trackCategorieById(index: number, item: ICategorie): number {
    return item.id!;
  }

  trackCoranById(index: number, item: ICoran): number {
    return item.id!;
  }

  trackDepartementById(index: number, item: IDepartement): number {
    return item.id!;
  }

  getSelectedXamXam(option: IXamXam, selectedVals?: IXamXam[]): IXamXam {
    if (selectedVals) {
      for (const selectedVal of selectedVals) {
        if (option.id === selectedVal.id) {
          return selectedVal;
        }
      }
    }
    return option;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMembre>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(membre: IMembre): void {
    this.editForm.patchValue({
      id: membre.id,
      nom: membre.nom,
      prenom: membre.prenom,
      telephone: membre.telephone,
      sexe: membre.sexe,
      email: membre.email,
      cni: membre.cni,
      adressDakar: membre.adressDakar,
      situationMatrimoniale: membre.situationMatrimoniale,
      adresse: membre.adresse,
      adresseVacance: membre.adresseVacance,
      profil: membre.profil,
      profilContentType: membre.profilContentType,
      dateNaissance: membre.dateNaissance,
      boursier: membre.boursier,
      daaraOrigine: membre.daaraOrigine,
      etatSante: membre.etatSante,
      titeur: membre.titeur,
      telephoneTiteur: membre.telephoneTiteur,
      xamXams: membre.xamXams,
      niveauXamXam: membre.niveauXamXam,
      profession: membre.profession,
      categorie: membre.categorie,
      coran: membre.coran,
      departement: membre.departement,
      user: membre.user,
    });

    this.xamXamsSharedCollection = this.xamXamService.addXamXamToCollectionIfMissing(
      this.xamXamsSharedCollection,
      ...(membre.xamXams ?? [])
    );
    this.niveauXamXamsSharedCollection = this.niveauXamXamService.addNiveauXamXamToCollectionIfMissing(
      this.niveauXamXamsSharedCollection,
      membre.niveauXamXam
    );
    this.professionsSharedCollection = this.professionService.addProfessionToCollectionIfMissing(
      this.professionsSharedCollection,
      membre.profession
    );
    this.categoriesSharedCollection = this.categorieService.addCategorieToCollectionIfMissing(
      this.categoriesSharedCollection,
      membre.categorie
    );
    this.coransSharedCollection = this.coranService.addCoranToCollectionIfMissing(this.coransSharedCollection, membre.coran);
    this.departementsSharedCollection = this.departementService.addDepartementToCollectionIfMissing(
      this.departementsSharedCollection,
      membre.departement
    );
  }

  protected loadRelationshipsOptions(): void {
    this.xamXamService
      .query()
      .pipe(map((res: HttpResponse<IXamXam[]>) => res.body ?? []))
      .pipe(
        map((xamXams: IXamXam[]) =>
          this.xamXamService.addXamXamToCollectionIfMissing(xamXams, ...(this.editForm.get('xamXams')!.value ?? []))
        )
      )
      .subscribe((xamXams: IXamXam[]) => (this.xamXamsSharedCollection = xamXams));

    this.niveauXamXamService
      .query()
      .pipe(map((res: HttpResponse<INiveauXamXam[]>) => res.body ?? []))
      .pipe(
        map((niveauXamXams: INiveauXamXam[]) =>
          this.niveauXamXamService.addNiveauXamXamToCollectionIfMissing(niveauXamXams, this.editForm.get('niveauXamXam')!.value)
        )
      )
      .subscribe((niveauXamXams: INiveauXamXam[]) => (this.niveauXamXamsSharedCollection = niveauXamXams));

    this.professionService
      .query()
      .pipe(map((res: HttpResponse<IProfession[]>) => res.body ?? []))
      .pipe(
        map((professions: IProfession[]) =>
          this.professionService.addProfessionToCollectionIfMissing(professions, this.editForm.get('profession')!.value)
        )
      )
      .subscribe((professions: IProfession[]) => (this.professionsSharedCollection = professions));

    this.categorieService
      .query()
      .pipe(map((res: HttpResponse<ICategorie[]>) => res.body ?? []))
      .pipe(
        map((categories: ICategorie[]) =>
          this.categorieService.addCategorieToCollectionIfMissing(categories, this.editForm.get('categorie')!.value)
        )
      )
      .subscribe((categories: ICategorie[]) => (this.categoriesSharedCollection = categories));

    this.coranService
      .query()
      .pipe(map((res: HttpResponse<ICoran[]>) => res.body ?? []))
      .pipe(map((corans: ICoran[]) => this.coranService.addCoranToCollectionIfMissing(corans, this.editForm.get('coran')!.value)))
      .subscribe((corans: ICoran[]) => (this.coransSharedCollection = corans));

    this.departementService
      .query()
      .pipe(map((res: HttpResponse<IDepartement[]>) => res.body ?? []))
      .pipe(
        map((departements: IDepartement[]) =>
          this.departementService.addDepartementToCollectionIfMissing(departements, this.editForm.get('departement')!.value)
        )
      )
      .subscribe((departements: IDepartement[]) => (this.departementsSharedCollection = departements));
  }

  protected createFromForm(): IMembre {
    return {
      ...new Membre(),
      id: this.editForm.get(['id'])!.value,
      nom: this.editForm.get(['nom'])!.value,
      prenom: this.editForm.get(['prenom'])!.value,
      telephone: this.editForm.get(['telephone'])!.value,
      sexe: this.editForm.get(['sexe'])!.value,
      email: this.editForm.get(['email'])!.value,
      cni: this.editForm.get(['cni'])!.value,
      adressDakar: this.editForm.get(['adressDakar'])!.value,
      situationMatrimoniale: this.editForm.get(['situationMatrimoniale'])!.value,
      adresse: this.editForm.get(['adresse'])!.value,
      adresseVacance: this.editForm.get(['adresseVacance'])!.value,
      profilContentType: this.editForm.get(['profilContentType'])!.value,
      profil: this.editForm.get(['profil'])!.value,
      dateNaissance: this.editForm.get(['dateNaissance'])!.value,
      boursier: this.editForm.get(['boursier'])!.value,
      daaraOrigine: this.editForm.get(['daaraOrigine'])!.value,
      etatSante: this.editForm.get(['etatSante'])!.value,
      titeur: this.editForm.get(['titeur'])!.value,
      telephoneTiteur: this.editForm.get(['telephoneTiteur'])!.value,
      xamXams: this.editForm.get(['xamXams'])!.value,
      niveauXamXam: this.editForm.get(['niveauXamXam'])!.value,
      profession: this.editForm.get(['profession'])!.value,
      categorie: this.editForm.get(['categorie'])!.value,
      coran: this.editForm.get(['coran'])!.value,
      departement: this.editForm.get(['departement'])!.value,
    };
  }
}
