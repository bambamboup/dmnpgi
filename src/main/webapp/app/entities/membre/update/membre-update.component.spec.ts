import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { MembreService } from '../service/membre.service';
import { IMembre, Membre } from '../membre.model';
import { IXamXam } from 'app/entities/xam-xam/xam-xam.model';
import { XamXamService } from 'app/entities/xam-xam/service/xam-xam.service';
import { INiveauXamXam } from 'app/entities/niveau-xam-xam/niveau-xam-xam.model';
import { NiveauXamXamService } from 'app/entities/niveau-xam-xam/service/niveau-xam-xam.service';
import { IProfession } from 'app/entities/profession/profession.model';
import { ProfessionService } from 'app/entities/profession/service/profession.service';
import { ICategorie } from 'app/entities/categorie/categorie.model';
import { CategorieService } from 'app/entities/categorie/service/categorie.service';
import { ICoran } from 'app/entities/coran/coran.model';
import { CoranService } from 'app/entities/coran/service/coran.service';
import { IDepartement } from 'app/entities/departement/departement.model';
import { DepartementService } from 'app/entities/departement/service/departement.service';

import { IUser } from 'app/entities/user/user.model';
import { UserService } from 'app/entities/user/user.service';

import { MembreUpdateComponent } from './membre-update.component';

describe('Membre Management Update Component', () => {
  let comp: MembreUpdateComponent;
  let fixture: ComponentFixture<MembreUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let membreService: MembreService;
  let xamXamService: XamXamService;
  let niveauXamXamService: NiveauXamXamService;
  let professionService: ProfessionService;
  let categorieService: CategorieService;
  let coranService: CoranService;
  let departementService: DepartementService;
  let userService: UserService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [MembreUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(MembreUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(MembreUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    membreService = TestBed.inject(MembreService);
    xamXamService = TestBed.inject(XamXamService);
    niveauXamXamService = TestBed.inject(NiveauXamXamService);
    professionService = TestBed.inject(ProfessionService);
    categorieService = TestBed.inject(CategorieService);
    coranService = TestBed.inject(CoranService);
    departementService = TestBed.inject(DepartementService);
    userService = TestBed.inject(UserService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call XamXam query and add missing value', () => {
      const membre: IMembre = { id: 456 };
      const xamXams: IXamXam[] = [{ id: 13636 }];
      membre.xamXams = xamXams;

      const xamXamCollection: IXamXam[] = [{ id: 81125 }];
      jest.spyOn(xamXamService, 'query').mockReturnValue(of(new HttpResponse({ body: xamXamCollection })));
      const additionalXamXams = [...xamXams];
      const expectedCollection: IXamXam[] = [...additionalXamXams, ...xamXamCollection];
      jest.spyOn(xamXamService, 'addXamXamToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ membre });
      comp.ngOnInit();

      expect(xamXamService.query).toHaveBeenCalled();
      expect(xamXamService.addXamXamToCollectionIfMissing).toHaveBeenCalledWith(xamXamCollection, ...additionalXamXams);
      expect(comp.xamXamsSharedCollection).toEqual(expectedCollection);
    });

    it('Should call NiveauXamXam query and add missing value', () => {
      const membre: IMembre = { id: 456 };
      const niveauXamXam: INiveauXamXam = { id: 92478 };
      membre.niveauXamXam = niveauXamXam;

      const niveauXamXamCollection: INiveauXamXam[] = [{ id: 43713 }];
      jest.spyOn(niveauXamXamService, 'query').mockReturnValue(of(new HttpResponse({ body: niveauXamXamCollection })));
      const additionalNiveauXamXams = [niveauXamXam];
      const expectedCollection: INiveauXamXam[] = [...additionalNiveauXamXams, ...niveauXamXamCollection];
      jest.spyOn(niveauXamXamService, 'addNiveauXamXamToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ membre });
      comp.ngOnInit();

      expect(niveauXamXamService.query).toHaveBeenCalled();
      expect(niveauXamXamService.addNiveauXamXamToCollectionIfMissing).toHaveBeenCalledWith(
        niveauXamXamCollection,
        ...additionalNiveauXamXams
      );
      expect(comp.niveauXamXamsSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Profession query and add missing value', () => {
      const membre: IMembre = { id: 456 };
      const profession: IProfession = { id: 27232 };
      membre.profession = profession;

      const professionCollection: IProfession[] = [{ id: 25693 }];
      jest.spyOn(professionService, 'query').mockReturnValue(of(new HttpResponse({ body: professionCollection })));
      const additionalProfessions = [profession];
      const expectedCollection: IProfession[] = [...additionalProfessions, ...professionCollection];
      jest.spyOn(professionService, 'addProfessionToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ membre });
      comp.ngOnInit();

      expect(professionService.query).toHaveBeenCalled();
      expect(professionService.addProfessionToCollectionIfMissing).toHaveBeenCalledWith(professionCollection, ...additionalProfessions);
      expect(comp.professionsSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Categorie query and add missing value', () => {
      const membre: IMembre = { id: 456 };
      const categorie: ICategorie = { id: 31155 };
      membre.categorie = categorie;

      const categorieCollection: ICategorie[] = [{ id: 5967 }];
      jest.spyOn(categorieService, 'query').mockReturnValue(of(new HttpResponse({ body: categorieCollection })));
      const additionalCategories = [categorie];
      const expectedCollection: ICategorie[] = [...additionalCategories, ...categorieCollection];
      jest.spyOn(categorieService, 'addCategorieToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ membre });
      comp.ngOnInit();

      expect(categorieService.query).toHaveBeenCalled();
      expect(categorieService.addCategorieToCollectionIfMissing).toHaveBeenCalledWith(categorieCollection, ...additionalCategories);
      expect(comp.categoriesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Coran query and add missing value', () => {
      const membre: IMembre = { id: 456 };
      const coran: ICoran = { id: 67687 };
      membre.coran = coran;

      const coranCollection: ICoran[] = [{ id: 64446 }];
      jest.spyOn(coranService, 'query').mockReturnValue(of(new HttpResponse({ body: coranCollection })));
      const additionalCorans = [coran];
      const expectedCollection: ICoran[] = [...additionalCorans, ...coranCollection];
      jest.spyOn(coranService, 'addCoranToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ membre });
      comp.ngOnInit();

      expect(coranService.query).toHaveBeenCalled();
      expect(coranService.addCoranToCollectionIfMissing).toHaveBeenCalledWith(coranCollection, ...additionalCorans);
      expect(comp.coransSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Departement query and add missing value', () => {
      const membre: IMembre = { id: 456 };
      const departement: IDepartement = { id: 49150 };
      membre.departement = departement;

      const departementCollection: IDepartement[] = [{ id: 83486 }];
      jest.spyOn(departementService, 'query').mockReturnValue(of(new HttpResponse({ body: departementCollection })));
      const additionalDepartements = [departement];
      const expectedCollection: IDepartement[] = [...additionalDepartements, ...departementCollection];
      jest.spyOn(departementService, 'addDepartementToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ membre });
      comp.ngOnInit();

      expect(departementService.query).toHaveBeenCalled();
      expect(departementService.addDepartementToCollectionIfMissing).toHaveBeenCalledWith(departementCollection, ...additionalDepartements);
      expect(comp.departementsSharedCollection).toEqual(expectedCollection);
    });

    it('Should call User query and add missing value', () => {
      const membre: IMembre = { id: 456 };
      const user: IUser = { id: 69335 };
      membre.user = user;

      const userCollection: IUser[] = [{ id: 1409 }];
      jest.spyOn(userService, 'query').mockReturnValue(of(new HttpResponse({ body: userCollection })));
      const additionalUsers = [user];
      const expectedCollection: IUser[] = [...additionalUsers, ...userCollection];
      jest.spyOn(userService, 'addUserToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ membre });
      comp.ngOnInit();

      expect(userService.query).toHaveBeenCalled();
      expect(userService.addUserToCollectionIfMissing).toHaveBeenCalledWith(userCollection, ...additionalUsers);
      expect(comp.usersSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const membre: IMembre = { id: 456 };
      const xamXams: IXamXam = { id: 32964 };
      membre.xamXams = [xamXams];
      const niveauXamXam: INiveauXamXam = { id: 96263 };
      membre.niveauXamXam = niveauXamXam;
      const profession: IProfession = { id: 13531 };
      membre.profession = profession;
      const categorie: ICategorie = { id: 19942 };
      membre.categorie = categorie;
      const coran: ICoran = { id: 21437 };
      membre.coran = coran;
      const departement: IDepartement = { id: 93676 };
      membre.departement = departement;
      const user: IUser = { id: 38551 };
      membre.user = user;

      activatedRoute.data = of({ membre });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(membre));
      expect(comp.xamXamsSharedCollection).toContain(xamXams);
      expect(comp.niveauXamXamsSharedCollection).toContain(niveauXamXam);
      expect(comp.professionsSharedCollection).toContain(profession);
      expect(comp.categoriesSharedCollection).toContain(categorie);
      expect(comp.coransSharedCollection).toContain(coran);
      expect(comp.departementsSharedCollection).toContain(departement);
      expect(comp.usersSharedCollection).toContain(user);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Membre>>();
      const membre = { id: 123 };
      jest.spyOn(membreService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ membre });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: membre }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(membreService.update).toHaveBeenCalledWith(membre);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Membre>>();
      const membre = new Membre();
      jest.spyOn(membreService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ membre });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: membre }));
      saveSubject.complete();

      // THEN
      expect(membreService.create).toHaveBeenCalledWith(membre);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Membre>>();
      const membre = { id: 123 };
      jest.spyOn(membreService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ membre });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(membreService.update).toHaveBeenCalledWith(membre);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackXamXamById', () => {
      it('Should return tracked XamXam primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackXamXamById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackNiveauXamXamById', () => {
      it('Should return tracked NiveauXamXam primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackNiveauXamXamById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackProfessionById', () => {
      it('Should return tracked Profession primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackProfessionById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackCategorieById', () => {
      it('Should return tracked Categorie primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackCategorieById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackCoranById', () => {
      it('Should return tracked Coran primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackCoranById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackDepartementById', () => {
      it('Should return tracked Departement primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackDepartementById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackUserById', () => {
      it('Should return tracked User primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackUserById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });

  describe('Getting selected relationships', () => {
    describe('getSelectedXamXam', () => {
      it('Should return option if no XamXam is selected', () => {
        const option = { id: 123 };
        const result = comp.getSelectedXamXam(option);
        expect(result === option).toEqual(true);
      });

      it('Should return selected XamXam for according option', () => {
        const option = { id: 123 };
        const selected = { id: 123 };
        const selected2 = { id: 456 };
        const result = comp.getSelectedXamXam(option, [selected2, selected]);
        expect(result === selected).toEqual(true);
        expect(result === selected2).toEqual(false);
        expect(result === option).toEqual(false);
      });

      it('Should return option if this XamXam is not selected', () => {
        const option = { id: 123 };
        const selected = { id: 456 };
        const result = comp.getSelectedXamXam(option, [selected]);
        expect(result === option).toEqual(true);
        expect(result === selected).toEqual(false);
      });
    });
  });
});
