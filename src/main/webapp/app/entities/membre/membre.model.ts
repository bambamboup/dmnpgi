import dayjs from 'dayjs/esm';
import { IXamXam } from 'app/entities/xam-xam/xam-xam.model';
import { INiveauXamXam } from 'app/entities/niveau-xam-xam/niveau-xam-xam.model';
import { IProfession } from 'app/entities/profession/profession.model';
import { ICategorie } from 'app/entities/categorie/categorie.model';
import { ICoran } from 'app/entities/coran/coran.model';
import { IDepartement } from 'app/entities/departement/departement.model';
import { IUser } from 'app/entities/user/user.model';
import { EnumTypeSexe } from 'app/entities/enumerations/enum-type-sexe.model';
import { EnumCivilite } from 'app/entities/enumerations/enum-civilite.model';
import { EnumEtatSante } from 'app/entities/enumerations/enum-etat-sante.model';

export interface IMembre {
  id?: number;
  nom?: string;
  prenom?: string;
  telephone?: string;
  sexe?: EnumTypeSexe;
  email?: string | null;
  cni?: string | null;
  adressDakar?: string | null;
  situationMatrimoniale?: EnumCivilite;
  adresse?: string | null;
  adresseVacance?: string | null;
  profilContentType?: string | null;
  profil?: string | null;
  dateNaissance?: dayjs.Dayjs | null;
  boursier?: boolean | null;
  daaraOrigine?: string | null;
  etatSante?: EnumEtatSante | null;
  titeur?: string | null;
  telephoneTiteur?: string | null;
  xamXams?: IXamXam[];
  niveauXamXam?: INiveauXamXam | null;
  profession?: IProfession | null;
  categorie?: ICategorie | null;
  coran?: ICoran | null;
  departement?: IDepartement | null;
  user?: IUser | null;
}

export class Membre implements IMembre {
  constructor(
    public id?: number,
    public nom?: string,
    public prenom?: string,
    public telephone?: string,
    public sexe?: EnumTypeSexe,
    public email?: string | null,
    public cni?: string | null,
    public adressDakar?: string | null,
    public situationMatrimoniale?: EnumCivilite,
    public adresse?: string | null,
    public adresseVacance?: string | null,
    public profilContentType?: string | null,
    public profil?: string | null,
    public dateNaissance?: dayjs.Dayjs | null,
    public boursier?: boolean | null,
    public daaraOrigine?: string | null,
    public etatSante?: EnumEtatSante | null,
    public titeur?: string | null,
    public telephoneTiteur?: string | null,
    public xamXams?: IXamXam[],
    public niveauXamXam?: INiveauXamXam | null,
    public profession?: IProfession | null,
    public categorie?: ICategorie | null,
    public coran?: ICoran | null,
    public departement?: IDepartement | null,
    public user?: IUser | null
  ) {
    this.boursier = this.boursier ?? false;
  }
}

export function getMembreIdentifier(membre: IMembre): number | undefined {
  return membre.id;
}
