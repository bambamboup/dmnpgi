import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IPresident } from '../president.model';
import { PresidentService } from '../service/president.service';

@Component({
  templateUrl: './president-delete-dialog.component.html',
})
export class PresidentDeleteDialogComponent {
  president?: IPresident;

  constructor(protected presidentService: PresidentService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.presidentService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
