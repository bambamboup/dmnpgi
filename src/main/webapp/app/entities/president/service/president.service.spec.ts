import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import dayjs from 'dayjs/esm';

import { DATE_FORMAT } from 'app/config/input.constants';
import { IPresident, President } from '../president.model';

import { PresidentService } from './president.service';

describe('President Service', () => {
  let service: PresidentService;
  let httpMock: HttpTestingController;
  let elemDefault: IPresident;
  let expectedResult: IPresident | IPresident[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(PresidentService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      dateDebut: currentDate,
      dateFin: currentDate,
      actif: false,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          dateDebut: currentDate.format(DATE_FORMAT),
          dateFin: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a President', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          dateDebut: currentDate.format(DATE_FORMAT),
          dateFin: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dateDebut: currentDate,
          dateFin: currentDate,
        },
        returnedFromService
      );

      service.create(new President()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a President', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          dateDebut: currentDate.format(DATE_FORMAT),
          dateFin: currentDate.format(DATE_FORMAT),
          actif: true,
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dateDebut: currentDate,
          dateFin: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a President', () => {
      const patchObject = Object.assign(
        {
          dateDebut: currentDate.format(DATE_FORMAT),
          actif: true,
        },
        new President()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          dateDebut: currentDate,
          dateFin: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of President', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          dateDebut: currentDate.format(DATE_FORMAT),
          dateFin: currentDate.format(DATE_FORMAT),
          actif: true,
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dateDebut: currentDate,
          dateFin: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a President', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addPresidentToCollectionIfMissing', () => {
      it('should add a President to an empty array', () => {
        const president: IPresident = { id: 123 };
        expectedResult = service.addPresidentToCollectionIfMissing([], president);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(president);
      });

      it('should not add a President to an array that contains it', () => {
        const president: IPresident = { id: 123 };
        const presidentCollection: IPresident[] = [
          {
            ...president,
          },
          { id: 456 },
        ];
        expectedResult = service.addPresidentToCollectionIfMissing(presidentCollection, president);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a President to an array that doesn't contain it", () => {
        const president: IPresident = { id: 123 };
        const presidentCollection: IPresident[] = [{ id: 456 }];
        expectedResult = service.addPresidentToCollectionIfMissing(presidentCollection, president);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(president);
      });

      it('should add only unique President to an array', () => {
        const presidentArray: IPresident[] = [{ id: 123 }, { id: 456 }, { id: 77919 }];
        const presidentCollection: IPresident[] = [{ id: 123 }];
        expectedResult = service.addPresidentToCollectionIfMissing(presidentCollection, ...presidentArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const president: IPresident = { id: 123 };
        const president2: IPresident = { id: 456 };
        expectedResult = service.addPresidentToCollectionIfMissing([], president, president2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(president);
        expect(expectedResult).toContain(president2);
      });

      it('should accept null and undefined values', () => {
        const president: IPresident = { id: 123 };
        expectedResult = service.addPresidentToCollectionIfMissing([], null, president, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(president);
      });

      it('should return initial array if no President is added', () => {
        const presidentCollection: IPresident[] = [{ id: 123 }];
        expectedResult = service.addPresidentToCollectionIfMissing(presidentCollection, undefined, null);
        expect(expectedResult).toEqual(presidentCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
