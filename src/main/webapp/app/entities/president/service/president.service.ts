import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IPresident, getPresidentIdentifier } from '../president.model';

export type EntityResponseType = HttpResponse<IPresident>;
export type EntityArrayResponseType = HttpResponse<IPresident[]>;

@Injectable({ providedIn: 'root' })
export class PresidentService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/presidents');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(president: IPresident): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(president);
    return this.http
      .post<IPresident>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(president: IPresident): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(president);
    return this.http
      .put<IPresident>(`${this.resourceUrl}/${getPresidentIdentifier(president) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(president: IPresident): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(president);
    return this.http
      .patch<IPresident>(`${this.resourceUrl}/${getPresidentIdentifier(president) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IPresident>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IPresident[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addPresidentToCollectionIfMissing(
    presidentCollection: IPresident[],
    ...presidentsToCheck: (IPresident | null | undefined)[]
  ): IPresident[] {
    const presidents: IPresident[] = presidentsToCheck.filter(isPresent);
    if (presidents.length > 0) {
      const presidentCollectionIdentifiers = presidentCollection.map(presidentItem => getPresidentIdentifier(presidentItem)!);
      const presidentsToAdd = presidents.filter(presidentItem => {
        const presidentIdentifier = getPresidentIdentifier(presidentItem);
        if (presidentIdentifier == null || presidentCollectionIdentifiers.includes(presidentIdentifier)) {
          return false;
        }
        presidentCollectionIdentifiers.push(presidentIdentifier);
        return true;
      });
      return [...presidentsToAdd, ...presidentCollection];
    }
    return presidentCollection;
  }

  protected convertDateFromClient(president: IPresident): IPresident {
    return Object.assign({}, president, {
      dateDebut: president.dateDebut?.isValid() ? president.dateDebut.format(DATE_FORMAT) : undefined,
      dateFin: president.dateFin?.isValid() ? president.dateFin.format(DATE_FORMAT) : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dateDebut = res.body.dateDebut ? dayjs(res.body.dateDebut) : undefined;
      res.body.dateFin = res.body.dateFin ? dayjs(res.body.dateFin) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((president: IPresident) => {
        president.dateDebut = president.dateDebut ? dayjs(president.dateDebut) : undefined;
        president.dateFin = president.dateFin ? dayjs(president.dateFin) : undefined;
      });
    }
    return res;
  }
}
