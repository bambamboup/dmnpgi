import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IPresident, President } from '../president.model';
import { PresidentService } from '../service/president.service';

@Injectable({ providedIn: 'root' })
export class PresidentRoutingResolveService implements Resolve<IPresident> {
  constructor(protected service: PresidentService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IPresident> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((president: HttpResponse<President>) => {
          if (president.body) {
            return of(president.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new President());
  }
}
