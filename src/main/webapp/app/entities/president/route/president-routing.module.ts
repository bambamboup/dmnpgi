import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { PresidentComponent } from '../list/president.component';
import { PresidentDetailComponent } from '../detail/president-detail.component';
import { PresidentUpdateComponent } from '../update/president-update.component';
import { PresidentRoutingResolveService } from './president-routing-resolve.service';

const presidentRoute: Routes = [
  {
    path: '',
    component: PresidentComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: PresidentDetailComponent,
    resolve: {
      president: PresidentRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: PresidentUpdateComponent,
    resolve: {
      president: PresidentRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: PresidentUpdateComponent,
    resolve: {
      president: PresidentRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(presidentRoute)],
  exports: [RouterModule],
})
export class PresidentRoutingModule {}
