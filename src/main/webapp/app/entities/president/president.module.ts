import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { PresidentComponent } from './list/president.component';
import { PresidentDetailComponent } from './detail/president-detail.component';
import { PresidentUpdateComponent } from './update/president-update.component';
import { PresidentDeleteDialogComponent } from './delete/president-delete-dialog.component';
import { PresidentRoutingModule } from './route/president-routing.module';

@NgModule({
  imports: [SharedModule, PresidentRoutingModule],
  declarations: [PresidentComponent, PresidentDetailComponent, PresidentUpdateComponent, PresidentDeleteDialogComponent],
  entryComponents: [PresidentDeleteDialogComponent],
})
export class PresidentModule {}
