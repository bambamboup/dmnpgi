import dayjs from 'dayjs/esm';
import { IMembre } from 'app/entities/membre/membre.model';
import { IEntite } from 'app/entities/entite/entite.model';

export interface IPresident {
  id?: number;
  dateDebut?: dayjs.Dayjs;
  dateFin?: dayjs.Dayjs | null;
  actif?: boolean | null;
  membre?: IMembre;
  entite?: IEntite;
}

export class President implements IPresident {
  constructor(
    public id?: number,
    public dateDebut?: dayjs.Dayjs,
    public dateFin?: dayjs.Dayjs | null,
    public actif?: boolean | null,
    public membre?: IMembre,
    public entite?: IEntite
  ) {
    this.actif = this.actif ?? false;
  }
}

export function getPresidentIdentifier(president: IPresident): number | undefined {
  return president.id;
}
