import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { PresidentService } from '../service/president.service';
import { IPresident, President } from '../president.model';
import { IMembre } from 'app/entities/membre/membre.model';
import { MembreService } from 'app/entities/membre/service/membre.service';
import { IEntite } from 'app/entities/entite/entite.model';
import { EntiteService } from 'app/entities/entite/service/entite.service';

import { PresidentUpdateComponent } from './president-update.component';

describe('President Management Update Component', () => {
  let comp: PresidentUpdateComponent;
  let fixture: ComponentFixture<PresidentUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let presidentService: PresidentService;
  let membreService: MembreService;
  let entiteService: EntiteService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [PresidentUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(PresidentUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(PresidentUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    presidentService = TestBed.inject(PresidentService);
    membreService = TestBed.inject(MembreService);
    entiteService = TestBed.inject(EntiteService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Membre query and add missing value', () => {
      const president: IPresident = { id: 456 };
      const membre: IMembre = { id: 7544 };
      president.membre = membre;

      const membreCollection: IMembre[] = [{ id: 38733 }];
      jest.spyOn(membreService, 'query').mockReturnValue(of(new HttpResponse({ body: membreCollection })));
      const additionalMembres = [membre];
      const expectedCollection: IMembre[] = [...additionalMembres, ...membreCollection];
      jest.spyOn(membreService, 'addMembreToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ president });
      comp.ngOnInit();

      expect(membreService.query).toHaveBeenCalled();
      expect(membreService.addMembreToCollectionIfMissing).toHaveBeenCalledWith(membreCollection, ...additionalMembres);
      expect(comp.membresSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Entite query and add missing value', () => {
      const president: IPresident = { id: 456 };
      const entite: IEntite = { id: 69018 };
      president.entite = entite;

      const entiteCollection: IEntite[] = [{ id: 94724 }];
      jest.spyOn(entiteService, 'query').mockReturnValue(of(new HttpResponse({ body: entiteCollection })));
      const additionalEntites = [entite];
      const expectedCollection: IEntite[] = [...additionalEntites, ...entiteCollection];
      jest.spyOn(entiteService, 'addEntiteToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ president });
      comp.ngOnInit();

      expect(entiteService.query).toHaveBeenCalled();
      expect(entiteService.addEntiteToCollectionIfMissing).toHaveBeenCalledWith(entiteCollection, ...additionalEntites);
      expect(comp.entitesSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const president: IPresident = { id: 456 };
      const membre: IMembre = { id: 67822 };
      president.membre = membre;
      const entite: IEntite = { id: 23546 };
      president.entite = entite;

      activatedRoute.data = of({ president });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(president));
      expect(comp.membresSharedCollection).toContain(membre);
      expect(comp.entitesSharedCollection).toContain(entite);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<President>>();
      const president = { id: 123 };
      jest.spyOn(presidentService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ president });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: president }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(presidentService.update).toHaveBeenCalledWith(president);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<President>>();
      const president = new President();
      jest.spyOn(presidentService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ president });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: president }));
      saveSubject.complete();

      // THEN
      expect(presidentService.create).toHaveBeenCalledWith(president);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<President>>();
      const president = { id: 123 };
      jest.spyOn(presidentService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ president });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(presidentService.update).toHaveBeenCalledWith(president);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackMembreById', () => {
      it('Should return tracked Membre primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackMembreById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackEntiteById', () => {
      it('Should return tracked Entite primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackEntiteById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
