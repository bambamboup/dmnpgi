import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IPresident, President } from '../president.model';
import { PresidentService } from '../service/president.service';
import { IMembre } from 'app/entities/membre/membre.model';
import { MembreService } from 'app/entities/membre/service/membre.service';
import { IEntite } from 'app/entities/entite/entite.model';
import { EntiteService } from 'app/entities/entite/service/entite.service';

@Component({
  selector: 'jhi-president-update',
  templateUrl: './president-update.component.html',
})
export class PresidentUpdateComponent implements OnInit {
  isSaving = false;

  membresSharedCollection: IMembre[] = [];
  entitesSharedCollection: IEntite[] = [];

  editForm = this.fb.group({
    id: [],
    dateDebut: [null, [Validators.required]],
    dateFin: [],
    actif: [],
    membre: [null, Validators.required],
    entite: [null, Validators.required],
  });

  constructor(
    protected presidentService: PresidentService,
    protected membreService: MembreService,
    protected entiteService: EntiteService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ president }) => {
      this.updateForm(president);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const president = this.createFromForm();
    if (president.id !== undefined) {
      this.subscribeToSaveResponse(this.presidentService.update(president));
    } else {
      this.subscribeToSaveResponse(this.presidentService.create(president));
    }
  }

  trackMembreById(index: number, item: IMembre): number {
    return item.id!;
  }

  trackEntiteById(index: number, item: IEntite): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPresident>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(president: IPresident): void {
    this.editForm.patchValue({
      id: president.id,
      dateDebut: president.dateDebut,
      dateFin: president.dateFin,
      actif: president.actif,
      membre: president.membre,
      entite: president.entite,
    });

    this.membresSharedCollection = this.membreService.addMembreToCollectionIfMissing(this.membresSharedCollection, president.membre);
    this.entitesSharedCollection = this.entiteService.addEntiteToCollectionIfMissing(this.entitesSharedCollection, president.entite);
  }

  protected loadRelationshipsOptions(): void {
    this.membreService
      .query()
      .pipe(map((res: HttpResponse<IMembre[]>) => res.body ?? []))
      .pipe(map((membres: IMembre[]) => this.membreService.addMembreToCollectionIfMissing(membres, this.editForm.get('membre')!.value)))
      .subscribe((membres: IMembre[]) => (this.membresSharedCollection = membres));

    this.entiteService
      .query()
      .pipe(map((res: HttpResponse<IEntite[]>) => res.body ?? []))
      .pipe(map((entites: IEntite[]) => this.entiteService.addEntiteToCollectionIfMissing(entites, this.editForm.get('entite')!.value)))
      .subscribe((entites: IEntite[]) => (this.entitesSharedCollection = entites));
  }

  protected createFromForm(): IPresident {
    return {
      ...new President(),
      id: this.editForm.get(['id'])!.value,
      dateDebut: this.editForm.get(['dateDebut'])!.value,
      dateFin: this.editForm.get(['dateFin'])!.value,
      actif: this.editForm.get(['actif'])!.value,
      membre: this.editForm.get(['membre'])!.value,
      entite: this.editForm.get(['entite'])!.value,
    };
  }
}
