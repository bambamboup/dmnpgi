import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PresidentDetailComponent } from './president-detail.component';

describe('President Management Detail Component', () => {
  let comp: PresidentDetailComponent;
  let fixture: ComponentFixture<PresidentDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PresidentDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ president: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(PresidentDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(PresidentDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load president on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.president).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
