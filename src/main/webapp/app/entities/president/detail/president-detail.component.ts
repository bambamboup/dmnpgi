import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPresident } from '../president.model';

@Component({
  selector: 'jhi-president-detail',
  templateUrl: './president-detail.component.html',
})
export class PresidentDetailComponent implements OnInit {
  president: IPresident | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ president }) => {
      this.president = president;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
