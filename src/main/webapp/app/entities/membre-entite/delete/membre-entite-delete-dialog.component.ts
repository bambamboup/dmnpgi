import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IMembreEntite } from '../membre-entite.model';
import { MembreEntiteService } from '../service/membre-entite.service';

@Component({
  templateUrl: './membre-entite-delete-dialog.component.html',
})
export class MembreEntiteDeleteDialogComponent {
  membreEntite?: IMembreEntite;

  constructor(protected membreEntiteService: MembreEntiteService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.membreEntiteService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
