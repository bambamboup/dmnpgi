import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { MembreEntiteComponent } from '../list/membre-entite.component';
import { MembreEntiteDetailComponent } from '../detail/membre-entite-detail.component';
import { MembreEntiteUpdateComponent } from '../update/membre-entite-update.component';
import { MembreEntiteRoutingResolveService } from './membre-entite-routing-resolve.service';

const membreEntiteRoute: Routes = [
  {
    path: '',
    component: MembreEntiteComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: MembreEntiteDetailComponent,
    resolve: {
      membreEntite: MembreEntiteRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: MembreEntiteUpdateComponent,
    resolve: {
      membreEntite: MembreEntiteRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: MembreEntiteUpdateComponent,
    resolve: {
      membreEntite: MembreEntiteRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(membreEntiteRoute)],
  exports: [RouterModule],
})
export class MembreEntiteRoutingModule {}
