import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, ActivatedRoute, Router, convertToParamMap } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { IMembreEntite, MembreEntite } from '../membre-entite.model';
import { MembreEntiteService } from '../service/membre-entite.service';

import { MembreEntiteRoutingResolveService } from './membre-entite-routing-resolve.service';

describe('MembreEntite routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: MembreEntiteRoutingResolveService;
  let service: MembreEntiteService;
  let resultMembreEntite: IMembreEntite | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: convertToParamMap({}),
            },
          },
        },
      ],
    });
    mockRouter = TestBed.inject(Router);
    jest.spyOn(mockRouter, 'navigate').mockImplementation(() => Promise.resolve(true));
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRoute).snapshot;
    routingResolveService = TestBed.inject(MembreEntiteRoutingResolveService);
    service = TestBed.inject(MembreEntiteService);
    resultMembreEntite = undefined;
  });

  describe('resolve', () => {
    it('should return IMembreEntite returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultMembreEntite = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultMembreEntite).toEqual({ id: 123 });
    });

    it('should return new IMembreEntite if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultMembreEntite = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultMembreEntite).toEqual(new MembreEntite());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as MembreEntite })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultMembreEntite = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultMembreEntite).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
