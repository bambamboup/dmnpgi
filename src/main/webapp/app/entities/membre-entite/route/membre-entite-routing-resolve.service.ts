import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IMembreEntite, MembreEntite } from '../membre-entite.model';
import { MembreEntiteService } from '../service/membre-entite.service';

@Injectable({ providedIn: 'root' })
export class MembreEntiteRoutingResolveService implements Resolve<IMembreEntite> {
  constructor(protected service: MembreEntiteService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IMembreEntite> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((membreEntite: HttpResponse<MembreEntite>) => {
          if (membreEntite.body) {
            return of(membreEntite.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new MembreEntite());
  }
}
