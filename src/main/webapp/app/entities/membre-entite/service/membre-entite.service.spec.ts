import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IMembreEntite, MembreEntite } from '../membre-entite.model';

import { MembreEntiteService } from './membre-entite.service';

describe('MembreEntite Service', () => {
  let service: MembreEntiteService;
  let httpMock: HttpTestingController;
  let elemDefault: IMembreEntite;
  let expectedResult: IMembreEntite | IMembreEntite[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(MembreEntiteService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      actif: false,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a MembreEntite', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new MembreEntite()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a MembreEntite', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          actif: true,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a MembreEntite', () => {
      const patchObject = Object.assign({}, new MembreEntite());

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of MembreEntite', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          actif: true,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a MembreEntite', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addMembreEntiteToCollectionIfMissing', () => {
      it('should add a MembreEntite to an empty array', () => {
        const membreEntite: IMembreEntite = { id: 123 };
        expectedResult = service.addMembreEntiteToCollectionIfMissing([], membreEntite);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(membreEntite);
      });

      it('should not add a MembreEntite to an array that contains it', () => {
        const membreEntite: IMembreEntite = { id: 123 };
        const membreEntiteCollection: IMembreEntite[] = [
          {
            ...membreEntite,
          },
          { id: 456 },
        ];
        expectedResult = service.addMembreEntiteToCollectionIfMissing(membreEntiteCollection, membreEntite);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a MembreEntite to an array that doesn't contain it", () => {
        const membreEntite: IMembreEntite = { id: 123 };
        const membreEntiteCollection: IMembreEntite[] = [{ id: 456 }];
        expectedResult = service.addMembreEntiteToCollectionIfMissing(membreEntiteCollection, membreEntite);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(membreEntite);
      });

      it('should add only unique MembreEntite to an array', () => {
        const membreEntiteArray: IMembreEntite[] = [{ id: 123 }, { id: 456 }, { id: 69775 }];
        const membreEntiteCollection: IMembreEntite[] = [{ id: 123 }];
        expectedResult = service.addMembreEntiteToCollectionIfMissing(membreEntiteCollection, ...membreEntiteArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const membreEntite: IMembreEntite = { id: 123 };
        const membreEntite2: IMembreEntite = { id: 456 };
        expectedResult = service.addMembreEntiteToCollectionIfMissing([], membreEntite, membreEntite2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(membreEntite);
        expect(expectedResult).toContain(membreEntite2);
      });

      it('should accept null and undefined values', () => {
        const membreEntite: IMembreEntite = { id: 123 };
        expectedResult = service.addMembreEntiteToCollectionIfMissing([], null, membreEntite, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(membreEntite);
      });

      it('should return initial array if no MembreEntite is added', () => {
        const membreEntiteCollection: IMembreEntite[] = [{ id: 123 }];
        expectedResult = service.addMembreEntiteToCollectionIfMissing(membreEntiteCollection, undefined, null);
        expect(expectedResult).toEqual(membreEntiteCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
