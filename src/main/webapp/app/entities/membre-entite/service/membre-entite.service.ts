import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IMembreEntite, getMembreEntiteIdentifier } from '../membre-entite.model';

export type EntityResponseType = HttpResponse<IMembreEntite>;
export type EntityArrayResponseType = HttpResponse<IMembreEntite[]>;

@Injectable({ providedIn: 'root' })
export class MembreEntiteService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/membre-entites');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(membreEntite: IMembreEntite): Observable<EntityResponseType> {
    return this.http.post<IMembreEntite>(this.resourceUrl, membreEntite, { observe: 'response' });
  }

  update(membreEntite: IMembreEntite): Observable<EntityResponseType> {
    return this.http.put<IMembreEntite>(`${this.resourceUrl}/${getMembreEntiteIdentifier(membreEntite) as number}`, membreEntite, {
      observe: 'response',
    });
  }

  partialUpdate(membreEntite: IMembreEntite): Observable<EntityResponseType> {
    return this.http.patch<IMembreEntite>(`${this.resourceUrl}/${getMembreEntiteIdentifier(membreEntite) as number}`, membreEntite, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IMembreEntite>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IMembreEntite[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addMembreEntiteToCollectionIfMissing(
    membreEntiteCollection: IMembreEntite[],
    ...membreEntitesToCheck: (IMembreEntite | null | undefined)[]
  ): IMembreEntite[] {
    const membreEntites: IMembreEntite[] = membreEntitesToCheck.filter(isPresent);
    if (membreEntites.length > 0) {
      const membreEntiteCollectionIdentifiers = membreEntiteCollection.map(
        membreEntiteItem => getMembreEntiteIdentifier(membreEntiteItem)!
      );
      const membreEntitesToAdd = membreEntites.filter(membreEntiteItem => {
        const membreEntiteIdentifier = getMembreEntiteIdentifier(membreEntiteItem);
        if (membreEntiteIdentifier == null || membreEntiteCollectionIdentifiers.includes(membreEntiteIdentifier)) {
          return false;
        }
        membreEntiteCollectionIdentifiers.push(membreEntiteIdentifier);
        return true;
      });
      return [...membreEntitesToAdd, ...membreEntiteCollection];
    }
    return membreEntiteCollection;
  }
}
