import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { MembreEntiteService } from '../service/membre-entite.service';
import { IMembreEntite, MembreEntite } from '../membre-entite.model';
import { IMembre } from 'app/entities/membre/membre.model';
import { MembreService } from 'app/entities/membre/service/membre.service';
import { IEntite } from 'app/entities/entite/entite.model';
import { EntiteService } from 'app/entities/entite/service/entite.service';

import { MembreEntiteUpdateComponent } from './membre-entite-update.component';

describe('MembreEntite Management Update Component', () => {
  let comp: MembreEntiteUpdateComponent;
  let fixture: ComponentFixture<MembreEntiteUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let membreEntiteService: MembreEntiteService;
  let membreService: MembreService;
  let entiteService: EntiteService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [MembreEntiteUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(MembreEntiteUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(MembreEntiteUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    membreEntiteService = TestBed.inject(MembreEntiteService);
    membreService = TestBed.inject(MembreService);
    entiteService = TestBed.inject(EntiteService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Membre query and add missing value', () => {
      const membreEntite: IMembreEntite = { id: 456 };
      const membre: IMembre = { id: 86961 };
      membreEntite.membre = membre;

      const membreCollection: IMembre[] = [{ id: 31643 }];
      jest.spyOn(membreService, 'query').mockReturnValue(of(new HttpResponse({ body: membreCollection })));
      const additionalMembres = [membre];
      const expectedCollection: IMembre[] = [...additionalMembres, ...membreCollection];
      jest.spyOn(membreService, 'addMembreToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ membreEntite });
      comp.ngOnInit();

      expect(membreService.query).toHaveBeenCalled();
      expect(membreService.addMembreToCollectionIfMissing).toHaveBeenCalledWith(membreCollection, ...additionalMembres);
      expect(comp.membresSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Entite query and add missing value', () => {
      const membreEntite: IMembreEntite = { id: 456 };
      const entite: IEntite = { id: 83368 };
      membreEntite.entite = entite;

      const entiteCollection: IEntite[] = [{ id: 2771 }];
      jest.spyOn(entiteService, 'query').mockReturnValue(of(new HttpResponse({ body: entiteCollection })));
      const additionalEntites = [entite];
      const expectedCollection: IEntite[] = [...additionalEntites, ...entiteCollection];
      jest.spyOn(entiteService, 'addEntiteToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ membreEntite });
      comp.ngOnInit();

      expect(entiteService.query).toHaveBeenCalled();
      expect(entiteService.addEntiteToCollectionIfMissing).toHaveBeenCalledWith(entiteCollection, ...additionalEntites);
      expect(comp.entitesSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const membreEntite: IMembreEntite = { id: 456 };
      const membre: IMembre = { id: 64626 };
      membreEntite.membre = membre;
      const entite: IEntite = { id: 56713 };
      membreEntite.entite = entite;

      activatedRoute.data = of({ membreEntite });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(membreEntite));
      expect(comp.membresSharedCollection).toContain(membre);
      expect(comp.entitesSharedCollection).toContain(entite);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<MembreEntite>>();
      const membreEntite = { id: 123 };
      jest.spyOn(membreEntiteService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ membreEntite });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: membreEntite }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(membreEntiteService.update).toHaveBeenCalledWith(membreEntite);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<MembreEntite>>();
      const membreEntite = new MembreEntite();
      jest.spyOn(membreEntiteService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ membreEntite });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: membreEntite }));
      saveSubject.complete();

      // THEN
      expect(membreEntiteService.create).toHaveBeenCalledWith(membreEntite);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<MembreEntite>>();
      const membreEntite = { id: 123 };
      jest.spyOn(membreEntiteService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ membreEntite });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(membreEntiteService.update).toHaveBeenCalledWith(membreEntite);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackMembreById', () => {
      it('Should return tracked Membre primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackMembreById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackEntiteById', () => {
      it('Should return tracked Entite primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackEntiteById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
