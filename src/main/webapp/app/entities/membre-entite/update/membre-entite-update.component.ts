import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IMembreEntite, MembreEntite } from '../membre-entite.model';
import { MembreEntiteService } from '../service/membre-entite.service';
import { IMembre } from 'app/entities/membre/membre.model';
import { MembreService } from 'app/entities/membre/service/membre.service';
import { IEntite } from 'app/entities/entite/entite.model';
import { EntiteService } from 'app/entities/entite/service/entite.service';

@Component({
  selector: 'jhi-membre-entite-update',
  templateUrl: './membre-entite-update.component.html',
})
export class MembreEntiteUpdateComponent implements OnInit {
  isSaving = false;

  membresSharedCollection: IMembre[] = [];
  entitesSharedCollection: IEntite[] = [];

  editForm = this.fb.group({
    id: [],
    actif: [],
    membre: [null, Validators.required],
    entite: [null, Validators.required],
  });

  constructor(
    protected membreEntiteService: MembreEntiteService,
    protected membreService: MembreService,
    protected entiteService: EntiteService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ membreEntite }) => {
      this.updateForm(membreEntite);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const membreEntite = this.createFromForm();
    if (membreEntite.id !== undefined) {
      this.subscribeToSaveResponse(this.membreEntiteService.update(membreEntite));
    } else {
      this.subscribeToSaveResponse(this.membreEntiteService.create(membreEntite));
    }
  }

  trackMembreById(index: number, item: IMembre): number {
    return item.id!;
  }

  trackEntiteById(index: number, item: IEntite): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMembreEntite>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(membreEntite: IMembreEntite): void {
    this.editForm.patchValue({
      id: membreEntite.id,
      actif: membreEntite.actif,
      membre: membreEntite.membre,
      entite: membreEntite.entite,
    });

    this.membresSharedCollection = this.membreService.addMembreToCollectionIfMissing(this.membresSharedCollection, membreEntite.membre);
    this.entitesSharedCollection = this.entiteService.addEntiteToCollectionIfMissing(this.entitesSharedCollection, membreEntite.entite);
  }

  protected loadRelationshipsOptions(): void {
    this.membreService
      .query()
      .pipe(map((res: HttpResponse<IMembre[]>) => res.body ?? []))
      .pipe(map((membres: IMembre[]) => this.membreService.addMembreToCollectionIfMissing(membres, this.editForm.get('membre')!.value)))
      .subscribe((membres: IMembre[]) => (this.membresSharedCollection = membres));

    this.entiteService
      .query()
      .pipe(map((res: HttpResponse<IEntite[]>) => res.body ?? []))
      .pipe(map((entites: IEntite[]) => this.entiteService.addEntiteToCollectionIfMissing(entites, this.editForm.get('entite')!.value)))
      .subscribe((entites: IEntite[]) => (this.entitesSharedCollection = entites));
  }

  protected createFromForm(): IMembreEntite {
    return {
      ...new MembreEntite(),
      id: this.editForm.get(['id'])!.value,
      actif: this.editForm.get(['actif'])!.value,
      membre: this.editForm.get(['membre'])!.value,
      entite: this.editForm.get(['entite'])!.value,
    };
  }
}
