import { IMembre } from 'app/entities/membre/membre.model';
import { IEntite } from 'app/entities/entite/entite.model';

export interface IMembreEntite {
  id?: number;
  actif?: boolean | null;
  membre?: IMembre;
  entite?: IEntite;
}

export class MembreEntite implements IMembreEntite {
  constructor(public id?: number, public actif?: boolean | null, public membre?: IMembre, public entite?: IEntite) {
    this.actif = this.actif ?? false;
  }
}

export function getMembreEntiteIdentifier(membreEntite: IMembreEntite): number | undefined {
  return membreEntite.id;
}
