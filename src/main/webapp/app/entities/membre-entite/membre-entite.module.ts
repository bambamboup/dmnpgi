import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { MembreEntiteComponent } from './list/membre-entite.component';
import { MembreEntiteDetailComponent } from './detail/membre-entite-detail.component';
import { MembreEntiteUpdateComponent } from './update/membre-entite-update.component';
import { MembreEntiteDeleteDialogComponent } from './delete/membre-entite-delete-dialog.component';
import { MembreEntiteRoutingModule } from './route/membre-entite-routing.module';

@NgModule({
  imports: [SharedModule, MembreEntiteRoutingModule],
  declarations: [MembreEntiteComponent, MembreEntiteDetailComponent, MembreEntiteUpdateComponent, MembreEntiteDeleteDialogComponent],
  entryComponents: [MembreEntiteDeleteDialogComponent],
})
export class MembreEntiteModule {}
