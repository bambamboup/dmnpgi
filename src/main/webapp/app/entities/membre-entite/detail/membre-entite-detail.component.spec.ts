import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MembreEntiteDetailComponent } from './membre-entite-detail.component';

describe('MembreEntite Management Detail Component', () => {
  let comp: MembreEntiteDetailComponent;
  let fixture: ComponentFixture<MembreEntiteDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MembreEntiteDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ membreEntite: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(MembreEntiteDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(MembreEntiteDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load membreEntite on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.membreEntite).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
