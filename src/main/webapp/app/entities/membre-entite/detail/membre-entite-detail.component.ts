import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMembreEntite } from '../membre-entite.model';

@Component({
  selector: 'jhi-membre-entite-detail',
  templateUrl: './membre-entite-detail.component.html',
})
export class MembreEntiteDetailComponent implements OnInit {
  membreEntite: IMembreEntite | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ membreEntite }) => {
      this.membreEntite = membreEntite;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
