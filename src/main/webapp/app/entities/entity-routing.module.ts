import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'xam-xam',
        data: { pageTitle: 'dmnApp.xamXam.home.title' },
        loadChildren: () => import('./xam-xam/xam-xam.module').then(m => m.XamXamModule),
      },
      {
        path: 'niveau-xam-xam',
        data: { pageTitle: 'dmnApp.niveauXamXam.home.title' },
        loadChildren: () => import('./niveau-xam-xam/niveau-xam-xam.module').then(m => m.NiveauXamXamModule),
      },
      {
        path: 'coran',
        data: { pageTitle: 'dmnApp.coran.home.title' },
        loadChildren: () => import('./coran/coran.module').then(m => m.CoranModule),
      },
      {
        path: 'faculte',
        data: { pageTitle: 'dmnApp.faculte.home.title' },
        loadChildren: () => import('./faculte/faculte.module').then(m => m.FaculteModule),
      },
      {
        path: 'departement',
        data: { pageTitle: 'dmnApp.departement.home.title' },
        loadChildren: () => import('./departement/departement.module').then(m => m.DepartementModule),
      },
      {
        path: 'profession',
        data: { pageTitle: 'dmnApp.profession.home.title' },
        loadChildren: () => import('./profession/profession.module').then(m => m.ProfessionModule),
      },
      {
        path: 'categorie',
        data: { pageTitle: 'dmnApp.categorie.home.title' },
        loadChildren: () => import('./categorie/categorie.module').then(m => m.CategorieModule),
      },
      {
        path: 'membre',
        data: { pageTitle: 'dmnApp.membre.home.title' },
        loadChildren: () => import('./membre/membre.module').then(m => m.MembreModule),
      },
      {
        path: 'mensualite',
        data: { pageTitle: 'dmnApp.mensualite.home.title' },
        loadChildren: () => import('./mensualite/mensualite.module').then(m => m.MensualiteModule),
      },
      {
        path: 'kourel',
        data: { pageTitle: 'dmnApp.kourel.home.title' },
        loadChildren: () => import('./kourel/kourel.module').then(m => m.KourelModule),
      },
      {
        path: 'caisse-mois',
        data: { pageTitle: 'dmnApp.caisseMois.home.title' },
        loadChildren: () => import('./caisse-mois/caisse-mois.module').then(m => m.CaisseMoisModule),
      },
      {
        path: 'membre-kourel',
        data: { pageTitle: 'dmnApp.membreKourel.home.title' },
        loadChildren: () => import('./membre-kourel/membre-kourel.module').then(m => m.MembreKourelModule),
      },
      {
        path: 'entite',
        data: { pageTitle: 'dmnApp.entite.home.title' },
        loadChildren: () => import('./entite/entite.module').then(m => m.EntiteModule),
      },
      {
        path: 'membre-entite',
        data: { pageTitle: 'dmnApp.membreEntite.home.title' },
        loadChildren: () => import('./membre-entite/membre-entite.module').then(m => m.MembreEntiteModule),
      },
      {
        path: 'president',
        data: { pageTitle: 'dmnApp.president.home.title' },
        loadChildren: () => import('./president/president.module').then(m => m.PresidentModule),
      },
      {
        path: 'enseignement',
        data: { pageTitle: 'dmnApp.enseignement.home.title' },
        loadChildren: () => import('./enseignement/enseignement.module').then(m => m.EnseignementModule),
      },
      {
        path: 'localite',
        data: { pageTitle: 'dmnApp.localite.home.title' },
        loadChildren: () => import('./localite/localite.module').then(m => m.LocaliteModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}
