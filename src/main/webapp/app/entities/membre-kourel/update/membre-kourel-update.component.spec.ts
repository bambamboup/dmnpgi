import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { MembreKourelService } from '../service/membre-kourel.service';
import { IMembreKourel, MembreKourel } from '../membre-kourel.model';
import { IMembre } from 'app/entities/membre/membre.model';
import { MembreService } from 'app/entities/membre/service/membre.service';
import { IKourel } from 'app/entities/kourel/kourel.model';
import { KourelService } from 'app/entities/kourel/service/kourel.service';

import { MembreKourelUpdateComponent } from './membre-kourel-update.component';

describe('MembreKourel Management Update Component', () => {
  let comp: MembreKourelUpdateComponent;
  let fixture: ComponentFixture<MembreKourelUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let membreKourelService: MembreKourelService;
  let membreService: MembreService;
  let kourelService: KourelService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [MembreKourelUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(MembreKourelUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(MembreKourelUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    membreKourelService = TestBed.inject(MembreKourelService);
    membreService = TestBed.inject(MembreService);
    kourelService = TestBed.inject(KourelService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Membre query and add missing value', () => {
      const membreKourel: IMembreKourel = { id: 456 };
      const membre: IMembre = { id: 31299 };
      membreKourel.membre = membre;

      const membreCollection: IMembre[] = [{ id: 92661 }];
      jest.spyOn(membreService, 'query').mockReturnValue(of(new HttpResponse({ body: membreCollection })));
      const additionalMembres = [membre];
      const expectedCollection: IMembre[] = [...additionalMembres, ...membreCollection];
      jest.spyOn(membreService, 'addMembreToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ membreKourel });
      comp.ngOnInit();

      expect(membreService.query).toHaveBeenCalled();
      expect(membreService.addMembreToCollectionIfMissing).toHaveBeenCalledWith(membreCollection, ...additionalMembres);
      expect(comp.membresSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Kourel query and add missing value', () => {
      const membreKourel: IMembreKourel = { id: 456 };
      const kourel: IKourel = { id: 57410 };
      membreKourel.kourel = kourel;

      const kourelCollection: IKourel[] = [{ id: 90381 }];
      jest.spyOn(kourelService, 'query').mockReturnValue(of(new HttpResponse({ body: kourelCollection })));
      const additionalKourels = [kourel];
      const expectedCollection: IKourel[] = [...additionalKourels, ...kourelCollection];
      jest.spyOn(kourelService, 'addKourelToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ membreKourel });
      comp.ngOnInit();

      expect(kourelService.query).toHaveBeenCalled();
      expect(kourelService.addKourelToCollectionIfMissing).toHaveBeenCalledWith(kourelCollection, ...additionalKourels);
      expect(comp.kourelsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const membreKourel: IMembreKourel = { id: 456 };
      const membre: IMembre = { id: 77224 };
      membreKourel.membre = membre;
      const kourel: IKourel = { id: 82851 };
      membreKourel.kourel = kourel;

      activatedRoute.data = of({ membreKourel });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(membreKourel));
      expect(comp.membresSharedCollection).toContain(membre);
      expect(comp.kourelsSharedCollection).toContain(kourel);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<MembreKourel>>();
      const membreKourel = { id: 123 };
      jest.spyOn(membreKourelService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ membreKourel });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: membreKourel }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(membreKourelService.update).toHaveBeenCalledWith(membreKourel);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<MembreKourel>>();
      const membreKourel = new MembreKourel();
      jest.spyOn(membreKourelService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ membreKourel });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: membreKourel }));
      saveSubject.complete();

      // THEN
      expect(membreKourelService.create).toHaveBeenCalledWith(membreKourel);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<MembreKourel>>();
      const membreKourel = { id: 123 };
      jest.spyOn(membreKourelService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ membreKourel });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(membreKourelService.update).toHaveBeenCalledWith(membreKourel);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackMembreById', () => {
      it('Should return tracked Membre primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackMembreById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackKourelById', () => {
      it('Should return tracked Kourel primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackKourelById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
