import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IMembreKourel, MembreKourel } from '../membre-kourel.model';
import { MembreKourelService } from '../service/membre-kourel.service';
import { IMembre } from 'app/entities/membre/membre.model';
import { MembreService } from 'app/entities/membre/service/membre.service';
import { IKourel } from 'app/entities/kourel/kourel.model';
import { KourelService } from 'app/entities/kourel/service/kourel.service';

@Component({
  selector: 'jhi-membre-kourel-update',
  templateUrl: './membre-kourel-update.component.html',
})
export class MembreKourelUpdateComponent implements OnInit {
  isSaving = false;

  membresSharedCollection: IMembre[] = [];
  kourelsSharedCollection: IKourel[] = [];

  editForm = this.fb.group({
    id: [],
    dateDebut: [],
    dateFin: [],
    actif: [],
    membre: [null, Validators.required],
    kourel: [null, Validators.required],
  });

  constructor(
    protected membreKourelService: MembreKourelService,
    protected membreService: MembreService,
    protected kourelService: KourelService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ membreKourel }) => {
      this.updateForm(membreKourel);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const membreKourel = this.createFromForm();
    if (membreKourel.id !== undefined) {
      this.subscribeToSaveResponse(this.membreKourelService.update(membreKourel));
    } else {
      this.subscribeToSaveResponse(this.membreKourelService.create(membreKourel));
    }
  }

  trackMembreById(index: number, item: IMembre): number {
    return item.id!;
  }

  trackKourelById(index: number, item: IKourel): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMembreKourel>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(membreKourel: IMembreKourel): void {
    this.editForm.patchValue({
      id: membreKourel.id,
      dateDebut: membreKourel.dateDebut,
      dateFin: membreKourel.dateFin,
      actif: membreKourel.actif,
      membre: membreKourel.membre,
      kourel: membreKourel.kourel,
    });

    this.membresSharedCollection = this.membreService.addMembreToCollectionIfMissing(this.membresSharedCollection, membreKourel.membre);
    this.kourelsSharedCollection = this.kourelService.addKourelToCollectionIfMissing(this.kourelsSharedCollection, membreKourel.kourel);
  }

  protected loadRelationshipsOptions(): void {
    this.membreService
      .query()
      .pipe(map((res: HttpResponse<IMembre[]>) => res.body ?? []))
      .pipe(map((membres: IMembre[]) => this.membreService.addMembreToCollectionIfMissing(membres, this.editForm.get('membre')!.value)))
      .subscribe((membres: IMembre[]) => (this.membresSharedCollection = membres));

    this.kourelService
      .query()
      .pipe(map((res: HttpResponse<IKourel[]>) => res.body ?? []))
      .pipe(map((kourels: IKourel[]) => this.kourelService.addKourelToCollectionIfMissing(kourels, this.editForm.get('kourel')!.value)))
      .subscribe((kourels: IKourel[]) => (this.kourelsSharedCollection = kourels));
  }

  protected createFromForm(): IMembreKourel {
    return {
      ...new MembreKourel(),
      id: this.editForm.get(['id'])!.value,
      dateDebut: this.editForm.get(['dateDebut'])!.value,
      dateFin: this.editForm.get(['dateFin'])!.value,
      actif: this.editForm.get(['actif'])!.value,
      membre: this.editForm.get(['membre'])!.value,
      kourel: this.editForm.get(['kourel'])!.value,
    };
  }
}
