import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { MembreKourelComponent } from './list/membre-kourel.component';
import { MembreKourelDetailComponent } from './detail/membre-kourel-detail.component';
import { MembreKourelUpdateComponent } from './update/membre-kourel-update.component';
import { MembreKourelDeleteDialogComponent } from './delete/membre-kourel-delete-dialog.component';
import { MembreKourelRoutingModule } from './route/membre-kourel-routing.module';

@NgModule({
  imports: [SharedModule, MembreKourelRoutingModule],
  declarations: [MembreKourelComponent, MembreKourelDetailComponent, MembreKourelUpdateComponent, MembreKourelDeleteDialogComponent],
  entryComponents: [MembreKourelDeleteDialogComponent],
})
export class MembreKourelModule {}
