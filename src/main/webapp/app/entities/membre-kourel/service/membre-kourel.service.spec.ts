import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import dayjs from 'dayjs/esm';

import { DATE_FORMAT } from 'app/config/input.constants';
import { IMembreKourel, MembreKourel } from '../membre-kourel.model';

import { MembreKourelService } from './membre-kourel.service';

describe('MembreKourel Service', () => {
  let service: MembreKourelService;
  let httpMock: HttpTestingController;
  let elemDefault: IMembreKourel;
  let expectedResult: IMembreKourel | IMembreKourel[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(MembreKourelService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      dateDebut: currentDate,
      dateFin: currentDate,
      actif: false,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          dateDebut: currentDate.format(DATE_FORMAT),
          dateFin: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a MembreKourel', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          dateDebut: currentDate.format(DATE_FORMAT),
          dateFin: currentDate.format(DATE_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dateDebut: currentDate,
          dateFin: currentDate,
        },
        returnedFromService
      );

      service.create(new MembreKourel()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a MembreKourel', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          dateDebut: currentDate.format(DATE_FORMAT),
          dateFin: currentDate.format(DATE_FORMAT),
          actif: true,
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dateDebut: currentDate,
          dateFin: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a MembreKourel', () => {
      const patchObject = Object.assign(
        {
          dateFin: currentDate.format(DATE_FORMAT),
        },
        new MembreKourel()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          dateDebut: currentDate,
          dateFin: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of MembreKourel', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          dateDebut: currentDate.format(DATE_FORMAT),
          dateFin: currentDate.format(DATE_FORMAT),
          actif: true,
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          dateDebut: currentDate,
          dateFin: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a MembreKourel', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addMembreKourelToCollectionIfMissing', () => {
      it('should add a MembreKourel to an empty array', () => {
        const membreKourel: IMembreKourel = { id: 123 };
        expectedResult = service.addMembreKourelToCollectionIfMissing([], membreKourel);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(membreKourel);
      });

      it('should not add a MembreKourel to an array that contains it', () => {
        const membreKourel: IMembreKourel = { id: 123 };
        const membreKourelCollection: IMembreKourel[] = [
          {
            ...membreKourel,
          },
          { id: 456 },
        ];
        expectedResult = service.addMembreKourelToCollectionIfMissing(membreKourelCollection, membreKourel);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a MembreKourel to an array that doesn't contain it", () => {
        const membreKourel: IMembreKourel = { id: 123 };
        const membreKourelCollection: IMembreKourel[] = [{ id: 456 }];
        expectedResult = service.addMembreKourelToCollectionIfMissing(membreKourelCollection, membreKourel);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(membreKourel);
      });

      it('should add only unique MembreKourel to an array', () => {
        const membreKourelArray: IMembreKourel[] = [{ id: 123 }, { id: 456 }, { id: 35891 }];
        const membreKourelCollection: IMembreKourel[] = [{ id: 123 }];
        expectedResult = service.addMembreKourelToCollectionIfMissing(membreKourelCollection, ...membreKourelArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const membreKourel: IMembreKourel = { id: 123 };
        const membreKourel2: IMembreKourel = { id: 456 };
        expectedResult = service.addMembreKourelToCollectionIfMissing([], membreKourel, membreKourel2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(membreKourel);
        expect(expectedResult).toContain(membreKourel2);
      });

      it('should accept null and undefined values', () => {
        const membreKourel: IMembreKourel = { id: 123 };
        expectedResult = service.addMembreKourelToCollectionIfMissing([], null, membreKourel, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(membreKourel);
      });

      it('should return initial array if no MembreKourel is added', () => {
        const membreKourelCollection: IMembreKourel[] = [{ id: 123 }];
        expectedResult = service.addMembreKourelToCollectionIfMissing(membreKourelCollection, undefined, null);
        expect(expectedResult).toEqual(membreKourelCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
