import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IMembreKourel, getMembreKourelIdentifier } from '../membre-kourel.model';

export type EntityResponseType = HttpResponse<IMembreKourel>;
export type EntityArrayResponseType = HttpResponse<IMembreKourel[]>;

@Injectable({ providedIn: 'root' })
export class MembreKourelService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/membre-kourels');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(membreKourel: IMembreKourel): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(membreKourel);
    return this.http
      .post<IMembreKourel>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(membreKourel: IMembreKourel): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(membreKourel);
    return this.http
      .put<IMembreKourel>(`${this.resourceUrl}/${getMembreKourelIdentifier(membreKourel) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(membreKourel: IMembreKourel): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(membreKourel);
    return this.http
      .patch<IMembreKourel>(`${this.resourceUrl}/${getMembreKourelIdentifier(membreKourel) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IMembreKourel>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IMembreKourel[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addMembreKourelToCollectionIfMissing(
    membreKourelCollection: IMembreKourel[],
    ...membreKourelsToCheck: (IMembreKourel | null | undefined)[]
  ): IMembreKourel[] {
    const membreKourels: IMembreKourel[] = membreKourelsToCheck.filter(isPresent);
    if (membreKourels.length > 0) {
      const membreKourelCollectionIdentifiers = membreKourelCollection.map(
        membreKourelItem => getMembreKourelIdentifier(membreKourelItem)!
      );
      const membreKourelsToAdd = membreKourels.filter(membreKourelItem => {
        const membreKourelIdentifier = getMembreKourelIdentifier(membreKourelItem);
        if (membreKourelIdentifier == null || membreKourelCollectionIdentifiers.includes(membreKourelIdentifier)) {
          return false;
        }
        membreKourelCollectionIdentifiers.push(membreKourelIdentifier);
        return true;
      });
      return [...membreKourelsToAdd, ...membreKourelCollection];
    }
    return membreKourelCollection;
  }

  protected convertDateFromClient(membreKourel: IMembreKourel): IMembreKourel {
    return Object.assign({}, membreKourel, {
      dateDebut: membreKourel.dateDebut?.isValid() ? membreKourel.dateDebut.format(DATE_FORMAT) : undefined,
      dateFin: membreKourel.dateFin?.isValid() ? membreKourel.dateFin.format(DATE_FORMAT) : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.dateDebut = res.body.dateDebut ? dayjs(res.body.dateDebut) : undefined;
      res.body.dateFin = res.body.dateFin ? dayjs(res.body.dateFin) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((membreKourel: IMembreKourel) => {
        membreKourel.dateDebut = membreKourel.dateDebut ? dayjs(membreKourel.dateDebut) : undefined;
        membreKourel.dateFin = membreKourel.dateFin ? dayjs(membreKourel.dateFin) : undefined;
      });
    }
    return res;
  }
}
