import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { MembreKourelComponent } from '../list/membre-kourel.component';
import { MembreKourelDetailComponent } from '../detail/membre-kourel-detail.component';
import { MembreKourelUpdateComponent } from '../update/membre-kourel-update.component';
import { MembreKourelRoutingResolveService } from './membre-kourel-routing-resolve.service';

const membreKourelRoute: Routes = [
  {
    path: '',
    component: MembreKourelComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: MembreKourelDetailComponent,
    resolve: {
      membreKourel: MembreKourelRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: MembreKourelUpdateComponent,
    resolve: {
      membreKourel: MembreKourelRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: MembreKourelUpdateComponent,
    resolve: {
      membreKourel: MembreKourelRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(membreKourelRoute)],
  exports: [RouterModule],
})
export class MembreKourelRoutingModule {}
