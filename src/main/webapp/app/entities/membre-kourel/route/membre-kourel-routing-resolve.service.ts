import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IMembreKourel, MembreKourel } from '../membre-kourel.model';
import { MembreKourelService } from '../service/membre-kourel.service';

@Injectable({ providedIn: 'root' })
export class MembreKourelRoutingResolveService implements Resolve<IMembreKourel> {
  constructor(protected service: MembreKourelService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IMembreKourel> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((membreKourel: HttpResponse<MembreKourel>) => {
          if (membreKourel.body) {
            return of(membreKourel.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new MembreKourel());
  }
}
