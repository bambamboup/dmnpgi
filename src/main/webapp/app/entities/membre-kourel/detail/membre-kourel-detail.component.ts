import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMembreKourel } from '../membre-kourel.model';

@Component({
  selector: 'jhi-membre-kourel-detail',
  templateUrl: './membre-kourel-detail.component.html',
})
export class MembreKourelDetailComponent implements OnInit {
  membreKourel: IMembreKourel | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ membreKourel }) => {
      this.membreKourel = membreKourel;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
