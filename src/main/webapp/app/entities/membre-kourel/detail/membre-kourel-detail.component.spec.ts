import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MembreKourelDetailComponent } from './membre-kourel-detail.component';

describe('MembreKourel Management Detail Component', () => {
  let comp: MembreKourelDetailComponent;
  let fixture: ComponentFixture<MembreKourelDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MembreKourelDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ membreKourel: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(MembreKourelDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(MembreKourelDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load membreKourel on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.membreKourel).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
