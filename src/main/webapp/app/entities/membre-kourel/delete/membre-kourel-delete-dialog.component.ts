import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IMembreKourel } from '../membre-kourel.model';
import { MembreKourelService } from '../service/membre-kourel.service';

@Component({
  templateUrl: './membre-kourel-delete-dialog.component.html',
})
export class MembreKourelDeleteDialogComponent {
  membreKourel?: IMembreKourel;

  constructor(protected membreKourelService: MembreKourelService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.membreKourelService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
