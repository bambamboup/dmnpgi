import dayjs from 'dayjs/esm';
import { IMembre } from 'app/entities/membre/membre.model';
import { IKourel } from 'app/entities/kourel/kourel.model';

export interface IMembreKourel {
  id?: number;
  dateDebut?: dayjs.Dayjs | null;
  dateFin?: dayjs.Dayjs | null;
  actif?: boolean | null;
  membre?: IMembre;
  kourel?: IKourel;
}

export class MembreKourel implements IMembreKourel {
  constructor(
    public id?: number,
    public dateDebut?: dayjs.Dayjs | null,
    public dateFin?: dayjs.Dayjs | null,
    public actif?: boolean | null,
    public membre?: IMembre,
    public kourel?: IKourel
  ) {
    this.actif = this.actif ?? false;
  }
}

export function getMembreKourelIdentifier(membreKourel: IMembreKourel): number | undefined {
  return membreKourel.id;
}
