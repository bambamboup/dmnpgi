export interface INiveauXamXam {
  id?: number;
  libelle?: string;
  poids?: number;
}

export class NiveauXamXam implements INiveauXamXam {
  constructor(public id?: number, public libelle?: string, public poids?: number) {}
}

export function getNiveauXamXamIdentifier(niveauXamXam: INiveauXamXam): number | undefined {
  return niveauXamXam.id;
}
