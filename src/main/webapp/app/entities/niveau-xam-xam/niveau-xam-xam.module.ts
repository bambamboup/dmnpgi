import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { NiveauXamXamComponent } from './list/niveau-xam-xam.component';
import { NiveauXamXamDetailComponent } from './detail/niveau-xam-xam-detail.component';
import { NiveauXamXamUpdateComponent } from './update/niveau-xam-xam-update.component';
import { NiveauXamXamDeleteDialogComponent } from './delete/niveau-xam-xam-delete-dialog.component';
import { NiveauXamXamRoutingModule } from './route/niveau-xam-xam-routing.module';

@NgModule({
  imports: [SharedModule, NiveauXamXamRoutingModule],
  declarations: [NiveauXamXamComponent, NiveauXamXamDetailComponent, NiveauXamXamUpdateComponent, NiveauXamXamDeleteDialogComponent],
  entryComponents: [NiveauXamXamDeleteDialogComponent],
})
export class NiveauXamXamModule {}
