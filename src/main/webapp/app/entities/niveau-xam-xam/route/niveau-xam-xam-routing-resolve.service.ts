import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { INiveauXamXam, NiveauXamXam } from '../niveau-xam-xam.model';
import { NiveauXamXamService } from '../service/niveau-xam-xam.service';

@Injectable({ providedIn: 'root' })
export class NiveauXamXamRoutingResolveService implements Resolve<INiveauXamXam> {
  constructor(protected service: NiveauXamXamService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<INiveauXamXam> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((niveauXamXam: HttpResponse<NiveauXamXam>) => {
          if (niveauXamXam.body) {
            return of(niveauXamXam.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new NiveauXamXam());
  }
}
