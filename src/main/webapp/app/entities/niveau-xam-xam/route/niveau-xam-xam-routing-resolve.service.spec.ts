import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, ActivatedRoute, Router, convertToParamMap } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { INiveauXamXam, NiveauXamXam } from '../niveau-xam-xam.model';
import { NiveauXamXamService } from '../service/niveau-xam-xam.service';

import { NiveauXamXamRoutingResolveService } from './niveau-xam-xam-routing-resolve.service';

describe('NiveauXamXam routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: NiveauXamXamRoutingResolveService;
  let service: NiveauXamXamService;
  let resultNiveauXamXam: INiveauXamXam | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: convertToParamMap({}),
            },
          },
        },
      ],
    });
    mockRouter = TestBed.inject(Router);
    jest.spyOn(mockRouter, 'navigate').mockImplementation(() => Promise.resolve(true));
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRoute).snapshot;
    routingResolveService = TestBed.inject(NiveauXamXamRoutingResolveService);
    service = TestBed.inject(NiveauXamXamService);
    resultNiveauXamXam = undefined;
  });

  describe('resolve', () => {
    it('should return INiveauXamXam returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultNiveauXamXam = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultNiveauXamXam).toEqual({ id: 123 });
    });

    it('should return new INiveauXamXam if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultNiveauXamXam = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultNiveauXamXam).toEqual(new NiveauXamXam());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as NiveauXamXam })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultNiveauXamXam = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultNiveauXamXam).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
