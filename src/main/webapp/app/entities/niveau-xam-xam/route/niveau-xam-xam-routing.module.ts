import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { NiveauXamXamComponent } from '../list/niveau-xam-xam.component';
import { NiveauXamXamDetailComponent } from '../detail/niveau-xam-xam-detail.component';
import { NiveauXamXamUpdateComponent } from '../update/niveau-xam-xam-update.component';
import { NiveauXamXamRoutingResolveService } from './niveau-xam-xam-routing-resolve.service';

const niveauXamXamRoute: Routes = [
  {
    path: '',
    component: NiveauXamXamComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: NiveauXamXamDetailComponent,
    resolve: {
      niveauXamXam: NiveauXamXamRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: NiveauXamXamUpdateComponent,
    resolve: {
      niveauXamXam: NiveauXamXamRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: NiveauXamXamUpdateComponent,
    resolve: {
      niveauXamXam: NiveauXamXamRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(niveauXamXamRoute)],
  exports: [RouterModule],
})
export class NiveauXamXamRoutingModule {}
