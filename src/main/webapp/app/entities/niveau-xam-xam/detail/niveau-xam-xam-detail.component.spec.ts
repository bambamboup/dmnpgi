import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { NiveauXamXamDetailComponent } from './niveau-xam-xam-detail.component';

describe('NiveauXamXam Management Detail Component', () => {
  let comp: NiveauXamXamDetailComponent;
  let fixture: ComponentFixture<NiveauXamXamDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [NiveauXamXamDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ niveauXamXam: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(NiveauXamXamDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(NiveauXamXamDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load niveauXamXam on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.niveauXamXam).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
