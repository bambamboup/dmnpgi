import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { INiveauXamXam } from '../niveau-xam-xam.model';

@Component({
  selector: 'jhi-niveau-xam-xam-detail',
  templateUrl: './niveau-xam-xam-detail.component.html',
})
export class NiveauXamXamDetailComponent implements OnInit {
  niveauXamXam: INiveauXamXam | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ niveauXamXam }) => {
      this.niveauXamXam = niveauXamXam;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
