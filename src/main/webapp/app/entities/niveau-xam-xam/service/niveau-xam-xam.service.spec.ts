import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { INiveauXamXam, NiveauXamXam } from '../niveau-xam-xam.model';

import { NiveauXamXamService } from './niveau-xam-xam.service';

describe('NiveauXamXam Service', () => {
  let service: NiveauXamXamService;
  let httpMock: HttpTestingController;
  let elemDefault: INiveauXamXam;
  let expectedResult: INiveauXamXam | INiveauXamXam[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(NiveauXamXamService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      libelle: 'AAAAAAA',
      poids: 0,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a NiveauXamXam', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new NiveauXamXam()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a NiveauXamXam', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          libelle: 'BBBBBB',
          poids: 1,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a NiveauXamXam', () => {
      const patchObject = Object.assign({}, new NiveauXamXam());

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of NiveauXamXam', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          libelle: 'BBBBBB',
          poids: 1,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a NiveauXamXam', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addNiveauXamXamToCollectionIfMissing', () => {
      it('should add a NiveauXamXam to an empty array', () => {
        const niveauXamXam: INiveauXamXam = { id: 123 };
        expectedResult = service.addNiveauXamXamToCollectionIfMissing([], niveauXamXam);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(niveauXamXam);
      });

      it('should not add a NiveauXamXam to an array that contains it', () => {
        const niveauXamXam: INiveauXamXam = { id: 123 };
        const niveauXamXamCollection: INiveauXamXam[] = [
          {
            ...niveauXamXam,
          },
          { id: 456 },
        ];
        expectedResult = service.addNiveauXamXamToCollectionIfMissing(niveauXamXamCollection, niveauXamXam);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a NiveauXamXam to an array that doesn't contain it", () => {
        const niveauXamXam: INiveauXamXam = { id: 123 };
        const niveauXamXamCollection: INiveauXamXam[] = [{ id: 456 }];
        expectedResult = service.addNiveauXamXamToCollectionIfMissing(niveauXamXamCollection, niveauXamXam);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(niveauXamXam);
      });

      it('should add only unique NiveauXamXam to an array', () => {
        const niveauXamXamArray: INiveauXamXam[] = [{ id: 123 }, { id: 456 }, { id: 87355 }];
        const niveauXamXamCollection: INiveauXamXam[] = [{ id: 123 }];
        expectedResult = service.addNiveauXamXamToCollectionIfMissing(niveauXamXamCollection, ...niveauXamXamArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const niveauXamXam: INiveauXamXam = { id: 123 };
        const niveauXamXam2: INiveauXamXam = { id: 456 };
        expectedResult = service.addNiveauXamXamToCollectionIfMissing([], niveauXamXam, niveauXamXam2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(niveauXamXam);
        expect(expectedResult).toContain(niveauXamXam2);
      });

      it('should accept null and undefined values', () => {
        const niveauXamXam: INiveauXamXam = { id: 123 };
        expectedResult = service.addNiveauXamXamToCollectionIfMissing([], null, niveauXamXam, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(niveauXamXam);
      });

      it('should return initial array if no NiveauXamXam is added', () => {
        const niveauXamXamCollection: INiveauXamXam[] = [{ id: 123 }];
        expectedResult = service.addNiveauXamXamToCollectionIfMissing(niveauXamXamCollection, undefined, null);
        expect(expectedResult).toEqual(niveauXamXamCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
