import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { INiveauXamXam, getNiveauXamXamIdentifier } from '../niveau-xam-xam.model';

export type EntityResponseType = HttpResponse<INiveauXamXam>;
export type EntityArrayResponseType = HttpResponse<INiveauXamXam[]>;

@Injectable({ providedIn: 'root' })
export class NiveauXamXamService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/niveau-xam-xams');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(niveauXamXam: INiveauXamXam): Observable<EntityResponseType> {
    return this.http.post<INiveauXamXam>(this.resourceUrl, niveauXamXam, { observe: 'response' });
  }

  update(niveauXamXam: INiveauXamXam): Observable<EntityResponseType> {
    return this.http.put<INiveauXamXam>(`${this.resourceUrl}/${getNiveauXamXamIdentifier(niveauXamXam) as number}`, niveauXamXam, {
      observe: 'response',
    });
  }

  partialUpdate(niveauXamXam: INiveauXamXam): Observable<EntityResponseType> {
    return this.http.patch<INiveauXamXam>(`${this.resourceUrl}/${getNiveauXamXamIdentifier(niveauXamXam) as number}`, niveauXamXam, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<INiveauXamXam>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<INiveauXamXam[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addNiveauXamXamToCollectionIfMissing(
    niveauXamXamCollection: INiveauXamXam[],
    ...niveauXamXamsToCheck: (INiveauXamXam | null | undefined)[]
  ): INiveauXamXam[] {
    const niveauXamXams: INiveauXamXam[] = niveauXamXamsToCheck.filter(isPresent);
    if (niveauXamXams.length > 0) {
      const niveauXamXamCollectionIdentifiers = niveauXamXamCollection.map(
        niveauXamXamItem => getNiveauXamXamIdentifier(niveauXamXamItem)!
      );
      const niveauXamXamsToAdd = niveauXamXams.filter(niveauXamXamItem => {
        const niveauXamXamIdentifier = getNiveauXamXamIdentifier(niveauXamXamItem);
        if (niveauXamXamIdentifier == null || niveauXamXamCollectionIdentifiers.includes(niveauXamXamIdentifier)) {
          return false;
        }
        niveauXamXamCollectionIdentifiers.push(niveauXamXamIdentifier);
        return true;
      });
      return [...niveauXamXamsToAdd, ...niveauXamXamCollection];
    }
    return niveauXamXamCollection;
  }
}
