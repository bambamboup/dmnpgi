import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { INiveauXamXam } from '../niveau-xam-xam.model';
import { NiveauXamXamService } from '../service/niveau-xam-xam.service';

@Component({
  templateUrl: './niveau-xam-xam-delete-dialog.component.html',
})
export class NiveauXamXamDeleteDialogComponent {
  niveauXamXam?: INiveauXamXam;

  constructor(protected niveauXamXamService: NiveauXamXamService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.niveauXamXamService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
