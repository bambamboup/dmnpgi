import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { INiveauXamXam, NiveauXamXam } from '../niveau-xam-xam.model';
import { NiveauXamXamService } from '../service/niveau-xam-xam.service';

@Component({
  selector: 'jhi-niveau-xam-xam-update',
  templateUrl: './niveau-xam-xam-update.component.html',
})
export class NiveauXamXamUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    libelle: [null, [Validators.required]],
    poids: [null, [Validators.required]],
  });

  constructor(protected niveauXamXamService: NiveauXamXamService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ niveauXamXam }) => {
      this.updateForm(niveauXamXam);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const niveauXamXam = this.createFromForm();
    if (niveauXamXam.id !== undefined) {
      this.subscribeToSaveResponse(this.niveauXamXamService.update(niveauXamXam));
    } else {
      this.subscribeToSaveResponse(this.niveauXamXamService.create(niveauXamXam));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<INiveauXamXam>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(niveauXamXam: INiveauXamXam): void {
    this.editForm.patchValue({
      id: niveauXamXam.id,
      libelle: niveauXamXam.libelle,
      poids: niveauXamXam.poids,
    });
  }

  protected createFromForm(): INiveauXamXam {
    return {
      ...new NiveauXamXam(),
      id: this.editForm.get(['id'])!.value,
      libelle: this.editForm.get(['libelle'])!.value,
      poids: this.editForm.get(['poids'])!.value,
    };
  }
}
