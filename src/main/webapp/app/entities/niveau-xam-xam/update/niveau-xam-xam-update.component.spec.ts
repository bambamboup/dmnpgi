import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { NiveauXamXamService } from '../service/niveau-xam-xam.service';
import { INiveauXamXam, NiveauXamXam } from '../niveau-xam-xam.model';

import { NiveauXamXamUpdateComponent } from './niveau-xam-xam-update.component';

describe('NiveauXamXam Management Update Component', () => {
  let comp: NiveauXamXamUpdateComponent;
  let fixture: ComponentFixture<NiveauXamXamUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let niveauXamXamService: NiveauXamXamService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [NiveauXamXamUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(NiveauXamXamUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(NiveauXamXamUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    niveauXamXamService = TestBed.inject(NiveauXamXamService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const niveauXamXam: INiveauXamXam = { id: 456 };

      activatedRoute.data = of({ niveauXamXam });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(niveauXamXam));
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<NiveauXamXam>>();
      const niveauXamXam = { id: 123 };
      jest.spyOn(niveauXamXamService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ niveauXamXam });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: niveauXamXam }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(niveauXamXamService.update).toHaveBeenCalledWith(niveauXamXam);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<NiveauXamXam>>();
      const niveauXamXam = new NiveauXamXam();
      jest.spyOn(niveauXamXamService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ niveauXamXam });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: niveauXamXam }));
      saveSubject.complete();

      // THEN
      expect(niveauXamXamService.create).toHaveBeenCalledWith(niveauXamXam);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<NiveauXamXam>>();
      const niveauXamXam = { id: 123 };
      jest.spyOn(niveauXamXamService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ niveauXamXam });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(niveauXamXamService.update).toHaveBeenCalledWith(niveauXamXam);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
