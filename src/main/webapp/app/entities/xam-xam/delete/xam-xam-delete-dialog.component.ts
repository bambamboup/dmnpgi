import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IXamXam } from '../xam-xam.model';
import { XamXamService } from '../service/xam-xam.service';

@Component({
  templateUrl: './xam-xam-delete-dialog.component.html',
})
export class XamXamDeleteDialogComponent {
  xamXam?: IXamXam;

  constructor(protected xamXamService: XamXamService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.xamXamService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
