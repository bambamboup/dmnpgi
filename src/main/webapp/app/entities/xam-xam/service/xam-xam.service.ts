import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IXamXam, getXamXamIdentifier } from '../xam-xam.model';

export type EntityResponseType = HttpResponse<IXamXam>;
export type EntityArrayResponseType = HttpResponse<IXamXam[]>;

@Injectable({ providedIn: 'root' })
export class XamXamService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/xam-xams');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(xamXam: IXamXam): Observable<EntityResponseType> {
    return this.http.post<IXamXam>(this.resourceUrl, xamXam, { observe: 'response' });
  }

  update(xamXam: IXamXam): Observable<EntityResponseType> {
    return this.http.put<IXamXam>(`${this.resourceUrl}/${getXamXamIdentifier(xamXam) as number}`, xamXam, { observe: 'response' });
  }

  partialUpdate(xamXam: IXamXam): Observable<EntityResponseType> {
    return this.http.patch<IXamXam>(`${this.resourceUrl}/${getXamXamIdentifier(xamXam) as number}`, xamXam, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IXamXam>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IXamXam[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addXamXamToCollectionIfMissing(xamXamCollection: IXamXam[], ...xamXamsToCheck: (IXamXam | null | undefined)[]): IXamXam[] {
    const xamXams: IXamXam[] = xamXamsToCheck.filter(isPresent);
    if (xamXams.length > 0) {
      const xamXamCollectionIdentifiers = xamXamCollection.map(xamXamItem => getXamXamIdentifier(xamXamItem)!);
      const xamXamsToAdd = xamXams.filter(xamXamItem => {
        const xamXamIdentifier = getXamXamIdentifier(xamXamItem);
        if (xamXamIdentifier == null || xamXamCollectionIdentifiers.includes(xamXamIdentifier)) {
          return false;
        }
        xamXamCollectionIdentifiers.push(xamXamIdentifier);
        return true;
      });
      return [...xamXamsToAdd, ...xamXamCollection];
    }
    return xamXamCollection;
  }
}
