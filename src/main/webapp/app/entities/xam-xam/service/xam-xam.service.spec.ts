import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IXamXam, XamXam } from '../xam-xam.model';

import { XamXamService } from './xam-xam.service';

describe('XamXam Service', () => {
  let service: XamXamService;
  let httpMock: HttpTestingController;
  let elemDefault: IXamXam;
  let expectedResult: IXamXam | IXamXam[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(XamXamService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      libelle: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a XamXam', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new XamXam()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a XamXam', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          libelle: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a XamXam', () => {
      const patchObject = Object.assign({}, new XamXam());

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of XamXam', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          libelle: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a XamXam', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addXamXamToCollectionIfMissing', () => {
      it('should add a XamXam to an empty array', () => {
        const xamXam: IXamXam = { id: 123 };
        expectedResult = service.addXamXamToCollectionIfMissing([], xamXam);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(xamXam);
      });

      it('should not add a XamXam to an array that contains it', () => {
        const xamXam: IXamXam = { id: 123 };
        const xamXamCollection: IXamXam[] = [
          {
            ...xamXam,
          },
          { id: 456 },
        ];
        expectedResult = service.addXamXamToCollectionIfMissing(xamXamCollection, xamXam);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a XamXam to an array that doesn't contain it", () => {
        const xamXam: IXamXam = { id: 123 };
        const xamXamCollection: IXamXam[] = [{ id: 456 }];
        expectedResult = service.addXamXamToCollectionIfMissing(xamXamCollection, xamXam);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(xamXam);
      });

      it('should add only unique XamXam to an array', () => {
        const xamXamArray: IXamXam[] = [{ id: 123 }, { id: 456 }, { id: 94433 }];
        const xamXamCollection: IXamXam[] = [{ id: 123 }];
        expectedResult = service.addXamXamToCollectionIfMissing(xamXamCollection, ...xamXamArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const xamXam: IXamXam = { id: 123 };
        const xamXam2: IXamXam = { id: 456 };
        expectedResult = service.addXamXamToCollectionIfMissing([], xamXam, xamXam2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(xamXam);
        expect(expectedResult).toContain(xamXam2);
      });

      it('should accept null and undefined values', () => {
        const xamXam: IXamXam = { id: 123 };
        expectedResult = service.addXamXamToCollectionIfMissing([], null, xamXam, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(xamXam);
      });

      it('should return initial array if no XamXam is added', () => {
        const xamXamCollection: IXamXam[] = [{ id: 123 }];
        expectedResult = service.addXamXamToCollectionIfMissing(xamXamCollection, undefined, null);
        expect(expectedResult).toEqual(xamXamCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
