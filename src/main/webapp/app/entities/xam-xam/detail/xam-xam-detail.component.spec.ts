import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { XamXamDetailComponent } from './xam-xam-detail.component';

describe('XamXam Management Detail Component', () => {
  let comp: XamXamDetailComponent;
  let fixture: ComponentFixture<XamXamDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [XamXamDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ xamXam: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(XamXamDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(XamXamDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load xamXam on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.xamXam).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
