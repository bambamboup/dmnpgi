import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IXamXam } from '../xam-xam.model';

@Component({
  selector: 'jhi-xam-xam-detail',
  templateUrl: './xam-xam-detail.component.html',
})
export class XamXamDetailComponent implements OnInit {
  xamXam: IXamXam | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ xamXam }) => {
      this.xamXam = xamXam;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
