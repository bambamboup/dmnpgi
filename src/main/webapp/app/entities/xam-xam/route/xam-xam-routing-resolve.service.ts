import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IXamXam, XamXam } from '../xam-xam.model';
import { XamXamService } from '../service/xam-xam.service';

@Injectable({ providedIn: 'root' })
export class XamXamRoutingResolveService implements Resolve<IXamXam> {
  constructor(protected service: XamXamService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IXamXam> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((xamXam: HttpResponse<XamXam>) => {
          if (xamXam.body) {
            return of(xamXam.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new XamXam());
  }
}
