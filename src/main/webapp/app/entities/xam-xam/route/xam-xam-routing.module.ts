import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { XamXamComponent } from '../list/xam-xam.component';
import { XamXamDetailComponent } from '../detail/xam-xam-detail.component';
import { XamXamUpdateComponent } from '../update/xam-xam-update.component';
import { XamXamRoutingResolveService } from './xam-xam-routing-resolve.service';

const xamXamRoute: Routes = [
  {
    path: '',
    component: XamXamComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: XamXamDetailComponent,
    resolve: {
      xamXam: XamXamRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: XamXamUpdateComponent,
    resolve: {
      xamXam: XamXamRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: XamXamUpdateComponent,
    resolve: {
      xamXam: XamXamRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(xamXamRoute)],
  exports: [RouterModule],
})
export class XamXamRoutingModule {}
