import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IXamXam, XamXam } from '../xam-xam.model';
import { XamXamService } from '../service/xam-xam.service';
import { INiveauXamXam } from 'app/entities/niveau-xam-xam/niveau-xam-xam.model';
import { NiveauXamXamService } from 'app/entities/niveau-xam-xam/service/niveau-xam-xam.service';

@Component({
  selector: 'jhi-xam-xam-update',
  templateUrl: './xam-xam-update.component.html',
})
export class XamXamUpdateComponent implements OnInit {
  isSaving = false;

  niveauXamXamsSharedCollection: INiveauXamXam[] = [];

  editForm = this.fb.group({
    id: [],
    libelle: [null, [Validators.required]],
    niveauXamXam: [null, Validators.required],
  });

  constructor(
    protected xamXamService: XamXamService,
    protected niveauXamXamService: NiveauXamXamService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ xamXam }) => {
      this.updateForm(xamXam);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const xamXam = this.createFromForm();
    if (xamXam.id !== undefined) {
      this.subscribeToSaveResponse(this.xamXamService.update(xamXam));
    } else {
      this.subscribeToSaveResponse(this.xamXamService.create(xamXam));
    }
  }

  trackNiveauXamXamById(index: number, item: INiveauXamXam): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IXamXam>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(xamXam: IXamXam): void {
    this.editForm.patchValue({
      id: xamXam.id,
      libelle: xamXam.libelle,
      niveauXamXam: xamXam.niveauXamXam,
    });

    this.niveauXamXamsSharedCollection = this.niveauXamXamService.addNiveauXamXamToCollectionIfMissing(
      this.niveauXamXamsSharedCollection,
      xamXam.niveauXamXam
    );
  }

  protected loadRelationshipsOptions(): void {
    this.niveauXamXamService
      .query()
      .pipe(map((res: HttpResponse<INiveauXamXam[]>) => res.body ?? []))
      .pipe(
        map((niveauXamXams: INiveauXamXam[]) =>
          this.niveauXamXamService.addNiveauXamXamToCollectionIfMissing(niveauXamXams, this.editForm.get('niveauXamXam')!.value)
        )
      )
      .subscribe((niveauXamXams: INiveauXamXam[]) => (this.niveauXamXamsSharedCollection = niveauXamXams));
  }

  protected createFromForm(): IXamXam {
    return {
      ...new XamXam(),
      id: this.editForm.get(['id'])!.value,
      libelle: this.editForm.get(['libelle'])!.value,
      niveauXamXam: this.editForm.get(['niveauXamXam'])!.value,
    };
  }
}
