import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { XamXamService } from '../service/xam-xam.service';
import { IXamXam, XamXam } from '../xam-xam.model';
import { INiveauXamXam } from 'app/entities/niveau-xam-xam/niveau-xam-xam.model';
import { NiveauXamXamService } from 'app/entities/niveau-xam-xam/service/niveau-xam-xam.service';

import { XamXamUpdateComponent } from './xam-xam-update.component';

describe('XamXam Management Update Component', () => {
  let comp: XamXamUpdateComponent;
  let fixture: ComponentFixture<XamXamUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let xamXamService: XamXamService;
  let niveauXamXamService: NiveauXamXamService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [XamXamUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(XamXamUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(XamXamUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    xamXamService = TestBed.inject(XamXamService);
    niveauXamXamService = TestBed.inject(NiveauXamXamService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call NiveauXamXam query and add missing value', () => {
      const xamXam: IXamXam = { id: 456 };
      const niveauXamXam: INiveauXamXam = { id: 31365 };
      xamXam.niveauXamXam = niveauXamXam;

      const niveauXamXamCollection: INiveauXamXam[] = [{ id: 66541 }];
      jest.spyOn(niveauXamXamService, 'query').mockReturnValue(of(new HttpResponse({ body: niveauXamXamCollection })));
      const additionalNiveauXamXams = [niveauXamXam];
      const expectedCollection: INiveauXamXam[] = [...additionalNiveauXamXams, ...niveauXamXamCollection];
      jest.spyOn(niveauXamXamService, 'addNiveauXamXamToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ xamXam });
      comp.ngOnInit();

      expect(niveauXamXamService.query).toHaveBeenCalled();
      expect(niveauXamXamService.addNiveauXamXamToCollectionIfMissing).toHaveBeenCalledWith(
        niveauXamXamCollection,
        ...additionalNiveauXamXams
      );
      expect(comp.niveauXamXamsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const xamXam: IXamXam = { id: 456 };
      const niveauXamXam: INiveauXamXam = { id: 73421 };
      xamXam.niveauXamXam = niveauXamXam;

      activatedRoute.data = of({ xamXam });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(xamXam));
      expect(comp.niveauXamXamsSharedCollection).toContain(niveauXamXam);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<XamXam>>();
      const xamXam = { id: 123 };
      jest.spyOn(xamXamService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ xamXam });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: xamXam }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(xamXamService.update).toHaveBeenCalledWith(xamXam);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<XamXam>>();
      const xamXam = new XamXam();
      jest.spyOn(xamXamService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ xamXam });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: xamXam }));
      saveSubject.complete();

      // THEN
      expect(xamXamService.create).toHaveBeenCalledWith(xamXam);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<XamXam>>();
      const xamXam = { id: 123 };
      jest.spyOn(xamXamService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ xamXam });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(xamXamService.update).toHaveBeenCalledWith(xamXam);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackNiveauXamXamById', () => {
      it('Should return tracked NiveauXamXam primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackNiveauXamXamById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
