import { INiveauXamXam } from 'app/entities/niveau-xam-xam/niveau-xam-xam.model';

export interface IXamXam {
  id?: number;
  libelle?: string;
  niveauXamXam?: INiveauXamXam;
}

export class XamXam implements IXamXam {
  constructor(public id?: number, public libelle?: string, public niveauXamXam?: INiveauXamXam) {}
}

export function getXamXamIdentifier(xamXam: IXamXam): number | undefined {
  return xamXam.id;
}
