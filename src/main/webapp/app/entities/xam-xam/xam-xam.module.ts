import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { XamXamComponent } from './list/xam-xam.component';
import { XamXamDetailComponent } from './detail/xam-xam-detail.component';
import { XamXamUpdateComponent } from './update/xam-xam-update.component';
import { XamXamDeleteDialogComponent } from './delete/xam-xam-delete-dialog.component';
import { XamXamRoutingModule } from './route/xam-xam-routing.module';

@NgModule({
  imports: [SharedModule, XamXamRoutingModule],
  declarations: [XamXamComponent, XamXamDetailComponent, XamXamUpdateComponent, XamXamDeleteDialogComponent],
  entryComponents: [XamXamDeleteDialogComponent],
})
export class XamXamModule {}
