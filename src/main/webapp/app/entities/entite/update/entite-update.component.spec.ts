import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { EntiteService } from '../service/entite.service';
import { IEntite, Entite } from '../entite.model';

import { EntiteUpdateComponent } from './entite-update.component';

describe('Entite Management Update Component', () => {
  let comp: EntiteUpdateComponent;
  let fixture: ComponentFixture<EntiteUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let entiteService: EntiteService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [EntiteUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(EntiteUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(EntiteUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    entiteService = TestBed.inject(EntiteService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const entite: IEntite = { id: 456 };

      activatedRoute.data = of({ entite });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(entite));
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Entite>>();
      const entite = { id: 123 };
      jest.spyOn(entiteService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ entite });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: entite }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(entiteService.update).toHaveBeenCalledWith(entite);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Entite>>();
      const entite = new Entite();
      jest.spyOn(entiteService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ entite });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: entite }));
      saveSubject.complete();

      // THEN
      expect(entiteService.create).toHaveBeenCalledWith(entite);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Entite>>();
      const entite = { id: 123 };
      jest.spyOn(entiteService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ entite });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(entiteService.update).toHaveBeenCalledWith(entite);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
