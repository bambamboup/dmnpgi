import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IEntite, Entite } from '../entite.model';
import { EntiteService } from '../service/entite.service';
import { EnumTypeEntite } from 'app/entities/enumerations/enum-type-entite.model';

@Component({
  selector: 'jhi-entite-update',
  templateUrl: './entite-update.component.html',
})
export class EntiteUpdateComponent implements OnInit {
  isSaving = false;
  enumTypeEntiteValues = Object.keys(EnumTypeEntite);

  editForm = this.fb.group({
    id: [],
    nom: [null, [Validators.required]],
    description: [],
    typeEntite: [null, [Validators.required]],
  });

  constructor(protected entiteService: EntiteService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ entite }) => {
      this.updateForm(entite);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const entite = this.createFromForm();
    if (entite.id !== undefined) {
      this.subscribeToSaveResponse(this.entiteService.update(entite));
    } else {
      this.subscribeToSaveResponse(this.entiteService.create(entite));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IEntite>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(entite: IEntite): void {
    this.editForm.patchValue({
      id: entite.id,
      nom: entite.nom,
      description: entite.description,
      typeEntite: entite.typeEntite,
    });
  }

  protected createFromForm(): IEntite {
    return {
      ...new Entite(),
      id: this.editForm.get(['id'])!.value,
      nom: this.editForm.get(['nom'])!.value,
      description: this.editForm.get(['description'])!.value,
      typeEntite: this.editForm.get(['typeEntite'])!.value,
    };
  }
}
