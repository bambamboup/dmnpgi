import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { EnumTypeEntite } from 'app/entities/enumerations/enum-type-entite.model';
import { IEntite, Entite } from '../entite.model';

import { EntiteService } from './entite.service';

describe('Entite Service', () => {
  let service: EntiteService;
  let httpMock: HttpTestingController;
  let elemDefault: IEntite;
  let expectedResult: IEntite | IEntite[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(EntiteService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      nom: 'AAAAAAA',
      description: 'AAAAAAA',
      typeEntite: EnumTypeEntite.CELLULE,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a Entite', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new Entite()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Entite', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          nom: 'BBBBBB',
          description: 'BBBBBB',
          typeEntite: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Entite', () => {
      const patchObject = Object.assign(
        {
          description: 'BBBBBB',
        },
        new Entite()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Entite', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          nom: 'BBBBBB',
          description: 'BBBBBB',
          typeEntite: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a Entite', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addEntiteToCollectionIfMissing', () => {
      it('should add a Entite to an empty array', () => {
        const entite: IEntite = { id: 123 };
        expectedResult = service.addEntiteToCollectionIfMissing([], entite);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(entite);
      });

      it('should not add a Entite to an array that contains it', () => {
        const entite: IEntite = { id: 123 };
        const entiteCollection: IEntite[] = [
          {
            ...entite,
          },
          { id: 456 },
        ];
        expectedResult = service.addEntiteToCollectionIfMissing(entiteCollection, entite);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Entite to an array that doesn't contain it", () => {
        const entite: IEntite = { id: 123 };
        const entiteCollection: IEntite[] = [{ id: 456 }];
        expectedResult = service.addEntiteToCollectionIfMissing(entiteCollection, entite);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(entite);
      });

      it('should add only unique Entite to an array', () => {
        const entiteArray: IEntite[] = [{ id: 123 }, { id: 456 }, { id: 69037 }];
        const entiteCollection: IEntite[] = [{ id: 123 }];
        expectedResult = service.addEntiteToCollectionIfMissing(entiteCollection, ...entiteArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const entite: IEntite = { id: 123 };
        const entite2: IEntite = { id: 456 };
        expectedResult = service.addEntiteToCollectionIfMissing([], entite, entite2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(entite);
        expect(expectedResult).toContain(entite2);
      });

      it('should accept null and undefined values', () => {
        const entite: IEntite = { id: 123 };
        expectedResult = service.addEntiteToCollectionIfMissing([], null, entite, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(entite);
      });

      it('should return initial array if no Entite is added', () => {
        const entiteCollection: IEntite[] = [{ id: 123 }];
        expectedResult = service.addEntiteToCollectionIfMissing(entiteCollection, undefined, null);
        expect(expectedResult).toEqual(entiteCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
