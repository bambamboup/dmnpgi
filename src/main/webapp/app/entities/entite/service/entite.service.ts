import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IEntite, getEntiteIdentifier } from '../entite.model';

export type EntityResponseType = HttpResponse<IEntite>;
export type EntityArrayResponseType = HttpResponse<IEntite[]>;

@Injectable({ providedIn: 'root' })
export class EntiteService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/entites');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(entite: IEntite): Observable<EntityResponseType> {
    return this.http.post<IEntite>(this.resourceUrl, entite, { observe: 'response' });
  }

  update(entite: IEntite): Observable<EntityResponseType> {
    return this.http.put<IEntite>(`${this.resourceUrl}/${getEntiteIdentifier(entite) as number}`, entite, { observe: 'response' });
  }

  partialUpdate(entite: IEntite): Observable<EntityResponseType> {
    return this.http.patch<IEntite>(`${this.resourceUrl}/${getEntiteIdentifier(entite) as number}`, entite, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IEntite>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IEntite[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addEntiteToCollectionIfMissing(entiteCollection: IEntite[], ...entitesToCheck: (IEntite | null | undefined)[]): IEntite[] {
    const entites: IEntite[] = entitesToCheck.filter(isPresent);
    if (entites.length > 0) {
      const entiteCollectionIdentifiers = entiteCollection.map(entiteItem => getEntiteIdentifier(entiteItem)!);
      const entitesToAdd = entites.filter(entiteItem => {
        const entiteIdentifier = getEntiteIdentifier(entiteItem);
        if (entiteIdentifier == null || entiteCollectionIdentifiers.includes(entiteIdentifier)) {
          return false;
        }
        entiteCollectionIdentifiers.push(entiteIdentifier);
        return true;
      });
      return [...entitesToAdd, ...entiteCollection];
    }
    return entiteCollection;
  }
}
