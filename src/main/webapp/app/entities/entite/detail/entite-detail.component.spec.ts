import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { EntiteDetailComponent } from './entite-detail.component';

describe('Entite Management Detail Component', () => {
  let comp: EntiteDetailComponent;
  let fixture: ComponentFixture<EntiteDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EntiteDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ entite: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(EntiteDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(EntiteDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load entite on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.entite).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
