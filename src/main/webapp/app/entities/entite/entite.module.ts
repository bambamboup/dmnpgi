import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { EntiteComponent } from './list/entite.component';
import { EntiteDetailComponent } from './detail/entite-detail.component';
import { EntiteUpdateComponent } from './update/entite-update.component';
import { EntiteDeleteDialogComponent } from './delete/entite-delete-dialog.component';
import { EntiteRoutingModule } from './route/entite-routing.module';

@NgModule({
  imports: [SharedModule, EntiteRoutingModule],
  declarations: [EntiteComponent, EntiteDetailComponent, EntiteUpdateComponent, EntiteDeleteDialogComponent],
  entryComponents: [EntiteDeleteDialogComponent],
})
export class EntiteModule {}
