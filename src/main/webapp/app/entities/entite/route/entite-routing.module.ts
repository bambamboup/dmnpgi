import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { EntiteComponent } from '../list/entite.component';
import { EntiteDetailComponent } from '../detail/entite-detail.component';
import { EntiteUpdateComponent } from '../update/entite-update.component';
import { EntiteRoutingResolveService } from './entite-routing-resolve.service';

const entiteRoute: Routes = [
  {
    path: '',
    component: EntiteComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: EntiteDetailComponent,
    resolve: {
      entite: EntiteRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: EntiteUpdateComponent,
    resolve: {
      entite: EntiteRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: EntiteUpdateComponent,
    resolve: {
      entite: EntiteRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(entiteRoute)],
  exports: [RouterModule],
})
export class EntiteRoutingModule {}
