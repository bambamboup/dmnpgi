import { EnumTypeEntite } from 'app/entities/enumerations/enum-type-entite.model';

export interface IEntite {
  id?: number;
  nom?: string;
  description?: string | null;
  typeEntite?: EnumTypeEntite;
}

export class Entite implements IEntite {
  constructor(public id?: number, public nom?: string, public description?: string | null, public typeEntite?: EnumTypeEntite) {}
}

export function getEntiteIdentifier(entite: IEntite): number | undefined {
  return entite.id;
}
