import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IEntite } from '../entite.model';
import { EntiteService } from '../service/entite.service';

@Component({
  templateUrl: './entite-delete-dialog.component.html',
})
export class EntiteDeleteDialogComponent {
  entite?: IEntite;

  constructor(protected entiteService: EntiteService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.entiteService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
