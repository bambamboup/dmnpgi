export interface ILocalite {
  id?: number;
  nom?: string;
}

export class Localite implements ILocalite {
  constructor(public id?: number, public nom?: string) {}
}

export function getLocaliteIdentifier(localite: ILocalite): number | undefined {
  return localite.id;
}
