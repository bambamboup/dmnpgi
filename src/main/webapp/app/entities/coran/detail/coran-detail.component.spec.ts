import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { CoranDetailComponent } from './coran-detail.component';

describe('Coran Management Detail Component', () => {
  let comp: CoranDetailComponent;
  let fixture: ComponentFixture<CoranDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CoranDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ coran: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(CoranDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(CoranDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load coran on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.coran).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
