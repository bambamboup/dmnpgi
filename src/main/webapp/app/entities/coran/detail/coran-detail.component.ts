import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICoran } from '../coran.model';

@Component({
  selector: 'jhi-coran-detail',
  templateUrl: './coran-detail.component.html',
})
export class CoranDetailComponent implements OnInit {
  coran: ICoran | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ coran }) => {
      this.coran = coran;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
