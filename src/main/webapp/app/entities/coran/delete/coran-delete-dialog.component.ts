import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ICoran } from '../coran.model';
import { CoranService } from '../service/coran.service';

@Component({
  templateUrl: './coran-delete-dialog.component.html',
})
export class CoranDeleteDialogComponent {
  coran?: ICoran;

  constructor(protected coranService: CoranService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.coranService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
