import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ICoran, Coran } from '../coran.model';
import { CoranService } from '../service/coran.service';

@Injectable({ providedIn: 'root' })
export class CoranRoutingResolveService implements Resolve<ICoran> {
  constructor(protected service: CoranService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICoran> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((coran: HttpResponse<Coran>) => {
          if (coran.body) {
            return of(coran.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Coran());
  }
}
