import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { CoranComponent } from '../list/coran.component';
import { CoranDetailComponent } from '../detail/coran-detail.component';
import { CoranUpdateComponent } from '../update/coran-update.component';
import { CoranRoutingResolveService } from './coran-routing-resolve.service';

const coranRoute: Routes = [
  {
    path: '',
    component: CoranComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CoranDetailComponent,
    resolve: {
      coran: CoranRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CoranUpdateComponent,
    resolve: {
      coran: CoranRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CoranUpdateComponent,
    resolve: {
      coran: CoranRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(coranRoute)],
  exports: [RouterModule],
})
export class CoranRoutingModule {}
