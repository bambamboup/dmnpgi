import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { CoranComponent } from './list/coran.component';
import { CoranDetailComponent } from './detail/coran-detail.component';
import { CoranUpdateComponent } from './update/coran-update.component';
import { CoranDeleteDialogComponent } from './delete/coran-delete-dialog.component';
import { CoranRoutingModule } from './route/coran-routing.module';

@NgModule({
  imports: [SharedModule, CoranRoutingModule],
  declarations: [CoranComponent, CoranDetailComponent, CoranUpdateComponent, CoranDeleteDialogComponent],
  entryComponents: [CoranDeleteDialogComponent],
})
export class CoranModule {}
