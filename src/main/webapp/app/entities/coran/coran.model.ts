export interface ICoran {
  id?: number;
  libelle?: string;
}

export class Coran implements ICoran {
  constructor(public id?: number, public libelle?: string) {}
}

export function getCoranIdentifier(coran: ICoran): number | undefined {
  return coran.id;
}
