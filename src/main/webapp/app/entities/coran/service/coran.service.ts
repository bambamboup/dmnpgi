import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ICoran, getCoranIdentifier } from '../coran.model';

export type EntityResponseType = HttpResponse<ICoran>;
export type EntityArrayResponseType = HttpResponse<ICoran[]>;

@Injectable({ providedIn: 'root' })
export class CoranService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/corans');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(coran: ICoran): Observable<EntityResponseType> {
    return this.http.post<ICoran>(this.resourceUrl, coran, { observe: 'response' });
  }

  update(coran: ICoran): Observable<EntityResponseType> {
    return this.http.put<ICoran>(`${this.resourceUrl}/${getCoranIdentifier(coran) as number}`, coran, { observe: 'response' });
  }

  partialUpdate(coran: ICoran): Observable<EntityResponseType> {
    return this.http.patch<ICoran>(`${this.resourceUrl}/${getCoranIdentifier(coran) as number}`, coran, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICoran>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICoran[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addCoranToCollectionIfMissing(coranCollection: ICoran[], ...coransToCheck: (ICoran | null | undefined)[]): ICoran[] {
    const corans: ICoran[] = coransToCheck.filter(isPresent);
    if (corans.length > 0) {
      const coranCollectionIdentifiers = coranCollection.map(coranItem => getCoranIdentifier(coranItem)!);
      const coransToAdd = corans.filter(coranItem => {
        const coranIdentifier = getCoranIdentifier(coranItem);
        if (coranIdentifier == null || coranCollectionIdentifiers.includes(coranIdentifier)) {
          return false;
        }
        coranCollectionIdentifiers.push(coranIdentifier);
        return true;
      });
      return [...coransToAdd, ...coranCollection];
    }
    return coranCollection;
  }
}
