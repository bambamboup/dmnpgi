import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ICoran, Coran } from '../coran.model';

import { CoranService } from './coran.service';

describe('Coran Service', () => {
  let service: CoranService;
  let httpMock: HttpTestingController;
  let elemDefault: ICoran;
  let expectedResult: ICoran | ICoran[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(CoranService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      libelle: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a Coran', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new Coran()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Coran', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          libelle: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Coran', () => {
      const patchObject = Object.assign(
        {
          libelle: 'BBBBBB',
        },
        new Coran()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Coran', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          libelle: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a Coran', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addCoranToCollectionIfMissing', () => {
      it('should add a Coran to an empty array', () => {
        const coran: ICoran = { id: 123 };
        expectedResult = service.addCoranToCollectionIfMissing([], coran);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(coran);
      });

      it('should not add a Coran to an array that contains it', () => {
        const coran: ICoran = { id: 123 };
        const coranCollection: ICoran[] = [
          {
            ...coran,
          },
          { id: 456 },
        ];
        expectedResult = service.addCoranToCollectionIfMissing(coranCollection, coran);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Coran to an array that doesn't contain it", () => {
        const coran: ICoran = { id: 123 };
        const coranCollection: ICoran[] = [{ id: 456 }];
        expectedResult = service.addCoranToCollectionIfMissing(coranCollection, coran);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(coran);
      });

      it('should add only unique Coran to an array', () => {
        const coranArray: ICoran[] = [{ id: 123 }, { id: 456 }, { id: 8614 }];
        const coranCollection: ICoran[] = [{ id: 123 }];
        expectedResult = service.addCoranToCollectionIfMissing(coranCollection, ...coranArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const coran: ICoran = { id: 123 };
        const coran2: ICoran = { id: 456 };
        expectedResult = service.addCoranToCollectionIfMissing([], coran, coran2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(coran);
        expect(expectedResult).toContain(coran2);
      });

      it('should accept null and undefined values', () => {
        const coran: ICoran = { id: 123 };
        expectedResult = service.addCoranToCollectionIfMissing([], null, coran, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(coran);
      });

      it('should return initial array if no Coran is added', () => {
        const coranCollection: ICoran[] = [{ id: 123 }];
        expectedResult = service.addCoranToCollectionIfMissing(coranCollection, undefined, null);
        expect(expectedResult).toEqual(coranCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
