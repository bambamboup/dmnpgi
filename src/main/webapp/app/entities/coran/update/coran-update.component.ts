import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { ICoran, Coran } from '../coran.model';
import { CoranService } from '../service/coran.service';

@Component({
  selector: 'jhi-coran-update',
  templateUrl: './coran-update.component.html',
})
export class CoranUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    libelle: [null, [Validators.required]],
  });

  constructor(protected coranService: CoranService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ coran }) => {
      this.updateForm(coran);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const coran = this.createFromForm();
    if (coran.id !== undefined) {
      this.subscribeToSaveResponse(this.coranService.update(coran));
    } else {
      this.subscribeToSaveResponse(this.coranService.create(coran));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICoran>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(coran: ICoran): void {
    this.editForm.patchValue({
      id: coran.id,
      libelle: coran.libelle,
    });
  }

  protected createFromForm(): ICoran {
    return {
      ...new Coran(),
      id: this.editForm.get(['id'])!.value,
      libelle: this.editForm.get(['libelle'])!.value,
    };
  }
}
