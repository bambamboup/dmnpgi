import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { CoranService } from '../service/coran.service';
import { ICoran, Coran } from '../coran.model';

import { CoranUpdateComponent } from './coran-update.component';

describe('Coran Management Update Component', () => {
  let comp: CoranUpdateComponent;
  let fixture: ComponentFixture<CoranUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let coranService: CoranService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [CoranUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(CoranUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(CoranUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    coranService = TestBed.inject(CoranService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const coran: ICoran = { id: 456 };

      activatedRoute.data = of({ coran });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(coran));
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Coran>>();
      const coran = { id: 123 };
      jest.spyOn(coranService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ coran });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: coran }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(coranService.update).toHaveBeenCalledWith(coran);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Coran>>();
      const coran = new Coran();
      jest.spyOn(coranService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ coran });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: coran }));
      saveSubject.complete();

      // THEN
      expect(coranService.create).toHaveBeenCalledWith(coran);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Coran>>();
      const coran = { id: 123 };
      jest.spyOn(coranService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ coran });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(coranService.update).toHaveBeenCalledWith(coran);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
