export interface IFaculte {
  id?: number;
  nom?: string;
}

export class Faculte implements IFaculte {
  constructor(public id?: number, public nom?: string) {}
}

export function getFaculteIdentifier(faculte: IFaculte): number | undefined {
  return faculte.id;
}
