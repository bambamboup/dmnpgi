package dmn.ucad.domain;

import static org.assertj.core.api.Assertions.assertThat;

import dmn.ucad.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class CaisseMoisTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CaisseMois.class);
        CaisseMois caisseMois1 = new CaisseMois();
        caisseMois1.setId(1L);
        CaisseMois caisseMois2 = new CaisseMois();
        caisseMois2.setId(caisseMois1.getId());
        assertThat(caisseMois1).isEqualTo(caisseMois2);
        caisseMois2.setId(2L);
        assertThat(caisseMois1).isNotEqualTo(caisseMois2);
        caisseMois1.setId(null);
        assertThat(caisseMois1).isNotEqualTo(caisseMois2);
    }
}
