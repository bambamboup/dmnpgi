package dmn.ucad.domain;

import static org.assertj.core.api.Assertions.assertThat;

import dmn.ucad.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class NiveauXamXamTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(NiveauXamXam.class);
        NiveauXamXam niveauXamXam1 = new NiveauXamXam();
        niveauXamXam1.setId(1L);
        NiveauXamXam niveauXamXam2 = new NiveauXamXam();
        niveauXamXam2.setId(niveauXamXam1.getId());
        assertThat(niveauXamXam1).isEqualTo(niveauXamXam2);
        niveauXamXam2.setId(2L);
        assertThat(niveauXamXam1).isNotEqualTo(niveauXamXam2);
        niveauXamXam1.setId(null);
        assertThat(niveauXamXam1).isNotEqualTo(niveauXamXam2);
    }
}
