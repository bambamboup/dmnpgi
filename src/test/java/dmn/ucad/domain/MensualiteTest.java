package dmn.ucad.domain;

import static org.assertj.core.api.Assertions.assertThat;

import dmn.ucad.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class MensualiteTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Mensualite.class);
        Mensualite mensualite1 = new Mensualite();
        mensualite1.setId(1L);
        Mensualite mensualite2 = new Mensualite();
        mensualite2.setId(mensualite1.getId());
        assertThat(mensualite1).isEqualTo(mensualite2);
        mensualite2.setId(2L);
        assertThat(mensualite1).isNotEqualTo(mensualite2);
        mensualite1.setId(null);
        assertThat(mensualite1).isNotEqualTo(mensualite2);
    }
}
