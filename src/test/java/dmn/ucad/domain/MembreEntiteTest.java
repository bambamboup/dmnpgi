package dmn.ucad.domain;

import static org.assertj.core.api.Assertions.assertThat;

import dmn.ucad.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class MembreEntiteTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MembreEntite.class);
        MembreEntite membreEntite1 = new MembreEntite();
        membreEntite1.setId(1L);
        MembreEntite membreEntite2 = new MembreEntite();
        membreEntite2.setId(membreEntite1.getId());
        assertThat(membreEntite1).isEqualTo(membreEntite2);
        membreEntite2.setId(2L);
        assertThat(membreEntite1).isNotEqualTo(membreEntite2);
        membreEntite1.setId(null);
        assertThat(membreEntite1).isNotEqualTo(membreEntite2);
    }
}
