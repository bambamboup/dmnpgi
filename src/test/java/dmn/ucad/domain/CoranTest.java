package dmn.ucad.domain;

import static org.assertj.core.api.Assertions.assertThat;

import dmn.ucad.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class CoranTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Coran.class);
        Coran coran1 = new Coran();
        coran1.setId(1L);
        Coran coran2 = new Coran();
        coran2.setId(coran1.getId());
        assertThat(coran1).isEqualTo(coran2);
        coran2.setId(2L);
        assertThat(coran1).isNotEqualTo(coran2);
        coran1.setId(null);
        assertThat(coran1).isNotEqualTo(coran2);
    }
}
