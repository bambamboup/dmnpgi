package dmn.ucad.domain;

import static org.assertj.core.api.Assertions.assertThat;

import dmn.ucad.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class XamXamTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(XamXam.class);
        XamXam xamXam1 = new XamXam();
        xamXam1.setId(1L);
        XamXam xamXam2 = new XamXam();
        xamXam2.setId(xamXam1.getId());
        assertThat(xamXam1).isEqualTo(xamXam2);
        xamXam2.setId(2L);
        assertThat(xamXam1).isNotEqualTo(xamXam2);
        xamXam1.setId(null);
        assertThat(xamXam1).isNotEqualTo(xamXam2);
    }
}
