package dmn.ucad.domain;

import static org.assertj.core.api.Assertions.assertThat;

import dmn.ucad.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class MembreKourelTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MembreKourel.class);
        MembreKourel membreKourel1 = new MembreKourel();
        membreKourel1.setId(1L);
        MembreKourel membreKourel2 = new MembreKourel();
        membreKourel2.setId(membreKourel1.getId());
        assertThat(membreKourel1).isEqualTo(membreKourel2);
        membreKourel2.setId(2L);
        assertThat(membreKourel1).isNotEqualTo(membreKourel2);
        membreKourel1.setId(null);
        assertThat(membreKourel1).isNotEqualTo(membreKourel2);
    }
}
