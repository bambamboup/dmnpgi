package dmn.ucad.domain;

import static org.assertj.core.api.Assertions.assertThat;

import dmn.ucad.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class PresidentTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(President.class);
        President president1 = new President();
        president1.setId(1L);
        President president2 = new President();
        president2.setId(president1.getId());
        assertThat(president1).isEqualTo(president2);
        president2.setId(2L);
        assertThat(president1).isNotEqualTo(president2);
        president1.setId(null);
        assertThat(president1).isNotEqualTo(president2);
    }
}
