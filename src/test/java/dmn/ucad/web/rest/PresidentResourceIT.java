package dmn.ucad.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import dmn.ucad.IntegrationTest;
import dmn.ucad.domain.Entite;
import dmn.ucad.domain.Membre;
import dmn.ucad.domain.President;
import dmn.ucad.repository.PresidentRepository;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link PresidentResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class PresidentResourceIT {

    private static final LocalDate DEFAULT_DATE_DEBUT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_DEBUT = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_DATE_FIN = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_FIN = LocalDate.now(ZoneId.systemDefault());

    private static final Boolean DEFAULT_ACTIF = false;
    private static final Boolean UPDATED_ACTIF = true;

    private static final String ENTITY_API_URL = "/api/presidents";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PresidentRepository presidentRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPresidentMockMvc;

    private President president;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static President createEntity(EntityManager em) {
        President president = new President().dateDebut(DEFAULT_DATE_DEBUT).dateFin(DEFAULT_DATE_FIN).actif(DEFAULT_ACTIF);
        // Add required entity
        Membre membre;
        if (TestUtil.findAll(em, Membre.class).isEmpty()) {
            membre = MembreResourceIT.createEntity(em);
            em.persist(membre);
            em.flush();
        } else {
            membre = TestUtil.findAll(em, Membre.class).get(0);
        }
        president.setMembre(membre);
        // Add required entity
        Entite entite;
        if (TestUtil.findAll(em, Entite.class).isEmpty()) {
            entite = EntiteResourceIT.createEntity(em);
            em.persist(entite);
            em.flush();
        } else {
            entite = TestUtil.findAll(em, Entite.class).get(0);
        }
        president.setEntite(entite);
        return president;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static President createUpdatedEntity(EntityManager em) {
        President president = new President().dateDebut(UPDATED_DATE_DEBUT).dateFin(UPDATED_DATE_FIN).actif(UPDATED_ACTIF);
        // Add required entity
        Membre membre;
        if (TestUtil.findAll(em, Membre.class).isEmpty()) {
            membre = MembreResourceIT.createUpdatedEntity(em);
            em.persist(membre);
            em.flush();
        } else {
            membre = TestUtil.findAll(em, Membre.class).get(0);
        }
        president.setMembre(membre);
        // Add required entity
        Entite entite;
        if (TestUtil.findAll(em, Entite.class).isEmpty()) {
            entite = EntiteResourceIT.createUpdatedEntity(em);
            em.persist(entite);
            em.flush();
        } else {
            entite = TestUtil.findAll(em, Entite.class).get(0);
        }
        president.setEntite(entite);
        return president;
    }

    @BeforeEach
    public void initTest() {
        president = createEntity(em);
    }

    @Test
    @Transactional
    void createPresident() throws Exception {
        int databaseSizeBeforeCreate = presidentRepository.findAll().size();
        // Create the President
        restPresidentMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(president)))
            .andExpect(status().isCreated());

        // Validate the President in the database
        List<President> presidentList = presidentRepository.findAll();
        assertThat(presidentList).hasSize(databaseSizeBeforeCreate + 1);
        President testPresident = presidentList.get(presidentList.size() - 1);
        assertThat(testPresident.getDateDebut()).isEqualTo(DEFAULT_DATE_DEBUT);
        assertThat(testPresident.getDateFin()).isEqualTo(DEFAULT_DATE_FIN);
        assertThat(testPresident.getActif()).isEqualTo(DEFAULT_ACTIF);
    }

    @Test
    @Transactional
    void createPresidentWithExistingId() throws Exception {
        // Create the President with an existing ID
        president.setId(1L);

        int databaseSizeBeforeCreate = presidentRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restPresidentMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(president)))
            .andExpect(status().isBadRequest());

        // Validate the President in the database
        List<President> presidentList = presidentRepository.findAll();
        assertThat(presidentList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkDateDebutIsRequired() throws Exception {
        int databaseSizeBeforeTest = presidentRepository.findAll().size();
        // set the field null
        president.setDateDebut(null);

        // Create the President, which fails.

        restPresidentMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(president)))
            .andExpect(status().isBadRequest());

        List<President> presidentList = presidentRepository.findAll();
        assertThat(presidentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllPresidents() throws Exception {
        // Initialize the database
        presidentRepository.saveAndFlush(president);

        // Get all the presidentList
        restPresidentMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(president.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateDebut").value(hasItem(DEFAULT_DATE_DEBUT.toString())))
            .andExpect(jsonPath("$.[*].dateFin").value(hasItem(DEFAULT_DATE_FIN.toString())))
            .andExpect(jsonPath("$.[*].actif").value(hasItem(DEFAULT_ACTIF.booleanValue())));
    }

    @Test
    @Transactional
    void getPresident() throws Exception {
        // Initialize the database
        presidentRepository.saveAndFlush(president);

        // Get the president
        restPresidentMockMvc
            .perform(get(ENTITY_API_URL_ID, president.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(president.getId().intValue()))
            .andExpect(jsonPath("$.dateDebut").value(DEFAULT_DATE_DEBUT.toString()))
            .andExpect(jsonPath("$.dateFin").value(DEFAULT_DATE_FIN.toString()))
            .andExpect(jsonPath("$.actif").value(DEFAULT_ACTIF.booleanValue()));
    }

    @Test
    @Transactional
    void getNonExistingPresident() throws Exception {
        // Get the president
        restPresidentMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewPresident() throws Exception {
        // Initialize the database
        presidentRepository.saveAndFlush(president);

        int databaseSizeBeforeUpdate = presidentRepository.findAll().size();

        // Update the president
        President updatedPresident = presidentRepository.findById(president.getId()).get();
        // Disconnect from session so that the updates on updatedPresident are not directly saved in db
        em.detach(updatedPresident);
        updatedPresident.dateDebut(UPDATED_DATE_DEBUT).dateFin(UPDATED_DATE_FIN).actif(UPDATED_ACTIF);

        restPresidentMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedPresident.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedPresident))
            )
            .andExpect(status().isOk());

        // Validate the President in the database
        List<President> presidentList = presidentRepository.findAll();
        assertThat(presidentList).hasSize(databaseSizeBeforeUpdate);
        President testPresident = presidentList.get(presidentList.size() - 1);
        assertThat(testPresident.getDateDebut()).isEqualTo(UPDATED_DATE_DEBUT);
        assertThat(testPresident.getDateFin()).isEqualTo(UPDATED_DATE_FIN);
        assertThat(testPresident.getActif()).isEqualTo(UPDATED_ACTIF);
    }

    @Test
    @Transactional
    void putNonExistingPresident() throws Exception {
        int databaseSizeBeforeUpdate = presidentRepository.findAll().size();
        president.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPresidentMockMvc
            .perform(
                put(ENTITY_API_URL_ID, president.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(president))
            )
            .andExpect(status().isBadRequest());

        // Validate the President in the database
        List<President> presidentList = presidentRepository.findAll();
        assertThat(presidentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchPresident() throws Exception {
        int databaseSizeBeforeUpdate = presidentRepository.findAll().size();
        president.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPresidentMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(president))
            )
            .andExpect(status().isBadRequest());

        // Validate the President in the database
        List<President> presidentList = presidentRepository.findAll();
        assertThat(presidentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamPresident() throws Exception {
        int databaseSizeBeforeUpdate = presidentRepository.findAll().size();
        president.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPresidentMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(president)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the President in the database
        List<President> presidentList = presidentRepository.findAll();
        assertThat(presidentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdatePresidentWithPatch() throws Exception {
        // Initialize the database
        presidentRepository.saveAndFlush(president);

        int databaseSizeBeforeUpdate = presidentRepository.findAll().size();

        // Update the president using partial update
        President partialUpdatedPresident = new President();
        partialUpdatedPresident.setId(president.getId());

        partialUpdatedPresident.dateFin(UPDATED_DATE_FIN);

        restPresidentMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPresident.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPresident))
            )
            .andExpect(status().isOk());

        // Validate the President in the database
        List<President> presidentList = presidentRepository.findAll();
        assertThat(presidentList).hasSize(databaseSizeBeforeUpdate);
        President testPresident = presidentList.get(presidentList.size() - 1);
        assertThat(testPresident.getDateDebut()).isEqualTo(DEFAULT_DATE_DEBUT);
        assertThat(testPresident.getDateFin()).isEqualTo(UPDATED_DATE_FIN);
        assertThat(testPresident.getActif()).isEqualTo(DEFAULT_ACTIF);
    }

    @Test
    @Transactional
    void fullUpdatePresidentWithPatch() throws Exception {
        // Initialize the database
        presidentRepository.saveAndFlush(president);

        int databaseSizeBeforeUpdate = presidentRepository.findAll().size();

        // Update the president using partial update
        President partialUpdatedPresident = new President();
        partialUpdatedPresident.setId(president.getId());

        partialUpdatedPresident.dateDebut(UPDATED_DATE_DEBUT).dateFin(UPDATED_DATE_FIN).actif(UPDATED_ACTIF);

        restPresidentMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPresident.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPresident))
            )
            .andExpect(status().isOk());

        // Validate the President in the database
        List<President> presidentList = presidentRepository.findAll();
        assertThat(presidentList).hasSize(databaseSizeBeforeUpdate);
        President testPresident = presidentList.get(presidentList.size() - 1);
        assertThat(testPresident.getDateDebut()).isEqualTo(UPDATED_DATE_DEBUT);
        assertThat(testPresident.getDateFin()).isEqualTo(UPDATED_DATE_FIN);
        assertThat(testPresident.getActif()).isEqualTo(UPDATED_ACTIF);
    }

    @Test
    @Transactional
    void patchNonExistingPresident() throws Exception {
        int databaseSizeBeforeUpdate = presidentRepository.findAll().size();
        president.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPresidentMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, president.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(president))
            )
            .andExpect(status().isBadRequest());

        // Validate the President in the database
        List<President> presidentList = presidentRepository.findAll();
        assertThat(presidentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchPresident() throws Exception {
        int databaseSizeBeforeUpdate = presidentRepository.findAll().size();
        president.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPresidentMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(president))
            )
            .andExpect(status().isBadRequest());

        // Validate the President in the database
        List<President> presidentList = presidentRepository.findAll();
        assertThat(presidentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamPresident() throws Exception {
        int databaseSizeBeforeUpdate = presidentRepository.findAll().size();
        president.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPresidentMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(president))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the President in the database
        List<President> presidentList = presidentRepository.findAll();
        assertThat(presidentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deletePresident() throws Exception {
        // Initialize the database
        presidentRepository.saveAndFlush(president);

        int databaseSizeBeforeDelete = presidentRepository.findAll().size();

        // Delete the president
        restPresidentMockMvc
            .perform(delete(ENTITY_API_URL_ID, president.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<President> presidentList = presidentRepository.findAll();
        assertThat(presidentList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
