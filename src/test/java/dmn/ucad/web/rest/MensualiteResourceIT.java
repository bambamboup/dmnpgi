package dmn.ucad.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import dmn.ucad.IntegrationTest;
import dmn.ucad.domain.Categorie;
import dmn.ucad.domain.Mensualite;
import dmn.ucad.domain.enumeration.EnumTypeMensualite;
import dmn.ucad.repository.MensualiteRepository;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link MensualiteResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class MensualiteResourceIT {

    private static final LocalDate DEFAULT_DATE_DEBUT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_DEBUT = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_DATE_FIN = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_FIN = LocalDate.now(ZoneId.systemDefault());

    private static final Boolean DEFAULT_ACTIF = false;
    private static final Boolean UPDATED_ACTIF = true;

    private static final Integer DEFAULT_MONTANT = 1;
    private static final Integer UPDATED_MONTANT = 2;

    private static final EnumTypeMensualite DEFAULT_TYPE_MENSUALITE = EnumTypeMensualite.KHABANE;
    private static final EnumTypeMensualite UPDATED_TYPE_MENSUALITE = EnumTypeMensualite.MENSUALITE;

    private static final String ENTITY_API_URL = "/api/mensualites";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private MensualiteRepository mensualiteRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restMensualiteMockMvc;

    private Mensualite mensualite;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Mensualite createEntity(EntityManager em) {
        Mensualite mensualite = new Mensualite()
            .dateDebut(DEFAULT_DATE_DEBUT)
            .dateFin(DEFAULT_DATE_FIN)
            .actif(DEFAULT_ACTIF)
            .montant(DEFAULT_MONTANT)
            .typeMensualite(DEFAULT_TYPE_MENSUALITE);
        // Add required entity
        Categorie categorie;
        if (TestUtil.findAll(em, Categorie.class).isEmpty()) {
            categorie = CategorieResourceIT.createEntity(em);
            em.persist(categorie);
            em.flush();
        } else {
            categorie = TestUtil.findAll(em, Categorie.class).get(0);
        }
        mensualite.setCategorie(categorie);
        return mensualite;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Mensualite createUpdatedEntity(EntityManager em) {
        Mensualite mensualite = new Mensualite()
            .dateDebut(UPDATED_DATE_DEBUT)
            .dateFin(UPDATED_DATE_FIN)
            .actif(UPDATED_ACTIF)
            .montant(UPDATED_MONTANT)
            .typeMensualite(UPDATED_TYPE_MENSUALITE);
        // Add required entity
        Categorie categorie;
        if (TestUtil.findAll(em, Categorie.class).isEmpty()) {
            categorie = CategorieResourceIT.createUpdatedEntity(em);
            em.persist(categorie);
            em.flush();
        } else {
            categorie = TestUtil.findAll(em, Categorie.class).get(0);
        }
        mensualite.setCategorie(categorie);
        return mensualite;
    }

    @BeforeEach
    public void initTest() {
        mensualite = createEntity(em);
    }

    @Test
    @Transactional
    void createMensualite() throws Exception {
        int databaseSizeBeforeCreate = mensualiteRepository.findAll().size();
        // Create the Mensualite
        restMensualiteMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(mensualite)))
            .andExpect(status().isCreated());

        // Validate the Mensualite in the database
        List<Mensualite> mensualiteList = mensualiteRepository.findAll();
        assertThat(mensualiteList).hasSize(databaseSizeBeforeCreate + 1);
        Mensualite testMensualite = mensualiteList.get(mensualiteList.size() - 1);
        assertThat(testMensualite.getDateDebut()).isEqualTo(DEFAULT_DATE_DEBUT);
        assertThat(testMensualite.getDateFin()).isEqualTo(DEFAULT_DATE_FIN);
        assertThat(testMensualite.getActif()).isEqualTo(DEFAULT_ACTIF);
        assertThat(testMensualite.getMontant()).isEqualTo(DEFAULT_MONTANT);
        assertThat(testMensualite.getTypeMensualite()).isEqualTo(DEFAULT_TYPE_MENSUALITE);
    }

    @Test
    @Transactional
    void createMensualiteWithExistingId() throws Exception {
        // Create the Mensualite with an existing ID
        mensualite.setId(1L);

        int databaseSizeBeforeCreate = mensualiteRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restMensualiteMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(mensualite)))
            .andExpect(status().isBadRequest());

        // Validate the Mensualite in the database
        List<Mensualite> mensualiteList = mensualiteRepository.findAll();
        assertThat(mensualiteList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkMontantIsRequired() throws Exception {
        int databaseSizeBeforeTest = mensualiteRepository.findAll().size();
        // set the field null
        mensualite.setMontant(null);

        // Create the Mensualite, which fails.

        restMensualiteMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(mensualite)))
            .andExpect(status().isBadRequest());

        List<Mensualite> mensualiteList = mensualiteRepository.findAll();
        assertThat(mensualiteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkTypeMensualiteIsRequired() throws Exception {
        int databaseSizeBeforeTest = mensualiteRepository.findAll().size();
        // set the field null
        mensualite.setTypeMensualite(null);

        // Create the Mensualite, which fails.

        restMensualiteMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(mensualite)))
            .andExpect(status().isBadRequest());

        List<Mensualite> mensualiteList = mensualiteRepository.findAll();
        assertThat(mensualiteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllMensualites() throws Exception {
        // Initialize the database
        mensualiteRepository.saveAndFlush(mensualite);

        // Get all the mensualiteList
        restMensualiteMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(mensualite.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateDebut").value(hasItem(DEFAULT_DATE_DEBUT.toString())))
            .andExpect(jsonPath("$.[*].dateFin").value(hasItem(DEFAULT_DATE_FIN.toString())))
            .andExpect(jsonPath("$.[*].actif").value(hasItem(DEFAULT_ACTIF.booleanValue())))
            .andExpect(jsonPath("$.[*].montant").value(hasItem(DEFAULT_MONTANT)))
            .andExpect(jsonPath("$.[*].typeMensualite").value(hasItem(DEFAULT_TYPE_MENSUALITE.toString())));
    }

    @Test
    @Transactional
    void getMensualite() throws Exception {
        // Initialize the database
        mensualiteRepository.saveAndFlush(mensualite);

        // Get the mensualite
        restMensualiteMockMvc
            .perform(get(ENTITY_API_URL_ID, mensualite.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(mensualite.getId().intValue()))
            .andExpect(jsonPath("$.dateDebut").value(DEFAULT_DATE_DEBUT.toString()))
            .andExpect(jsonPath("$.dateFin").value(DEFAULT_DATE_FIN.toString()))
            .andExpect(jsonPath("$.actif").value(DEFAULT_ACTIF.booleanValue()))
            .andExpect(jsonPath("$.montant").value(DEFAULT_MONTANT))
            .andExpect(jsonPath("$.typeMensualite").value(DEFAULT_TYPE_MENSUALITE.toString()));
    }

    @Test
    @Transactional
    void getNonExistingMensualite() throws Exception {
        // Get the mensualite
        restMensualiteMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewMensualite() throws Exception {
        // Initialize the database
        mensualiteRepository.saveAndFlush(mensualite);

        int databaseSizeBeforeUpdate = mensualiteRepository.findAll().size();

        // Update the mensualite
        Mensualite updatedMensualite = mensualiteRepository.findById(mensualite.getId()).get();
        // Disconnect from session so that the updates on updatedMensualite are not directly saved in db
        em.detach(updatedMensualite);
        updatedMensualite
            .dateDebut(UPDATED_DATE_DEBUT)
            .dateFin(UPDATED_DATE_FIN)
            .actif(UPDATED_ACTIF)
            .montant(UPDATED_MONTANT)
            .typeMensualite(UPDATED_TYPE_MENSUALITE);

        restMensualiteMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedMensualite.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedMensualite))
            )
            .andExpect(status().isOk());

        // Validate the Mensualite in the database
        List<Mensualite> mensualiteList = mensualiteRepository.findAll();
        assertThat(mensualiteList).hasSize(databaseSizeBeforeUpdate);
        Mensualite testMensualite = mensualiteList.get(mensualiteList.size() - 1);
        assertThat(testMensualite.getDateDebut()).isEqualTo(UPDATED_DATE_DEBUT);
        assertThat(testMensualite.getDateFin()).isEqualTo(UPDATED_DATE_FIN);
        assertThat(testMensualite.getActif()).isEqualTo(UPDATED_ACTIF);
        assertThat(testMensualite.getMontant()).isEqualTo(UPDATED_MONTANT);
        assertThat(testMensualite.getTypeMensualite()).isEqualTo(UPDATED_TYPE_MENSUALITE);
    }

    @Test
    @Transactional
    void putNonExistingMensualite() throws Exception {
        int databaseSizeBeforeUpdate = mensualiteRepository.findAll().size();
        mensualite.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMensualiteMockMvc
            .perform(
                put(ENTITY_API_URL_ID, mensualite.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(mensualite))
            )
            .andExpect(status().isBadRequest());

        // Validate the Mensualite in the database
        List<Mensualite> mensualiteList = mensualiteRepository.findAll();
        assertThat(mensualiteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchMensualite() throws Exception {
        int databaseSizeBeforeUpdate = mensualiteRepository.findAll().size();
        mensualite.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMensualiteMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(mensualite))
            )
            .andExpect(status().isBadRequest());

        // Validate the Mensualite in the database
        List<Mensualite> mensualiteList = mensualiteRepository.findAll();
        assertThat(mensualiteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamMensualite() throws Exception {
        int databaseSizeBeforeUpdate = mensualiteRepository.findAll().size();
        mensualite.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMensualiteMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(mensualite)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Mensualite in the database
        List<Mensualite> mensualiteList = mensualiteRepository.findAll();
        assertThat(mensualiteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateMensualiteWithPatch() throws Exception {
        // Initialize the database
        mensualiteRepository.saveAndFlush(mensualite);

        int databaseSizeBeforeUpdate = mensualiteRepository.findAll().size();

        // Update the mensualite using partial update
        Mensualite partialUpdatedMensualite = new Mensualite();
        partialUpdatedMensualite.setId(mensualite.getId());

        partialUpdatedMensualite.dateFin(UPDATED_DATE_FIN);

        restMensualiteMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMensualite.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMensualite))
            )
            .andExpect(status().isOk());

        // Validate the Mensualite in the database
        List<Mensualite> mensualiteList = mensualiteRepository.findAll();
        assertThat(mensualiteList).hasSize(databaseSizeBeforeUpdate);
        Mensualite testMensualite = mensualiteList.get(mensualiteList.size() - 1);
        assertThat(testMensualite.getDateDebut()).isEqualTo(DEFAULT_DATE_DEBUT);
        assertThat(testMensualite.getDateFin()).isEqualTo(UPDATED_DATE_FIN);
        assertThat(testMensualite.getActif()).isEqualTo(DEFAULT_ACTIF);
        assertThat(testMensualite.getMontant()).isEqualTo(DEFAULT_MONTANT);
        assertThat(testMensualite.getTypeMensualite()).isEqualTo(DEFAULT_TYPE_MENSUALITE);
    }

    @Test
    @Transactional
    void fullUpdateMensualiteWithPatch() throws Exception {
        // Initialize the database
        mensualiteRepository.saveAndFlush(mensualite);

        int databaseSizeBeforeUpdate = mensualiteRepository.findAll().size();

        // Update the mensualite using partial update
        Mensualite partialUpdatedMensualite = new Mensualite();
        partialUpdatedMensualite.setId(mensualite.getId());

        partialUpdatedMensualite
            .dateDebut(UPDATED_DATE_DEBUT)
            .dateFin(UPDATED_DATE_FIN)
            .actif(UPDATED_ACTIF)
            .montant(UPDATED_MONTANT)
            .typeMensualite(UPDATED_TYPE_MENSUALITE);

        restMensualiteMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMensualite.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMensualite))
            )
            .andExpect(status().isOk());

        // Validate the Mensualite in the database
        List<Mensualite> mensualiteList = mensualiteRepository.findAll();
        assertThat(mensualiteList).hasSize(databaseSizeBeforeUpdate);
        Mensualite testMensualite = mensualiteList.get(mensualiteList.size() - 1);
        assertThat(testMensualite.getDateDebut()).isEqualTo(UPDATED_DATE_DEBUT);
        assertThat(testMensualite.getDateFin()).isEqualTo(UPDATED_DATE_FIN);
        assertThat(testMensualite.getActif()).isEqualTo(UPDATED_ACTIF);
        assertThat(testMensualite.getMontant()).isEqualTo(UPDATED_MONTANT);
        assertThat(testMensualite.getTypeMensualite()).isEqualTo(UPDATED_TYPE_MENSUALITE);
    }

    @Test
    @Transactional
    void patchNonExistingMensualite() throws Exception {
        int databaseSizeBeforeUpdate = mensualiteRepository.findAll().size();
        mensualite.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMensualiteMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, mensualite.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(mensualite))
            )
            .andExpect(status().isBadRequest());

        // Validate the Mensualite in the database
        List<Mensualite> mensualiteList = mensualiteRepository.findAll();
        assertThat(mensualiteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchMensualite() throws Exception {
        int databaseSizeBeforeUpdate = mensualiteRepository.findAll().size();
        mensualite.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMensualiteMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(mensualite))
            )
            .andExpect(status().isBadRequest());

        // Validate the Mensualite in the database
        List<Mensualite> mensualiteList = mensualiteRepository.findAll();
        assertThat(mensualiteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamMensualite() throws Exception {
        int databaseSizeBeforeUpdate = mensualiteRepository.findAll().size();
        mensualite.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMensualiteMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(mensualite))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Mensualite in the database
        List<Mensualite> mensualiteList = mensualiteRepository.findAll();
        assertThat(mensualiteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteMensualite() throws Exception {
        // Initialize the database
        mensualiteRepository.saveAndFlush(mensualite);

        int databaseSizeBeforeDelete = mensualiteRepository.findAll().size();

        // Delete the mensualite
        restMensualiteMockMvc
            .perform(delete(ENTITY_API_URL_ID, mensualite.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Mensualite> mensualiteList = mensualiteRepository.findAll();
        assertThat(mensualiteList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
