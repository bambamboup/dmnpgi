package dmn.ucad.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import dmn.ucad.IntegrationTest;
import dmn.ucad.domain.NiveauXamXam;
import dmn.ucad.repository.NiveauXamXamRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link NiveauXamXamResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class NiveauXamXamResourceIT {

    private static final String DEFAULT_LIBELLE = "AAAAAAAAAA";
    private static final String UPDATED_LIBELLE = "BBBBBBBBBB";

    private static final Integer DEFAULT_POIDS = 1;
    private static final Integer UPDATED_POIDS = 2;

    private static final String ENTITY_API_URL = "/api/niveau-xam-xams";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private NiveauXamXamRepository niveauXamXamRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restNiveauXamXamMockMvc;

    private NiveauXamXam niveauXamXam;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NiveauXamXam createEntity(EntityManager em) {
        NiveauXamXam niveauXamXam = new NiveauXamXam().libelle(DEFAULT_LIBELLE).poids(DEFAULT_POIDS);
        return niveauXamXam;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NiveauXamXam createUpdatedEntity(EntityManager em) {
        NiveauXamXam niveauXamXam = new NiveauXamXam().libelle(UPDATED_LIBELLE).poids(UPDATED_POIDS);
        return niveauXamXam;
    }

    @BeforeEach
    public void initTest() {
        niveauXamXam = createEntity(em);
    }

    @Test
    @Transactional
    void createNiveauXamXam() throws Exception {
        int databaseSizeBeforeCreate = niveauXamXamRepository.findAll().size();
        // Create the NiveauXamXam
        restNiveauXamXamMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(niveauXamXam)))
            .andExpect(status().isCreated());

        // Validate the NiveauXamXam in the database
        List<NiveauXamXam> niveauXamXamList = niveauXamXamRepository.findAll();
        assertThat(niveauXamXamList).hasSize(databaseSizeBeforeCreate + 1);
        NiveauXamXam testNiveauXamXam = niveauXamXamList.get(niveauXamXamList.size() - 1);
        assertThat(testNiveauXamXam.getLibelle()).isEqualTo(DEFAULT_LIBELLE);
        assertThat(testNiveauXamXam.getPoids()).isEqualTo(DEFAULT_POIDS);
    }

    @Test
    @Transactional
    void createNiveauXamXamWithExistingId() throws Exception {
        // Create the NiveauXamXam with an existing ID
        niveauXamXam.setId(1L);

        int databaseSizeBeforeCreate = niveauXamXamRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restNiveauXamXamMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(niveauXamXam)))
            .andExpect(status().isBadRequest());

        // Validate the NiveauXamXam in the database
        List<NiveauXamXam> niveauXamXamList = niveauXamXamRepository.findAll();
        assertThat(niveauXamXamList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkLibelleIsRequired() throws Exception {
        int databaseSizeBeforeTest = niveauXamXamRepository.findAll().size();
        // set the field null
        niveauXamXam.setLibelle(null);

        // Create the NiveauXamXam, which fails.

        restNiveauXamXamMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(niveauXamXam)))
            .andExpect(status().isBadRequest());

        List<NiveauXamXam> niveauXamXamList = niveauXamXamRepository.findAll();
        assertThat(niveauXamXamList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkPoidsIsRequired() throws Exception {
        int databaseSizeBeforeTest = niveauXamXamRepository.findAll().size();
        // set the field null
        niveauXamXam.setPoids(null);

        // Create the NiveauXamXam, which fails.

        restNiveauXamXamMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(niveauXamXam)))
            .andExpect(status().isBadRequest());

        List<NiveauXamXam> niveauXamXamList = niveauXamXamRepository.findAll();
        assertThat(niveauXamXamList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllNiveauXamXams() throws Exception {
        // Initialize the database
        niveauXamXamRepository.saveAndFlush(niveauXamXam);

        // Get all the niveauXamXamList
        restNiveauXamXamMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(niveauXamXam.getId().intValue())))
            .andExpect(jsonPath("$.[*].libelle").value(hasItem(DEFAULT_LIBELLE)))
            .andExpect(jsonPath("$.[*].poids").value(hasItem(DEFAULT_POIDS)));
    }

    @Test
    @Transactional
    void getNiveauXamXam() throws Exception {
        // Initialize the database
        niveauXamXamRepository.saveAndFlush(niveauXamXam);

        // Get the niveauXamXam
        restNiveauXamXamMockMvc
            .perform(get(ENTITY_API_URL_ID, niveauXamXam.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(niveauXamXam.getId().intValue()))
            .andExpect(jsonPath("$.libelle").value(DEFAULT_LIBELLE))
            .andExpect(jsonPath("$.poids").value(DEFAULT_POIDS));
    }

    @Test
    @Transactional
    void getNonExistingNiveauXamXam() throws Exception {
        // Get the niveauXamXam
        restNiveauXamXamMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewNiveauXamXam() throws Exception {
        // Initialize the database
        niveauXamXamRepository.saveAndFlush(niveauXamXam);

        int databaseSizeBeforeUpdate = niveauXamXamRepository.findAll().size();

        // Update the niveauXamXam
        NiveauXamXam updatedNiveauXamXam = niveauXamXamRepository.findById(niveauXamXam.getId()).get();
        // Disconnect from session so that the updates on updatedNiveauXamXam are not directly saved in db
        em.detach(updatedNiveauXamXam);
        updatedNiveauXamXam.libelle(UPDATED_LIBELLE).poids(UPDATED_POIDS);

        restNiveauXamXamMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedNiveauXamXam.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedNiveauXamXam))
            )
            .andExpect(status().isOk());

        // Validate the NiveauXamXam in the database
        List<NiveauXamXam> niveauXamXamList = niveauXamXamRepository.findAll();
        assertThat(niveauXamXamList).hasSize(databaseSizeBeforeUpdate);
        NiveauXamXam testNiveauXamXam = niveauXamXamList.get(niveauXamXamList.size() - 1);
        assertThat(testNiveauXamXam.getLibelle()).isEqualTo(UPDATED_LIBELLE);
        assertThat(testNiveauXamXam.getPoids()).isEqualTo(UPDATED_POIDS);
    }

    @Test
    @Transactional
    void putNonExistingNiveauXamXam() throws Exception {
        int databaseSizeBeforeUpdate = niveauXamXamRepository.findAll().size();
        niveauXamXam.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restNiveauXamXamMockMvc
            .perform(
                put(ENTITY_API_URL_ID, niveauXamXam.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(niveauXamXam))
            )
            .andExpect(status().isBadRequest());

        // Validate the NiveauXamXam in the database
        List<NiveauXamXam> niveauXamXamList = niveauXamXamRepository.findAll();
        assertThat(niveauXamXamList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchNiveauXamXam() throws Exception {
        int databaseSizeBeforeUpdate = niveauXamXamRepository.findAll().size();
        niveauXamXam.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restNiveauXamXamMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(niveauXamXam))
            )
            .andExpect(status().isBadRequest());

        // Validate the NiveauXamXam in the database
        List<NiveauXamXam> niveauXamXamList = niveauXamXamRepository.findAll();
        assertThat(niveauXamXamList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamNiveauXamXam() throws Exception {
        int databaseSizeBeforeUpdate = niveauXamXamRepository.findAll().size();
        niveauXamXam.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restNiveauXamXamMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(niveauXamXam)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the NiveauXamXam in the database
        List<NiveauXamXam> niveauXamXamList = niveauXamXamRepository.findAll();
        assertThat(niveauXamXamList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateNiveauXamXamWithPatch() throws Exception {
        // Initialize the database
        niveauXamXamRepository.saveAndFlush(niveauXamXam);

        int databaseSizeBeforeUpdate = niveauXamXamRepository.findAll().size();

        // Update the niveauXamXam using partial update
        NiveauXamXam partialUpdatedNiveauXamXam = new NiveauXamXam();
        partialUpdatedNiveauXamXam.setId(niveauXamXam.getId());

        partialUpdatedNiveauXamXam.libelle(UPDATED_LIBELLE);

        restNiveauXamXamMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedNiveauXamXam.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedNiveauXamXam))
            )
            .andExpect(status().isOk());

        // Validate the NiveauXamXam in the database
        List<NiveauXamXam> niveauXamXamList = niveauXamXamRepository.findAll();
        assertThat(niveauXamXamList).hasSize(databaseSizeBeforeUpdate);
        NiveauXamXam testNiveauXamXam = niveauXamXamList.get(niveauXamXamList.size() - 1);
        assertThat(testNiveauXamXam.getLibelle()).isEqualTo(UPDATED_LIBELLE);
        assertThat(testNiveauXamXam.getPoids()).isEqualTo(DEFAULT_POIDS);
    }

    @Test
    @Transactional
    void fullUpdateNiveauXamXamWithPatch() throws Exception {
        // Initialize the database
        niveauXamXamRepository.saveAndFlush(niveauXamXam);

        int databaseSizeBeforeUpdate = niveauXamXamRepository.findAll().size();

        // Update the niveauXamXam using partial update
        NiveauXamXam partialUpdatedNiveauXamXam = new NiveauXamXam();
        partialUpdatedNiveauXamXam.setId(niveauXamXam.getId());

        partialUpdatedNiveauXamXam.libelle(UPDATED_LIBELLE).poids(UPDATED_POIDS);

        restNiveauXamXamMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedNiveauXamXam.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedNiveauXamXam))
            )
            .andExpect(status().isOk());

        // Validate the NiveauXamXam in the database
        List<NiveauXamXam> niveauXamXamList = niveauXamXamRepository.findAll();
        assertThat(niveauXamXamList).hasSize(databaseSizeBeforeUpdate);
        NiveauXamXam testNiveauXamXam = niveauXamXamList.get(niveauXamXamList.size() - 1);
        assertThat(testNiveauXamXam.getLibelle()).isEqualTo(UPDATED_LIBELLE);
        assertThat(testNiveauXamXam.getPoids()).isEqualTo(UPDATED_POIDS);
    }

    @Test
    @Transactional
    void patchNonExistingNiveauXamXam() throws Exception {
        int databaseSizeBeforeUpdate = niveauXamXamRepository.findAll().size();
        niveauXamXam.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restNiveauXamXamMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, niveauXamXam.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(niveauXamXam))
            )
            .andExpect(status().isBadRequest());

        // Validate the NiveauXamXam in the database
        List<NiveauXamXam> niveauXamXamList = niveauXamXamRepository.findAll();
        assertThat(niveauXamXamList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchNiveauXamXam() throws Exception {
        int databaseSizeBeforeUpdate = niveauXamXamRepository.findAll().size();
        niveauXamXam.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restNiveauXamXamMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(niveauXamXam))
            )
            .andExpect(status().isBadRequest());

        // Validate the NiveauXamXam in the database
        List<NiveauXamXam> niveauXamXamList = niveauXamXamRepository.findAll();
        assertThat(niveauXamXamList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamNiveauXamXam() throws Exception {
        int databaseSizeBeforeUpdate = niveauXamXamRepository.findAll().size();
        niveauXamXam.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restNiveauXamXamMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(niveauXamXam))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the NiveauXamXam in the database
        List<NiveauXamXam> niveauXamXamList = niveauXamXamRepository.findAll();
        assertThat(niveauXamXamList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteNiveauXamXam() throws Exception {
        // Initialize the database
        niveauXamXamRepository.saveAndFlush(niveauXamXam);

        int databaseSizeBeforeDelete = niveauXamXamRepository.findAll().size();

        // Delete the niveauXamXam
        restNiveauXamXamMockMvc
            .perform(delete(ENTITY_API_URL_ID, niveauXamXam.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<NiveauXamXam> niveauXamXamList = niveauXamXamRepository.findAll();
        assertThat(niveauXamXamList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
