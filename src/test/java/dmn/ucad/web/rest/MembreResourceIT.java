package dmn.ucad.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import dmn.ucad.IntegrationTest;
import dmn.ucad.domain.Membre;
import dmn.ucad.domain.XamXam;
import dmn.ucad.domain.enumeration.EnumCivilite;
import dmn.ucad.domain.enumeration.EnumEtatSante;
import dmn.ucad.domain.enumeration.EnumTypeSexe;
import dmn.ucad.repository.MembreRepository;
import dmn.ucad.service.MembreService;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

/**
 * Integration tests for the {@link MembreResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class MembreResourceIT {

    private static final String DEFAULT_NOM = "AAAAAAAAAA";
    private static final String UPDATED_NOM = "BBBBBBBBBB";

    private static final String DEFAULT_PRENOM = "AAAAAAAAAA";
    private static final String UPDATED_PRENOM = "BBBBBBBBBB";

    private static final String DEFAULT_TELEPHONE = "AAAAAAAAAA";
    private static final String UPDATED_TELEPHONE = "BBBBBBBBBB";

    private static final EnumTypeSexe DEFAULT_SEXE = EnumTypeSexe.MASCULIN;
    private static final EnumTypeSexe UPDATED_SEXE = EnumTypeSexe.FEMININ;

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_CNI = "AAAAAAAAAA";
    private static final String UPDATED_CNI = "BBBBBBBBBB";

    private static final String DEFAULT_ADRESS_DAKAR = "AAAAAAAAAA";
    private static final String UPDATED_ADRESS_DAKAR = "BBBBBBBBBB";

    private static final EnumCivilite DEFAULT_SITUATION_MATRIMONIALE = EnumCivilite.CELLIBATAIRE;
    private static final EnumCivilite UPDATED_SITUATION_MATRIMONIALE = EnumCivilite.MARIE;

    private static final String DEFAULT_ADRESSE = "AAAAAAAAAA";
    private static final String UPDATED_ADRESSE = "BBBBBBBBBB";

    private static final String DEFAULT_ADRESSE_VACANCE = "AAAAAAAAAA";
    private static final String UPDATED_ADRESSE_VACANCE = "BBBBBBBBBB";

    private static final byte[] DEFAULT_PROFIL = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_PROFIL = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_PROFIL_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_PROFIL_CONTENT_TYPE = "image/png";

    private static final LocalDate DEFAULT_DATE_NAISSANCE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_NAISSANCE = LocalDate.now(ZoneId.systemDefault());

    private static final Boolean DEFAULT_BOURSIER = false;
    private static final Boolean UPDATED_BOURSIER = true;

    private static final String DEFAULT_DAARA_ORIGINE = "AAAAAAAAAA";
    private static final String UPDATED_DAARA_ORIGINE = "BBBBBBBBBB";

    private static final EnumEtatSante DEFAULT_ETAT_SANTE = EnumEtatSante.INSTABLE;
    private static final EnumEtatSante UPDATED_ETAT_SANTE = EnumEtatSante.STABLE;

    private static final String DEFAULT_TITEUR = "AAAAAAAAAA";
    private static final String UPDATED_TITEUR = "BBBBBBBBBB";

    private static final String DEFAULT_TELEPHONE_TITEUR = "AAAAAAAAAA";
    private static final String UPDATED_TELEPHONE_TITEUR = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/membres";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private MembreRepository membreRepository;

    @Mock
    private MembreRepository membreRepositoryMock;

    @Mock
    private MembreService membreServiceMock;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restMembreMockMvc;

    private Membre membre;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Membre createEntity(EntityManager em) {
        Membre membre = new Membre()
            .nom(DEFAULT_NOM)
            .prenom(DEFAULT_PRENOM)
            .telephone(DEFAULT_TELEPHONE)
            .sexe(DEFAULT_SEXE)
            .email(DEFAULT_EMAIL)
            .cni(DEFAULT_CNI)
            .adressDakar(DEFAULT_ADRESS_DAKAR)
            .situationMatrimoniale(DEFAULT_SITUATION_MATRIMONIALE)
            .adresse(DEFAULT_ADRESSE)
            .adresseVacance(DEFAULT_ADRESSE_VACANCE)
            .profil(DEFAULT_PROFIL)
            .profilContentType(DEFAULT_PROFIL_CONTENT_TYPE)
            .dateNaissance(DEFAULT_DATE_NAISSANCE)
            .boursier(DEFAULT_BOURSIER)
            .daaraOrigine(DEFAULT_DAARA_ORIGINE)
            .etatSante(DEFAULT_ETAT_SANTE)
            .titeur(DEFAULT_TITEUR)
            .telephoneTiteur(DEFAULT_TELEPHONE_TITEUR);
        // Add required entity
        XamXam xamXam;
        if (TestUtil.findAll(em, XamXam.class).isEmpty()) {
            xamXam = XamXamResourceIT.createEntity(em);
            em.persist(xamXam);
            em.flush();
        } else {
            xamXam = TestUtil.findAll(em, XamXam.class).get(0);
        }
        membre.getXamXams().add(xamXam);
        return membre;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Membre createUpdatedEntity(EntityManager em) {
        Membre membre = new Membre()
            .nom(UPDATED_NOM)
            .prenom(UPDATED_PRENOM)
            .telephone(UPDATED_TELEPHONE)
            .sexe(UPDATED_SEXE)
            .email(UPDATED_EMAIL)
            .cni(UPDATED_CNI)
            .adressDakar(UPDATED_ADRESS_DAKAR)
            .situationMatrimoniale(UPDATED_SITUATION_MATRIMONIALE)
            .adresse(UPDATED_ADRESSE)
            .adresseVacance(UPDATED_ADRESSE_VACANCE)
            .profil(UPDATED_PROFIL)
            .profilContentType(UPDATED_PROFIL_CONTENT_TYPE)
            .dateNaissance(UPDATED_DATE_NAISSANCE)
            .boursier(UPDATED_BOURSIER)
            .daaraOrigine(UPDATED_DAARA_ORIGINE)
            .etatSante(UPDATED_ETAT_SANTE)
            .titeur(UPDATED_TITEUR)
            .telephoneTiteur(UPDATED_TELEPHONE_TITEUR);
        // Add required entity
        XamXam xamXam;
        if (TestUtil.findAll(em, XamXam.class).isEmpty()) {
            xamXam = XamXamResourceIT.createUpdatedEntity(em);
            em.persist(xamXam);
            em.flush();
        } else {
            xamXam = TestUtil.findAll(em, XamXam.class).get(0);
        }
        membre.getXamXams().add(xamXam);
        return membre;
    }

    @BeforeEach
    public void initTest() {
        membre = createEntity(em);
    }

    @Test
    @Transactional
    void createMembre() throws Exception {
        int databaseSizeBeforeCreate = membreRepository.findAll().size();
        // Create the Membre
        restMembreMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(membre)))
            .andExpect(status().isCreated());

        // Validate the Membre in the database
        List<Membre> membreList = membreRepository.findAll();
        assertThat(membreList).hasSize(databaseSizeBeforeCreate + 1);
        Membre testMembre = membreList.get(membreList.size() - 1);
        assertThat(testMembre.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testMembre.getPrenom()).isEqualTo(DEFAULT_PRENOM);
        assertThat(testMembre.getTelephone()).isEqualTo(DEFAULT_TELEPHONE);
        assertThat(testMembre.getSexe()).isEqualTo(DEFAULT_SEXE);
        assertThat(testMembre.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testMembre.getCni()).isEqualTo(DEFAULT_CNI);
        assertThat(testMembre.getAdressDakar()).isEqualTo(DEFAULT_ADRESS_DAKAR);
        assertThat(testMembre.getSituationMatrimoniale()).isEqualTo(DEFAULT_SITUATION_MATRIMONIALE);
        assertThat(testMembre.getAdresse()).isEqualTo(DEFAULT_ADRESSE);
        assertThat(testMembre.getAdresseVacance()).isEqualTo(DEFAULT_ADRESSE_VACANCE);
        assertThat(testMembre.getProfil()).isEqualTo(DEFAULT_PROFIL);
        assertThat(testMembre.getProfilContentType()).isEqualTo(DEFAULT_PROFIL_CONTENT_TYPE);
        assertThat(testMembre.getDateNaissance()).isEqualTo(DEFAULT_DATE_NAISSANCE);
        assertThat(testMembre.getBoursier()).isEqualTo(DEFAULT_BOURSIER);
        assertThat(testMembre.getDaaraOrigine()).isEqualTo(DEFAULT_DAARA_ORIGINE);
        assertThat(testMembre.getEtatSante()).isEqualTo(DEFAULT_ETAT_SANTE);
        assertThat(testMembre.getTiteur()).isEqualTo(DEFAULT_TITEUR);
        assertThat(testMembre.getTelephoneTiteur()).isEqualTo(DEFAULT_TELEPHONE_TITEUR);
    }

    @Test
    @Transactional
    void createMembreWithExistingId() throws Exception {
        // Create the Membre with an existing ID
        membre.setId(1L);

        int databaseSizeBeforeCreate = membreRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restMembreMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(membre)))
            .andExpect(status().isBadRequest());

        // Validate the Membre in the database
        List<Membre> membreList = membreRepository.findAll();
        assertThat(membreList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNomIsRequired() throws Exception {
        int databaseSizeBeforeTest = membreRepository.findAll().size();
        // set the field null
        membre.setNom(null);

        // Create the Membre, which fails.

        restMembreMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(membre)))
            .andExpect(status().isBadRequest());

        List<Membre> membreList = membreRepository.findAll();
        assertThat(membreList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkPrenomIsRequired() throws Exception {
        int databaseSizeBeforeTest = membreRepository.findAll().size();
        // set the field null
        membre.setPrenom(null);

        // Create the Membre, which fails.

        restMembreMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(membre)))
            .andExpect(status().isBadRequest());

        List<Membre> membreList = membreRepository.findAll();
        assertThat(membreList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkTelephoneIsRequired() throws Exception {
        int databaseSizeBeforeTest = membreRepository.findAll().size();
        // set the field null
        membre.setTelephone(null);

        // Create the Membre, which fails.

        restMembreMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(membre)))
            .andExpect(status().isBadRequest());

        List<Membre> membreList = membreRepository.findAll();
        assertThat(membreList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkSexeIsRequired() throws Exception {
        int databaseSizeBeforeTest = membreRepository.findAll().size();
        // set the field null
        membre.setSexe(null);

        // Create the Membre, which fails.

        restMembreMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(membre)))
            .andExpect(status().isBadRequest());

        List<Membre> membreList = membreRepository.findAll();
        assertThat(membreList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkSituationMatrimonialeIsRequired() throws Exception {
        int databaseSizeBeforeTest = membreRepository.findAll().size();
        // set the field null
        membre.setSituationMatrimoniale(null);

        // Create the Membre, which fails.

        restMembreMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(membre)))
            .andExpect(status().isBadRequest());

        List<Membre> membreList = membreRepository.findAll();
        assertThat(membreList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllMembres() throws Exception {
        // Initialize the database
        membreRepository.saveAndFlush(membre);

        // Get all the membreList
        restMembreMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(membre.getId().intValue())))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM)))
            .andExpect(jsonPath("$.[*].prenom").value(hasItem(DEFAULT_PRENOM)))
            .andExpect(jsonPath("$.[*].telephone").value(hasItem(DEFAULT_TELEPHONE)))
            .andExpect(jsonPath("$.[*].sexe").value(hasItem(DEFAULT_SEXE.toString())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].cni").value(hasItem(DEFAULT_CNI)))
            .andExpect(jsonPath("$.[*].adressDakar").value(hasItem(DEFAULT_ADRESS_DAKAR)))
            .andExpect(jsonPath("$.[*].situationMatrimoniale").value(hasItem(DEFAULT_SITUATION_MATRIMONIALE.toString())))
            .andExpect(jsonPath("$.[*].adresse").value(hasItem(DEFAULT_ADRESSE)))
            .andExpect(jsonPath("$.[*].adresseVacance").value(hasItem(DEFAULT_ADRESSE_VACANCE)))
            .andExpect(jsonPath("$.[*].profilContentType").value(hasItem(DEFAULT_PROFIL_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].profil").value(hasItem(Base64Utils.encodeToString(DEFAULT_PROFIL))))
            .andExpect(jsonPath("$.[*].dateNaissance").value(hasItem(DEFAULT_DATE_NAISSANCE.toString())))
            .andExpect(jsonPath("$.[*].boursier").value(hasItem(DEFAULT_BOURSIER.booleanValue())))
            .andExpect(jsonPath("$.[*].daaraOrigine").value(hasItem(DEFAULT_DAARA_ORIGINE)))
            .andExpect(jsonPath("$.[*].etatSante").value(hasItem(DEFAULT_ETAT_SANTE.toString())))
            .andExpect(jsonPath("$.[*].titeur").value(hasItem(DEFAULT_TITEUR)))
            .andExpect(jsonPath("$.[*].telephoneTiteur").value(hasItem(DEFAULT_TELEPHONE_TITEUR)));
    }

    @SuppressWarnings({ "unchecked" })
    void getAllMembresWithEagerRelationshipsIsEnabled() throws Exception {
        when(membreServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restMembreMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(membreServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({ "unchecked" })
    void getAllMembresWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(membreServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restMembreMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(membreServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    void getMembre() throws Exception {
        // Initialize the database
        membreRepository.saveAndFlush(membre);

        // Get the membre
        restMembreMockMvc
            .perform(get(ENTITY_API_URL_ID, membre.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(membre.getId().intValue()))
            .andExpect(jsonPath("$.nom").value(DEFAULT_NOM))
            .andExpect(jsonPath("$.prenom").value(DEFAULT_PRENOM))
            .andExpect(jsonPath("$.telephone").value(DEFAULT_TELEPHONE))
            .andExpect(jsonPath("$.sexe").value(DEFAULT_SEXE.toString()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.cni").value(DEFAULT_CNI))
            .andExpect(jsonPath("$.adressDakar").value(DEFAULT_ADRESS_DAKAR))
            .andExpect(jsonPath("$.situationMatrimoniale").value(DEFAULT_SITUATION_MATRIMONIALE.toString()))
            .andExpect(jsonPath("$.adresse").value(DEFAULT_ADRESSE))
            .andExpect(jsonPath("$.adresseVacance").value(DEFAULT_ADRESSE_VACANCE))
            .andExpect(jsonPath("$.profilContentType").value(DEFAULT_PROFIL_CONTENT_TYPE))
            .andExpect(jsonPath("$.profil").value(Base64Utils.encodeToString(DEFAULT_PROFIL)))
            .andExpect(jsonPath("$.dateNaissance").value(DEFAULT_DATE_NAISSANCE.toString()))
            .andExpect(jsonPath("$.boursier").value(DEFAULT_BOURSIER.booleanValue()))
            .andExpect(jsonPath("$.daaraOrigine").value(DEFAULT_DAARA_ORIGINE))
            .andExpect(jsonPath("$.etatSante").value(DEFAULT_ETAT_SANTE.toString()))
            .andExpect(jsonPath("$.titeur").value(DEFAULT_TITEUR))
            .andExpect(jsonPath("$.telephoneTiteur").value(DEFAULT_TELEPHONE_TITEUR));
    }

    @Test
    @Transactional
    void getNonExistingMembre() throws Exception {
        // Get the membre
        restMembreMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewMembre() throws Exception {
        // Initialize the database
        membreRepository.saveAndFlush(membre);

        int databaseSizeBeforeUpdate = membreRepository.findAll().size();

        // Update the membre
        Membre updatedMembre = membreRepository.findById(membre.getId()).get();
        // Disconnect from session so that the updates on updatedMembre are not directly saved in db
        em.detach(updatedMembre);
        updatedMembre
            .nom(UPDATED_NOM)
            .prenom(UPDATED_PRENOM)
            .telephone(UPDATED_TELEPHONE)
            .sexe(UPDATED_SEXE)
            .email(UPDATED_EMAIL)
            .cni(UPDATED_CNI)
            .adressDakar(UPDATED_ADRESS_DAKAR)
            .situationMatrimoniale(UPDATED_SITUATION_MATRIMONIALE)
            .adresse(UPDATED_ADRESSE)
            .adresseVacance(UPDATED_ADRESSE_VACANCE)
            .profil(UPDATED_PROFIL)
            .profilContentType(UPDATED_PROFIL_CONTENT_TYPE)
            .dateNaissance(UPDATED_DATE_NAISSANCE)
            .boursier(UPDATED_BOURSIER)
            .daaraOrigine(UPDATED_DAARA_ORIGINE)
            .etatSante(UPDATED_ETAT_SANTE)
            .titeur(UPDATED_TITEUR)
            .telephoneTiteur(UPDATED_TELEPHONE_TITEUR);

        restMembreMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedMembre.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedMembre))
            )
            .andExpect(status().isOk());

        // Validate the Membre in the database
        List<Membre> membreList = membreRepository.findAll();
        assertThat(membreList).hasSize(databaseSizeBeforeUpdate);
        Membre testMembre = membreList.get(membreList.size() - 1);
        assertThat(testMembre.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testMembre.getPrenom()).isEqualTo(UPDATED_PRENOM);
        assertThat(testMembre.getTelephone()).isEqualTo(UPDATED_TELEPHONE);
        assertThat(testMembre.getSexe()).isEqualTo(UPDATED_SEXE);
        assertThat(testMembre.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testMembre.getCni()).isEqualTo(UPDATED_CNI);
        assertThat(testMembre.getAdressDakar()).isEqualTo(UPDATED_ADRESS_DAKAR);
        assertThat(testMembre.getSituationMatrimoniale()).isEqualTo(UPDATED_SITUATION_MATRIMONIALE);
        assertThat(testMembre.getAdresse()).isEqualTo(UPDATED_ADRESSE);
        assertThat(testMembre.getAdresseVacance()).isEqualTo(UPDATED_ADRESSE_VACANCE);
        assertThat(testMembre.getProfil()).isEqualTo(UPDATED_PROFIL);
        assertThat(testMembre.getProfilContentType()).isEqualTo(UPDATED_PROFIL_CONTENT_TYPE);
        assertThat(testMembre.getDateNaissance()).isEqualTo(UPDATED_DATE_NAISSANCE);
        assertThat(testMembre.getBoursier()).isEqualTo(UPDATED_BOURSIER);
        assertThat(testMembre.getDaaraOrigine()).isEqualTo(UPDATED_DAARA_ORIGINE);
        assertThat(testMembre.getEtatSante()).isEqualTo(UPDATED_ETAT_SANTE);
        assertThat(testMembre.getTiteur()).isEqualTo(UPDATED_TITEUR);
        assertThat(testMembre.getTelephoneTiteur()).isEqualTo(UPDATED_TELEPHONE_TITEUR);
    }

    @Test
    @Transactional
    void putNonExistingMembre() throws Exception {
        int databaseSizeBeforeUpdate = membreRepository.findAll().size();
        membre.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMembreMockMvc
            .perform(
                put(ENTITY_API_URL_ID, membre.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(membre))
            )
            .andExpect(status().isBadRequest());

        // Validate the Membre in the database
        List<Membre> membreList = membreRepository.findAll();
        assertThat(membreList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchMembre() throws Exception {
        int databaseSizeBeforeUpdate = membreRepository.findAll().size();
        membre.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMembreMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(membre))
            )
            .andExpect(status().isBadRequest());

        // Validate the Membre in the database
        List<Membre> membreList = membreRepository.findAll();
        assertThat(membreList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamMembre() throws Exception {
        int databaseSizeBeforeUpdate = membreRepository.findAll().size();
        membre.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMembreMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(membre)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Membre in the database
        List<Membre> membreList = membreRepository.findAll();
        assertThat(membreList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateMembreWithPatch() throws Exception {
        // Initialize the database
        membreRepository.saveAndFlush(membre);

        int databaseSizeBeforeUpdate = membreRepository.findAll().size();

        // Update the membre using partial update
        Membre partialUpdatedMembre = new Membre();
        partialUpdatedMembre.setId(membre.getId());

        partialUpdatedMembre
            .nom(UPDATED_NOM)
            .situationMatrimoniale(UPDATED_SITUATION_MATRIMONIALE)
            .adresse(UPDATED_ADRESSE)
            .daaraOrigine(UPDATED_DAARA_ORIGINE)
            .etatSante(UPDATED_ETAT_SANTE)
            .titeur(UPDATED_TITEUR);

        restMembreMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMembre.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMembre))
            )
            .andExpect(status().isOk());

        // Validate the Membre in the database
        List<Membre> membreList = membreRepository.findAll();
        assertThat(membreList).hasSize(databaseSizeBeforeUpdate);
        Membre testMembre = membreList.get(membreList.size() - 1);
        assertThat(testMembre.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testMembre.getPrenom()).isEqualTo(DEFAULT_PRENOM);
        assertThat(testMembre.getTelephone()).isEqualTo(DEFAULT_TELEPHONE);
        assertThat(testMembre.getSexe()).isEqualTo(DEFAULT_SEXE);
        assertThat(testMembre.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testMembre.getCni()).isEqualTo(DEFAULT_CNI);
        assertThat(testMembre.getAdressDakar()).isEqualTo(DEFAULT_ADRESS_DAKAR);
        assertThat(testMembre.getSituationMatrimoniale()).isEqualTo(UPDATED_SITUATION_MATRIMONIALE);
        assertThat(testMembre.getAdresse()).isEqualTo(UPDATED_ADRESSE);
        assertThat(testMembre.getAdresseVacance()).isEqualTo(DEFAULT_ADRESSE_VACANCE);
        assertThat(testMembre.getProfil()).isEqualTo(DEFAULT_PROFIL);
        assertThat(testMembre.getProfilContentType()).isEqualTo(DEFAULT_PROFIL_CONTENT_TYPE);
        assertThat(testMembre.getDateNaissance()).isEqualTo(DEFAULT_DATE_NAISSANCE);
        assertThat(testMembre.getBoursier()).isEqualTo(DEFAULT_BOURSIER);
        assertThat(testMembre.getDaaraOrigine()).isEqualTo(UPDATED_DAARA_ORIGINE);
        assertThat(testMembre.getEtatSante()).isEqualTo(UPDATED_ETAT_SANTE);
        assertThat(testMembre.getTiteur()).isEqualTo(UPDATED_TITEUR);
        assertThat(testMembre.getTelephoneTiteur()).isEqualTo(DEFAULT_TELEPHONE_TITEUR);
    }

    @Test
    @Transactional
    void fullUpdateMembreWithPatch() throws Exception {
        // Initialize the database
        membreRepository.saveAndFlush(membre);

        int databaseSizeBeforeUpdate = membreRepository.findAll().size();

        // Update the membre using partial update
        Membre partialUpdatedMembre = new Membre();
        partialUpdatedMembre.setId(membre.getId());

        partialUpdatedMembre
            .nom(UPDATED_NOM)
            .prenom(UPDATED_PRENOM)
            .telephone(UPDATED_TELEPHONE)
            .sexe(UPDATED_SEXE)
            .email(UPDATED_EMAIL)
            .cni(UPDATED_CNI)
            .adressDakar(UPDATED_ADRESS_DAKAR)
            .situationMatrimoniale(UPDATED_SITUATION_MATRIMONIALE)
            .adresse(UPDATED_ADRESSE)
            .adresseVacance(UPDATED_ADRESSE_VACANCE)
            .profil(UPDATED_PROFIL)
            .profilContentType(UPDATED_PROFIL_CONTENT_TYPE)
            .dateNaissance(UPDATED_DATE_NAISSANCE)
            .boursier(UPDATED_BOURSIER)
            .daaraOrigine(UPDATED_DAARA_ORIGINE)
            .etatSante(UPDATED_ETAT_SANTE)
            .titeur(UPDATED_TITEUR)
            .telephoneTiteur(UPDATED_TELEPHONE_TITEUR);

        restMembreMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMembre.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMembre))
            )
            .andExpect(status().isOk());

        // Validate the Membre in the database
        List<Membre> membreList = membreRepository.findAll();
        assertThat(membreList).hasSize(databaseSizeBeforeUpdate);
        Membre testMembre = membreList.get(membreList.size() - 1);
        assertThat(testMembre.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testMembre.getPrenom()).isEqualTo(UPDATED_PRENOM);
        assertThat(testMembre.getTelephone()).isEqualTo(UPDATED_TELEPHONE);
        assertThat(testMembre.getSexe()).isEqualTo(UPDATED_SEXE);
        assertThat(testMembre.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testMembre.getCni()).isEqualTo(UPDATED_CNI);
        assertThat(testMembre.getAdressDakar()).isEqualTo(UPDATED_ADRESS_DAKAR);
        assertThat(testMembre.getSituationMatrimoniale()).isEqualTo(UPDATED_SITUATION_MATRIMONIALE);
        assertThat(testMembre.getAdresse()).isEqualTo(UPDATED_ADRESSE);
        assertThat(testMembre.getAdresseVacance()).isEqualTo(UPDATED_ADRESSE_VACANCE);
        assertThat(testMembre.getProfil()).isEqualTo(UPDATED_PROFIL);
        assertThat(testMembre.getProfilContentType()).isEqualTo(UPDATED_PROFIL_CONTENT_TYPE);
        assertThat(testMembre.getDateNaissance()).isEqualTo(UPDATED_DATE_NAISSANCE);
        assertThat(testMembre.getBoursier()).isEqualTo(UPDATED_BOURSIER);
        assertThat(testMembre.getDaaraOrigine()).isEqualTo(UPDATED_DAARA_ORIGINE);
        assertThat(testMembre.getEtatSante()).isEqualTo(UPDATED_ETAT_SANTE);
        assertThat(testMembre.getTiteur()).isEqualTo(UPDATED_TITEUR);
        assertThat(testMembre.getTelephoneTiteur()).isEqualTo(UPDATED_TELEPHONE_TITEUR);
    }

    @Test
    @Transactional
    void patchNonExistingMembre() throws Exception {
        int databaseSizeBeforeUpdate = membreRepository.findAll().size();
        membre.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMembreMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, membre.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(membre))
            )
            .andExpect(status().isBadRequest());

        // Validate the Membre in the database
        List<Membre> membreList = membreRepository.findAll();
        assertThat(membreList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchMembre() throws Exception {
        int databaseSizeBeforeUpdate = membreRepository.findAll().size();
        membre.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMembreMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(membre))
            )
            .andExpect(status().isBadRequest());

        // Validate the Membre in the database
        List<Membre> membreList = membreRepository.findAll();
        assertThat(membreList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamMembre() throws Exception {
        int databaseSizeBeforeUpdate = membreRepository.findAll().size();
        membre.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMembreMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(membre)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Membre in the database
        List<Membre> membreList = membreRepository.findAll();
        assertThat(membreList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteMembre() throws Exception {
        // Initialize the database
        membreRepository.saveAndFlush(membre);

        int databaseSizeBeforeDelete = membreRepository.findAll().size();

        // Delete the membre
        restMembreMockMvc
            .perform(delete(ENTITY_API_URL_ID, membre.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Membre> membreList = membreRepository.findAll();
        assertThat(membreList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
