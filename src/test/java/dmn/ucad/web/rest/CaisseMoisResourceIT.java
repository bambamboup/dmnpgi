package dmn.ucad.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import dmn.ucad.IntegrationTest;
import dmn.ucad.domain.CaisseMois;
import dmn.ucad.domain.Membre;
import dmn.ucad.repository.CaisseMoisRepository;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link CaisseMoisResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class CaisseMoisResourceIT {

    private static final LocalDate DEFAULT_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Integer DEFAULT_MONTANT = 1;
    private static final Integer UPDATED_MONTANT = 2;

    private static final Boolean DEFAULT_VALIDE = false;
    private static final Boolean UPDATED_VALIDE = true;

    private static final String ENTITY_API_URL = "/api/caisse-mois";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private CaisseMoisRepository caisseMoisRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCaisseMoisMockMvc;

    private CaisseMois caisseMois;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CaisseMois createEntity(EntityManager em) {
        CaisseMois caisseMois = new CaisseMois().date(DEFAULT_DATE).montant(DEFAULT_MONTANT).valide(DEFAULT_VALIDE);
        // Add required entity
        Membre membre;
        if (TestUtil.findAll(em, Membre.class).isEmpty()) {
            membre = MembreResourceIT.createEntity(em);
            em.persist(membre);
            em.flush();
        } else {
            membre = TestUtil.findAll(em, Membre.class).get(0);
        }
        caisseMois.setMembre(membre);
        return caisseMois;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CaisseMois createUpdatedEntity(EntityManager em) {
        CaisseMois caisseMois = new CaisseMois().date(UPDATED_DATE).montant(UPDATED_MONTANT).valide(UPDATED_VALIDE);
        // Add required entity
        Membre membre;
        if (TestUtil.findAll(em, Membre.class).isEmpty()) {
            membre = MembreResourceIT.createUpdatedEntity(em);
            em.persist(membre);
            em.flush();
        } else {
            membre = TestUtil.findAll(em, Membre.class).get(0);
        }
        caisseMois.setMembre(membre);
        return caisseMois;
    }

    @BeforeEach
    public void initTest() {
        caisseMois = createEntity(em);
    }

    @Test
    @Transactional
    void createCaisseMois() throws Exception {
        int databaseSizeBeforeCreate = caisseMoisRepository.findAll().size();
        // Create the CaisseMois
        restCaisseMoisMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(caisseMois)))
            .andExpect(status().isCreated());

        // Validate the CaisseMois in the database
        List<CaisseMois> caisseMoisList = caisseMoisRepository.findAll();
        assertThat(caisseMoisList).hasSize(databaseSizeBeforeCreate + 1);
        CaisseMois testCaisseMois = caisseMoisList.get(caisseMoisList.size() - 1);
        assertThat(testCaisseMois.getDate()).isEqualTo(DEFAULT_DATE);
        assertThat(testCaisseMois.getMontant()).isEqualTo(DEFAULT_MONTANT);
        assertThat(testCaisseMois.getValide()).isEqualTo(DEFAULT_VALIDE);
    }

    @Test
    @Transactional
    void createCaisseMoisWithExistingId() throws Exception {
        // Create the CaisseMois with an existing ID
        caisseMois.setId(1L);

        int databaseSizeBeforeCreate = caisseMoisRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restCaisseMoisMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(caisseMois)))
            .andExpect(status().isBadRequest());

        // Validate the CaisseMois in the database
        List<CaisseMois> caisseMoisList = caisseMoisRepository.findAll();
        assertThat(caisseMoisList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkMontantIsRequired() throws Exception {
        int databaseSizeBeforeTest = caisseMoisRepository.findAll().size();
        // set the field null
        caisseMois.setMontant(null);

        // Create the CaisseMois, which fails.

        restCaisseMoisMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(caisseMois)))
            .andExpect(status().isBadRequest());

        List<CaisseMois> caisseMoisList = caisseMoisRepository.findAll();
        assertThat(caisseMoisList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllCaisseMois() throws Exception {
        // Initialize the database
        caisseMoisRepository.saveAndFlush(caisseMois);

        // Get all the caisseMoisList
        restCaisseMoisMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(caisseMois.getId().intValue())))
            .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())))
            .andExpect(jsonPath("$.[*].montant").value(hasItem(DEFAULT_MONTANT)))
            .andExpect(jsonPath("$.[*].valide").value(hasItem(DEFAULT_VALIDE.booleanValue())));
    }

    @Test
    @Transactional
    void getCaisseMois() throws Exception {
        // Initialize the database
        caisseMoisRepository.saveAndFlush(caisseMois);

        // Get the caisseMois
        restCaisseMoisMockMvc
            .perform(get(ENTITY_API_URL_ID, caisseMois.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(caisseMois.getId().intValue()))
            .andExpect(jsonPath("$.date").value(DEFAULT_DATE.toString()))
            .andExpect(jsonPath("$.montant").value(DEFAULT_MONTANT))
            .andExpect(jsonPath("$.valide").value(DEFAULT_VALIDE.booleanValue()));
    }

    @Test
    @Transactional
    void getNonExistingCaisseMois() throws Exception {
        // Get the caisseMois
        restCaisseMoisMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewCaisseMois() throws Exception {
        // Initialize the database
        caisseMoisRepository.saveAndFlush(caisseMois);

        int databaseSizeBeforeUpdate = caisseMoisRepository.findAll().size();

        // Update the caisseMois
        CaisseMois updatedCaisseMois = caisseMoisRepository.findById(caisseMois.getId()).get();
        // Disconnect from session so that the updates on updatedCaisseMois are not directly saved in db
        em.detach(updatedCaisseMois);
        updatedCaisseMois.date(UPDATED_DATE).montant(UPDATED_MONTANT).valide(UPDATED_VALIDE);

        restCaisseMoisMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedCaisseMois.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedCaisseMois))
            )
            .andExpect(status().isOk());

        // Validate the CaisseMois in the database
        List<CaisseMois> caisseMoisList = caisseMoisRepository.findAll();
        assertThat(caisseMoisList).hasSize(databaseSizeBeforeUpdate);
        CaisseMois testCaisseMois = caisseMoisList.get(caisseMoisList.size() - 1);
        assertThat(testCaisseMois.getDate()).isEqualTo(UPDATED_DATE);
        assertThat(testCaisseMois.getMontant()).isEqualTo(UPDATED_MONTANT);
        assertThat(testCaisseMois.getValide()).isEqualTo(UPDATED_VALIDE);
    }

    @Test
    @Transactional
    void putNonExistingCaisseMois() throws Exception {
        int databaseSizeBeforeUpdate = caisseMoisRepository.findAll().size();
        caisseMois.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCaisseMoisMockMvc
            .perform(
                put(ENTITY_API_URL_ID, caisseMois.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(caisseMois))
            )
            .andExpect(status().isBadRequest());

        // Validate the CaisseMois in the database
        List<CaisseMois> caisseMoisList = caisseMoisRepository.findAll();
        assertThat(caisseMoisList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchCaisseMois() throws Exception {
        int databaseSizeBeforeUpdate = caisseMoisRepository.findAll().size();
        caisseMois.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCaisseMoisMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(caisseMois))
            )
            .andExpect(status().isBadRequest());

        // Validate the CaisseMois in the database
        List<CaisseMois> caisseMoisList = caisseMoisRepository.findAll();
        assertThat(caisseMoisList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamCaisseMois() throws Exception {
        int databaseSizeBeforeUpdate = caisseMoisRepository.findAll().size();
        caisseMois.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCaisseMoisMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(caisseMois)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the CaisseMois in the database
        List<CaisseMois> caisseMoisList = caisseMoisRepository.findAll();
        assertThat(caisseMoisList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateCaisseMoisWithPatch() throws Exception {
        // Initialize the database
        caisseMoisRepository.saveAndFlush(caisseMois);

        int databaseSizeBeforeUpdate = caisseMoisRepository.findAll().size();

        // Update the caisseMois using partial update
        CaisseMois partialUpdatedCaisseMois = new CaisseMois();
        partialUpdatedCaisseMois.setId(caisseMois.getId());

        partialUpdatedCaisseMois.date(UPDATED_DATE).montant(UPDATED_MONTANT);

        restCaisseMoisMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCaisseMois.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCaisseMois))
            )
            .andExpect(status().isOk());

        // Validate the CaisseMois in the database
        List<CaisseMois> caisseMoisList = caisseMoisRepository.findAll();
        assertThat(caisseMoisList).hasSize(databaseSizeBeforeUpdate);
        CaisseMois testCaisseMois = caisseMoisList.get(caisseMoisList.size() - 1);
        assertThat(testCaisseMois.getDate()).isEqualTo(UPDATED_DATE);
        assertThat(testCaisseMois.getMontant()).isEqualTo(UPDATED_MONTANT);
        assertThat(testCaisseMois.getValide()).isEqualTo(DEFAULT_VALIDE);
    }

    @Test
    @Transactional
    void fullUpdateCaisseMoisWithPatch() throws Exception {
        // Initialize the database
        caisseMoisRepository.saveAndFlush(caisseMois);

        int databaseSizeBeforeUpdate = caisseMoisRepository.findAll().size();

        // Update the caisseMois using partial update
        CaisseMois partialUpdatedCaisseMois = new CaisseMois();
        partialUpdatedCaisseMois.setId(caisseMois.getId());

        partialUpdatedCaisseMois.date(UPDATED_DATE).montant(UPDATED_MONTANT).valide(UPDATED_VALIDE);

        restCaisseMoisMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCaisseMois.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCaisseMois))
            )
            .andExpect(status().isOk());

        // Validate the CaisseMois in the database
        List<CaisseMois> caisseMoisList = caisseMoisRepository.findAll();
        assertThat(caisseMoisList).hasSize(databaseSizeBeforeUpdate);
        CaisseMois testCaisseMois = caisseMoisList.get(caisseMoisList.size() - 1);
        assertThat(testCaisseMois.getDate()).isEqualTo(UPDATED_DATE);
        assertThat(testCaisseMois.getMontant()).isEqualTo(UPDATED_MONTANT);
        assertThat(testCaisseMois.getValide()).isEqualTo(UPDATED_VALIDE);
    }

    @Test
    @Transactional
    void patchNonExistingCaisseMois() throws Exception {
        int databaseSizeBeforeUpdate = caisseMoisRepository.findAll().size();
        caisseMois.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCaisseMoisMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, caisseMois.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(caisseMois))
            )
            .andExpect(status().isBadRequest());

        // Validate the CaisseMois in the database
        List<CaisseMois> caisseMoisList = caisseMoisRepository.findAll();
        assertThat(caisseMoisList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchCaisseMois() throws Exception {
        int databaseSizeBeforeUpdate = caisseMoisRepository.findAll().size();
        caisseMois.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCaisseMoisMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(caisseMois))
            )
            .andExpect(status().isBadRequest());

        // Validate the CaisseMois in the database
        List<CaisseMois> caisseMoisList = caisseMoisRepository.findAll();
        assertThat(caisseMoisList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamCaisseMois() throws Exception {
        int databaseSizeBeforeUpdate = caisseMoisRepository.findAll().size();
        caisseMois.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCaisseMoisMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(caisseMois))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the CaisseMois in the database
        List<CaisseMois> caisseMoisList = caisseMoisRepository.findAll();
        assertThat(caisseMoisList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteCaisseMois() throws Exception {
        // Initialize the database
        caisseMoisRepository.saveAndFlush(caisseMois);

        int databaseSizeBeforeDelete = caisseMoisRepository.findAll().size();

        // Delete the caisseMois
        restCaisseMoisMockMvc
            .perform(delete(ENTITY_API_URL_ID, caisseMois.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CaisseMois> caisseMoisList = caisseMoisRepository.findAll();
        assertThat(caisseMoisList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
