package dmn.ucad.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import dmn.ucad.IntegrationTest;
import dmn.ucad.domain.Entite;
import dmn.ucad.domain.enumeration.EnumTypeEntite;
import dmn.ucad.repository.EntiteRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link EntiteResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class EntiteResourceIT {

    private static final String DEFAULT_NOM = "AAAAAAAAAA";
    private static final String UPDATED_NOM = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final EnumTypeEntite DEFAULT_TYPE_ENTITE = EnumTypeEntite.CELLULE;
    private static final EnumTypeEntite UPDATED_TYPE_ENTITE = EnumTypeEntite.COMMISSION;

    private static final String ENTITY_API_URL = "/api/entites";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private EntiteRepository entiteRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restEntiteMockMvc;

    private Entite entite;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Entite createEntity(EntityManager em) {
        Entite entite = new Entite().nom(DEFAULT_NOM).description(DEFAULT_DESCRIPTION).typeEntite(DEFAULT_TYPE_ENTITE);
        return entite;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Entite createUpdatedEntity(EntityManager em) {
        Entite entite = new Entite().nom(UPDATED_NOM).description(UPDATED_DESCRIPTION).typeEntite(UPDATED_TYPE_ENTITE);
        return entite;
    }

    @BeforeEach
    public void initTest() {
        entite = createEntity(em);
    }

    @Test
    @Transactional
    void createEntite() throws Exception {
        int databaseSizeBeforeCreate = entiteRepository.findAll().size();
        // Create the Entite
        restEntiteMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(entite)))
            .andExpect(status().isCreated());

        // Validate the Entite in the database
        List<Entite> entiteList = entiteRepository.findAll();
        assertThat(entiteList).hasSize(databaseSizeBeforeCreate + 1);
        Entite testEntite = entiteList.get(entiteList.size() - 1);
        assertThat(testEntite.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testEntite.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testEntite.getTypeEntite()).isEqualTo(DEFAULT_TYPE_ENTITE);
    }

    @Test
    @Transactional
    void createEntiteWithExistingId() throws Exception {
        // Create the Entite with an existing ID
        entite.setId(1L);

        int databaseSizeBeforeCreate = entiteRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restEntiteMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(entite)))
            .andExpect(status().isBadRequest());

        // Validate the Entite in the database
        List<Entite> entiteList = entiteRepository.findAll();
        assertThat(entiteList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNomIsRequired() throws Exception {
        int databaseSizeBeforeTest = entiteRepository.findAll().size();
        // set the field null
        entite.setNom(null);

        // Create the Entite, which fails.

        restEntiteMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(entite)))
            .andExpect(status().isBadRequest());

        List<Entite> entiteList = entiteRepository.findAll();
        assertThat(entiteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkTypeEntiteIsRequired() throws Exception {
        int databaseSizeBeforeTest = entiteRepository.findAll().size();
        // set the field null
        entite.setTypeEntite(null);

        // Create the Entite, which fails.

        restEntiteMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(entite)))
            .andExpect(status().isBadRequest());

        List<Entite> entiteList = entiteRepository.findAll();
        assertThat(entiteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllEntites() throws Exception {
        // Initialize the database
        entiteRepository.saveAndFlush(entite);

        // Get all the entiteList
        restEntiteMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(entite.getId().intValue())))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].typeEntite").value(hasItem(DEFAULT_TYPE_ENTITE.toString())));
    }

    @Test
    @Transactional
    void getEntite() throws Exception {
        // Initialize the database
        entiteRepository.saveAndFlush(entite);

        // Get the entite
        restEntiteMockMvc
            .perform(get(ENTITY_API_URL_ID, entite.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(entite.getId().intValue()))
            .andExpect(jsonPath("$.nom").value(DEFAULT_NOM))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.typeEntite").value(DEFAULT_TYPE_ENTITE.toString()));
    }

    @Test
    @Transactional
    void getNonExistingEntite() throws Exception {
        // Get the entite
        restEntiteMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewEntite() throws Exception {
        // Initialize the database
        entiteRepository.saveAndFlush(entite);

        int databaseSizeBeforeUpdate = entiteRepository.findAll().size();

        // Update the entite
        Entite updatedEntite = entiteRepository.findById(entite.getId()).get();
        // Disconnect from session so that the updates on updatedEntite are not directly saved in db
        em.detach(updatedEntite);
        updatedEntite.nom(UPDATED_NOM).description(UPDATED_DESCRIPTION).typeEntite(UPDATED_TYPE_ENTITE);

        restEntiteMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedEntite.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedEntite))
            )
            .andExpect(status().isOk());

        // Validate the Entite in the database
        List<Entite> entiteList = entiteRepository.findAll();
        assertThat(entiteList).hasSize(databaseSizeBeforeUpdate);
        Entite testEntite = entiteList.get(entiteList.size() - 1);
        assertThat(testEntite.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testEntite.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testEntite.getTypeEntite()).isEqualTo(UPDATED_TYPE_ENTITE);
    }

    @Test
    @Transactional
    void putNonExistingEntite() throws Exception {
        int databaseSizeBeforeUpdate = entiteRepository.findAll().size();
        entite.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEntiteMockMvc
            .perform(
                put(ENTITY_API_URL_ID, entite.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(entite))
            )
            .andExpect(status().isBadRequest());

        // Validate the Entite in the database
        List<Entite> entiteList = entiteRepository.findAll();
        assertThat(entiteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchEntite() throws Exception {
        int databaseSizeBeforeUpdate = entiteRepository.findAll().size();
        entite.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEntiteMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(entite))
            )
            .andExpect(status().isBadRequest());

        // Validate the Entite in the database
        List<Entite> entiteList = entiteRepository.findAll();
        assertThat(entiteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamEntite() throws Exception {
        int databaseSizeBeforeUpdate = entiteRepository.findAll().size();
        entite.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEntiteMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(entite)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Entite in the database
        List<Entite> entiteList = entiteRepository.findAll();
        assertThat(entiteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateEntiteWithPatch() throws Exception {
        // Initialize the database
        entiteRepository.saveAndFlush(entite);

        int databaseSizeBeforeUpdate = entiteRepository.findAll().size();

        // Update the entite using partial update
        Entite partialUpdatedEntite = new Entite();
        partialUpdatedEntite.setId(entite.getId());

        partialUpdatedEntite.nom(UPDATED_NOM).typeEntite(UPDATED_TYPE_ENTITE);

        restEntiteMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedEntite.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedEntite))
            )
            .andExpect(status().isOk());

        // Validate the Entite in the database
        List<Entite> entiteList = entiteRepository.findAll();
        assertThat(entiteList).hasSize(databaseSizeBeforeUpdate);
        Entite testEntite = entiteList.get(entiteList.size() - 1);
        assertThat(testEntite.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testEntite.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testEntite.getTypeEntite()).isEqualTo(UPDATED_TYPE_ENTITE);
    }

    @Test
    @Transactional
    void fullUpdateEntiteWithPatch() throws Exception {
        // Initialize the database
        entiteRepository.saveAndFlush(entite);

        int databaseSizeBeforeUpdate = entiteRepository.findAll().size();

        // Update the entite using partial update
        Entite partialUpdatedEntite = new Entite();
        partialUpdatedEntite.setId(entite.getId());

        partialUpdatedEntite.nom(UPDATED_NOM).description(UPDATED_DESCRIPTION).typeEntite(UPDATED_TYPE_ENTITE);

        restEntiteMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedEntite.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedEntite))
            )
            .andExpect(status().isOk());

        // Validate the Entite in the database
        List<Entite> entiteList = entiteRepository.findAll();
        assertThat(entiteList).hasSize(databaseSizeBeforeUpdate);
        Entite testEntite = entiteList.get(entiteList.size() - 1);
        assertThat(testEntite.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testEntite.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testEntite.getTypeEntite()).isEqualTo(UPDATED_TYPE_ENTITE);
    }

    @Test
    @Transactional
    void patchNonExistingEntite() throws Exception {
        int databaseSizeBeforeUpdate = entiteRepository.findAll().size();
        entite.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEntiteMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, entite.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(entite))
            )
            .andExpect(status().isBadRequest());

        // Validate the Entite in the database
        List<Entite> entiteList = entiteRepository.findAll();
        assertThat(entiteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchEntite() throws Exception {
        int databaseSizeBeforeUpdate = entiteRepository.findAll().size();
        entite.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEntiteMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(entite))
            )
            .andExpect(status().isBadRequest());

        // Validate the Entite in the database
        List<Entite> entiteList = entiteRepository.findAll();
        assertThat(entiteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamEntite() throws Exception {
        int databaseSizeBeforeUpdate = entiteRepository.findAll().size();
        entite.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEntiteMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(entite)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Entite in the database
        List<Entite> entiteList = entiteRepository.findAll();
        assertThat(entiteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteEntite() throws Exception {
        // Initialize the database
        entiteRepository.saveAndFlush(entite);

        int databaseSizeBeforeDelete = entiteRepository.findAll().size();

        // Delete the entite
        restEntiteMockMvc
            .perform(delete(ENTITY_API_URL_ID, entite.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Entite> entiteList = entiteRepository.findAll();
        assertThat(entiteList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
