package dmn.ucad.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import dmn.ucad.IntegrationTest;
import dmn.ucad.domain.Kourel;
import dmn.ucad.domain.Membre;
import dmn.ucad.domain.MembreKourel;
import dmn.ucad.repository.MembreKourelRepository;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link MembreKourelResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class MembreKourelResourceIT {

    private static final LocalDate DEFAULT_DATE_DEBUT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_DEBUT = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_DATE_FIN = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_FIN = LocalDate.now(ZoneId.systemDefault());

    private static final Boolean DEFAULT_ACTIF = false;
    private static final Boolean UPDATED_ACTIF = true;

    private static final String ENTITY_API_URL = "/api/membre-kourels";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private MembreKourelRepository membreKourelRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restMembreKourelMockMvc;

    private MembreKourel membreKourel;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MembreKourel createEntity(EntityManager em) {
        MembreKourel membreKourel = new MembreKourel().dateDebut(DEFAULT_DATE_DEBUT).dateFin(DEFAULT_DATE_FIN).actif(DEFAULT_ACTIF);
        // Add required entity
        Membre membre;
        if (TestUtil.findAll(em, Membre.class).isEmpty()) {
            membre = MembreResourceIT.createEntity(em);
            em.persist(membre);
            em.flush();
        } else {
            membre = TestUtil.findAll(em, Membre.class).get(0);
        }
        membreKourel.setMembre(membre);
        // Add required entity
        Kourel kourel;
        if (TestUtil.findAll(em, Kourel.class).isEmpty()) {
            kourel = KourelResourceIT.createEntity(em);
            em.persist(kourel);
            em.flush();
        } else {
            kourel = TestUtil.findAll(em, Kourel.class).get(0);
        }
        membreKourel.setKourel(kourel);
        return membreKourel;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MembreKourel createUpdatedEntity(EntityManager em) {
        MembreKourel membreKourel = new MembreKourel().dateDebut(UPDATED_DATE_DEBUT).dateFin(UPDATED_DATE_FIN).actif(UPDATED_ACTIF);
        // Add required entity
        Membre membre;
        if (TestUtil.findAll(em, Membre.class).isEmpty()) {
            membre = MembreResourceIT.createUpdatedEntity(em);
            em.persist(membre);
            em.flush();
        } else {
            membre = TestUtil.findAll(em, Membre.class).get(0);
        }
        membreKourel.setMembre(membre);
        // Add required entity
        Kourel kourel;
        if (TestUtil.findAll(em, Kourel.class).isEmpty()) {
            kourel = KourelResourceIT.createUpdatedEntity(em);
            em.persist(kourel);
            em.flush();
        } else {
            kourel = TestUtil.findAll(em, Kourel.class).get(0);
        }
        membreKourel.setKourel(kourel);
        return membreKourel;
    }

    @BeforeEach
    public void initTest() {
        membreKourel = createEntity(em);
    }

    @Test
    @Transactional
    void createMembreKourel() throws Exception {
        int databaseSizeBeforeCreate = membreKourelRepository.findAll().size();
        // Create the MembreKourel
        restMembreKourelMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(membreKourel)))
            .andExpect(status().isCreated());

        // Validate the MembreKourel in the database
        List<MembreKourel> membreKourelList = membreKourelRepository.findAll();
        assertThat(membreKourelList).hasSize(databaseSizeBeforeCreate + 1);
        MembreKourel testMembreKourel = membreKourelList.get(membreKourelList.size() - 1);
        assertThat(testMembreKourel.getDateDebut()).isEqualTo(DEFAULT_DATE_DEBUT);
        assertThat(testMembreKourel.getDateFin()).isEqualTo(DEFAULT_DATE_FIN);
        assertThat(testMembreKourel.getActif()).isEqualTo(DEFAULT_ACTIF);
    }

    @Test
    @Transactional
    void createMembreKourelWithExistingId() throws Exception {
        // Create the MembreKourel with an existing ID
        membreKourel.setId(1L);

        int databaseSizeBeforeCreate = membreKourelRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restMembreKourelMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(membreKourel)))
            .andExpect(status().isBadRequest());

        // Validate the MembreKourel in the database
        List<MembreKourel> membreKourelList = membreKourelRepository.findAll();
        assertThat(membreKourelList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllMembreKourels() throws Exception {
        // Initialize the database
        membreKourelRepository.saveAndFlush(membreKourel);

        // Get all the membreKourelList
        restMembreKourelMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(membreKourel.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateDebut").value(hasItem(DEFAULT_DATE_DEBUT.toString())))
            .andExpect(jsonPath("$.[*].dateFin").value(hasItem(DEFAULT_DATE_FIN.toString())))
            .andExpect(jsonPath("$.[*].actif").value(hasItem(DEFAULT_ACTIF.booleanValue())));
    }

    @Test
    @Transactional
    void getMembreKourel() throws Exception {
        // Initialize the database
        membreKourelRepository.saveAndFlush(membreKourel);

        // Get the membreKourel
        restMembreKourelMockMvc
            .perform(get(ENTITY_API_URL_ID, membreKourel.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(membreKourel.getId().intValue()))
            .andExpect(jsonPath("$.dateDebut").value(DEFAULT_DATE_DEBUT.toString()))
            .andExpect(jsonPath("$.dateFin").value(DEFAULT_DATE_FIN.toString()))
            .andExpect(jsonPath("$.actif").value(DEFAULT_ACTIF.booleanValue()));
    }

    @Test
    @Transactional
    void getNonExistingMembreKourel() throws Exception {
        // Get the membreKourel
        restMembreKourelMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewMembreKourel() throws Exception {
        // Initialize the database
        membreKourelRepository.saveAndFlush(membreKourel);

        int databaseSizeBeforeUpdate = membreKourelRepository.findAll().size();

        // Update the membreKourel
        MembreKourel updatedMembreKourel = membreKourelRepository.findById(membreKourel.getId()).get();
        // Disconnect from session so that the updates on updatedMembreKourel are not directly saved in db
        em.detach(updatedMembreKourel);
        updatedMembreKourel.dateDebut(UPDATED_DATE_DEBUT).dateFin(UPDATED_DATE_FIN).actif(UPDATED_ACTIF);

        restMembreKourelMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedMembreKourel.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedMembreKourel))
            )
            .andExpect(status().isOk());

        // Validate the MembreKourel in the database
        List<MembreKourel> membreKourelList = membreKourelRepository.findAll();
        assertThat(membreKourelList).hasSize(databaseSizeBeforeUpdate);
        MembreKourel testMembreKourel = membreKourelList.get(membreKourelList.size() - 1);
        assertThat(testMembreKourel.getDateDebut()).isEqualTo(UPDATED_DATE_DEBUT);
        assertThat(testMembreKourel.getDateFin()).isEqualTo(UPDATED_DATE_FIN);
        assertThat(testMembreKourel.getActif()).isEqualTo(UPDATED_ACTIF);
    }

    @Test
    @Transactional
    void putNonExistingMembreKourel() throws Exception {
        int databaseSizeBeforeUpdate = membreKourelRepository.findAll().size();
        membreKourel.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMembreKourelMockMvc
            .perform(
                put(ENTITY_API_URL_ID, membreKourel.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(membreKourel))
            )
            .andExpect(status().isBadRequest());

        // Validate the MembreKourel in the database
        List<MembreKourel> membreKourelList = membreKourelRepository.findAll();
        assertThat(membreKourelList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchMembreKourel() throws Exception {
        int databaseSizeBeforeUpdate = membreKourelRepository.findAll().size();
        membreKourel.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMembreKourelMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(membreKourel))
            )
            .andExpect(status().isBadRequest());

        // Validate the MembreKourel in the database
        List<MembreKourel> membreKourelList = membreKourelRepository.findAll();
        assertThat(membreKourelList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamMembreKourel() throws Exception {
        int databaseSizeBeforeUpdate = membreKourelRepository.findAll().size();
        membreKourel.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMembreKourelMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(membreKourel)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the MembreKourel in the database
        List<MembreKourel> membreKourelList = membreKourelRepository.findAll();
        assertThat(membreKourelList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateMembreKourelWithPatch() throws Exception {
        // Initialize the database
        membreKourelRepository.saveAndFlush(membreKourel);

        int databaseSizeBeforeUpdate = membreKourelRepository.findAll().size();

        // Update the membreKourel using partial update
        MembreKourel partialUpdatedMembreKourel = new MembreKourel();
        partialUpdatedMembreKourel.setId(membreKourel.getId());

        restMembreKourelMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMembreKourel.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMembreKourel))
            )
            .andExpect(status().isOk());

        // Validate the MembreKourel in the database
        List<MembreKourel> membreKourelList = membreKourelRepository.findAll();
        assertThat(membreKourelList).hasSize(databaseSizeBeforeUpdate);
        MembreKourel testMembreKourel = membreKourelList.get(membreKourelList.size() - 1);
        assertThat(testMembreKourel.getDateDebut()).isEqualTo(DEFAULT_DATE_DEBUT);
        assertThat(testMembreKourel.getDateFin()).isEqualTo(DEFAULT_DATE_FIN);
        assertThat(testMembreKourel.getActif()).isEqualTo(DEFAULT_ACTIF);
    }

    @Test
    @Transactional
    void fullUpdateMembreKourelWithPatch() throws Exception {
        // Initialize the database
        membreKourelRepository.saveAndFlush(membreKourel);

        int databaseSizeBeforeUpdate = membreKourelRepository.findAll().size();

        // Update the membreKourel using partial update
        MembreKourel partialUpdatedMembreKourel = new MembreKourel();
        partialUpdatedMembreKourel.setId(membreKourel.getId());

        partialUpdatedMembreKourel.dateDebut(UPDATED_DATE_DEBUT).dateFin(UPDATED_DATE_FIN).actif(UPDATED_ACTIF);

        restMembreKourelMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMembreKourel.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMembreKourel))
            )
            .andExpect(status().isOk());

        // Validate the MembreKourel in the database
        List<MembreKourel> membreKourelList = membreKourelRepository.findAll();
        assertThat(membreKourelList).hasSize(databaseSizeBeforeUpdate);
        MembreKourel testMembreKourel = membreKourelList.get(membreKourelList.size() - 1);
        assertThat(testMembreKourel.getDateDebut()).isEqualTo(UPDATED_DATE_DEBUT);
        assertThat(testMembreKourel.getDateFin()).isEqualTo(UPDATED_DATE_FIN);
        assertThat(testMembreKourel.getActif()).isEqualTo(UPDATED_ACTIF);
    }

    @Test
    @Transactional
    void patchNonExistingMembreKourel() throws Exception {
        int databaseSizeBeforeUpdate = membreKourelRepository.findAll().size();
        membreKourel.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMembreKourelMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, membreKourel.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(membreKourel))
            )
            .andExpect(status().isBadRequest());

        // Validate the MembreKourel in the database
        List<MembreKourel> membreKourelList = membreKourelRepository.findAll();
        assertThat(membreKourelList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchMembreKourel() throws Exception {
        int databaseSizeBeforeUpdate = membreKourelRepository.findAll().size();
        membreKourel.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMembreKourelMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(membreKourel))
            )
            .andExpect(status().isBadRequest());

        // Validate the MembreKourel in the database
        List<MembreKourel> membreKourelList = membreKourelRepository.findAll();
        assertThat(membreKourelList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamMembreKourel() throws Exception {
        int databaseSizeBeforeUpdate = membreKourelRepository.findAll().size();
        membreKourel.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMembreKourelMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(membreKourel))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the MembreKourel in the database
        List<MembreKourel> membreKourelList = membreKourelRepository.findAll();
        assertThat(membreKourelList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteMembreKourel() throws Exception {
        // Initialize the database
        membreKourelRepository.saveAndFlush(membreKourel);

        int databaseSizeBeforeDelete = membreKourelRepository.findAll().size();

        // Delete the membreKourel
        restMembreKourelMockMvc
            .perform(delete(ENTITY_API_URL_ID, membreKourel.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<MembreKourel> membreKourelList = membreKourelRepository.findAll();
        assertThat(membreKourelList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
