package dmn.ucad.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import dmn.ucad.IntegrationTest;
import dmn.ucad.domain.NiveauXamXam;
import dmn.ucad.domain.XamXam;
import dmn.ucad.repository.XamXamRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link XamXamResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class XamXamResourceIT {

    private static final String DEFAULT_LIBELLE = "AAAAAAAAAA";
    private static final String UPDATED_LIBELLE = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/xam-xams";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private XamXamRepository xamXamRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restXamXamMockMvc;

    private XamXam xamXam;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static XamXam createEntity(EntityManager em) {
        XamXam xamXam = new XamXam().libelle(DEFAULT_LIBELLE);
        // Add required entity
        NiveauXamXam niveauXamXam;
        if (TestUtil.findAll(em, NiveauXamXam.class).isEmpty()) {
            niveauXamXam = NiveauXamXamResourceIT.createEntity(em);
            em.persist(niveauXamXam);
            em.flush();
        } else {
            niveauXamXam = TestUtil.findAll(em, NiveauXamXam.class).get(0);
        }
        xamXam.setNiveauXamXam(niveauXamXam);
        return xamXam;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static XamXam createUpdatedEntity(EntityManager em) {
        XamXam xamXam = new XamXam().libelle(UPDATED_LIBELLE);
        // Add required entity
        NiveauXamXam niveauXamXam;
        if (TestUtil.findAll(em, NiveauXamXam.class).isEmpty()) {
            niveauXamXam = NiveauXamXamResourceIT.createUpdatedEntity(em);
            em.persist(niveauXamXam);
            em.flush();
        } else {
            niveauXamXam = TestUtil.findAll(em, NiveauXamXam.class).get(0);
        }
        xamXam.setNiveauXamXam(niveauXamXam);
        return xamXam;
    }

    @BeforeEach
    public void initTest() {
        xamXam = createEntity(em);
    }

    @Test
    @Transactional
    void createXamXam() throws Exception {
        int databaseSizeBeforeCreate = xamXamRepository.findAll().size();
        // Create the XamXam
        restXamXamMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(xamXam)))
            .andExpect(status().isCreated());

        // Validate the XamXam in the database
        List<XamXam> xamXamList = xamXamRepository.findAll();
        assertThat(xamXamList).hasSize(databaseSizeBeforeCreate + 1);
        XamXam testXamXam = xamXamList.get(xamXamList.size() - 1);
        assertThat(testXamXam.getLibelle()).isEqualTo(DEFAULT_LIBELLE);
    }

    @Test
    @Transactional
    void createXamXamWithExistingId() throws Exception {
        // Create the XamXam with an existing ID
        xamXam.setId(1L);

        int databaseSizeBeforeCreate = xamXamRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restXamXamMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(xamXam)))
            .andExpect(status().isBadRequest());

        // Validate the XamXam in the database
        List<XamXam> xamXamList = xamXamRepository.findAll();
        assertThat(xamXamList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkLibelleIsRequired() throws Exception {
        int databaseSizeBeforeTest = xamXamRepository.findAll().size();
        // set the field null
        xamXam.setLibelle(null);

        // Create the XamXam, which fails.

        restXamXamMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(xamXam)))
            .andExpect(status().isBadRequest());

        List<XamXam> xamXamList = xamXamRepository.findAll();
        assertThat(xamXamList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllXamXams() throws Exception {
        // Initialize the database
        xamXamRepository.saveAndFlush(xamXam);

        // Get all the xamXamList
        restXamXamMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(xamXam.getId().intValue())))
            .andExpect(jsonPath("$.[*].libelle").value(hasItem(DEFAULT_LIBELLE)));
    }

    @Test
    @Transactional
    void getXamXam() throws Exception {
        // Initialize the database
        xamXamRepository.saveAndFlush(xamXam);

        // Get the xamXam
        restXamXamMockMvc
            .perform(get(ENTITY_API_URL_ID, xamXam.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(xamXam.getId().intValue()))
            .andExpect(jsonPath("$.libelle").value(DEFAULT_LIBELLE));
    }

    @Test
    @Transactional
    void getNonExistingXamXam() throws Exception {
        // Get the xamXam
        restXamXamMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewXamXam() throws Exception {
        // Initialize the database
        xamXamRepository.saveAndFlush(xamXam);

        int databaseSizeBeforeUpdate = xamXamRepository.findAll().size();

        // Update the xamXam
        XamXam updatedXamXam = xamXamRepository.findById(xamXam.getId()).get();
        // Disconnect from session so that the updates on updatedXamXam are not directly saved in db
        em.detach(updatedXamXam);
        updatedXamXam.libelle(UPDATED_LIBELLE);

        restXamXamMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedXamXam.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedXamXam))
            )
            .andExpect(status().isOk());

        // Validate the XamXam in the database
        List<XamXam> xamXamList = xamXamRepository.findAll();
        assertThat(xamXamList).hasSize(databaseSizeBeforeUpdate);
        XamXam testXamXam = xamXamList.get(xamXamList.size() - 1);
        assertThat(testXamXam.getLibelle()).isEqualTo(UPDATED_LIBELLE);
    }

    @Test
    @Transactional
    void putNonExistingXamXam() throws Exception {
        int databaseSizeBeforeUpdate = xamXamRepository.findAll().size();
        xamXam.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restXamXamMockMvc
            .perform(
                put(ENTITY_API_URL_ID, xamXam.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(xamXam))
            )
            .andExpect(status().isBadRequest());

        // Validate the XamXam in the database
        List<XamXam> xamXamList = xamXamRepository.findAll();
        assertThat(xamXamList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchXamXam() throws Exception {
        int databaseSizeBeforeUpdate = xamXamRepository.findAll().size();
        xamXam.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restXamXamMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(xamXam))
            )
            .andExpect(status().isBadRequest());

        // Validate the XamXam in the database
        List<XamXam> xamXamList = xamXamRepository.findAll();
        assertThat(xamXamList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamXamXam() throws Exception {
        int databaseSizeBeforeUpdate = xamXamRepository.findAll().size();
        xamXam.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restXamXamMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(xamXam)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the XamXam in the database
        List<XamXam> xamXamList = xamXamRepository.findAll();
        assertThat(xamXamList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateXamXamWithPatch() throws Exception {
        // Initialize the database
        xamXamRepository.saveAndFlush(xamXam);

        int databaseSizeBeforeUpdate = xamXamRepository.findAll().size();

        // Update the xamXam using partial update
        XamXam partialUpdatedXamXam = new XamXam();
        partialUpdatedXamXam.setId(xamXam.getId());

        restXamXamMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedXamXam.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedXamXam))
            )
            .andExpect(status().isOk());

        // Validate the XamXam in the database
        List<XamXam> xamXamList = xamXamRepository.findAll();
        assertThat(xamXamList).hasSize(databaseSizeBeforeUpdate);
        XamXam testXamXam = xamXamList.get(xamXamList.size() - 1);
        assertThat(testXamXam.getLibelle()).isEqualTo(DEFAULT_LIBELLE);
    }

    @Test
    @Transactional
    void fullUpdateXamXamWithPatch() throws Exception {
        // Initialize the database
        xamXamRepository.saveAndFlush(xamXam);

        int databaseSizeBeforeUpdate = xamXamRepository.findAll().size();

        // Update the xamXam using partial update
        XamXam partialUpdatedXamXam = new XamXam();
        partialUpdatedXamXam.setId(xamXam.getId());

        partialUpdatedXamXam.libelle(UPDATED_LIBELLE);

        restXamXamMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedXamXam.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedXamXam))
            )
            .andExpect(status().isOk());

        // Validate the XamXam in the database
        List<XamXam> xamXamList = xamXamRepository.findAll();
        assertThat(xamXamList).hasSize(databaseSizeBeforeUpdate);
        XamXam testXamXam = xamXamList.get(xamXamList.size() - 1);
        assertThat(testXamXam.getLibelle()).isEqualTo(UPDATED_LIBELLE);
    }

    @Test
    @Transactional
    void patchNonExistingXamXam() throws Exception {
        int databaseSizeBeforeUpdate = xamXamRepository.findAll().size();
        xamXam.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restXamXamMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, xamXam.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(xamXam))
            )
            .andExpect(status().isBadRequest());

        // Validate the XamXam in the database
        List<XamXam> xamXamList = xamXamRepository.findAll();
        assertThat(xamXamList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchXamXam() throws Exception {
        int databaseSizeBeforeUpdate = xamXamRepository.findAll().size();
        xamXam.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restXamXamMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(xamXam))
            )
            .andExpect(status().isBadRequest());

        // Validate the XamXam in the database
        List<XamXam> xamXamList = xamXamRepository.findAll();
        assertThat(xamXamList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamXamXam() throws Exception {
        int databaseSizeBeforeUpdate = xamXamRepository.findAll().size();
        xamXam.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restXamXamMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(xamXam)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the XamXam in the database
        List<XamXam> xamXamList = xamXamRepository.findAll();
        assertThat(xamXamList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteXamXam() throws Exception {
        // Initialize the database
        xamXamRepository.saveAndFlush(xamXam);

        int databaseSizeBeforeDelete = xamXamRepository.findAll().size();

        // Delete the xamXam
        restXamXamMockMvc
            .perform(delete(ENTITY_API_URL_ID, xamXam.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<XamXam> xamXamList = xamXamRepository.findAll();
        assertThat(xamXamList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
