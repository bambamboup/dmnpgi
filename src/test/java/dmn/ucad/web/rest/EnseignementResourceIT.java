package dmn.ucad.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import dmn.ucad.IntegrationTest;
import dmn.ucad.domain.Enseignement;
import dmn.ucad.domain.Membre;
import dmn.ucad.repository.EnseignementRepository;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link EnseignementResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class EnseignementResourceIT {

    private static final LocalDate DEFAULT_DATE_DEBUT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_DEBUT = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_DATE_FIN = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_FIN = LocalDate.now(ZoneId.systemDefault());

    private static final Boolean DEFAULT_ENCOURS = false;
    private static final Boolean UPDATED_ENCOURS = true;

    private static final String ENTITY_API_URL = "/api/enseignements";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private EnseignementRepository enseignementRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restEnseignementMockMvc;

    private Enseignement enseignement;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Enseignement createEntity(EntityManager em) {
        Enseignement enseignement = new Enseignement().dateDebut(DEFAULT_DATE_DEBUT).dateFin(DEFAULT_DATE_FIN).encours(DEFAULT_ENCOURS);
        // Add required entity
        Membre membre;
        if (TestUtil.findAll(em, Membre.class).isEmpty()) {
            membre = MembreResourceIT.createEntity(em);
            em.persist(membre);
            em.flush();
        } else {
            membre = TestUtil.findAll(em, Membre.class).get(0);
        }
        enseignement.setMembre(membre);
        return enseignement;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Enseignement createUpdatedEntity(EntityManager em) {
        Enseignement enseignement = new Enseignement().dateDebut(UPDATED_DATE_DEBUT).dateFin(UPDATED_DATE_FIN).encours(UPDATED_ENCOURS);
        // Add required entity
        Membre membre;
        if (TestUtil.findAll(em, Membre.class).isEmpty()) {
            membre = MembreResourceIT.createUpdatedEntity(em);
            em.persist(membre);
            em.flush();
        } else {
            membre = TestUtil.findAll(em, Membre.class).get(0);
        }
        enseignement.setMembre(membre);
        return enseignement;
    }

    @BeforeEach
    public void initTest() {
        enseignement = createEntity(em);
    }

    @Test
    @Transactional
    void createEnseignement() throws Exception {
        int databaseSizeBeforeCreate = enseignementRepository.findAll().size();
        // Create the Enseignement
        restEnseignementMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(enseignement)))
            .andExpect(status().isCreated());

        // Validate the Enseignement in the database
        List<Enseignement> enseignementList = enseignementRepository.findAll();
        assertThat(enseignementList).hasSize(databaseSizeBeforeCreate + 1);
        Enseignement testEnseignement = enseignementList.get(enseignementList.size() - 1);
        assertThat(testEnseignement.getDateDebut()).isEqualTo(DEFAULT_DATE_DEBUT);
        assertThat(testEnseignement.getDateFin()).isEqualTo(DEFAULT_DATE_FIN);
        assertThat(testEnseignement.getEncours()).isEqualTo(DEFAULT_ENCOURS);
    }

    @Test
    @Transactional
    void createEnseignementWithExistingId() throws Exception {
        // Create the Enseignement with an existing ID
        enseignement.setId(1L);

        int databaseSizeBeforeCreate = enseignementRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restEnseignementMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(enseignement)))
            .andExpect(status().isBadRequest());

        // Validate the Enseignement in the database
        List<Enseignement> enseignementList = enseignementRepository.findAll();
        assertThat(enseignementList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkDateDebutIsRequired() throws Exception {
        int databaseSizeBeforeTest = enseignementRepository.findAll().size();
        // set the field null
        enseignement.setDateDebut(null);

        // Create the Enseignement, which fails.

        restEnseignementMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(enseignement)))
            .andExpect(status().isBadRequest());

        List<Enseignement> enseignementList = enseignementRepository.findAll();
        assertThat(enseignementList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllEnseignements() throws Exception {
        // Initialize the database
        enseignementRepository.saveAndFlush(enseignement);

        // Get all the enseignementList
        restEnseignementMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(enseignement.getId().intValue())))
            .andExpect(jsonPath("$.[*].dateDebut").value(hasItem(DEFAULT_DATE_DEBUT.toString())))
            .andExpect(jsonPath("$.[*].dateFin").value(hasItem(DEFAULT_DATE_FIN.toString())))
            .andExpect(jsonPath("$.[*].encours").value(hasItem(DEFAULT_ENCOURS.booleanValue())));
    }

    @Test
    @Transactional
    void getEnseignement() throws Exception {
        // Initialize the database
        enseignementRepository.saveAndFlush(enseignement);

        // Get the enseignement
        restEnseignementMockMvc
            .perform(get(ENTITY_API_URL_ID, enseignement.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(enseignement.getId().intValue()))
            .andExpect(jsonPath("$.dateDebut").value(DEFAULT_DATE_DEBUT.toString()))
            .andExpect(jsonPath("$.dateFin").value(DEFAULT_DATE_FIN.toString()))
            .andExpect(jsonPath("$.encours").value(DEFAULT_ENCOURS.booleanValue()));
    }

    @Test
    @Transactional
    void getNonExistingEnseignement() throws Exception {
        // Get the enseignement
        restEnseignementMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewEnseignement() throws Exception {
        // Initialize the database
        enseignementRepository.saveAndFlush(enseignement);

        int databaseSizeBeforeUpdate = enseignementRepository.findAll().size();

        // Update the enseignement
        Enseignement updatedEnseignement = enseignementRepository.findById(enseignement.getId()).get();
        // Disconnect from session so that the updates on updatedEnseignement are not directly saved in db
        em.detach(updatedEnseignement);
        updatedEnseignement.dateDebut(UPDATED_DATE_DEBUT).dateFin(UPDATED_DATE_FIN).encours(UPDATED_ENCOURS);

        restEnseignementMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedEnseignement.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedEnseignement))
            )
            .andExpect(status().isOk());

        // Validate the Enseignement in the database
        List<Enseignement> enseignementList = enseignementRepository.findAll();
        assertThat(enseignementList).hasSize(databaseSizeBeforeUpdate);
        Enseignement testEnseignement = enseignementList.get(enseignementList.size() - 1);
        assertThat(testEnseignement.getDateDebut()).isEqualTo(UPDATED_DATE_DEBUT);
        assertThat(testEnseignement.getDateFin()).isEqualTo(UPDATED_DATE_FIN);
        assertThat(testEnseignement.getEncours()).isEqualTo(UPDATED_ENCOURS);
    }

    @Test
    @Transactional
    void putNonExistingEnseignement() throws Exception {
        int databaseSizeBeforeUpdate = enseignementRepository.findAll().size();
        enseignement.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEnseignementMockMvc
            .perform(
                put(ENTITY_API_URL_ID, enseignement.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(enseignement))
            )
            .andExpect(status().isBadRequest());

        // Validate the Enseignement in the database
        List<Enseignement> enseignementList = enseignementRepository.findAll();
        assertThat(enseignementList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchEnseignement() throws Exception {
        int databaseSizeBeforeUpdate = enseignementRepository.findAll().size();
        enseignement.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEnseignementMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(enseignement))
            )
            .andExpect(status().isBadRequest());

        // Validate the Enseignement in the database
        List<Enseignement> enseignementList = enseignementRepository.findAll();
        assertThat(enseignementList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamEnseignement() throws Exception {
        int databaseSizeBeforeUpdate = enseignementRepository.findAll().size();
        enseignement.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEnseignementMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(enseignement)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Enseignement in the database
        List<Enseignement> enseignementList = enseignementRepository.findAll();
        assertThat(enseignementList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateEnseignementWithPatch() throws Exception {
        // Initialize the database
        enseignementRepository.saveAndFlush(enseignement);

        int databaseSizeBeforeUpdate = enseignementRepository.findAll().size();

        // Update the enseignement using partial update
        Enseignement partialUpdatedEnseignement = new Enseignement();
        partialUpdatedEnseignement.setId(enseignement.getId());

        partialUpdatedEnseignement.dateDebut(UPDATED_DATE_DEBUT).dateFin(UPDATED_DATE_FIN).encours(UPDATED_ENCOURS);

        restEnseignementMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedEnseignement.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedEnseignement))
            )
            .andExpect(status().isOk());

        // Validate the Enseignement in the database
        List<Enseignement> enseignementList = enseignementRepository.findAll();
        assertThat(enseignementList).hasSize(databaseSizeBeforeUpdate);
        Enseignement testEnseignement = enseignementList.get(enseignementList.size() - 1);
        assertThat(testEnseignement.getDateDebut()).isEqualTo(UPDATED_DATE_DEBUT);
        assertThat(testEnseignement.getDateFin()).isEqualTo(UPDATED_DATE_FIN);
        assertThat(testEnseignement.getEncours()).isEqualTo(UPDATED_ENCOURS);
    }

    @Test
    @Transactional
    void fullUpdateEnseignementWithPatch() throws Exception {
        // Initialize the database
        enseignementRepository.saveAndFlush(enseignement);

        int databaseSizeBeforeUpdate = enseignementRepository.findAll().size();

        // Update the enseignement using partial update
        Enseignement partialUpdatedEnseignement = new Enseignement();
        partialUpdatedEnseignement.setId(enseignement.getId());

        partialUpdatedEnseignement.dateDebut(UPDATED_DATE_DEBUT).dateFin(UPDATED_DATE_FIN).encours(UPDATED_ENCOURS);

        restEnseignementMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedEnseignement.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedEnseignement))
            )
            .andExpect(status().isOk());

        // Validate the Enseignement in the database
        List<Enseignement> enseignementList = enseignementRepository.findAll();
        assertThat(enseignementList).hasSize(databaseSizeBeforeUpdate);
        Enseignement testEnseignement = enseignementList.get(enseignementList.size() - 1);
        assertThat(testEnseignement.getDateDebut()).isEqualTo(UPDATED_DATE_DEBUT);
        assertThat(testEnseignement.getDateFin()).isEqualTo(UPDATED_DATE_FIN);
        assertThat(testEnseignement.getEncours()).isEqualTo(UPDATED_ENCOURS);
    }

    @Test
    @Transactional
    void patchNonExistingEnseignement() throws Exception {
        int databaseSizeBeforeUpdate = enseignementRepository.findAll().size();
        enseignement.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEnseignementMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, enseignement.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(enseignement))
            )
            .andExpect(status().isBadRequest());

        // Validate the Enseignement in the database
        List<Enseignement> enseignementList = enseignementRepository.findAll();
        assertThat(enseignementList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchEnseignement() throws Exception {
        int databaseSizeBeforeUpdate = enseignementRepository.findAll().size();
        enseignement.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEnseignementMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(enseignement))
            )
            .andExpect(status().isBadRequest());

        // Validate the Enseignement in the database
        List<Enseignement> enseignementList = enseignementRepository.findAll();
        assertThat(enseignementList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamEnseignement() throws Exception {
        int databaseSizeBeforeUpdate = enseignementRepository.findAll().size();
        enseignement.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEnseignementMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(enseignement))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Enseignement in the database
        List<Enseignement> enseignementList = enseignementRepository.findAll();
        assertThat(enseignementList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteEnseignement() throws Exception {
        // Initialize the database
        enseignementRepository.saveAndFlush(enseignement);

        int databaseSizeBeforeDelete = enseignementRepository.findAll().size();

        // Delete the enseignement
        restEnseignementMockMvc
            .perform(delete(ENTITY_API_URL_ID, enseignement.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Enseignement> enseignementList = enseignementRepository.findAll();
        assertThat(enseignementList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
