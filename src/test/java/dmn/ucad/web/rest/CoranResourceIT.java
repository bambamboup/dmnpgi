package dmn.ucad.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import dmn.ucad.IntegrationTest;
import dmn.ucad.domain.Coran;
import dmn.ucad.repository.CoranRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link CoranResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class CoranResourceIT {

    private static final String DEFAULT_LIBELLE = "AAAAAAAAAA";
    private static final String UPDATED_LIBELLE = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/corans";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private CoranRepository coranRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCoranMockMvc;

    private Coran coran;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Coran createEntity(EntityManager em) {
        Coran coran = new Coran().libelle(DEFAULT_LIBELLE);
        return coran;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Coran createUpdatedEntity(EntityManager em) {
        Coran coran = new Coran().libelle(UPDATED_LIBELLE);
        return coran;
    }

    @BeforeEach
    public void initTest() {
        coran = createEntity(em);
    }

    @Test
    @Transactional
    void createCoran() throws Exception {
        int databaseSizeBeforeCreate = coranRepository.findAll().size();
        // Create the Coran
        restCoranMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(coran)))
            .andExpect(status().isCreated());

        // Validate the Coran in the database
        List<Coran> coranList = coranRepository.findAll();
        assertThat(coranList).hasSize(databaseSizeBeforeCreate + 1);
        Coran testCoran = coranList.get(coranList.size() - 1);
        assertThat(testCoran.getLibelle()).isEqualTo(DEFAULT_LIBELLE);
    }

    @Test
    @Transactional
    void createCoranWithExistingId() throws Exception {
        // Create the Coran with an existing ID
        coran.setId(1L);

        int databaseSizeBeforeCreate = coranRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restCoranMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(coran)))
            .andExpect(status().isBadRequest());

        // Validate the Coran in the database
        List<Coran> coranList = coranRepository.findAll();
        assertThat(coranList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkLibelleIsRequired() throws Exception {
        int databaseSizeBeforeTest = coranRepository.findAll().size();
        // set the field null
        coran.setLibelle(null);

        // Create the Coran, which fails.

        restCoranMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(coran)))
            .andExpect(status().isBadRequest());

        List<Coran> coranList = coranRepository.findAll();
        assertThat(coranList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllCorans() throws Exception {
        // Initialize the database
        coranRepository.saveAndFlush(coran);

        // Get all the coranList
        restCoranMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(coran.getId().intValue())))
            .andExpect(jsonPath("$.[*].libelle").value(hasItem(DEFAULT_LIBELLE)));
    }

    @Test
    @Transactional
    void getCoran() throws Exception {
        // Initialize the database
        coranRepository.saveAndFlush(coran);

        // Get the coran
        restCoranMockMvc
            .perform(get(ENTITY_API_URL_ID, coran.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(coran.getId().intValue()))
            .andExpect(jsonPath("$.libelle").value(DEFAULT_LIBELLE));
    }

    @Test
    @Transactional
    void getNonExistingCoran() throws Exception {
        // Get the coran
        restCoranMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewCoran() throws Exception {
        // Initialize the database
        coranRepository.saveAndFlush(coran);

        int databaseSizeBeforeUpdate = coranRepository.findAll().size();

        // Update the coran
        Coran updatedCoran = coranRepository.findById(coran.getId()).get();
        // Disconnect from session so that the updates on updatedCoran are not directly saved in db
        em.detach(updatedCoran);
        updatedCoran.libelle(UPDATED_LIBELLE);

        restCoranMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedCoran.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedCoran))
            )
            .andExpect(status().isOk());

        // Validate the Coran in the database
        List<Coran> coranList = coranRepository.findAll();
        assertThat(coranList).hasSize(databaseSizeBeforeUpdate);
        Coran testCoran = coranList.get(coranList.size() - 1);
        assertThat(testCoran.getLibelle()).isEqualTo(UPDATED_LIBELLE);
    }

    @Test
    @Transactional
    void putNonExistingCoran() throws Exception {
        int databaseSizeBeforeUpdate = coranRepository.findAll().size();
        coran.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCoranMockMvc
            .perform(
                put(ENTITY_API_URL_ID, coran.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(coran))
            )
            .andExpect(status().isBadRequest());

        // Validate the Coran in the database
        List<Coran> coranList = coranRepository.findAll();
        assertThat(coranList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchCoran() throws Exception {
        int databaseSizeBeforeUpdate = coranRepository.findAll().size();
        coran.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCoranMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(coran))
            )
            .andExpect(status().isBadRequest());

        // Validate the Coran in the database
        List<Coran> coranList = coranRepository.findAll();
        assertThat(coranList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamCoran() throws Exception {
        int databaseSizeBeforeUpdate = coranRepository.findAll().size();
        coran.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCoranMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(coran)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Coran in the database
        List<Coran> coranList = coranRepository.findAll();
        assertThat(coranList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateCoranWithPatch() throws Exception {
        // Initialize the database
        coranRepository.saveAndFlush(coran);

        int databaseSizeBeforeUpdate = coranRepository.findAll().size();

        // Update the coran using partial update
        Coran partialUpdatedCoran = new Coran();
        partialUpdatedCoran.setId(coran.getId());

        restCoranMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCoran.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCoran))
            )
            .andExpect(status().isOk());

        // Validate the Coran in the database
        List<Coran> coranList = coranRepository.findAll();
        assertThat(coranList).hasSize(databaseSizeBeforeUpdate);
        Coran testCoran = coranList.get(coranList.size() - 1);
        assertThat(testCoran.getLibelle()).isEqualTo(DEFAULT_LIBELLE);
    }

    @Test
    @Transactional
    void fullUpdateCoranWithPatch() throws Exception {
        // Initialize the database
        coranRepository.saveAndFlush(coran);

        int databaseSizeBeforeUpdate = coranRepository.findAll().size();

        // Update the coran using partial update
        Coran partialUpdatedCoran = new Coran();
        partialUpdatedCoran.setId(coran.getId());

        partialUpdatedCoran.libelle(UPDATED_LIBELLE);

        restCoranMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCoran.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCoran))
            )
            .andExpect(status().isOk());

        // Validate the Coran in the database
        List<Coran> coranList = coranRepository.findAll();
        assertThat(coranList).hasSize(databaseSizeBeforeUpdate);
        Coran testCoran = coranList.get(coranList.size() - 1);
        assertThat(testCoran.getLibelle()).isEqualTo(UPDATED_LIBELLE);
    }

    @Test
    @Transactional
    void patchNonExistingCoran() throws Exception {
        int databaseSizeBeforeUpdate = coranRepository.findAll().size();
        coran.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCoranMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, coran.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(coran))
            )
            .andExpect(status().isBadRequest());

        // Validate the Coran in the database
        List<Coran> coranList = coranRepository.findAll();
        assertThat(coranList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchCoran() throws Exception {
        int databaseSizeBeforeUpdate = coranRepository.findAll().size();
        coran.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCoranMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(coran))
            )
            .andExpect(status().isBadRequest());

        // Validate the Coran in the database
        List<Coran> coranList = coranRepository.findAll();
        assertThat(coranList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamCoran() throws Exception {
        int databaseSizeBeforeUpdate = coranRepository.findAll().size();
        coran.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCoranMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(coran)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Coran in the database
        List<Coran> coranList = coranRepository.findAll();
        assertThat(coranList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteCoran() throws Exception {
        // Initialize the database
        coranRepository.saveAndFlush(coran);

        int databaseSizeBeforeDelete = coranRepository.findAll().size();

        // Delete the coran
        restCoranMockMvc
            .perform(delete(ENTITY_API_URL_ID, coran.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Coran> coranList = coranRepository.findAll();
        assertThat(coranList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
