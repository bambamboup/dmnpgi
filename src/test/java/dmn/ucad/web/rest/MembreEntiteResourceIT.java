package dmn.ucad.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import dmn.ucad.IntegrationTest;
import dmn.ucad.domain.Entite;
import dmn.ucad.domain.Membre;
import dmn.ucad.domain.MembreEntite;
import dmn.ucad.repository.MembreEntiteRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link MembreEntiteResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class MembreEntiteResourceIT {

    private static final Boolean DEFAULT_ACTIF = false;
    private static final Boolean UPDATED_ACTIF = true;

    private static final String ENTITY_API_URL = "/api/membre-entites";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private MembreEntiteRepository membreEntiteRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restMembreEntiteMockMvc;

    private MembreEntite membreEntite;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MembreEntite createEntity(EntityManager em) {
        MembreEntite membreEntite = new MembreEntite().actif(DEFAULT_ACTIF);
        // Add required entity
        Membre membre;
        if (TestUtil.findAll(em, Membre.class).isEmpty()) {
            membre = MembreResourceIT.createEntity(em);
            em.persist(membre);
            em.flush();
        } else {
            membre = TestUtil.findAll(em, Membre.class).get(0);
        }
        membreEntite.setMembre(membre);
        // Add required entity
        Entite entite;
        if (TestUtil.findAll(em, Entite.class).isEmpty()) {
            entite = EntiteResourceIT.createEntity(em);
            em.persist(entite);
            em.flush();
        } else {
            entite = TestUtil.findAll(em, Entite.class).get(0);
        }
        membreEntite.setEntite(entite);
        return membreEntite;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MembreEntite createUpdatedEntity(EntityManager em) {
        MembreEntite membreEntite = new MembreEntite().actif(UPDATED_ACTIF);
        // Add required entity
        Membre membre;
        if (TestUtil.findAll(em, Membre.class).isEmpty()) {
            membre = MembreResourceIT.createUpdatedEntity(em);
            em.persist(membre);
            em.flush();
        } else {
            membre = TestUtil.findAll(em, Membre.class).get(0);
        }
        membreEntite.setMembre(membre);
        // Add required entity
        Entite entite;
        if (TestUtil.findAll(em, Entite.class).isEmpty()) {
            entite = EntiteResourceIT.createUpdatedEntity(em);
            em.persist(entite);
            em.flush();
        } else {
            entite = TestUtil.findAll(em, Entite.class).get(0);
        }
        membreEntite.setEntite(entite);
        return membreEntite;
    }

    @BeforeEach
    public void initTest() {
        membreEntite = createEntity(em);
    }

    @Test
    @Transactional
    void createMembreEntite() throws Exception {
        int databaseSizeBeforeCreate = membreEntiteRepository.findAll().size();
        // Create the MembreEntite
        restMembreEntiteMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(membreEntite)))
            .andExpect(status().isCreated());

        // Validate the MembreEntite in the database
        List<MembreEntite> membreEntiteList = membreEntiteRepository.findAll();
        assertThat(membreEntiteList).hasSize(databaseSizeBeforeCreate + 1);
        MembreEntite testMembreEntite = membreEntiteList.get(membreEntiteList.size() - 1);
        assertThat(testMembreEntite.getActif()).isEqualTo(DEFAULT_ACTIF);
    }

    @Test
    @Transactional
    void createMembreEntiteWithExistingId() throws Exception {
        // Create the MembreEntite with an existing ID
        membreEntite.setId(1L);

        int databaseSizeBeforeCreate = membreEntiteRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restMembreEntiteMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(membreEntite)))
            .andExpect(status().isBadRequest());

        // Validate the MembreEntite in the database
        List<MembreEntite> membreEntiteList = membreEntiteRepository.findAll();
        assertThat(membreEntiteList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllMembreEntites() throws Exception {
        // Initialize the database
        membreEntiteRepository.saveAndFlush(membreEntite);

        // Get all the membreEntiteList
        restMembreEntiteMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(membreEntite.getId().intValue())))
            .andExpect(jsonPath("$.[*].actif").value(hasItem(DEFAULT_ACTIF.booleanValue())));
    }

    @Test
    @Transactional
    void getMembreEntite() throws Exception {
        // Initialize the database
        membreEntiteRepository.saveAndFlush(membreEntite);

        // Get the membreEntite
        restMembreEntiteMockMvc
            .perform(get(ENTITY_API_URL_ID, membreEntite.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(membreEntite.getId().intValue()))
            .andExpect(jsonPath("$.actif").value(DEFAULT_ACTIF.booleanValue()));
    }

    @Test
    @Transactional
    void getNonExistingMembreEntite() throws Exception {
        // Get the membreEntite
        restMembreEntiteMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewMembreEntite() throws Exception {
        // Initialize the database
        membreEntiteRepository.saveAndFlush(membreEntite);

        int databaseSizeBeforeUpdate = membreEntiteRepository.findAll().size();

        // Update the membreEntite
        MembreEntite updatedMembreEntite = membreEntiteRepository.findById(membreEntite.getId()).get();
        // Disconnect from session so that the updates on updatedMembreEntite are not directly saved in db
        em.detach(updatedMembreEntite);
        updatedMembreEntite.actif(UPDATED_ACTIF);

        restMembreEntiteMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedMembreEntite.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedMembreEntite))
            )
            .andExpect(status().isOk());

        // Validate the MembreEntite in the database
        List<MembreEntite> membreEntiteList = membreEntiteRepository.findAll();
        assertThat(membreEntiteList).hasSize(databaseSizeBeforeUpdate);
        MembreEntite testMembreEntite = membreEntiteList.get(membreEntiteList.size() - 1);
        assertThat(testMembreEntite.getActif()).isEqualTo(UPDATED_ACTIF);
    }

    @Test
    @Transactional
    void putNonExistingMembreEntite() throws Exception {
        int databaseSizeBeforeUpdate = membreEntiteRepository.findAll().size();
        membreEntite.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMembreEntiteMockMvc
            .perform(
                put(ENTITY_API_URL_ID, membreEntite.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(membreEntite))
            )
            .andExpect(status().isBadRequest());

        // Validate the MembreEntite in the database
        List<MembreEntite> membreEntiteList = membreEntiteRepository.findAll();
        assertThat(membreEntiteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchMembreEntite() throws Exception {
        int databaseSizeBeforeUpdate = membreEntiteRepository.findAll().size();
        membreEntite.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMembreEntiteMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(membreEntite))
            )
            .andExpect(status().isBadRequest());

        // Validate the MembreEntite in the database
        List<MembreEntite> membreEntiteList = membreEntiteRepository.findAll();
        assertThat(membreEntiteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamMembreEntite() throws Exception {
        int databaseSizeBeforeUpdate = membreEntiteRepository.findAll().size();
        membreEntite.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMembreEntiteMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(membreEntite)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the MembreEntite in the database
        List<MembreEntite> membreEntiteList = membreEntiteRepository.findAll();
        assertThat(membreEntiteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateMembreEntiteWithPatch() throws Exception {
        // Initialize the database
        membreEntiteRepository.saveAndFlush(membreEntite);

        int databaseSizeBeforeUpdate = membreEntiteRepository.findAll().size();

        // Update the membreEntite using partial update
        MembreEntite partialUpdatedMembreEntite = new MembreEntite();
        partialUpdatedMembreEntite.setId(membreEntite.getId());

        partialUpdatedMembreEntite.actif(UPDATED_ACTIF);

        restMembreEntiteMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMembreEntite.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMembreEntite))
            )
            .andExpect(status().isOk());

        // Validate the MembreEntite in the database
        List<MembreEntite> membreEntiteList = membreEntiteRepository.findAll();
        assertThat(membreEntiteList).hasSize(databaseSizeBeforeUpdate);
        MembreEntite testMembreEntite = membreEntiteList.get(membreEntiteList.size() - 1);
        assertThat(testMembreEntite.getActif()).isEqualTo(UPDATED_ACTIF);
    }

    @Test
    @Transactional
    void fullUpdateMembreEntiteWithPatch() throws Exception {
        // Initialize the database
        membreEntiteRepository.saveAndFlush(membreEntite);

        int databaseSizeBeforeUpdate = membreEntiteRepository.findAll().size();

        // Update the membreEntite using partial update
        MembreEntite partialUpdatedMembreEntite = new MembreEntite();
        partialUpdatedMembreEntite.setId(membreEntite.getId());

        partialUpdatedMembreEntite.actif(UPDATED_ACTIF);

        restMembreEntiteMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMembreEntite.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMembreEntite))
            )
            .andExpect(status().isOk());

        // Validate the MembreEntite in the database
        List<MembreEntite> membreEntiteList = membreEntiteRepository.findAll();
        assertThat(membreEntiteList).hasSize(databaseSizeBeforeUpdate);
        MembreEntite testMembreEntite = membreEntiteList.get(membreEntiteList.size() - 1);
        assertThat(testMembreEntite.getActif()).isEqualTo(UPDATED_ACTIF);
    }

    @Test
    @Transactional
    void patchNonExistingMembreEntite() throws Exception {
        int databaseSizeBeforeUpdate = membreEntiteRepository.findAll().size();
        membreEntite.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMembreEntiteMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, membreEntite.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(membreEntite))
            )
            .andExpect(status().isBadRequest());

        // Validate the MembreEntite in the database
        List<MembreEntite> membreEntiteList = membreEntiteRepository.findAll();
        assertThat(membreEntiteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchMembreEntite() throws Exception {
        int databaseSizeBeforeUpdate = membreEntiteRepository.findAll().size();
        membreEntite.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMembreEntiteMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(membreEntite))
            )
            .andExpect(status().isBadRequest());

        // Validate the MembreEntite in the database
        List<MembreEntite> membreEntiteList = membreEntiteRepository.findAll();
        assertThat(membreEntiteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamMembreEntite() throws Exception {
        int databaseSizeBeforeUpdate = membreEntiteRepository.findAll().size();
        membreEntite.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMembreEntiteMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(membreEntite))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the MembreEntite in the database
        List<MembreEntite> membreEntiteList = membreEntiteRepository.findAll();
        assertThat(membreEntiteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteMembreEntite() throws Exception {
        // Initialize the database
        membreEntiteRepository.saveAndFlush(membreEntite);

        int databaseSizeBeforeDelete = membreEntiteRepository.findAll().size();

        // Delete the membreEntite
        restMembreEntiteMockMvc
            .perform(delete(ENTITY_API_URL_ID, membreEntite.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<MembreEntite> membreEntiteList = membreEntiteRepository.findAll();
        assertThat(membreEntiteList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
